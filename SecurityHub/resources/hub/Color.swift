//
//  ExtensionUtil.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import UIKit

extension UIColor {
    static func colorFromHex(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0,
            green: ((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0,
            blue: ((CGFloat)(rgbValue & 0xFF))/255.0,
            alpha: 1
        )
    }
    
    class var hubBackgroundColor: UIColor { return colorFromHex(0xefeff4) }
    
    class var hubMainColor: UIColor { return colorFromHex(0x3abeff) }
    static var hubMainColorUINT: UInt = 0x0095B6
    
    class var hubLightMainColor: UIColor { return colorFromHex(0x58c6e8) }
    
    class var hubDarkMainColor: UIColor { return colorFromHex(0x006786) }
    
    class var hubRipleBackgroundColor: UIColor { return colorFromHex(0x006c85) }
    
    class var hubRipleColor: UIColor { return UIColor( red: 255, green: 255, blue: 255, alpha: 0.5) }
    
    class var hubTransparent: UIColor { return UIColor( red: 255, green: 255, blue: 255, alpha: 0.0) }
    
    class var hubGreenColor: UIColor { return colorFromHex(0x4CAF50) }
    class var hubGreenDarkColor: UIColor { return colorFromHex(0x1B5E20) }
    
    class var hubRedColor: UIColor { return colorFromHex(0xf9543e) }
    class var hubRedDarkColor: UIColor { return colorFromHex(0xb71c1c) }

    class var hubYellowColor: UIColor { return colorFromHex(0xF9A825) }
    class var hubYellowDarkColor: UIColor { return colorFromHex(0xF57F17) }
    
    class var hubTextBlack: UIColor { return colorFromHex(0x414042) }
    class var hubGold: UIColor { return colorFromHex(0xffdf57) }
    class var hubHint: UIColor { return colorFromHex(0xa7a9ac) }
    
    class var hubBlur: UIColor { return colorFromHex(0xa3d8d8) }
    class var hubGrey: UIColor { return colorFromHex(0xbcbcbc) }
    
    class var hubSeparator: UIColor { return colorFromHex(0xd6d6d6) }
    
    class var hubOrange: UIColor { return colorFromHex(0xff954d) }
}
