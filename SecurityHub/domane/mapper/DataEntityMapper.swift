//
//  DataEntityMapper.swift
//  SecurityHub
//
//  Created by Timerlan on 05/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import SQLite


class DataEntityMapper {
    static func dSite(_ site: Sites, _ type: NUpdateType, _ affects: [DAffectEntity], _ armStatus: DSiteEntity.ArmStatus) -> DSiteEntity {
        return DSiteEntity(id: site.id, site: site, type: type, affects: affects, armStatus: armStatus)
    }
    static func dSite(_ nSite: NSiteEntity, _ affects: [DAffectEntity], _ armStatus: DSiteEntity.ArmStatus) -> DSiteEntity {
        return DSiteEntity(id: nSite.id, site: nSite.site, type: nSite.type, affects: affects, armStatus: armStatus)
    }
    static func nSite(_ site: Sites, _ type: NUpdateType) -> NSiteEntity {
        return NSiteEntity(id: site.id, site: site, type: type)
    }
    
    static func dDevice(_ di: DDeviceWithSitesInfo, _ type: NUpdateType, _ affects: [DAffectEntity], _ armStatus: DDeviceEntity.ArmStatus) -> DDeviceEntity {
        return DDeviceEntity(id: di.device.id, siteIds: di.siteIds, siteNames: di.siteNames, device: di.device, type: type, affects: affects, armStatus: armStatus)
    }
    static func dDevice(_ di: NDeviceEntity, _ affects: [DAffectEntity], _ armStatus: DDeviceEntity.ArmStatus) -> DDeviceEntity {
        return DDeviceEntity(id: di.id, siteIds: di.siteIds, siteNames: di.siteNames, device: di.device, type: di.type, affects: affects, armStatus: armStatus)
    }
    static func nDevice(_ di: DDeviceWithSitesInfo, _ type: NUpdateType) -> NDeviceEntity {
        return NDeviceEntity(id: di.device.id, siteIds: di.siteIds, siteNames: di.siteNames, device: di.device, type: type)
    }
    
    static func dSection(_ si: DSectionWithSitesDevicesInfo, _ type: NUpdateType, _ affects: [DAffectEntity]) -> DSectionEntity {
        let armStatus: DSectionEntity.ArmStatus = si.section.arm == 0 ? .disarmed : .armed
        return DSectionEntity(id: si.section.id, siteIds: si.siteIds, siteNames: si.siteNames, deviceId: si.section.device, deviceName: si.deviceName, configVersion: si.configVersion, cluster_id: si.cluster_id, section: si.section, type: type, affects: affects, armStatus: armStatus)
    }
    static func dSection(_ si: NSectionEntity, _ affects: [DAffectEntity]) -> DSectionEntity {
        let armStatus: DSectionEntity.ArmStatus = si.section?.arm == 0 ? .disarmed : .armed
        return DSectionEntity(id: si.id, siteIds: si.siteIds, siteNames: si.siteNames, deviceId: si.deviceId, deviceName: si.deviceName, configVersion: si.configVersion, cluster_id: si.cluster_id, section: si.section, type: si.type, affects: affects, armStatus: armStatus)
    }
    static func nSection(_ si: DSectionWithSitesDevicesInfo, _ type: NUpdateType) -> NSectionEntity {
        return NSectionEntity(id: si.section.id, siteIds: si.siteIds, siteNames: si.siteNames, deviceId: si.section.device, deviceName: si.deviceName, configVersion: si.configVersion, cluster_id: si.cluster_id, sectionId: si.section.section, section: si.section, type: type)
    }
    
    static func dZone(_ zi: DZoneWithSitesDeviceSectionInfo, _ type: NUpdateType, _ affects: [DAffectEntity]) -> DZoneEntity {
        var armStatus: DZoneEntity.ArmStatus?
        if zi.sectionDetector == HubConst.SECTION_TYPE_SECURITY { armStatus = zi.sectionArm > 0 ? .armed : .disarmed }
        return DZoneEntity(id: zi.zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneDetector: zi.zone.detector, zone: zi.zone, type: type, affects: affects, armStatus: armStatus)
    }
    static func dZone(_ zi: NZoneEntity, _ affects: [DAffectEntity]) -> DZoneEntity {
        var armStatus: DZoneEntity.ArmStatus?
        if zi.sectionDetector == HubConst.SECTION_TYPE_SECURITY { armStatus = zi.sectionArm > 0 ? .armed : .disarmed }
        return DZoneEntity(id: zi.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.deviceId, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.sectionId, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneDetector: zi.zoneDetector, zone: zi.zone, type: zi.type, affects: affects, armStatus: armStatus)
    }
    static func nZone(_ zi: DZoneWithSitesDeviceSectionInfo, _ type: NUpdateType) -> NZoneEntity {
        return NZoneEntity(id: zi.zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, sectionArm: zi.sectionArm, zoneId: zi.zone.zone, zoneDetector: zi.zone.detector, zone: zi.zone, type: type)
    }
    
    static func dRelay(_ zi: DZoneWithSitesDeviceSectionInfo, _ type: NUpdateType, _ affects: [DAffectEntity], _ armStatus: DZoneEntity.ArmStatus, _ script: DScriptInfo?) -> DZoneEntity {
        return DZoneEntity(id: zi.zone.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.zone.device, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.zone.section, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneDetector: zi.zone.detector, zone: zi.zone, type: type, affects: affects, armStatus: armStatus, script: script)
    }
    static func dRelay(_ zi: NZoneEntity, _ affects: [DAffectEntity], _ armStatus: DZoneEntity.ArmStatus, _ script: DScriptInfo? ) -> DZoneEntity {
        return DZoneEntity(id: zi.id, siteIds: zi.siteIds, siteNames: zi.siteNames, deviceId: zi.deviceId, deviceName: zi.deviceName, configVersion: zi.configVersion, cluster: zi.cluster, sectionId: zi.sectionId, sectionName: zi.sectionName, sectionDetector: zi.sectionDetector, zoneDetector: zi.zoneDetector, zone: zi.zone, type: zi.type, affects: affects, armStatus: armStatus, script: script)
    }
    
    static func dAffect(obj: Array<Optional<Binding>>) -> DAffectEntity {
        let icon = obj[4] as? String ?? "ic_action_info_grey_small"
        let device = obj[0] as? String ?? R.strings.device_no_name
        let sectionZone = getSectionZone(section: obj[1] as? String, zone: obj[2] as? String)
        let desc = obj[3] as? String ?? R.strings.no_affect_desc
        let _class = obj[5] as? Int64 ?? 17
        let id = obj[6] as? Int64 ?? 0
        return DAffectEntity(id: id, icon: icon, device: device, sectionZone: sectionZone, desc: desc, _class: _class)
    }
    
    static func dScript(_ script: DScriptInfo, library: HubLibraryScripts, type: NUpdateType) -> DScriptEntity {
        let library = library.items.first(where: { $0.uid == script.uid })?.data
        return DScriptEntity(deviceId: script.deviceId, configVersion: script.configVersion, id: script.id, uid: script.uid, bind: script.bind, zoneName: script.zoneName, enabled: script.enabled, params: script.params, library: library, type: type)
    }
    
    static func deviceWithSiteInfo(_ obj: Array<Optional<Binding>>) -> DDeviceWithSiteInfo? {
        if  let id = obj[0] as? Int64,
            let domain = obj[2] as? Int64,
            let account = obj[3] as? Int64,
            let cluster_id = obj[4] as? Int64,
            let configVersion = obj[5] as? Int64,
            let time = obj[6] as? Int64,
            let siteId = obj[7] as? Int64,
            let siteName = obj[8] as? String {
            let device = Devices(id: id, domain: domain, account: account, cluster_id: cluster_id, configVersion: configVersion, time: time)
            return DDeviceWithSiteInfo(device: device, siteId: siteId, siteName: siteName)
        }
        return nil
    }
    
    static func sectionWithSiteDeviceInfo(_ obj: Array<Optional<Binding>>) -> DSectionWithSiteDeviceInfo? {
        if  let device = obj[1] as? Int64,
            let section = obj[2] as? Int64,
            let name = obj[3] as? String,
            let detector = obj[4] as? Int64,
            let arm = obj[5] as? Int64,
            let time = obj[6] as? Int64,
            let siteId = obj[7] as? Int64,
            let siteName = obj[8] as? String,
            let configVersion = obj[9] as? Int64,
            let cluster_id = obj[10] as? Int64,
            let deviceName = obj[11] as? String {
            let section = Sections(device: device, section: section, name: name, detector: detector, arm: arm, time: time)
            return DSectionWithSiteDeviceInfo(section: section, siteId: siteId, siteName: siteName, deviceName: deviceName, configVersion: configVersion, cluster_id: cluster_id)
        }
        return nil
    }
    
    static func zoneWithSiteDeviceSectionInfo(_ obj: Array<Optional<Binding>>) -> DZoneWithSiteDeviceSectionInfo? {
        if  let device = obj[1] as? Int64,
            let section = obj[2] as? Int64,
            let zone = obj[3] as? Int64,
            let name = obj[4] as? String,
            let delay = obj[5] as? Int64,
            let detector = obj[6] as? Int64,
            let physic = obj[7] as? Int64,
            let time = obj[8] as? Int64,
            let siteId = obj[9] as? Int64,
            let siteName = obj[10] as? String,
            let deviceName = obj[11] as? String,
            let sectionName = obj[12] as? String,
            let sectionDetector = obj[13] as? Int64,
            let sectionArm = obj[14] as? Int64,
            let configVersion = obj[15] as? Int64,
            let cluster = obj[16] as? Int64 {
            let zone = Zones(device: device, section: section, zone: zone, name: name, delay: delay, detector: detector, physic: physic, time: time)
            return DZoneWithSiteDeviceSectionInfo(zone: zone, siteId: siteId, siteName: siteName, deviceName: deviceName, configVersion: configVersion, cluster: cluster, sectionName: sectionName, sectionDetector: sectionDetector, sectionArm: sectionArm)
        }
        return nil
    }
    
    static func affectRelation(_ obj: Array<Optional<Binding>>) -> DAffectRelation? {
        if  let device  = obj[0] as? Int64,
            let section = obj[1] as? Int64,
            let zone    = obj[2] as? Int64 {
            return DAffectRelation(deviceId: device, sectionId: section, zoneId: zone)
        }
        return nil
    }
    
    static func script(_ obj: Array<Optional<Binding>>) -> DScriptInfo? {
        if  let device = obj[0] as? Int64,
            let configVersion = obj[1] as? Int64,
            let id = obj[2] as? Int64,
            let uid = obj[3] as? String,
            let bind = obj[4] as? Int64,
            let enabled = obj[5] as? Int64,
            let params = obj[6] as? String,
            let extra = obj[7] as? String,
            let zoneName = obj[8] as? String {
            return DScriptInfo(deviceId: device, configVersion: configVersion, id: id, uid: uid, bind: bind, zoneName: zoneName, enabled: enabled == 1, params: params.toJsonArray(), extra: extra.toJson())
        }
        return nil
    }
    
    private static func getSectionZone(section: String?, zone: String?) -> String?{
        var location: String?
        if let section = section { location = section }
        if let zone = zone, let _ = section { location! += " → \(zone)" }
        else if let zone = zone { location = zone }
        return location
    }
}
