//
//  XMainCellRelayEntityBuilder.swift
//  SecurityHub
//
//  Created by Timerlan on 29/04/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XMainCellRelayEntityBuilder: XMainCellBaseEntityBuilder {
    static func create(zone z: Zones) -> XMainCellRelayEntityBuilder{
        let eb = XMainCellRelayEntityBuilder(z)
        eb.ce.id = z.id
        eb.ce.title = z.name
        eb.ce.isFolding = false
        eb.ce.foldingInfo.isOpen = true
        return eb
    }
    override func updateType(_ updateType: NUpdateType) -> XMainCellRelayEntityBuilder {
        return super.updateType(updateType) as! XMainCellRelayEntityBuilder
    }
    override func time(_ time: Int64) -> XMainCellRelayEntityBuilder {
        return super.time(time) as! XMainCellRelayEntityBuilder
    }
    override func siteName(_ name: String?) -> XMainCellRelayEntityBuilder {
        return super.siteName(name) as! XMainCellRelayEntityBuilder
    }
    override func deviceName(_ name: String?) -> XMainCellRelayEntityBuilder {
        return super.deviceName(name) as! XMainCellRelayEntityBuilder
    }
    override func sectionName(_ name: String?) -> XMainCellRelayEntityBuilder {
        return super.sectionName(name) as! XMainCellRelayEntityBuilder
    }
    func setAffects(affects: [DAffectEntity], type: String, index: Int64) -> XMainCellRelayEntityBuilder {
        ce.info = "\(type)\n\(R.strings.relay_exits) \(index)"
        ce.affectsMini = toIcons(affects)
        ce.affects = toEventElements(affects)
        return self
    }
    func setStatus(affects: [DAffectEntity], status: DZoneEntity.ArmStatus?) -> XMainCellRelayEntityBuilder {
        if affects.contains(where: { $0._class == 0}) {
            ce.image = XImages.zone_alarm_name
        } else {
            if status == .armed {
                ce.image = XImages.relay_on_big_name
            } else {
                ce.imageColor = DEFAULT_UNSELECTED_UINT
                ce.image = XImages.relay_off_name
            }
        }
        ce.status = status == .armed ? R.strings.relay_on :
            status == .disarmed ? R.strings.relay_off : ""
        return self
    }
    func isExit(affects: [DAffectEntity], status: DZoneEntity.ArmStatus?) -> XMainCellRelayEntityBuilder {        
        ce.image = affects.contains(where: { $0._class == 0}) ? XImages.zone_alarm_name : XImages.zone_default_name
//        ce.smallImage = status == .armed ? XImages.zone_armed_big :
//            status == .disarmed ? XImages.zone_disarmed_big : nil
//        
//        ce.status = status == .armed ? R.strings.armed :
//            status == .disarmed ? R.strings.disarmed : ""
        return self
    }
    func isRelay(affects: [DAffectEntity]) -> XMainCellRelayEntityBuilder {
        ce.image = affects.contains(where: { $0._class == 0}) ? XImages.zone_alarm_name : XImages.relay_off_name
        return self
    }
    func isAutoRelay(affects: [DAffectEntity], buttons: [String : Int64], _switch: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellRelayEntityBuilder {
        ce.image = XImages.auto_relay_name
        for button in buttons {
            ce.buttons.append(
                XObjectEntity.Button(
                    title: button.key,
                    color: DEFAULT_COLOR_MAIN_UINT,
                    obj: obj,
                    value: button.value,
                    void: _switch
                )
            )
        }
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Buttons
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func buttonCommands( on: @escaping ((_ obj: Any, _ value: Any)->Void),
                         off: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellRelayEntityBuilder {
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.relay_button_on,
                                 color: DEFAULT_COLOR_YELLOW_UINT,
                                 obj: obj,
                                 value: "",
                                 void: on)
        )
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.relay_button_off,
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj: obj,
                                 value: "",
                                 void: off)
        )
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Menu
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func menuRename(_ rename: @escaping ((_ obj: Any)->Void)) -> XMainCellRelayEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_rele_rename, obj: obj, void: rename)
        )
        return self
    }
    func menuScript(_ script:  @escaping ((_ obj: Any)->Void)) -> XMainCellRelayEntityBuilder {
        if (XTargetUtils.isScripts)
        {
            ce.smallImage = XImages.script_relay_name
            ce.menu.append(
                XObjectEntity.Menu(title: R.strings.menu_relay_script, obj: obj, void: script)
            )
        }
        return self
    }
    func menuScriptAdd(_ script_add:  @escaping ((_ obj: Any)->Void)) -> XMainCellRelayEntityBuilder {
        if (XTargetUtils.isScripts)
        {
            ce.menu.append(
                XObjectEntity.Menu(title: R.strings.menu_relay_script_add, obj: obj, void: script_add)
            )
        }
        return self
    }
    func menuDelete(_ delete: @escaping ((_ obj: Any)->Void)) -> XMainCellRelayEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_rele_delete, obj: obj, void: delete)
        )
        return self
    }
}
