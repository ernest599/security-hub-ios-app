//
//  XMainMapper.swift
//  SecurityHub
//
//  Created by Timerlan on 26.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit


class XMainCellSiteEntityBuilder: XMainCellBaseEntityBuilder {
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Insert OR Update init
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static func create(site s: Sites) -> XMainCellSiteEntityBuilder{
        let eb = XMainCellSiteEntityBuilder(s)
        eb.ce.id = s.id
        eb.ce.title = s.name
        return eb
    }
    override func updateType(_ updateType: NUpdateType) -> XMainCellSiteEntityBuilder {
        return super.updateType(updateType) as! XMainCellSiteEntityBuilder
    }
    override func time(_ time: Int64) -> XMainCellSiteEntityBuilder {
        return super.time(time) as! XMainCellSiteEntityBuilder
    }
    func status(affects: [DAffectEntity], status: DSiteEntity.ArmStatus) -> XMainCellSiteEntityBuilder {
        if affects.contains(where: { $0._class == 0}) {
            ce.image = XImages.site_alarm_big_name
        } else {
            if (XTargetUtils.target == .teleplus)
            {
               ce.image = XImages.site_default_name_teleplus
            }else {
                ce.imageColor = DEFAULT_UNSELECTED_UINT
                ce.image = XImages.site_default_name
            }
            
        }
     
        switch status {
        case .armed:
            ce.smallImage   = XImages.site_armed_big_name
            ce.status       = R.strings.armed
        case .disarmed:
            ce.smallImage   = XImages.site_disarmed_big_name
            ce.status       = R.strings.disarmed
        case .notFullArmed:
            ce.smallImage   = XImages.site_c_armed_big_name
            ce.status       = R.strings.c_armed
        }

        ce.affectsMini = toIcons(affects)
        ce.affects = toEventElements(affects)
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Buttons
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func buttonCommands(arm: @escaping ((_ obj: Any, _ value: Any)->Void), disarm: @escaping ((_ obj: Any, _ value: Any)->Void), need: Bool) -> XMainCellSiteEntityBuilder{
        if !need { return self }
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_arm,
                                  color: DEFAULT_COLOR_GREEN_UINT,
                                  obj: obj,
                                  value: "nil",
                                  void: arm)
        )
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_disarm,
                                  color: DEFAULT_COLOR_RED_UINT,
                                  obj: obj,
                                  value: "nil",
                                  void: disarm)
        )
        return self
    }
    func buttonDevice(_ showDevices:  @escaping ((_ obj: Any, _ value: Any)->Void), need: Bool) -> XMainCellSiteEntityBuilder {
        if !need { return self }
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_devices,
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj: obj,
                                 value: "nil",
                                 void: showDevices)
        )
        return self
    }
    func buttonRele(_ showReles: @escaping ((_ obj: Any, _ value: Any)->Void), need: Bool) -> XMainCellSiteEntityBuilder {
        if !need { return self }
        if (XTargetUtils.target != .myuvo)
        {
            ce.buttons.append(
                XObjectEntity.Button(title: R.strings.button_reles_exits,
                                     color: DEFAULT_COLOR_MAIN_UINT,
                                     obj: obj,
                                     value: "nil",
                                     void: showReles)
            )
        }
        else
        {
            ce.buttons.append(
                           XObjectEntity.Button(title: R.strings.button_smart_home,
                                                color: DEFAULT_COLOR_MAIN_UINT,
                                                obj: obj,
                                                value: "nil",
                                                void: showReles)
                       )
        }
        
        return self
    }
    func buttonListHozorgan(_ list_hozorgan: @escaping ((_ obj: Any, _ value: Any)->Void), need: Bool) -> XMainCellSiteEntityBuilder {
        if !need { return self }
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_manage_hozorgan,
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj: obj,
                                 value: "nil",
                                 void: list_hozorgan)
        )
        return self
    }
    func buttonSection(_ showSections: @escaping ((_ obj: Any, _ value: Any)->Void), need: Bool) -> XMainCellSiteEntityBuilder {
        if !need { return self }
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_sections,
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj:   obj,
                                 value: "nil",
                                 void:  showSections)
        )
        return self
    }
    func buttonZone(_ showZones: @escaping ((_ obj: Any, _ value: Any)->Void), need: Bool) -> XMainCellSiteEntityBuilder {
        if !need { return self }
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_zones,
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj:   obj,
                                 value: "nil",
                                 void:  showZones)
        )
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Menu
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func menuAbout(_ about: @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_site_about, obj: obj, void: about)
        )
        return self
    }
    func menuRename(_ rename: @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_site_rename, obj: obj, void: rename)
        )
        return self
    }
    func menuScrips(_ scripts:  @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        if (XTargetUtils.isScripts)
        {
            ce.menu.append(
                XObjectEntity.Menu(title: R.strings.menu_site_scripts, obj: obj, void: scripts)
            )
        }
        return self
    }
    func menuAddDevice(_ add_device: @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_site_add_device, obj: obj, void: add_device)
        )
        return self
    }
    func menuDelete(_ delete: @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_site_delete, obj: obj, void: delete)
        )
        return self
    }
    func menuDelegate(_ delegate: @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_site_delegate, obj: obj, void: delegate)
        )
        return self
    }
    func menuDeleteDelegate(_ delete_delegate: @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_site_delete_delegate, obj: obj, void: delete_delegate)
        )
        return self
    }
    func menuListDelegate(_ list_delegate: @escaping ((_ obj: Any)->Void)) -> XMainCellSiteEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_site_list_delegate, obj: obj, void: list_delegate)
        )
        return self
    }
}
