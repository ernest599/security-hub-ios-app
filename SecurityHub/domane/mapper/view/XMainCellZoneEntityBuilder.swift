//
//  XMainCellZoneEntityBuilder.swift
//  SecurityHub
//
//  Created by Timerlan on 25/04/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XMainCellZoneEntityBuilder: XMainCellBaseEntityBuilder {
    static func create(zone z: Zones) -> XMainCellZoneEntityBuilder{
        let eb = XMainCellZoneEntityBuilder(z)
        eb.ce.id = z.id
        eb.ce.title = z.name
        return eb
    }
    override func updateType(_ updateType: NUpdateType) -> XMainCellZoneEntityBuilder {
        return super.updateType(updateType) as! XMainCellZoneEntityBuilder
    }
    override func time(_ time: Int64) -> XMainCellZoneEntityBuilder {
        return super.time(time) as! XMainCellZoneEntityBuilder
    }
    override func siteName(_ name: String?) -> XMainCellZoneEntityBuilder {
        return super.siteName(name) as! XMainCellZoneEntityBuilder
    }
    override func deviceName(_ name: String?) -> XMainCellZoneEntityBuilder {
        return super.deviceName(name) as! XMainCellZoneEntityBuilder
    }
    override func sectionName(_ name: String?) -> XMainCellZoneEntityBuilder {
        return super.sectionName(name) as! XMainCellZoneEntityBuilder
    }
    func setStatus(affects: [DAffectEntity], status: DZoneEntity.ArmStatus?, type: String, index: Int64, delay: Int64) -> XMainCellZoneEntityBuilder {
        ce.info = "\(type)\n\(R.strings.zone) \(index)" + "\( (delay > 0 ? "\n\(R.strings.zone_delay_on)" : "") )"
        
        ce.image = affects.contains(where: { $0._class == 0}) ? XImages.zone_alarm_name : XImages.zone_default_name
        ce.smallImage = status == .armed ? XImages.zone_armed_big_name :
            status == .disarmed ? XImages.zone_disarmed_big_name : nil

        ce.status = status == .armed ? R.strings.armed :
            status == .disarmed ? R.strings.disarmed : ""

        ce.affectsMini = toIcons(affects)
        ce.affects = toEventElements(affects)
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Buttons
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func buttonFireCommands(_ fire_reset: @escaping ((_ obj: Any, _ value: Any)->Void) ) -> XMainCellZoneEntityBuilder {
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.zone_fire_disable,
                                 color: DEFAULT_COLOR_RED_UINT,
                                 obj: obj,
                                 value: "nil",
                                 void: fire_reset)
        )
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Menu
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func menuRename(_ rename: @escaping ((_ obj: Any)->Void)) -> XMainCellZoneEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_zone_rename, obj: obj, void: rename)
        )
        return self
    }
    func menuDelete(_ delete: @escaping ((_ obj: Any)->Void)) -> XMainCellZoneEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_zone_delete, obj: obj, void: delete)
        )
        return self
    }
    func menuDelay(_ delay: Int64, _ set_delay: @escaping ((_ obj: Any)->Void)) -> XMainCellZoneEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: delay > 0 ? R.strings.menu_zone_delay_off : R.strings.menu_zone_delay_on, obj: obj, void: set_delay)
        )
        return self
    }
}
