//
//  XMainCellSectionEntityBuilder.swift
//  SecurityHub
//
//  Created by Timerlan on 28.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XMainCellSectionEntityBuilder: XMainCellBaseEntityBuilder {
    static func create(section s: Sections) -> XMainCellSectionEntityBuilder{
        let eb = XMainCellSectionEntityBuilder(s)
        eb.ce.id = s.id
        eb.ce.title = s.name
        return eb
    }
    override func updateType(_ updateType: NUpdateType) -> XMainCellSectionEntityBuilder {
        return super.updateType(updateType) as! XMainCellSectionEntityBuilder
    }
    override func time(_ time: Int64) -> XMainCellSectionEntityBuilder {
        return super.time(time) as! XMainCellSectionEntityBuilder
    }
    override func siteName(_ name: String?) -> XMainCellSectionEntityBuilder {
        return super.siteName(name) as! XMainCellSectionEntityBuilder
    }
    override func deviceName(_ name: String?) -> XMainCellSectionEntityBuilder {
        return super.deviceName(name) as! XMainCellSectionEntityBuilder
    }
    func isDevice(affects: [DAffectEntity], image: String) -> XMainCellSectionEntityBuilder {
        if affects.contains(where: { $0._class == 0}) {
            ce.image = XImages.section_alarm_big_name
        } else {
            ce.imageColor = DEFAULT_UNSELECTED_UINT
            ce.image = image
        }
        ce.smallImage = nil
        ce.info = R.strings.controller
        ce.status = ""
        ce.affectsMini = toIcons(affects)
        ce.affects = toEventElements(affects)
        return self
    }
    func isSecurity(affects: [DAffectEntity], status: DSectionEntity.ArmStatus, index: Int64) -> XMainCellSectionEntityBuilder {
        if affects.contains(where: { $0._class == 0}) {
            ce.image = XImages.section_alarm_big_name
        } else {
            ce.imageColor = DEFAULT_UNSELECTED_UINT
            ce.image = XImages.section_alarm_default_name
        }
        ce.smallImage = status == .armed ? XImages.section_armed_big_name :
            status == .disarmed ? XImages.section_disarmed_big_name : nil
        ce.status = status == .armed ? R.strings.armed : R.strings.disarmed
        ce.info = "\(R.strings.section_secutiry)\n\(R.strings.section) \(index)"
        ce.affectsMini = toIcons(affects)
        ce.affects = toEventElements(affects)
        return self
    }
    func isNotSecutiry(affects: [DAffectEntity], name: String, image: String, index: Int64) -> XMainCellSectionEntityBuilder {
        if affects.contains(where: { $0._class == 0}) {
            ce.image = XImages.section_alarm_big_name
        } else {
            ce.imageColor = DEFAULT_UNSELECTED_UINT
            ce.image = image
        }
        ce.smallImage = nil
        ce.info = "\(name)\n\(R.strings.section) \(index)"
        ce.status = ""
        ce.affectsMini = toIcons(affects)
        ce.affects = toEventElements(affects)
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Buttons
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func buttonCommands(arm: @escaping ((_ obj: Any, _ value: Any)->Void),
                        disarm: @escaping ((_ obj: Any, _ value: Any)->Void)) -> XMainCellSectionEntityBuilder
    {
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_arm,
                                 color: DEFAULT_COLOR_GREEN_UINT,
                                 obj: obj,
                                 value: "",
                                 void: arm)
        )
        ce.buttons.append(
            XObjectEntity.Button(title: R.strings.button_disarm,
                                 color: DEFAULT_COLOR_RED_UINT,
                                 obj: obj,
                                 value: "",
                                 void: disarm)
        )
        return self
    }
    func zones(name: String, showZones: @escaping ((_ obj: Any, _ value: Any)->Void) )-> XMainCellSectionEntityBuilder {
        ce.buttons.append(
            XObjectEntity.Button(title: name,
                                 color: DEFAULT_COLOR_MAIN_UINT,
                                 obj:   obj,
                                 value: "",
                                 void:  showZones)
        )
        return self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Menu
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func menuRenameСontroller(_ rename: @escaping ((_ obj: Any)->Void)) -> XMainCellSectionEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_section_zero_rename, obj: obj, void: rename)
        )
        return self
    }
    func menuUssd(_ ussd: @escaping ((_ obj: Any)->Void)) -> XMainCellSectionEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_section_ussd, obj: obj, void: ussd)
        )
        return self
    }
    func menuRename(_ rename: @escaping ((_ obj: Any)->Void)) -> XMainCellSectionEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_section_rename, obj: obj, void: rename)
        )
        return self
    }
    func menuUpdateAndDelete(update: @escaping ((_ obj: Any)->Void),
                             delete: @escaping ((_ obj: Any)->Void)) -> XMainCellSectionEntityBuilder {
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_section_update, obj: obj, void: update)
        )
        ce.menu.append(
            XObjectEntity.Menu(title: R.strings.menu_section_delete, obj: obj, void: delete)
        )
        return self
    }
}
