//
//  XMainCellBaseEntityBuilder.swift
//  SecurityHub
//
//  Created by Timerlan on 28.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XMainCellBaseEntityBuilder {
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Delete init
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static func create(id: Int64) -> XObjectEntity {
        var ce = XObjectEntity()
        ce.id = id
        ce.updateType = .delete
        return ce
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Base init
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var ce = XObjectEntity()
    var obj: Any
    
    init(_ obj: Any) {
        self.obj = obj
    }
    func updateType(_ updateType: NUpdateType) -> XMainCellBaseEntityBuilder {
        ce.updateType = updateType
        return self
    }
    func time(_ time: Int64) -> XMainCellBaseEntityBuilder {
        ce.time = time == 0 ? "--:--:--" : time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short)
        return self
    }
    func siteName(_ name: String?) -> XMainCellBaseEntityBuilder {
        ce.objectName = name
        return self
    }
    func deviceName(_ name: String?) -> XMainCellBaseEntityBuilder {
        ce.deviceName = name
        return self
    }
    func sectionName(_ name: String?) -> XMainCellBaseEntityBuilder {
        ce.sectionName = name
        return self
    }
    func build(affectClick:  @escaping ((_ affect_id: Int64)->Void)) -> XObjectEntity {
        ce.affectClick = affectClick
        ce.foldingInfo.closeHeight = Float(ObjectCloseView.height)
        ce.foldingInfo.openHeight = Float(EventView.height) * Float(ce.affects.count) +
            Float(ObjectOpenView.minHeight) +
            Float(ceil(Double(ce.buttons.count) / 2.0)) * Float(ObjectOpenView.buttonLineHeight)
        return ce
    }

    func toIcons(_ affects: [DAffectEntity]) -> [String] {
        let icons = affects.map { (da) -> String in return da.icon }
        var result: [String] = []
        for icon in icons { if !result.contains(icon) && icon != XImages.site_alarm_small_name { result.append(icon) } }
        return result
    }
    
    func toEventElements(_ affects: [DAffectEntity]) -> [XObjectEntity.Affect] {
        return affects.map({ (da) -> XObjectEntity.Affect in
            return XObjectEntity.Affect(id: da.id,icon: da.icon, device: da.device, sectionZone: da.sectionZone, desc: da.desc)
        })
    }
}
