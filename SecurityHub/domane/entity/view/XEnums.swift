//
//  XEnums.swift
//  SecurityHub
//
//  Created by Timerlan on 29.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

enum XSiteEntityImageState {
    case alarmed, armed, notFullArmed, disarmed
}

enum XSiteEntityTextState {
    case armed, notFullArmed, disarmed
}

enum XDeviceImageState {
    case alarmed, armed, notFullArmed, disarmed
}

enum XDeviceTextState {
    case armed, notFullArmed, disarmed
}

//enum XDeviceType {
//    case 
//}

enum XSectionState {
    case alarmed, armed, disarmed
}
