//
//  DDeviceEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 07/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

struct DDeviceEntity {
    let id: Int64
    let siteIds: [Int64]
    let siteNames: String
    let device: Devices?
    let type: NUpdateType
    let affects: [DAffectEntity]
    let armStatus: ArmStatus

    enum ArmStatus {
        case armed, disarmed, notFullArmed
    }
}

struct DDeviceWithSiteInfo {
    let device: Devices
    let siteId: Int64
    let siteName: String
}

struct DDeviceWithSitesInfo {
    let device: Devices
    var siteIds: [Int64]
    var siteNames: String
}

struct DDeviceSitesInfo {
    var siteIds: [Int64]
    var siteNames: String
}

struct DDeviceInfo {
    var name: String
    var sectionCount: Int64
    var zoneCount: Int64
    var isConnected: Bool
    var lastEventTime: Int64?
}

struct DDeviceTypeInfo {
    var type: DDeviceType
    var name: String
    var icon: String
}

enum DDeviceType {
    case SecurityHub, SecurityButton, Astra, Dozor, Other
}
