//
//  DSectionsEntity.swift
//  SecurityHub
//
//  Created by Timerlan on 07/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

struct DSectionNameAndTypeEntity {
    let name: String
    let type: DSectionTypeEntity
}

struct DSectionEntity {
    let id: Int64
    let siteIds: [Int64]
    let siteNames: String
    let deviceId: Int64
    let deviceName: String
    let configVersion: Int64
    let cluster_id: Int64
    let section: Sections?
    let type: NUpdateType
    let affects: [DAffectEntity]
    let armStatus: ArmStatus
    
    enum ArmStatus {
        case armed, disarmed
    }
}

struct DSectionWithSiteDeviceInfo {
    let section: Sections
    var siteId: Int64
    var siteName: String
    var deviceName: String
    var configVersion: Int64
    var cluster_id: Int64
}

struct DSectionWithSitesDevicesInfo {
    let section: Sections
    var siteIds: [Int64]
    var siteNames: String
    var deviceName: String
    var configVersion: Int64
    var cluster_id: Int64
}

struct DSectionSitesDevicesInfo {
    var siteIds: [Int64]
    var siteNames: String
}

struct DSectionTypeEntity {
    let id: Int64
    let name: String
    let color: UIColor
    let icon: UIImage
}


