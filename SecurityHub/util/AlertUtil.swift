//
//  AlertHelpers.swift
//  SecurityHub
//
//  Created by Timerlan on 12.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import UIKit
import RxSwift
import SCLAlertView
import AVFoundation


class AlertUtil {
    static let appearance = SCLAlertView.SCLAppearance(
        kTitleFont: UIFont(name: "HelveticaNeue", size: 16)!,
        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
        kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!
    )
    
    static func urlAlert(_ title: String, message: String, url: String, nV: UINavigationController?){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: R.strings.button_continue, style: .default, handler: { (action) in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string: url)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            } else {
                UIApplication.shared.openURL(URL(string: url)!)
            }
        }))
        alert.addAction(UIAlertAction.init(title: R.strings.button_cancel, style: .cancel, handler: nil))
        nV?.present(alert, animated: false)
    }
    
    static func exitAlert(_ exitListener: (()->Void)?, mes: String = R.strings.alert_exit_message) -> UIAlertController{
        let alert = UIAlertController.init(title: R.strings.title_confirm, message: mes, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: R.strings.button_confirm, style: .default, handler: { (action) in exitListener?() }))
        alert.addAction(UIAlertAction.init(title: R.strings.button_cancel, style: .cancel, handler: nil))
        return alert
    }
    
    static func errorAlert(title: String = R.strings.title_error, _ message: String){
        SCLAlertView(appearance: AlertUtil.appearance).showError(title, subTitle: message, closeButtonTitle: R.strings.button_ok)
    }
    
    static func successAlert(title: String = R.strings.title_success, _ message: String){
        SCLAlertView(appearance: AlertUtil.appearance).showSuccess(title, subTitle: message, closeButtonTitle: R.strings.button_ok)
    }
    
    static func infoAlert(title: String = R.strings.title_success, _ message: String){
         SCLAlertView(appearance: AlertUtil.appearance).showInfo(title, subTitle: message, closeButtonTitle: R.strings.button_ok, colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func infoAlertUser(_ message: String, name: String, listener: @escaping ((_ text: String?)->Void), change: @escaping ((_ text: String?)->Void)){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton(R.strings.button_rename, action: { renameXAlert(text: name, listener) })
        alert.addButton(R.strings.button_change_password, action: { changePasAlert(change) })
        alert.showInfo("", subTitle: message, closeButtonTitle: R.strings.button_ok,colorStyle: UIColor.hubMainColorUINT)
    }
    static func infoAlertApp(_ message: String){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.showInfo("", subTitle: message, closeButtonTitle: R.strings.button_ok,colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func warAlert(title: String = R.strings.title_warning, _ message: String) {
        SCLAlertView(appearance: AlertUtil.appearance).showNotice(title, subTitle: message, closeButtonTitle: R.strings.button_ok)
    }
    
    static func warAlert(title: String = R.strings.title_warning, message: String,
                         actionText: String, actionVoid:   @escaping (() -> Void),
                         canceled: @escaping (() -> Void)) {
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton(actionText, action: { actionVoid() })
        alert.showTitle(title, subTitle: message, timeout: nil, completeText: message,
                               style: .notice, colorStyle: SCLAlertViewStyle.notice.defaultColorInt, colorTextButton: 0xFFFFFF,
                               circleIconImage: nil, animationStyle: .topToBottom)
    }
    
    static func myXAlert(_ title: String? = nil, messages: [String], voids: [(()->Void)?], complited: (()->Void)? = nil) -> UIAlertController{
        let alert = UIAlertController.init(title: title, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for mes in messages.enumerated() {
            alert.addAction(UIAlertAction.init(title: mes.element, style: .default, handler: { (action) in voids[mes.offset]?() }))
        }
        alert.addAction(UIAlertAction.init(title: R.strings.button_cancel, style: .cancel, handler: nil))
        return alert
    }
    
    struct XAlertAction { var message: String; var void: (()->Void) }
    static func myXAlert(_ title: String? = nil, actions: [XAlertAction]) -> UIAlertController {
        let alert = UIAlertController.init(title: title, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        for action in actions{ alert.addAction(UIAlertAction.init(title: action.message, style: .default, handler: { _ in action.void() })) }
        alert.addAction(UIAlertAction.init(title: R.strings.button_cancel, style: .cancel, handler: nil))
        return alert
    }
    
    static func delegateAlert(){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField(R.strings.alert_input_delegation)
        alert.addButton(R.strings.button_add_delegation) {
            _ = DataManager.shared.delegate_Req(txt.text)
            .subscribe(onNext: { r in
                successAlert(R.strings.alert_delegation_success)
                _ = Observable<Int>.timer(.milliseconds(500), scheduler: MainScheduler.init())
                .subscribe(onNext: { t in  DataManager.shared.updateAffects() })
            });
        }
        alert.showEdit(R.strings.title_add_site, subTitle: R.strings.alert_delegation_desc, closeButtonTitle: R.strings.button_cancel,
                       colorStyle: UIColor.hubMainColorUINT, animationStyle: .topToBottom)
    }
    
    static func renameXAlert(text: String, asciiCapable: Bool = false, _ listener: @escaping ((_ text: String)->Void)){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField(R.strings.alert_input_name)
        txt.text = text
        if asciiCapable { txt.keyboardType = .asciiCapable }
        alert.addButton(R.strings.button_rename) {
            if txt.text?.count ?? 0 == 0 {
                errorAlert(R.strings.error_input_name)
            }else{
                listener(txt.text!)
            }
        }
        alert.showEdit(R.strings.title_rename, subTitle: "", closeButtonTitle: R.strings.button_cancel, colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func changePasAlert(_ listener: @escaping ((_ text: String?)->Void)){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField(R.strings.alert_input_password)
        alert.addButton(R.strings.button_change_password) { listener(txt.text) }
        alert.showEdit(R.strings.title_change_password, subTitle: "", closeButtonTitle: R.strings.button_cancel, colorStyle: UIColor.hubMainColorUINT)
    }
    
    static func ussdAlert(_ device: Int64){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField("")
        txt.text = "*100#"
        txt.keyboardType = .numbersAndPunctuation
        alert.addButton(R.strings.button_send) {
            _ = DataManager.shared.ussd(device: device, text: txt.text ?? "*100#").subscribe()
        }
        alert.showWait(R.strings.title_ussd, subTitle: R.strings.alert_input_ussd, closeButtonTitle: R.strings.button_cancel)
    }
    
    static func deleteAlert(_ listener: @escaping (()->Void), text: String, canceled: (()->Void)? = nil){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton(R.strings.button_delete) { listener() }
        alert.showWarning(text, subTitle: "", closeButtonTitle: R.strings.button_cancel)
    }
    
    static func deleteXAlert(text: String, canceled: (()->Void)? = nil, actionText: String = R.strings.button_delete,_ listener: @escaping (()->Void)) {
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        alert.addButton(actionText) { listener() }
        alert.showWarning(text, subTitle: "", closeButtonTitle: R.strings.button_cancel)
    }
    
    static func alertDatePicker(_ listener: @escaping (( _ date: Date)->Void), max: Int64, min: Int64) -> UIAlertController {
        let datePicker: UIDatePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 10, y: 30, width: UIDevice.current.userInterfaceIdiom == .pad ? 250 : UIScreen.main.bounds.size.width - 40, height: UIDevice.current.userInterfaceIdiom == .pad ? 170 : 190)
        datePicker.timeZone = NSTimeZone.local;datePicker.datePickerMode = .date;datePicker.backgroundColor = UIColor.clear
        datePicker.minimumDate = Date.init(timeIntervalSince1970: TimeInterval(min))
        datePicker.maximumDate = Date.init(timeIntervalSince1970: TimeInterval(max))
        let message = "\n\n\n\n\n\n\n\n"
        let alert = UIAlertController(title: R.strings.alert_select_date, message: message, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet )
        alert.isModalInPopover = true
        alert.view.addSubview(datePicker)
        alert.addAction(UIAlertAction(title: R.strings.button_select, style: .default, handler: {_ in
            listener(datePicker.date)
        }))
        alert.addAction( UIAlertAction(title: R.strings.button_cancel, style: .cancel, handler: nil))
        return alert
    }

    static func progress(_ title: String, text: String, canceled: (()->Void)? = nil) -> SCLAlertView{
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
//        if canceled == nil {
//            alert.show = false
//        }
        alert.showWait( title, subTitle: text, closeButtonTitle: R.strings.button_cancel)
        return alert
    }
}

// Helper function inserted by Swift 4.2 migrator.
func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
