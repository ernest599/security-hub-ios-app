//
//  XTargetUtils.swift
//  SecurityHub
//
//  Created by Timerlan on 04/07/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

class XTargetUtils {
    static var target: XTargets { return XTargets(rawValue: Bundle.main.bundleIdentifier!) ?? XTargets.security_hub }
    static var server: String{
        switch target {
        case .test:         return "ws://95.161.194.5:1122/"
        case .dev:          return "ws://95.161.194.3:1133/"
        case .security_hub: return "ws://185.27.192.163:1122/"
        case .tattelecom:   return "ws://89.232.115.81:1122/"
        case .myuvo:        return "ws://95.165.234.85:1122/"
        case .omicron:      return "ws://80.76.185.205:1122/"
        case .krasnodar:    return "ws://83.239.21.74:1122/"
        case .saratov:      return "ws://88.147.147.88:1122/"
        case .samara:       return "ws://80.234.39.160:1122/"
//        case .kuzet:        return "ws://37.151.45.162:1122/"
        case .kuzet:        return "ws://178.91.48.54:1122"
        case .favorit:      return "ws://91.226.173.4:1122/"
        case .ksb:          return "ws://78.110.158.251:1122/"
        case .rostov:       return "ws://87.117.003.162:1122/"
        case .legion:       return "ws://81.89.126.26:1122/"
        case .teleplus:     return "ws://217.114.191.34:1122/"
        case .secmatrix:    return "ws://206.54.190.168:1122/"
        }
    }
    static var site: String? {
        switch target {
        case .test:             return "http://test.security-hub.ru"
        case .dev:              return "http://dev.security-hub.ru"
        case .security_hub:     return "https://security-hub.ru"
        case .tattelecom:       return "https://control.tattelecom.ru"
        case .myuvo:            return nil
        case .omicron:          return "https://sh.omicron57.ru"
        case .krasnodar:        return nil
//        case .saratov:          return "https://saratov.security-hub.ru"
        case .saratov:          return nil
        case .samara:           return "https://samara.security-hub.ru"
        case .kuzet:            return "http://pcnkuzet.kz"
        case .favorit:          return nil
        case .ksb:              return "http://www.ksb-rso.ru"
        case .rostov:           return "http://охранавнг.рф"
        case .legion:           return nil //"https://49legion.ru/"
        case .teleplus:         return nil
        case .secmatrix:        return "https://sistemasdeseguridad.club/reg"
        }
    }
    
    static var regLink: String? {
        switch target {
        case .test:             return "http://test.security-hub.ru"
        case .dev:              return "http://dev.security-hub.ru"
        case .security_hub:     return "https://cloud.security-hub.ru"
        case .tattelecom:       return "https://control.tattelecom.ru"
        case .myuvo:            return nil
        case .omicron:          return "https://sh.omicron57.ru"
        case .krasnodar:        return nil
//        case .saratov:          return "https://saratov.security-hub.ru"
        case .saratov:          return nil
        case .samara:           return "https://samara.security-hub.ru"
        case .kuzet:            return "http://kuzethub.kz"
        case .favorit:          return nil
        case .ksb:              return "http://www.ksb-rso.ru"
        case .rostov:           return "http://охранавнг.рф"
        case .legion:           return nil //"https://49legion.ru/"
        case .teleplus:         return nil
        case .secmatrix:        return "https://sistemasdeseguridad.club/reg"
        }
    }
    
    static var ivideon: String? {
        switch target {
        case .test:             return "https://test.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .dev:              return "https://dev.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .security_hub:     return "https://cloud.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .tattelecom:       return "https://control.tattelecom.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .myuvo:            return "https://moscow.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .omicron:          return "https://sh.omicron57.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .krasnodar:        return "https://krasnodar.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .saratov:          return "https://saratov.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .samara:           return "https://samara.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
//        case .kuzet:            return "https://cabinet.pcnkuzet.kz/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .kuzet:            return nil
        case .favorit:          return nil
        case .ksb:              return nil
        case .rostov:           return "https://rostov.security-hub.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .legion:           return "https://49legion.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .teleplus:         return "https://security.tele-plus.ru/iv.php?lang=\(DataManager.defaultHelper.language)"
        case .secmatrix:        return "https://sistemasdeseguridad.club/iv.php?lang=\(DataManager.defaultHelper.language)"
        }
    }
    static var isHubTarget: Bool{
        switch target {
        case .test, .dev, .security_hub: return true
        default: return false
        }
    }
    static var isScripts: Bool{
        switch target {
        case .test, .dev, .security_hub, .saratov, .krasnodar, .teleplus, .secmatrix, .myuvo: return true
        default: return false
        }
    }
}

enum XTargets: String {
    case test = "com.teko.dpk"
    case dev = "com.teko.dev"
    case security_hub = "com.teko.sh"
    case tattelecom = "com.ttk.dpk"
    case myuvo = "com.teko.myuvo"
    case omicron = "com.teko.omicron"
    case krasnodar = "com.teko.uvo.krasnodar"
    case saratov = "com.teko.uvo.saratov"
    case samara = "com.teko.uvo.samara"
    case kuzet = "com.teko.kuzet.monitoring"
    case favorit = "com.teko.favorit"
    case ksb = "com.teko.ksb"
    case rostov = "com.teko.uvo.rostov"
    case legion = "com.teko.legion"
    case teleplus = "com.teko.teleplus"
    case secmatrix = "com.teko.secmatrix"
}
