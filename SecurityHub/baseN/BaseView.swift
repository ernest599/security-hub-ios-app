import Foundation
import UIKit
import SnapKit

class BaseView: UIView {
    
    var connectionStateChange: Any?
    var nV: UINavigationController?
    var vc: UIViewController?
    var view: UIView!
    
    lazy var backArrowView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 20
        view.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 0.3)
        view.alpha = 0.02
        view.isUserInteractionEnabled = true
        
        let gesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(backClicked(gesture:))
        )
        gesture.minimumPressDuration = 0
        
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var contentView: UIScrollView = {
        var view = UIScrollView()
        
        view.backgroundColor = UIColor.white
        view.alwaysBounceVertical = true
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        view = UIApplication.shared.keyWindow! as UIWindow
        
        setBottomBackgroundColor(UIColor.black)
        addSubview(contentView)
        setContent()
        updateConstraints()
        
        connectionStateChange = NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: OperationQueue.main){ notification in
            if notification.object as? Int ?? 0 == -6 {
                NavigationHelper.shared.show(SplashController())
                return
            }
        }
    }
    
    public func removeConnactionStateChange() {
        if connectionStateChange != nil {
            NotificationCenter.default.removeObserver(connectionStateChange!)
            connectionStateChange = nil
        }
    }
    
    public func setBottomBackgroundColor(_ color: UIColor) {
        backgroundColor = color
    }
    
    func setContent() {}
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        contentView.snp.remakeConstraints{make in
            make.edges.equalToSuperview()
        }
    }
    
    func setNV(_ nV: UINavigationController?) {
        self.nV = nV
    }
    
    @objc func backClicked(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                self.backArrowView.alpha = 1
            }
        } else if gesture.state == .changed {
            if distanceBetween2Points(
                self.backArrowView.center,
                gesture.location(in: self.backArrowView.superview)
            ) > self.backArrowView.frame.height / 2 {
                UIView.animate(withDuration: 0.3) {
                    self.backArrowView.alpha = 0.02
                }
            }
        } else if gesture.state == .ended {
            if self.backArrowView.alpha == 1 {
                UIView.animate(withDuration: 0.3) {
                    self.backArrowView.alpha = 0.02
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.nV?.popViewController(animated: true)
                }
            }
        }
    }
    
    @objc func emptyGesture() {}
    
    func distanceBetween2Points(_ a: CGPoint, _ b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGFloat(sqrt(xDist * xDist + yDist * yDist))
    }
}
