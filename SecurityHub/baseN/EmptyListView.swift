import UIKit

class EmptyListView: XBaseView {
    
    lazy var table : UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        view.backgroundColor = DEFAULT_BACKGROUND
        return view
    }()
    
    var refreshControl: UIRefreshControl = {
        return UIRefreshControl()
    }()
    
    override func setContent() {
        table.backgroundView = refreshControl
        xView.addSubview(table)
    }
    
    override func setConstraints() {
        table.snp.remakeConstraints{make in
            make.top.leading.trailing.bottom.width.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
}

