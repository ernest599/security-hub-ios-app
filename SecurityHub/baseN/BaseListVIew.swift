//
//  BaseListVIew.swift
//  SecurityHub
//
//  Created by Timerlan on 06.07.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class BaseListView<B: UIView,T:BaseCell<B>>: BaseView, UITableViewDataSource, UITableViewDelegate {
    var objs: [SQLCommand] = []
    private var cellId: String = ""
    private var cellHeight: CGFloat = 0;
    private var select: ((_ obj: SQLCommand) -> Void)?
    private var refresh: (() -> Void)?

    private lazy var table : UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.borderWidth = 0.5
        view.backgroundColor = UIColor.hubBackgroundColor
        return view
    }()
    
    private var refreshControl: UIRefreshControl = { return UIRefreshControl() }()
    
    override func setContent() {
        table.alwaysBounceVertical = false
        table.dataSource = self
        table.delegate = self
        contentView.addSubview(refreshControl)
        contentView.addSubview(table)
        refreshControl.addTarget(self, action: #selector(update), for: UIControl.Event.valueChanged)
        NotificationCenter.default.addObserver(forName: HubNotification.SITES, object: nil, queue: OperationQueue.main){ notification in self.refreshControl.endRefreshing() }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        table.snp.remakeConstraints{make in
            make.top.equalTo(Constants.margin*3)
            make.width.equalTo(UIScreen.main.bounds.width)
            make.height.equalTo(CGFloat(objs.count) * cellHeight)
            make.bottomMargin.equalTo(-Constants.margin*3)
        }
    }
    
    func initTable(cellId: String, cellHeight: CGFloat, select: ((_ obj: SQLCommand) -> Void)? = nil, refresh: (()->Void)? = nil){
        self.cellId = cellId
        self.select = select
        self.refresh = refresh
        self.cellHeight = cellHeight
        table.register(T.self, forCellReuseIdentifier: cellId)
        if refresh == nil { refreshControl.isEnabled = false }
        table.reloadData()
    }
    
    func updateList(_ objs: [SQLCommand], type: NUpdateType) {
        let anim: UITableView.RowAnimation = DataManager.settingsHelper.needAnimation ? UITableView.RowAnimation.automatic : UITableView.RowAnimation.none
        refreshControl.endRefreshing()
        switch type{
        case .insert:
            table.beginUpdates()
            objs.forEach { (obj) in
                self.objs.append(obj)
                table.insertRows(at: [IndexPath(row: self.objs.count - 1, section: 0)], with: anim)
            }
            table.endUpdates()
        case .update:
            table.beginUpdates()
            objs.forEach { (obj) in
                if let pos = self.objs.firstIndex(where: {$0.id == obj.id}){
                    self.objs[pos] = obj
                    table.reloadRows(at: [IndexPath(row: pos, section: 0)], with: anim)
                }
            }
            table.endUpdates()
        case .delete:
            table.beginUpdates()
            objs.forEach { (obj) in
                if let pos = self.objs.firstIndex(where: {$0.id == obj.id}){
                    self.objs.remove(at: pos)
                    table.deleteRows(at: [IndexPath(row: pos, section: 0)], with: anim)
                }
            }
            table.endUpdates()
        }
        updateConstraints()
    }
    
    @objc func update(){ refresh?() }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {  return cellHeight }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return objs.count }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: T = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! T
        cell.setContent(objs[indexPath.row], isEnd: objs[indexPath.row].id == objs.last!.id )
        cell.setNV(nV)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        select?(objs[indexPath.row])
    }
}

