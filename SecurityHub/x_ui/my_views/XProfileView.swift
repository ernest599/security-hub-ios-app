//
//  XProfileView.swift
//  SecurityHub
//
//  Created by Dan on 13.02.2020.
//  Copyright © 2020 TEKO. All rights reserved.
//

import UIKit
import Eureka

class XProfileView : UIView {
    static let defaultRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 80)
    var title : String
    var subtitle : String
    
    private lazy var profileImage: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "profile_icon_view")
        //view.contentMode = .scaleAspectFit
        view.layer.shadowOffset = CGSize(width: 2, height: 2)  // 1
        view.layer.shadowOpacity = 0.7 // 2
        view.layer.shadowRadius = 3 // 3
        view.layer.shadowColor = UIColor.lightGray.cgColor // 4
        return view
    }()

    private lazy var profileName: UILabel = {
        var view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 22)
        view.numberOfLines = 10
        view.text = title
        return view
    }()
    private lazy var profilePermission: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.black
        view.font = UIFont.systemFont(ofSize: 12)
        view.numberOfLines = 11
        view.text = subtitle
        return view
    }()
    private lazy var arrowNext: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "arrow_right").withColor(color: UIColor.lightGray)
        return view
    }()
    
    init(frame: CGRect , title: String , subtitle: String){
        self.title = title
        self.subtitle = subtitle
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(profileImage)
        addSubview(profileName)
        addSubview(profilePermission)
        addSubview(arrowNext)
        setConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    private func setConstraints() {
       profileImage.snp.makeConstraints{ make in
            make.top.equalToSuperview().offset(8)
            make.left.equalTo(8)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
        profileName.snp.makeConstraints{ make in
            make.top.equalToSuperview().offset(18)
            make.left.equalTo(profileImage.snp.right).offset(10)
            make.width.equalTo(UIScreen.main.bounds.width)
            make.height.equalTo(24)
           // make.right.equalToSuperview().offset(-16)
        }
        profilePermission.snp.makeConstraints{ make in
            make.top.equalTo(profileName.snp.bottom).offset(5)
            make.left.equalTo(profileImage.snp.right).offset(10)
            make.width.equalTo(UIScreen.main.bounds.width)
            make.height.equalTo(20)
        }
        arrowNext.snp.makeConstraints{ make in
            make.top.equalToSuperview().offset(13)
            make.right.equalTo(0)
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }
 }
