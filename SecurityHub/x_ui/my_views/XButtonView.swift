//
//  XButtonVIew.swift
//  SecurityHub
//
//  Created by Timerlan on 21/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XButtonView: UIView {
    static let defaultRect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44)
    enum State { case disable, progress, enable }
    enum Style { case text, fill}
    var click : (()->Void)?
    var state : State  { didSet { changeState() } }
    var title : String {  didSet { changeState() } }
    private var mainColor: UIColor
    private var rippleColor: UIColor
    private var style : Style
    private lazy var loadView: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        view.center = view.center
        view.hidesWhenStopped = false
        return view
    }()
    private lazy var btnView : ZFRippleButton = {
        var view = ZFRippleButton()
        view.rippleColor = UIColor.clear
        view.ripplePercent = 1
        view.addTarget(self, action: #selector(_click), for: UIControl.Event.touchUpInside)
        return view
    }()
    @objc private func _click() { click?() }
    
    init(_ frame: CGRect = CGRect(), title: String , style: Style = .text, state: State = .enable, mainColor: UIColor = .hubMainColor, rippleColor: UIColor = .hubDarkMainColor) {
        self.title = title
        self.state = state
        self.style = style
        self.mainColor = mainColor
        self.rippleColor = rippleColor
        super.init(frame: frame)
        addSubview(btnView)
        addSubview(loadView)
        setConstraints()
        changeState()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    private func setConstraints() {
        btnView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        loadView.snp.remakeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalToSuperview().dividedBy(2)
            make.width.equalTo(loadView.snp.height)
        }
    }
    private func changeState() {
        switch style {
        case .fill:
            btnView.shadowRippleEnable = true
            btnView.rippleBackgroundColor = rippleColor
        case .text:
            btnView.shadowRippleEnable = false
            btnView.rippleBackgroundColor = UIColor.inLightGray
        }
        switch state {
        case .disable:
            btnView.isEnabled = false
            btnView.backgroundColor = style == .fill ? .lightGray : .white
            setTitleColor(style == .fill ? .white : .lightGray)
            loadView.stopAnimating()
            loadView.isHidden = true
        case .progress:
            setTitleColor(.clear)
            btnView.backgroundColor = style == .fill ? .lightGray : .white
            btnView.isEnabled = false
            loadView.startAnimating()
            loadView.isHidden = false
        case .enable:
            btnView.backgroundColor = style == .fill ? mainColor : .white
            setTitleColor(style == .fill ? .white : mainColor)
            btnView.isEnabled = true
            loadView.stopAnimating()
            loadView.isHidden = true
        }
    }
    private func setTitleColor(_ color: UIColor) {
        btnView.setAttributedTitle(
            NSAttributedString(string: title, attributes: [
                .font : UIFont.systemFont(ofSize: 16),
                .foregroundColor : color
            ]
        ), for: .normal)
    }
}
