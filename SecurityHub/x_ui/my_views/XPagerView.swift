//
//  XPagerView.swift
//  SecurityHub
//
//  Created by Timerlan on 24/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation
import UIKit
import FSPagerView

class XPagerView: UIView, FSPagerViewDataSource {
    private let images: [String]
    private lazy var pagerView: FSPagerView = {
        var view = FSPagerView()
        view.contentMode = UIView.ContentMode.center
        view.automaticSlidingInterval = 2.0
        view.isInfinite = false
        view.interitemSpacing = 10
        view.dataSource = self
        view.isUserInteractionEnabled = false
        view.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        return view
    }()
    
    init(_ images: [String], height: CGFloat = 180, margin: CGFloat = 0) {
        self.images = images
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: height))
        pagerView.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height: height - margin)
        backgroundColor = .white
        addSubview(pagerView)
        pagerView.snp.makeConstraints { (make) in make.edges.equalToSuperview() }
    }
    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int { return images.count }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        cell.imageView?.image = UIImage(named: images[index])
        return cell
    }
}
