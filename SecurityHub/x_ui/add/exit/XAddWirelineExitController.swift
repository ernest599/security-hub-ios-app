//
//  XAddWirelineExitController.swift
//  SecurityHub
//
//  Created by Timerlan on 15/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift


class XAddWirelineExitController: FormViewController {
    private let dm: DataManager
    private var devices: [DDeviceWithSitesInfo] = []
    
    private var device: DDeviceWithSitesInfo?
    private var type = 4
    private var detector = 15
    
    private var selectedDeviceRow: ListCheckRow<String>?
    private var deviceBlock: SelectableSection<ListCheckRow<String>>?
    private var nameRow: TextRow?
    private var numberRow: PhoneRow?
    private var createButton: XButtonView?
    private var disposable: Disposable?
    init(device_ids: [Int64]) {
        self.dm = DataManager.shared
        for id in device_ids { if let di = dm.getDeviceInfo(device_id: id) { devices.append(di) } }
        super.init(nibName: nil, bundle: nil)
        title = R.strings.exit_add_title
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDeviceSelectBlock()
        initExitInfoBlock()
        initCreateButton()
    }
    
    private func initDeviceSelectBlock() {
        if devices.count == 1 { return device = devices[0] }
        deviceBlock = SelectableSection<ListCheckRow<String>>(R.strings.title_select_device, selectionType: .singleSelection(enableDeselection: false))
        for _device in devices.enumerated() {
            deviceBlock! <<< ListCheckRow<String>(_device.element.device.name){
                $0.cell.tintColor = UIColor.hubMainColor
                $0.title = _device.element.device.name
                $0.selectableValue = _device.element.device.name
                if (_device.offset == 0 && HubConst.isHub(_device.element.device.configVersion, cluster: _device.element.device.cluster_id)) {
                    $0.value = _device.element.device.name
                    deviceSelected($0, _device.element)
                }
                $0.onChange { (_row) in if _row.value != nil { self.deviceSelected(_row, _device.element) } }
            }
        }
        form +++ deviceBlock!
    }
    private func initExitInfoBlock() {
        let typeRow = PushRow<String>() {
            $0.title = R.strings.add_wireline_exit_type
            $0.options = [R.strings.wireline_exit_mayak, R.strings.wireline_exit_sirena]
            $0.value = R.strings.wireline_exit_mayak
            $0.onChange { (row) in self.selectTypeRelay(row) }
        }
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = R.strings.wireline_exit_name_title
            $0.placeholder = R.strings.wireline_exit_name_placeholder
        }
        numberRow = PhoneRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = R.strings.add_wireline_exit_number
            $0.placeholder = R.strings.add_wireline_exit_put_number
            let numberRule = RuleClosure<String> { rowValue in
                if let row = rowValue, let value = Int64(row), (value > 0 && value < 7) { return nil }
                return ValidationError(msg: "Field required!")
            }
            $0.add(rule: numberRule)
            $0.validationOptions = .validatesOnChange
        }
        .cellUpdate { cell, row in if !row.isValid { cell.titleLabel?.textColor = .systemRed }  }
        form +++ Section(R.strings.add_wireline_exit_info )
        <<< nameRow!
        <<< numberRow!
        <<< typeRow
    }
    private func initCreateButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: R.strings.add_wireline_exit_add, style: .text)
        createButton?.click = self.createRelay
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
       
    private func deviceSelected(_ row: ListCheckRow<String>, _ device: DDeviceWithSitesInfo) {
        guard HubConst.isHub(device.device.configVersion, cluster: device.device.cluster_id) else {
            row.value = nil
            row.cell.isUserInteractionEnabled = false
            row.cell.contentView.alpha = 0.5
            selectedDeviceRow?.value = self.device?.device.name
            selectedDeviceRow?.updateCell()
            return AlertUtil.errorAlert(R.strings.error_device_not_hub)
        }
        self.device = device
        self.selectedDeviceRow = row
    }
    private func selectTypeRelay(_ row: PushRow<String>) {
        self.type = row.value == R.strings.wireline_exit_mayak ? 4 : 5
        self.detector = row.value == R.strings.wireline_exit_mayak ? 15 : 14
    }
    private func showProgress() {
        createButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }

    
    private func createRelay() {
        view.endEditing(true)
        guard let device_id = device?.device.id else { return AlertUtil.errorAlert(R.strings.add_wireline_exit_warning_select_device) }
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert(R.strings.add_wireline_exit_warning_put_name) }
        guard numberRow?.isValid == true, let number = Int64(numberRow?.value ?? ".") else { return AlertUtil.errorAlert(R.strings.add_wireline_exit_warning_put_correct_number) }
        let uid = String(format: "%02X", 5) + String(format: "%02X", number) + String(format: "%02X", type)
        showProgress()
        disposable = dm.addZone(device_id: device_id, uid: uid, section: 0, name: nameRow!.value!, detector: detector)
            .observe(on: MainScheduler.init())
              .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        guard let nV = navigationController, let last = nV.viewControllers.last(where: { $0 is XMainController}) else { return }
        nV.popToViewController(last, animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
