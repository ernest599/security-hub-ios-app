//
//  XAddWirelessRelayController.swift
//  SecurityHub
//
//  Created by Timerlan on 15/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import Eureka
import RxSwift


class XAddWirelessRelayController: FormViewController {
    private let dm: DataManager
    private let device_id: Int64
    private let uid: String
    private let type: DZoneTypeEntity
    private var detector = 23
    private var nameRow: TextRow?
    private var createButton: XButtonView?
    private var disposable: Disposable?
    init(device_id: Int64, uid: String, type: DZoneTypeEntity) {
        self.dm = DataManager.shared
        self.device_id = device_id
        self.uid = uid
        self.type = type
        super.init(nibName: nil, bundle: nil)
        title = R.strings.title_add_relay
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")  }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTypeInfoBlock()
        initRelayInfoBlock()
        initCreateButton()
    }
    
    private func initTypeInfoBlock() {
        let images = [type.img]
            form +++ Section(header: type.title, footer: type.subTitle)
            <<< ViewRow<XPagerView>().cellSetup { (cell, row) in cell.view = XPagerView(images, height: 200, margin: 40); cell.update() }
    }
    private func initRelayInfoBlock() {
        let typeRow = PushRow<String>() {
            $0.title = R.strings.add_wireless_relay_type
            if (XTargetUtils.isScripts)
            {
              $0.options = [R.strings.wireless_relay_defaul_manage, R.strings.wireless_relay_auto_manage]
            }
            else {$0.options = [R.strings.wireless_relay_defaul_manage]}
            $0.value = R.strings.wireless_relay_defaul_manage
            $0.onChange { (row) in self.selectTypeRelay(row) }
            $0.onPresent {form, selectorController in
                selectorController.enableDeselection = false }
        }
        nameRow = TextRow() {
            $0.cell.tintColor = .hubMainColor
            $0.title = R.strings.wireless_relay_name_title
            $0.placeholder = R.strings.wireless_relay_name_placeholder
        }
        form +++ Section(R.strings.add_wireless_relay_info )
        <<< nameRow!
        <<< typeRow
    }
    private func initCreateButton() {
        createButton = XButtonView(XButtonView.defaultRect, title: R.strings.add_wireless_relay_add, style: .text)
        createButton?.click = self.createRelay
        form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in cell.view = self.createButton; cell.update() }
    }
       
    
    private func selectTypeRelay(_ row: PushRow<String>) {
        self.detector = row.value == R.strings.wireless_relay_defaul_manage ? 23 : 13
    }
    private func showProgress() {
        createButton?.state = .progress
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = false
            row.baseCell.contentView.alpha = 0.5
        }
    }

    
    private func createRelay() {
        view.endEditing(true)
        if nameRow?.value == nil || nameRow?.value?.count == 0 { return AlertUtil.errorAlert(R.strings.add_wireless_relay_warning_put_name) }
        showProgress()
        disposable = dm.addZone(device_id: device_id, uid: uid, section: 0, name: nameRow!.value!, detector: detector)
            .observe(on: MainScheduler.init())
              .subscribe(onSuccess: self.finish)
    }
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
        guard let nV = navigationController, let last = nV.viewControllers.last(where: { $0 is XMainController}) else { return }
        nV.popToViewController(last, animated: true)
    }
    private func error(_ result: DCommandResult) {
        createButton?.state = .enable
        for row in form.allRows {
            row.baseCell.isUserInteractionEnabled = true
            row.baseCell.contentView.alpha = 1
        }
        view.endEditing(true)
        AlertUtil.errorAlert(result.message)
    }
}
