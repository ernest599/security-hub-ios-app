import UIKit
import RxSwift
import SwipeCellKit

class XScriptsController: XBaseController<XScriptsView> {
    
    private let device_ids: [Int64]?
    private let dm = DataManager.shared!
    private let disposables: CompositeDisposable
    
    var eventDisp: Disposable!
    
    init(device_ids: [Int64]? = nil) {
        self.device_ids = device_ids
        self.disposables = CompositeDisposable()
        
        super.init(nibName: nil, bundle: nil)
        
        title = R.strings.title_scripts
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    override func loadView() {
        super.loadView()
        
        xView.setNV(navigationController)
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        if (Roles.sendCommands && Roles.manageSectionZones) {
            rightButton()
            
            xView.needDelete = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.xView.deleteScript = self.deleteScript
            self.xView.updScriptVoid = self.updScript
            
            self.load()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        eventDisp = dm.events
            .asObservable()
            .subscribe(onNext: { events in
                DispatchQueue.main.async { [self] in
                    if events.count > 0 {
                        for event in events {
                            if event._class == 0 {
                                xView.changeBottomNavStatus(type: 0)

                                break
                            } else if event._class >= 1 && event._class <= 3 {
                                xView.changeBottomNavStatus(type: 1)
                            }
                        }
                    } else {
                        xView.changeBottomNavStatus(type: 2)
                    }
                }
            })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        eventDisp.dispose()
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        
        if parent == nil {
            disposables.dispose()
        }
    }
    
    private func load() {
        let disp = dm.getScripts(device_ids: device_ids)
            .subscribe(
                onNext:
                    self.xView.updScript,
                onError: {
                    _ in self.error(DCommandResult.ERROR)
                }
            )
        
        _ = disposables.insert(disp)
    }
    
    private func deleteScript(_ device_id: Int64, _ bind: Int64, _ name: String) {
        DispatchQueue.main.async {
            AlertUtil.deleteXAlert(text: "\(R.strings.scripts_delete_script) \"\(name)\"?") {
                let disp = self.dm.deleteScript(device_id: device_id, bind: bind)
                    .subscribe(
                        onSuccess:
                            self.finish,
                        onFailure: {
                            _ in self.error(DCommandResult.ERROR)
                        }
                    )
                
                _ = self.disposables.insert(disp)
            }
        }
    }
    
    private func finish(_ result: DCommandResult) {
        guard result.success else { return error(result) }
    }
    
    private func error(_ result: DCommandResult) {
        DispatchQueue.main.async {
            AlertUtil.errorAlert(result.message)
        }
    }
    
    override func rightClick(){
        let device_ids = self.device_ids ?? dm.getHubDeviceIds()
        
        switch device_ids.count {
        case 0:
            DispatchQueue.main.async {
                AlertUtil.warAlert(R.strings.add_zone_no_device_error)
            }
        case 1:
            guard dm.isHub(device_id: device_ids[0]) else { return
                    DispatchQueue.main.async {
                        AlertUtil.warAlert(R.strings.add_section_not_hub_error)
                    }
                }
            
            showOnAvailable(device_id: device_ids[0]) {
                self.add(device_ids: device_ids)
            }
        default:
            add(device_ids: device_ids)
        }
    }
    
    private func showOnAvailable(device_id: Int64, _ void: @escaping (()->Void)) {
        let _ = dm.isAvailableDevice(device_id: device_id)
            .do(onSuccess: { (result) in if result.success { void() } })
            .do(onSuccess: { (result) in
                if !result.success {
                    DispatchQueue.main.async {
                        AlertUtil.errorAlert(result.message)
                    }
                }
            })
            .subscribe()
    }
    
    private func add(device_ids: [Int64]) {
        guard let nV = navigationController else { return }
        
        let vc = XSelectScriptTypeController(device_ids: device_ids)
        nV.pushViewController(vc, animated: true)
    }
    
    private func updScript(_ script: DScriptEntity) {
        guard let nV = navigationController else { return }
        guard let library = script.library else { return self.error(DCommandResult.ERROR) }
        
        let vc = XAddScriptController(device_id: script.deviceId, script: library, params: script.params, name: script.getName(), bind: script.bind)
        nV.pushViewController(vc, animated: true)
    }
}
