import UIKit
import RxSwift
import SwipeCellKit

class XScriptsView: XBaseView, UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    
    public var needDelete: Bool = false
    public var deleteScript: ((_ device_id: Int64, _ bind: Int64, _ name: String) -> Void)?
    public var updScriptVoid: ((_ script: DScriptEntity) -> Void)?
    private var list: [DScriptEntity] = []
    var events: [Events] = []
    
    lazy var shadowLayer: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.regText
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var bottomNavFrame: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubTransparent
        
        return view
    }()
    
    lazy var bottomNavBackground: UIView = {
        return UIView()
    }()
    
    lazy var bottomNavLine: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubSeparator
        
        return view
    }()
    
    lazy var bottomNavState: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.numberOfLines = 1
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(stateClick)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var bottomNavIcons: UIStackView = {
        let view = UIStackView()
        
        view.axis = .horizontal
        view.distribution = .equalSpacing
        
        return view
    }()
    
    lazy var bottomNavMain: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_main!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavScripts: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_script!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubMainColor
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavEvents: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_history!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavCameras: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_cameras!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavSettings: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_menu!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let view = UITableView()
        
        view.separatorStyle = .none
        view.dataSource = self
        view.delegate = self
        view.register(XScriptCell.self, forCellReuseIdentifier: XScriptCell.cellId)
        
        return view
    }()
    
    override func setContent() {
        xView.addSubview(bottomNavFrame)
        xView.addSubview(bottomNavLine)
        bottomNavFrame.addSubview(bottomNavBackground)
        bottomNavFrame.addSubview(bottomNavIcons)
        bottomNavFrame.addSubview(bottomNavState)
        bottomNavIcons.addArrangedSubview(bottomNavMain)
        bottomNavIcons.addArrangedSubview(bottomNavScripts)
        bottomNavIcons.addArrangedSubview(bottomNavEvents)
        bottomNavIcons.addArrangedSubview(bottomNavCameras)
        bottomNavIcons.addArrangedSubview(bottomNavSettings)
        
        xView.addSubview(tableView)
    }
    
    override func setConstraints() {
        bottomNavFrame.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(100)
            make.bottom.equalToSuperview().offset(-50)
        }
        
        bottomNavBackground.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        bottomNavLine.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(1)
            make.bottom.equalTo(bottomNavFrame.snp.top)
        }
        
        bottomNavIcons.snp.remakeConstraints { make in
            make.height.equalTo(36)
            make.bottom.equalTo(xView.snp.bottom).offset(-100)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavState.snp.remakeConstraints { make in
            make.bottom.equalTo(bottomNavIcons.snp.top).offset(-20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavMain.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavScripts.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavEvents.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavCameras.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavSettings.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        changeBottomNavStatus(type: DataManager.shared.bottomNavState)
        
        tableView.snp.remakeConstraints{ make in
            make.width.top.equalToSuperview()
            make.bottom.equalTo(bottomNavLine.snp.top)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return XScriptCell.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: XScriptCell.cellId, for: indexPath)
    }
    
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? XScriptCell else { return }
        
        if needDelete {
            cell.delegate = self
        }
        
        cell.setContent(list[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        
        updScriptVoid?(list[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let script = list[indexPath.row]
        let manageAction = SwipeAction(style: .default, title: R.strings.button_delete) { _, _ in self.deleteScript?(script.deviceId, script.bind, script.getName() ?? "") }
        manageAction.backgroundColor = UIColor.systemRed
        manageAction.textColor = UIColor.white
        manageAction.image = nil
        
        return [manageAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        
        options.expansionStyle = .selection
        options.transitionStyle = .drag
        
        return options
    }
    
    func updScript(_ data: DScriptEntity) {
        DispatchQueue.main.async {
            let pos = self.list.firstIndex{ $0.id == data.id }
            
            if (data.type == .insert && pos == nil) {
                self.list.append(data)
                self.tableView.insertRows(at: [IndexPath(row: self.list.count - 1, section: 0)], with: .left)
            } else if let pos = pos, (data.type == .insert || data.type == .update) {
                self.list[pos] = data
                self.tableView.reloadRows(at: [IndexPath(row: pos, section: 0)], with: .fade)
            } else if let pos = pos, data.type == .delete {
                self.list.remove(at: pos)
                self.tableView.deleteRows(at: [IndexPath(row: pos, section: 0)], with: .left)
            }
        }
    }
    
    func changeBottomNavStatus(type: Int) {
        DataManager.shared.bottomNavState = type
        
        if type == 0 {
            bottomNavBackground.backgroundColor = UIColor.hubRedColor
            
            bottomNavState.text = R.strings.alarm
            bottomNavState.textColor = UIColor.white
            
            bottomNavScripts.tintColor = UIColor.hubMainColor
        } else if type == 1 {
            bottomNavBackground.backgroundColor = UIColor.hubGold
            
            bottomNavState.text = R.strings.criticalEvents
            bottomNavState.textColor = UIColor.hubTextBlack
            
            bottomNavScripts.tintColor = UIColor.hubTextBlack
        } else {
            bottomNavBackground.layer.cornerRadius = 0
            bottomNavBackground.backgroundColor = UIColor.white
            
            bottomNavMain.tintColor = UIColor.hubSeparator
            bottomNavScripts.tintColor = UIColor.hubMainColor
            bottomNavEvents.tintColor = UIColor.hubSeparator
            bottomNavCameras.tintColor = UIColor.hubSeparator
            bottomNavSettings.tintColor = UIColor.hubSeparator
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(100)
                make.bottom.equalToSuperview().offset(-50)
            }
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(1)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
        }
        
        if type != 2 {
            bottomNavBackground.layer.cornerRadius = 32
            bottomNavBackground.layer.masksToBounds = true
            bottomNavBackground.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            bottomNavMain.tintColor = UIColor.white
            bottomNavEvents.tintColor = UIColor.white
            bottomNavCameras.tintColor = UIColor.white
            bottomNavSettings.tintColor = UIColor.white
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(0)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(150)
                make.bottom.equalToSuperview().offset(-50)
            }
        }
    }
    
    @objc func bottomNavClicked(gesture: UITapGestureRecognizer) {
        var viewControllers = nV!.viewControllers
        _ = viewControllers.popLast()
        
        switch gesture.view {
        case bottomNavMain:
            viewControllers.append(XMainController(type: .site, name: nil))
        case bottomNavScripts:
            viewControllers.append(XScriptsController())
        case bottomNavEvents:
            viewControllers.append(AllEventsController())
        case bottomNavCameras:
            viewControllers.append(CameraListController())
        default:
            viewControllers.append(XSettingsController())
        }
        
        nV!.setViewControllers(viewControllers, animated: false)
    }
    
    @objc func stateClick(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.view.addSubview(self.shadowLayer)
                self.shadowLayer.addSubview(self.alertContainer)
                self.alertContainer.addSubview(self.alertText)
                
                self.alertText.text = self.bottomNavState.text
                
                self.shadowLayer.addGestureRecognizer(
                    UITapGestureRecognizer(
                        target: self,
                        action: #selector(self.hideAlert)
                    )
                )

                self.shadowLayer.snp.remakeConstraints { make in
                    make.width.height.equalToSuperview()
                }
                
                self.alertContainer.snp.remakeConstraints { make in
                    make.height.equalTo(100)
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                self.alertText.snp.remakeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(43)
                    make.trailing.equalToSuperview().offset(-43)
                }
                
                self.shadowLayer.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.shadowLayer.alpha = 1
                }
            }
        }
    }
    
    @objc func hideAlert(gesture: UILongPressGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.shadowLayer.alpha = 0
        }
    }
}
