//
//  XBaseView.swift
//  SecurityHub
//
//  Created by Timerlan on 15.02.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class XBaseView: UIView {
    
    var isBackGestureCanceled: Bool = false
    var nV: UINavigationController?
    private var gesturePosition: CGPoint!
    var view: UIView!
    var vc: UIViewController?
    
    lazy var xView = UIView()
    
    lazy var backArrowView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 25
        view.backgroundColor = UIColor(red: 0.69, green: 0.69, blue: 0.69, alpha: 0.4)
        view.alpha = 0
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xView.backgroundColor = DEFAULT_COLOR_WINDOW_BACKGROUND
        addSubview(xView)
        view = UIApplication.shared.keyWindow! as UIWindow
        xView.snp.remakeConstraints{  make in  make.edges.equalToSuperview() }
        setContent()
        setConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    func setContent(){}
    
    func setConstraints(){}
    
    override func updateConstraints() {
        super.updateConstraints()
        setConstraints()
    }
    
    func setNV(_ nV: UINavigationController?) {
        self.nV = nV
    }
    
    @objc func backClicked(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                self.backArrowView.alpha = 1
            }
        } else if gesture.state == .changed {
            if distanceBetween2Points(
                self.backArrowView.center,
                gesture.location(in: self.backArrowView.superview)
            ) > self.backArrowView.frame.height / 2 {
                UIView.animate(withDuration: 0.3) {
                    self.backArrowView.alpha = 0.02
                }
            }
        } else if gesture.state == .ended {
            if self.backArrowView.alpha == 1 {
                UIView.animate(withDuration: 0.3) {
                    self.backArrowView.alpha = 0.02
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.nV?.popViewController(animated: true)
                }
            }
        }
    }
    
    @objc func emptyGesture() {}
    
    func distanceBetween2Points(_ a: CGPoint, _ b: CGPoint) -> CGFloat {
        let xDist = a.x - b.x
        let yDist = a.y - b.y
        return CGFloat(sqrt(xDist * xDist + yDist * yDist))
    }
}
