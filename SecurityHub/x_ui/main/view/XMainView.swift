import UIKit
import RxSwift
import RxCocoa
import Localize_Swift

protocol XMainViewProtocol {
    func viewWillAppear()
    func viewWillDisappear()
    func tabItemClick()
}

class XMainView: XBaseView, XMainViewProtocol {
    
    static let TOP_TAG = 12445
    var olist: [XObjectEntity] = []
    var elist: [Events] = []
    var isToolbarHidden: Bool = false
    var oY: CGFloat = 0
    var eY: CGFloat = 0
    var scrollEnable = true
    var timerDisp: Disposable?
    var eTUDisp: Disposable?
    var xC: XBaseController<XMainView>! { didSet{ xN = xC.navigationController } }
    var xN: UINavigationController!
    var spinerView: XSpinerView!
    var onEventSegmentClick: (()->Void)?
    var onObjectSegmentClick: (()->Void)?
    
    lazy var shadowLayer: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.regText
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var bottomNavFrame: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubTransparent
        
        return view
    }()
    
    lazy var bottomNavBackground: UIView = {
        return UIView()
    }()
    
    lazy var bottomNavLine: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubSeparator
        
        return view
    }()
    
    lazy var bottomNavState: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.numberOfLines = 1
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        view.isHidden = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(stateClick)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var bottomNavIcons: UIStackView = {
        let view = UIStackView()
        
        view.axis = .horizontal
        view.distribution = .equalSpacing
        
        return view
    }()
    
    lazy var bottomNavMain: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_main!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubMainColor
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavScripts: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_script!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavEvents: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_history!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavCameras: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_cameras!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavSettings: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_menu!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var segmentBlock: UIView = {
        let view = UIView()
        
        view.tag = XMainView.TOP_TAG
        let layerBottom = CALayer()
        layerBottom.frame = CGRect(x: 0, y: segmentHeight, width: UIScreen.main.bounds.width, height: 0.7)
        layerBottom.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        view.backgroundColor = UIColor.white
        view.addSubview(segmentControl)
        
        return view
    }()
    
    lazy var segmentControl: UISegmentedControl = {
        let view = UISegmentedControl(items: [R.strings.sites, R.strings.segment_events])
        
        view.tintColor = DEFAULT_COLOR_TEXT_LIGHT
        view.selectedSegmentIndex = 0
        
        return view
    }()
    
    lazy var oTableView : UITableView = {
        let view = UITableView()
        
        view.separatorStyle = .none
        view.estimatedRowHeight = 130
        
        return view
    }()
    
    lazy var eTableView : UITableView = {
        let view = UITableView()
        
        view.separatorStyle = .none
        view.isHidden = true
        view.estimatedRowHeight = 130
        
        return view
    }()
    
    lazy var searchBar: XSearchBar = {
        let view = XSearchBar()
        
        view.tag = XMainView.TOP_TAG
        view.Height = searchHeight
        view.searchBarStyle = UISearchBar.Style.prominent
        view.placeholder = R.strings.searchBar_placeholder
        view.barTintColor = DEFAULT_COLOR_MAIN
        view.isTranslucent = false
        view.backgroundImage = UIImage()
        
        return view
    }()
  
    override func setContent() {
        xView.addSubview(bottomNavFrame)
        xView.addSubview(bottomNavLine)
        bottomNavFrame.addSubview(bottomNavBackground)
        bottomNavFrame.addSubview(bottomNavIcons)
        bottomNavFrame.addSubview(bottomNavState)
        bottomNavIcons.addArrangedSubview(bottomNavMain)
        bottomNavIcons.addArrangedSubview(bottomNavScripts)
        bottomNavIcons.addArrangedSubview(bottomNavEvents)
        bottomNavIcons.addArrangedSubview(bottomNavCameras)
        bottomNavIcons.addArrangedSubview(bottomNavSettings)
        
        xView.addSubview(oTableView)
        xView.addSubview(eTableView)
    }
    
    override func setConstraints() {
        bottomNavFrame.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(100)
            make.bottom.equalToSuperview().offset(-50)
        }
        
        bottomNavBackground.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        bottomNavLine.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(1)
            make.bottom.equalTo(bottomNavFrame.snp.top)
        }
        
        bottomNavIcons.snp.remakeConstraints { make in
            make.height.equalTo(36)
            make.bottom.equalTo(xView.snp.bottom).offset(-100)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavState.snp.remakeConstraints { make in
            make.bottom.equalTo(bottomNavIcons.snp.top).offset(-20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavMain.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
            
        bottomNavScripts.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavEvents.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavCameras.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }

        bottomNavSettings.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        changeBottomNavStatus(type: DataManager.shared.bottomNavState)
        
        oTableView.snp.remakeConstraints{ make in
            make.width.top.equalToSuperview()
            make.bottom.equalTo(bottomNavLine.snp.top)
        }
        
        eTableView.snp.remakeConstraints{ make in
            make.width.top.equalToSuperview()
            make.bottom.equalTo(bottomNavLine.snp.top)
        }
    }
    
    func viewWillAppear() {
        NavigationHelper.shared.statusBar(isHidden: true)
        
        xN.setNavigationBarHidden(isToolbarHidden, animated: false)
        segmentControl.isHidden = isToolbarHidden
        
        if let animatedTabBar = xN.tabBarController as? XMenuController {
            animatedTabBar.setTabBarHidden(true)
        }
        
        spinerView.isHidden = false
    }
    
    func viewWillDisappear() {
        NavigationHelper.shared.statusBar(isHidden: true)
        
        spinerView.isOpen = false
        spinerView.isHidden = true
        isToolbarHidden = false
    }

    func tabItemClick() {
        if spinerView.isOpen {
            spinerView.isOpen = false
        } else {
            setSearchBarHidden(false)
        }
    }
    
    func changeBottomNavStatus(type: Int) {
        DataManager.shared.bottomNavState = type
        
        if type == 0 {
            bottomNavBackground.backgroundColor = UIColor.hubRedColor
            
            bottomNavState.text = R.strings.alarm
            bottomNavState.textColor = UIColor.white
            bottomNavState.isHidden = false
            
            bottomNavMain.tintColor = UIColor.hubMainColor
        } else if type == 1 {
            bottomNavBackground.backgroundColor = UIColor.hubGold
            
            bottomNavState.text = R.strings.criticalEvents
            bottomNavState.textColor = UIColor.hubTextBlack
            bottomNavState.isHidden = false
            
            bottomNavMain.tintColor = UIColor.hubTextBlack
        } else {
            bottomNavBackground.layer.cornerRadius = 0
            bottomNavBackground.backgroundColor = UIColor.white
            
            bottomNavMain.tintColor = UIColor.hubMainColor
            bottomNavScripts.tintColor = UIColor.hubSeparator
            bottomNavEvents.tintColor = UIColor.hubSeparator
            bottomNavCameras.tintColor = UIColor.hubSeparator
            bottomNavSettings.tintColor = UIColor.hubSeparator
            
            bottomNavState.isHidden = true
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(100)
                make.bottom.equalToSuperview().offset(-50)
            }
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(1)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
        }
        
        if type != 2 {
            bottomNavBackground.layer.cornerRadius = 32
            bottomNavBackground.layer.masksToBounds = true
            bottomNavBackground.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            bottomNavScripts.tintColor = UIColor.white
            bottomNavEvents.tintColor = UIColor.white
            bottomNavCameras.tintColor = UIColor.white
            bottomNavSettings.tintColor = UIColor.white
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(0)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(150)
                make.bottom.equalToSuperview().offset(-50)
            }
        }
    }
    
    @objc func bottomNavClicked(gesture: UITapGestureRecognizer) {
        var viewControllers = xN.viewControllers
        _ = viewControllers.popLast()
        
        switch gesture.view {
        case bottomNavMain:
            viewControllers.append(XMainController(type: .site, name: nil))
        case bottomNavScripts:
            viewControllers.append(XScriptsController())
        case bottomNavEvents:
            viewControllers.append(AllEventsController())
        case bottomNavCameras:
            viewControllers.append(CameraListController())
        default:
            viewControllers.append(XSettingsController())
        }
        
        xN.setViewControllers(viewControllers, animated: false)
    }
    
    @objc func stateClick(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.view.addSubview(self.shadowLayer)
                self.shadowLayer.addSubview(self.alertContainer)
                self.alertContainer.addSubview(self.alertText)
                
                self.alertText.text = self.bottomNavState.text
                
                self.shadowLayer.addGestureRecognizer(
                    UITapGestureRecognizer(
                        target: self,
                        action: #selector(self.hideAlert)
                    )
                )

                self.shadowLayer.snp.remakeConstraints { make in
                    make.width.height.equalToSuperview()
                }
                
                self.alertContainer.snp.remakeConstraints { make in
                    make.height.equalTo(100)
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                self.alertText.snp.remakeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(43)
                    make.trailing.equalToSuperview().offset(-43)
                }
                
                self.shadowLayer.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.shadowLayer.alpha = 1
                }
            }
        }
    }
    
    @objc func hideAlert(gesture: UILongPressGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.shadowLayer.alpha = 0
        }
    }
}
