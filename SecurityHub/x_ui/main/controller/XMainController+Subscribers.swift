//
//  XMainController+Subscribers.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension XMainController {
    func eventSubscribe(_ e: (event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)){
        tableProtocol.addEvent(e.event, type: e.type, isFromBD: e.isFromBD)
    }

    func toXObjectEntity(_ ds: DSiteEntity) -> XObjectEntity {
        guard let site = ds.site else { return XMainCellBaseEntityBuilder.create(id: ds.id) }
        let ce = XMainCellSiteEntityBuilder.create(site: site)
            .updateType(ds.type)
            .status(affects: ds.affects, status: ds.armStatus)
            .buttonCommands(arm: arm, disarm: disarm, need: Roles.sendCommands)
            .buttonDevice(showDevices, need: DataManager.settingsHelper.needDevices)
            .buttonRele(showReles, need: !DataManager.settingsHelper.needDevices)
            .buttonListHozorgan(showHozorgan, need: !DataManager.settingsHelper.needDevices)
            .buttonSection(showSections, need: !DataManager.settingsHelper.needDevices && DataManager.settingsHelper.needSections)
            .buttonZone(showZones, need: !DataManager.settingsHelper.needDevices && !DataManager.settingsHelper.needSections)
            .menuAbout(about)
        if Roles.manageSites && site.delegated == 0 {
            _ = ce.menuRename(rename)
                .menuScrips(scripts)
                .menuDelegate(delegate)
                .menuDelete(delete)
        } else if Roles.manageSites && site.delegated == 2 {
            _ = ce.menuRename(rename)
                .menuScrips(scripts)
                .menuDelegate(delegate)
                .menuListDelegate(list_delegate)
                .menuDelete(delete)
        } else if Roles.manageSites && site.delegated == 1 {
            _ = ce.menuScrips(scripts)
                .menuDeleteDelegate(delete_delegate)
        }
        return ce.build(affectClick: showEvent)
    }
    
    func toXObjectEntity(_ dd: DDeviceEntity) -> XObjectEntity {
        guard let device = dd.device else { return XMainCellBaseEntityBuilder.create(id: dd.id) }
        let dTI = HubConst.getDeviceTypeInfo(device.configVersion, cluster: device.cluster_id)
        let ce = XMainCellDeviceEntityBuilder.create(device: device)
            .updateType(dd.type)
            .affects(affects: dd.affects, image: dTI.icon)
            .siteName(objectType == .site ? dd.siteNames : nil)

        switch dTI.type {
        case .SecurityHub:      return securityHubDevice(ce: ce, dd: dd)
        case .SecurityButton:   return securityButtonDevice(ce: ce)
        default:                return otherDevice(ce: ce, dd: dd)
        }
    }
    private func securityHubDevice(ce: XMainCellDeviceEntityBuilder, dd: DDeviceEntity) -> XObjectEntity {
        var ce = ce.status(dd.armStatus)
        if Roles.sendCommands { ce = ce.buttonCommands(arm: arm, disarm: disarm) }
        ce = ce.buttonRele(showReles).buttonListHozorgan(showHozorgan)
        if DataManager.settingsHelper.needSections { ce = ce.buttonSection(showSections) } else { ce = ce.buttonZone(showZones) }
        ce = ce.menuAbout(about).menuScrips(scripts)
        if Roles.sendCommands && Roles.manageSectionZones { ce = ce.menu(ussd: ussd, delete: delete, reboot: reboot, update: update) }
        return ce.build(affectClick: showEvent)
    }
    private func securityButtonDevice(ce: XMainCellDeviceEntityBuilder) -> XObjectEntity {
        return ce.build(affectClick: showEvent)
    }
    private func otherDevice(ce: XMainCellDeviceEntityBuilder, dd: DDeviceEntity) -> XObjectEntity {
        var ce = ce.status(dd.armStatus)
        if Roles.sendCommands { ce = ce.buttonCommands(arm: arm, disarm: disarm) }
        ce = ce.buttonRele(showReles).buttonListHozorgan(showHozorgan)
        if DataManager.settingsHelper.needSections { ce = ce.buttonSection(showSections) } else { ce = ce.buttonZone(showZones) }
        ce = ce.menuAbout(about)
        if Roles.sendCommands && Roles.manageSectionZones { ce = ce.menu(ussd: ussd, delete: delete, reboot: nil, update: nil) }
        return ce.build(affectClick: showEvent)
    }

    func toXObjectEntity(_ ds: DSectionEntity) -> XObjectEntity {
        guard let section = ds.section else { return XMainCellBaseEntityBuilder.create(id: ds.id) }
        let dTI = HubConst.getDeviceTypeInfo(ds.configVersion, cluster: ds.cluster_id)
        var ce = XMainCellSectionEntityBuilder.create(section: section).updateType(ds.type)
        if objectType == .site || objectType == .device { ce = ce.siteName(ds.siteNames).deviceName(ds.deviceName) }

        if section.section == 0 { return controller(ce: ce, ds: ds, dTI: dTI) }
        if section.detector == HubConst.SECTION_TYPE_SECURITY { return securitySection(ce: ce, ds: ds, dTI: dTI) }
        return otherSection(ce: ce, ds: ds, dTI: dTI)
    }
    private func controller(ce: XMainCellSectionEntityBuilder, ds: DSectionEntity, dTI: DDeviceTypeInfo) -> XObjectEntity {
        var ce = ce
        ce = ce.isDevice(affects: ds.affects, image: dTI.icon)
        if dTI.type != .SecurityButton { ce = ce.zones(name: R.strings.components, showZones: showZones) }
        if (dTI.type == .SecurityHub || dTI.type == .Astra) && Roles.sendCommands { ce = ce.menuUssd(ussd) }
        if dTI.type == .SecurityHub && Roles.manageSectionZones { ce = ce.menuRenameСontroller(rename) }
        return ce.build(affectClick: showEvent)
    }
    private func securitySection(ce: XMainCellSectionEntityBuilder, ds: DSectionEntity, dTI: DDeviceTypeInfo) -> XObjectEntity {
        var ce = ce, section = ds.section!
        ce = ce.isSecurity(affects: ds.affects, status: ds.armStatus, index: section.section)
        if Roles.sendCommands { ce = ce.buttonCommands(arm: arm, disarm: disarm) }
        ce = ce.zones(name: R.strings.zones, showZones: showZones)
        if Roles.manageSectionZones && dTI.type == .SecurityHub {
            ce = ce.menuRename(rename)
            if Roles.sendCommands { ce = ce.menuUpdateAndDelete(update: update, delete: delete) }
        }
        return ce.build(affectClick: showEvent)
    }
    private func otherSection(ce: XMainCellSectionEntityBuilder, ds: DSectionEntity, dTI: DDeviceTypeInfo) -> XObjectEntity {
        var ce = ce, section = ds.section!
        let sec_type = HubConst.getSectionsTypes().first(where: { $0.key == section.detector}) ?? HubConst.getSectionsTypes().first!
        ce = ce.isNotSecutiry(affects: ds.affects, name: sec_type.value.name, image: sec_type.value.icon, index: section.section)
            .zones(name: R.strings.zones, showZones: showZones)
        if Roles.manageSectionZones && dTI.type == .SecurityHub {
            ce = ce.menuRename(rename)
            if Roles.sendCommands { ce = ce.menuUpdateAndDelete(update: update, delete: delete) }
        }
        return ce.build(affectClick: showEvent)
    }
    
    func toXObjectEntity(_ dz: DZoneEntity, isRelay: Bool) -> XObjectEntity {
        guard let zone = dz.zone else { return XMainCellBaseEntityBuilder.create(id: dz.id) }
        if isRelay && zone.detector == 13 { return relayWithScript(dz, zone) }
        else if isRelay { return _relay(dz, zone) }
        return _zone(dz, zone)
    }
    private func _zone(_ dz: DZoneEntity, _ zone: Zones) -> XObjectEntity {
        let dTI = HubConst.getDeviceTypeInfo(dz.configVersion, cluster: dz.cluster)
        
        var detector: String = HubConst.getDetectors(zone.detector)
        if (zone.detector > 255) {
            if let dType = R.zoneTypes.flatMap({ (z) -> [DDetectorTypeEntity] in return z.detectorTypes }).first(where: { (d) -> Bool in return d.id == (zone.detector & 255) }) {
                detector = dType.title
                if let aType = dType.alarmTypes.first(where: { (a) -> Bool in return a.id == zone.detector >> 8 }) {
                    detector = dType.title + "\n" + aType.title
                }
            }
        }
        
        var ce = XMainCellZoneEntityBuilder.create(zone: zone)
            .updateType(dz.type)
            .setStatus(affects: dz.affects, status: dz.armStatus, type: detector, index: zone.zone, delay: zone.delay)
        if objectType != .zone { ce = ce.siteName(dz.siteNames).deviceName(dz.deviceName).sectionName(dz.sectionName) }

        if (dz.sectionDetector == HubConst.SECTION_TYPE_FIRE ||
            dz.sectionDetector == HubConst.SECTION_TYPE_FIRE_DOUBLE) && Roles.sendCommands {
            ce = ce.buttonFireCommands(fire_reset)
        }
        if Roles.manageSectionZones && dTI.type == .SecurityHub { ce = ce.menuRename(rename) }
        if Roles.sendCommands && dz.sectionDetector == HubConst.SECTION_TYPE_SECURITY && dTI.type == .SecurityHub { ce = ce.menuDelay(zone.delay, delay) }
        if Roles.sendCommands && Roles.manageSectionZones && zone.zone != 100 && dTI.type == .SecurityHub { ce = ce.menuDelete(delete) }
        return ce.build(affectClick: showEvent)
    }
    
    private func _relay(_ dr: DZoneEntity, _ zone: Zones) -> XObjectEntity {
        let dTI = HubConst.getDeviceTypeInfo(dr.configVersion, cluster: dr.cluster)
        
        var detector: String = HubConst.getDetectors(zone.detector)
        if (zone.detector > 255) {
            if let dType = R.zoneTypes.flatMap({ (z) -> [DDetectorTypeEntity] in return z.detectorTypes }).first(where: { (d) -> Bool in return d.id == (zone.detector & 255) }) {
                detector = dType.title
                if let aType = dType.alarmTypes.first(where: { (a) -> Bool in return a.id == zone.detector >> 8 }) {
                    detector = detector + "\n" + aType.title
                }
            }
        }
        
        var ce = XMainCellRelayEntityBuilder.create(zone: zone)
            .updateType(dr.type)
            .setAffects(affects: dr.affects, type: detector, index: zone.zone)
        if zone.section != 0 { ce = ce.siteName(dr.siteNames).deviceName(dr.deviceName).sectionName(dr.sectionName) }
        else if objectType != .relay { ce = ce.siteName(dr.siteNames).deviceName(dr.deviceName) }

        if Roles.sendCommands && dr.zoneDetector == 23 { ce = ce.setStatus(affects: dr.affects, status: dr.armStatus).buttonCommands(on: on, off: off) }
        if HubConst.isExit(dr.zoneDetector) { ce = ce.isExit(affects: dr.affects, status: dr.armStatus) }
        else { ce = ce.setStatus(affects: dr.affects, status: dr.armStatus)  }
        if Roles.manageSectionZones && dTI.type == .SecurityHub {
            ce = ce.menuRename(rename)
            if Roles.sendCommands { ce = ce.menuDelete(delete) }
        }
        return ce.build(affectClick: showEvent)
    }
    
    private func relayWithScript(_ dr: DZoneEntity, _ zone: Zones) -> XObjectEntity {
        let dTI = HubConst.getDeviceTypeInfo(dr.configVersion, cluster: dr.cluster)
        let detector: String = HubConst.getDetectors(zone.detector)
        var ce = XMainCellRelayEntityBuilder.create(zone: zone)
            .updateType(dr.type)
            .setAffects(affects: dr.affects, type: detector, index: zone.zone)
        if zone.section != 0 { ce = ce.siteName(dr.siteNames).deviceName(dr.deviceName).sectionName(dr.sectionName) }
        else if objectType != .relay { ce = ce.siteName(dr.siteNames).deviceName(dr.deviceName) }
        
        if Roles.manageSectionZones && dTI.type == .SecurityHub {
            ce = ce.menuRename(rename)
            if dr.script == nil {
                ce = ce.menuScriptAdd(self.script_add)
            } else {
                ce = ce.menuScript(self.script)
            }
            if Roles.sendCommands { ce = ce.menuDelete(delete) }
        }
        //TODO: Колхоз
        
        let buttons = DataManager.shared.getScriptCommands(device_id: zone.device, zone_id: zone.zone)
        ce = ce.isAutoRelay(affects: dr.affects, buttons: buttons, _switch: _switch)
        
        return ce.build(affectClick: showEvent)
    }
}

