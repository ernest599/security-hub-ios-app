//
//  XMainController+Utils.swift
//  SecurityHub
//
//  Created by Timerlan on 04/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation

extension XMainController {
    func getSpinItems() -> [XSpinerItem] {
        var items: [XSpinerItem] = []
        let needDevices  = DataManager.settingsHelper.needDevices
        let needSections = DataManager.settingsHelper.needSections
        switch objectType {
        case .site:
            items.append(XSpinerItem(image: nil, title: name ?? R.strings.sites, subTitle: R.strings.sites))
            if needDevices {
                items.append(XSpinerItem(image: nil, title: R.strings.devices, subTitle: R.strings.devices))
            }
            if needSections{
                items.append(XSpinerItem(image: nil, title: R.strings.sections, subTitle: R.strings.sections))
            }
            items.append(XSpinerItem(image: nil, title: R.strings.zones, subTitle: R.strings.zones))
            items.append(XSpinerItem(image: nil, title: R.strings.rele, subTitle: R.strings.rele))
        case .device:
            items.append(XSpinerItem(image: nil, title: name ?? R.strings.devices, subTitle: R.strings.devices))
            if needSections {
                items.append(XSpinerItem(image: nil, title: R.strings.sections, subTitle: R.strings.sections))
            }
            items.append(XSpinerItem(image: nil, title: R.strings.zones, subTitle: R.strings.zones))
            items.append(XSpinerItem(image: nil, title: R.strings.rele, subTitle: R.strings.rele))
        case .section:
            items.append(XSpinerItem(image: nil, title: name ?? R.strings.sections, subTitle: R.strings.sections))
            items.append(XSpinerItem(image: nil, title: R.strings.zones, subTitle: R.strings.zones))
            items.append(XSpinerItem(image: nil, title: R.strings.rele, subTitle: R.strings.rele))
        case .zone:
            items.append(XSpinerItem(image: nil, title: name ?? R.strings.zones, subTitle: R.strings.zones))
        case .relay:
            items.append(XSpinerItem(image: nil, title: name ?? R.strings.rele, subTitle: R.strings.rele))
        }
        return items
    }
    func needRightButton() -> Bool {
        return (objectType == .site && Roles.manageSites) || (objectType == .device && Roles.manageDevices) || ((objectType == .section || objectType == .zone || objectType == .relay) && Roles.sendCommands && Roles.manageSectionZones)
    }
    func siteInfo(site: Sites, dSI: DSiteInfo) -> String {
        var text =  "\n\(R.strings.site_name): \(site.name)" +
                    "\n\(R.strings.domain) № \(site.domain)"
        if site.delegated == 1 { text += "\n\(R.strings.site_is_delegeted)" }
        text += "\n\(R.strings.operator_count): \(dSI.operatorCount)"
        if dSI.deviceInfos.count == 0 { text += "\n\n\(R.strings.site_no_devices)"; return text }
        text += "\n\n\(R.strings.site_devices): "
        for dDI in dSI.deviceInfos { text += deviceInfo(dDI: dDI) }
        text += "\n"
        return text
    }
    private func deviceInfo(dDI: DDeviceInfo) -> String {
        var text = "\n\(dDI.name)" +
                    "\n   \(R.strings.device_section_count): \(dDI.sectionCount)" +
                    "\n   \(R.strings.device_zone_count): \(dDI.zoneCount)" +
                    "\n   \(R.strings.device_connect_state): \(dDI.isConnected ? R.strings.device_is_connected : R.strings.device_is_not_connected)"
        if let time = dDI.lastEventTime {
            text += "\n   \(R.strings.device_last_event_time)" +
                    "\n   \(time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short))"
        }
        return text
    }
    func deviceInfo(device: Devices) -> String {
        var text =  "\n\(device.name)"
        if HubConst.isHub(device.configVersion, cluster: device.cluster_id), let version = HubConst.getHubVersion(device.configVersion) {
            text += "\n\(R.strings.device_version): \(version)"
        }
        text += "\n"
        return text
    }
}
