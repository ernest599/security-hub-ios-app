//
//  XMainCell.swift
//  SecurityHub
//
//  Created by Timerlan on 14.03.2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import SwipeCellKit

let XObjectCellId = "XMainCellId"

class XObjectCell: SwipeTableViewCell {
    private var openView = ObjectOpenView()
    private var closeView = ObjectCloseView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    open func setContent(_ obj: XObjectEntity){
        selectionStyle = .none
        closeView.set(title: obj.title, info: obj.info, status: obj.status, time: obj.time)
        closeView.setImage(obj.image, mainColor: obj.imageColor, imgSmall: obj.smallImage)
        closeView.setAffects(obj.affectsMini)
        closeView.setLocation(object: obj.objectName, device: obj.deviceName, section: obj.sectionName)
        
        openView.set(title: obj.title, info: obj.info, status: obj.status, time: obj.time)
        openView.setImage(obj.image, mainColor: obj.imageColor, imgSmall: obj.smallImage)
        openView.setAffects(obj.affects, affectClick: obj.affectClick)
        openView.setButtons(obj.buttons)
        openView.setLocation(object: obj.objectName, device: obj.deviceName, section: obj.sectionName)
    }

    func setOpen(_ open: Bool) {
        contentView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        if open {
            contentView.addSubview(openView)
            openView.snp.remakeConstraints{ (make) in make.edges.equalToSuperview() }
        }else{
            contentView.addSubview(closeView)
            closeView.snp.remakeConstraints{ (make) in make.edges.equalToSuperview() }
        }
    }
}
