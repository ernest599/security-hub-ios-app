import UIKit
import RxSwift

class XSettingsController: XBaseController<XSettingsView> {
    
    var settingsItems: [SettingsItem] = []
    var isConnectingIvideon = false
    private let dm = DataManager.shared!
    
    var eventDisp: Disposable!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        addTableData()
        
        xView.tableView.delegate = self
        xView.tableView.dataSource = self
        xView.tableView.register(
            SettingsCell.self,
            forCellReuseIdentifier: SettingsCell.cellId
        )
        
        xView.setNV(navigationController)
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        xView.profileUsername.text = DataManager.shared.getOperator().name
    }
    
    override func viewDidAppear(_ animated: Bool) {
        eventDisp = dm.events
            .asObservable()
            .subscribe(onNext: { events in
                DispatchQueue.main.async { [self] in
                    if events.count > 0 {
                        for event in events {
                            if event._class == 0 {
                                xView.changeBottomNavStatus(type: 0)

                                break
                            } else if event._class >= 1 && event._class <= 3 {
                                xView.changeBottomNavStatus(type: 1)
                            }
                        }
                    } else {
                        xView.changeBottomNavStatus(type: 2)
                    }
                }
            })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        eventDisp.dispose()
    }
    
    func addTableData() {
        settingsItems = [
            SettingsItem(
                title: R.strings.options_security,
                image: "itemSecurity",
                sectionName: R.strings.app,
                onClick: {
                    self.navigationController!.pushViewController(
                        SettingsController(R.strings.options_security),
                        animated: true
                    )
                }
            ),

            SettingsItem(
                title: R.strings.options_notification,
                image: "itemNotifications",
                sectionName: nil,
                onClick: {
                    self.navigationController!.pushViewController(
                        NotificationsController(),
                        animated: true
                    )
                }
            ),
        
            SettingsItem(
                title: R.strings.options_connection,
                image: "itemConnection",
                sectionName: nil,
                onClick: {
                    self.xView.textInAlert = R.strings.options_connection
                    self.xView.alertType = 2
                    
                    self.xView.showAlert()
                }
            ),
        
            SettingsItem(
                title: R.strings.options_sharing,
                image: "itemSharing",
                sectionName: nil,
                onClick: {
                    self.xView.textInAlert = R.strings.options_sharing
                    self.xView.alertType = 2
                    
                    self.xView.showAlert()
                }
            ),
        
            SettingsItem(
                title: R.strings.options_language,
                image: "itemLanguage",
                sectionName: nil,
                onClick: {
                    self.navigationController!.pushViewController(
                        LanguageController(),
                        animated: true
                    )
                }
            ),
        
            SettingsItem(
                title: R.strings.options_apn,
                image: "itemAPN",
                sectionName: R.strings.devices,
                onClick: {
                    self.xView.textInAlert = R.strings.options_apn
                    self.xView.alertType = 2
                    
                    self.xView.showAlert()
                }
            ),
        
            SettingsItem(
                title: R.strings.options_ivideon,
                image: "itemIvideon",
                sectionName: R.strings.options_accounts,
                onClick: {
                    if DataManager.settingsHelper.ivideonToken.isEmpty {
                        self.navigationController!.pushViewController(
                            IvideonAuthController(),
                            animated: true
                        )

                        self.isConnectingIvideon = true
                    } else {
                        self.xView.textInAlert = R.strings.camera_exit_message
                        self.xView.alertType = 4
                        
                        self.xView.showAlert()
                    }
                }
            ),
            
            SettingsItem(
                title: R.strings.options_chop,
                image: "itemChop",
                sectionName: R.strings.options_information,
                onClick: {
                    self.navigationController!.pushViewController(
                        ChopController(),
                        animated: true
                    )
                }
            ),
            
            SettingsItem(
                title: R.strings.options_shop,
                image: "itemShop",
                sectionName: nil,
                onClick: {
                    self.xView.textInAlert = R.strings.visitShop
                    self.xView.alertType = 3
                    
                    self.xView.showAlert()
                }
            ),
            
            SettingsItem(
                title: R.strings.options_faq,
                image: "itemFAQ",
                sectionName: nil,
                onClick: {
                    self.navigationController!.pushViewController(
                        WikiController(),
                        animated: true
                    )
                }
            ),
            
            SettingsItem(
                title: R.strings.options_about_app,
                image: "itemAbout",
                sectionName: nil,
                onClick: {
                    self.navigationController!.pushViewController(
                        AboutController(),
                        animated: true
                    )
                }
            )
        ]
        
        let roles = DataManager.shared.getUser().roles

        if roles & Roles.DOMEN_INGEN == 0 {
            settingsItems = settingsItems.filter { $0.title != R.strings.options_apn }
        }
        
        if roles & Roles.GUARD_KEYCHAIN != 0 {
            settingsItems = settingsItems.filter { $0.title != R.strings.options_ivideon }
        }
    }
}

extension XSettingsController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingsCell = (tableView.dequeueReusableCell(withIdentifier: SettingsCell.cellId, for: indexPath) as? SettingsCell)!
        let item = settingsItems[indexPath.row]
        
        cell.cellIdx = indexPath.row
        cell.cellSection.text =
            item.sectionName != nil
            ? item.sectionName
            : ""
        cell.cellTitle.text = item.title
        cell.cellImage.image = UIImage(named: item.image)
        cell.cellOnClick = item.onClick
        cell.updateConstraints()
        
        return cell
    }
}

class SettingsCell: UITableViewCell {
    
    static let cellId = "SettingsCell"
    var cellIdx = -1
    var cellOnClick: (() -> Void)!
    
    lazy var cellSection: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var cellImage: UIImageView = {
        return UIImageView()
    }()
    
    lazy var cellTitle: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 3
        
        return view
    }()
    
    lazy var cellFrame: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1)
        view.alpha = 0.02
        view.isUserInteractionEnabled = true
        
        let pressGesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(sectionPressed)
        )
        pressGesture.minimumPressDuration = 0.2
        view.addGestureRecognizer(pressGesture)
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(sectionTapped)
            )
        )
        
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        contentView.addSubview(cellFrame)
        contentView.addSubview(cellSection)
        contentView.addSubview(cellImage)
        contentView.addSubview(cellTitle)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    override func updateConstraints() {
        if !cellSection.text!.isEmpty {
            cellSection.snp.remakeConstraints { make in
                make.top.equalToSuperview().offset(20)
                make.leading.equalToSuperview().offset(24)
            }
            
            cellImage.snp.remakeConstraints { make in
                make.leading.equalToSuperview().offset(24)
                make.width.equalTo(36)
                make.top.equalTo(cellSection.snp.bottom).offset(20)
                make.bottom.equalToSuperview().offset(-10)
            }
        } else {
            cellImage.snp.remakeConstraints { make in
                make.leading.equalToSuperview().offset(24)
                make.width.equalTo(36)
                make.top.equalToSuperview().offset(20)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        cellTitle.snp.remakeConstraints { make in
            make.leading.equalTo(cellImage.snp.trailing).offset(17)
            make.trailing.equalToSuperview().offset(-24)
            make.centerY.equalTo(cellImage.snp.centerY)
        }
        
        cellFrame.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(cellImage.snp.top).offset(-10)
            make.bottom.equalTo(cellImage.snp.bottom).offset(10)
        }
        
        super.updateConstraints()
    }
    
    @objc func sectionPressed(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                gesture.view!.alpha = 1
            }
        } else if gesture.state == .changed {
            if gesture.location(in: gesture.view).y > gesture.view!.bounds.maxY || gesture.location(in: gesture.view).y < gesture.view!.bounds.minY {
                UIView.animate(withDuration: 0.3) {
                    gesture.view!.alpha = 0.02
                }
            }
        } else if gesture.state == .ended {
            if gesture.view!.alpha == 1 {
                UIView.animate(withDuration: 0.3) {
                    gesture.view!.alpha = 0.02
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.cellOnClick()
                }
            }
        }
    }
    
    @objc func sectionTapped(gesture: UITapGestureRecognizer) {
        gesture.view!.alpha = 1
        UIView.animate(withDuration: 0.3) {
            gesture.view!.alpha = 0.02
        }
                
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.cellOnClick()
        }
    }
}
