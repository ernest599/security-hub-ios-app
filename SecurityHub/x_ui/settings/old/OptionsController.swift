//
//  OptionsController.swift
//  SecurityHub
//
//  Created by Timerlan on 21.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import RxSwift
import Localize_Swift

class OptionsController: XBaseController<EmptyListView>, UITableViewDataSource, UITableViewDelegate {
    private var options: [Option] = []
    private var titleOp: String
    
    init(_ title: String = R.strings.title_options){
        self.titleOp = title
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    private var observer: Any?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        observer = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: OptionsCell.cellId), object: nil, queue: OperationQueue.main){ notification in
            let tit: String = notification.object as! String
            self.doit(tit: tit)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let o = observer {
            NotificationCenter.default.removeObserver(o)
            observer = nil
        }
    }
    
    override func loadView() {
        super.loadView()
        title = titleOp
        switch titleOp {
        case R.strings.title_options:          load()
        case R.strings.options_security:       security()
        case R.strings.options_events:         events()
        case R.strings.options_notification_classes: push_class()
        case R.strings.options_history:        history()
        case R.strings.options_language:       language();
        case R.strings.options_interface:      visual_interface();
        default: break;
        }
        if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == titleOp }) {
            self._class(id: _class.id, name: _class.name)
        }
        
        xView.table.register(OptionsCell.self, forCellReuseIdentifier: OptionsCell.cellId)
        xView.table.backgroundView = nil
        xView.table.dataSource = self
        xView.table.delegate = self
    }
    
    func doit(tit: String){
        if self.titleOp == R.strings.options_security && tit == R.strings.options_secutiry_need_pin {
            self.xView.table.reloadData()
            guard let nV = navigationController else { return }
            if !self.options[0].enable! {
                let setC = PinController(type: .newPin, code: nil, cancelTitle: R.strings.button_back,
                                        cancel: { nV.popViewController(animated: true) },
                                        complete: {
                                            DataManager.settingsHelper.needPin = !self.options[0].enable!;
                                            self.security()
                                            self.xView.table.reloadData()
                                            guard let vc = nV.viewControllers.last(where: { $0 is OptionsController }) else { return }
                                            nV.popToViewController(vc, animated: true)
                                        })
                nV.pushViewController(setC, animated: false)
            } else {
                let checkC = PinController(type: .inputPin, code: nil, cancelTitle: R.strings.button_back,
                                           cancel: { nV.popViewController(animated: true) },
                                           complete: {
                                                DataManager.settingsHelper.needPin = !self.options[0].enable!;
                                                self.security()
                                                self.xView.table.reloadData()
                                                nV.popViewController(animated: true)
                                           })
                nV.pushViewController(checkC, animated: false)
            }
        }else if self.titleOp == R.strings.options_security && tit == R.strings.options_secutiry_need_touch_id {
            self.options[2].enable = !self.options[2].enable!
            self.options[2].subtitle = self.options[2].enable! ? R.strings.options_secutiry_need_touch_id_on : R.strings.options_secutiry_need_touch_id_off
            self.xView.table.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
            DataManager.settingsHelper.touchId = self.options[2].enable!
        }else if self.titleOp == R.strings.options_events && tit == R.strings.options_notification_need {
            self.options[1].enable = !self.options[1].enable!
            DataManager.settingsHelper.needPush = self.options[1].enable!
            if DataManager.settingsHelper.needPush {
                DataManager.shared.setFCM()
            } else {
                DataManager.shared.deleteFCM()
            }
            events()
            self.xView.table.reloadData()
        }else if self.titleOp == R.strings.options_interface && tit == R.strings.options_interface_devices_need {
            options[0].enable! = !options[0].enable!
            DataManager.settingsHelper.needDevices = options[0].enable!
            NotificationCenter.default.post(name: .objectListStructUpdate, object: nil)
            self.visual_interface()
            self.xView.table.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }else if self.titleOp == R.strings.options_interface && tit == R.strings.options_interface_sections_need {
            options[1].enable = !options[1].enable!
            DataManager.settingsHelper.needSections = options[1].enable!
            NotificationCenter.default.post(name: .objectListStructUpdate, object: nil)
            self.visual_interface()
            self.xView.table.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
        }else if    let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == tit }),
                    self.titleOp == R.strings.options_history {
            if (DataManager.settingsHelper.storyMask & (1 << _class.id)) == 0 {
                DataManager.settingsHelper.storyMask = DataManager.settingsHelper.storyMask | (1 << _class.id)
            }else{
                DataManager.settingsHelper.storyMask = DataManager.settingsHelper.storyMask & ~(1 << _class.id)
            }
            NotificationCenter.default.post(name: .eventHistoryUpdate, object: nil)
        }else if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == titleOp }) {
            if (DataManager.settingsHelper.pushMask & (1 << _class.id)) == 0 {
                DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask | (1 << _class.id)
            }else{
                DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask & ~(1 << _class.id)
            }
            self._class(id: _class.id, name: _class.name)
            xView.table.reloadData()
            if DataManager.settingsHelper.needPush { DataManager.shared.setFCM() } else { DataManager.shared.deleteFCM() }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        var mes = ""
        for obj in DataManager.shared.getClasses() { if (DataManager.settingsHelper.storyMask & (1 << obj.id)) != 0 { mes += obj.name + ", " } }
        self.options.first(where: { $0.title == R.strings.options_history })?.subtitle = "\(R.strings.options_history_desc)\(mes)"
        self.xView.table.reloadData()
    }
    
    func load() {
        options.append( Option(title: DataManager.settingsHelper.needExpert ? R.strings.options_expert_mode_on : R.strings.options_expert_mode_off, subtitle: R.strings.options_expert_mode_desc) )
        
        options.append( Option(title: R.strings.options_header_user, isT: true) )
        options.append( Option(title: R.strings.options_current_operator, subtitle: R.strings.options_current_operator_desc) )
        if Roles.manageOperators {
            options.append( Option(title: R.strings.options_operators, subtitle: R.strings.options_operators_desc))
        }
        options.append( Option(title: R.strings.options_security, subtitle: R.strings.options_security_desc))
        options.append( Option(title: R.strings.options_events, subtitle: R.strings.options_events_desc))
        if DataManager.settingsHelper.needExpert { options.append( Option(title: R.strings.options_interface, subtitle: R.strings.options_interface_desc)) }
        options.append( Option(title: R.strings.options_exit, subtitle: R.strings.options_exit_desc) )
        
        if Roles.manageSites {
            options.append(Option(title: R.strings.options_header_site, isT: true))
            options.append(Option(title: R.strings.options_site_add, subtitle: R.strings.options_site_add_desc))
            options.append(Option(title: R.strings.options_site_delegate, subtitle: R.strings.options_site_delegate_desc))
        }
        
        options.append(Option(title: R.strings.options_information, isT: true))
        options.append(Option(title: R.strings.options_language, subtitle: "" ))
        options.append( Option(title: R.strings.options_faq, subtitle: ""))
        if XTargetUtils.isHubTarget && DataManager.defaultHelper.language == "ru" {
            options.append( Option(title: R.strings.options_chop, subtitle: R.strings.options_chop_desc ))
        }
        
        xView.table.reloadData()
    }
    
    func security() {
        options = []
        options.append(Option(title: R.strings.options_secutiry_need_pin, subtitle:
            (DataManager.settingsHelper.needPin ? R.strings.options_secutiry_need_pin_on : R.strings.options_secutiry_need_pin_off), enable: DataManager.settingsHelper.needPin))
        if DataManager.settingsHelper.needPin {
            options.append(Option(title: R.strings.options_secutiry_set_pin, subtitle: R.strings.options_secutiry_set_pin_desc))
            if SimpleTouch.isTouchIDEnabled == .success && UIScreen.main.nativeBounds.height != 2436 {
                options.append(Option(title: R.strings.options_secutiry_need_touch_id, subtitle: (DataManager.settingsHelper.touchId ? R.strings.options_secutiry_need_touch_id_on : R.strings.options_secutiry_need_touch_id_off), enable: DataManager.settingsHelper.touchId, isT: false))
            }
        }
        xView.table.reloadData()
    }
    
    func events() {
        options = []
        options.append(Option(title: R.strings.options_notification_title, isT: true))
        options.append(Option(title: R.strings.options_notification_need,
                              subtitle: (DataManager.settingsHelper.needPush ? R.strings.options_notification_need_on : R.strings.options_notification_need_off), enable: DataManager.settingsHelper.needPush))
        if DataManager.settingsHelper.needPush {
            options.append(Option(title: R.strings.options_notification_classes, subtitle: R.strings.options_notification_classes_desc))
        }
        if DataManager.settingsHelper.needExpert {
            options.append(Option(title: R.strings.options_history_title, isT: true))
            options.append(Option(title: R.strings.options_history, subtitle: R.strings.options_history_desc))
        }
        xView.table.reloadData()
    }
    
    func visual_interface() {
        options = []
        options.append(Option(title: R.strings.options_interface_devices_need, subtitle:
            (DataManager.settingsHelper.needDevices ? R.strings.options_interface_devices_need_on : R.strings.options_interface_devices_need_off), enable: DataManager.settingsHelper.needDevices))
        options.append(Option(title: R.strings.options_interface_sections_need, subtitle:
            (DataManager.settingsHelper.needSections ? R.strings.options_interface_sections_need_on : R.strings.options_interface_sections_need_off), enable: DataManager.settingsHelper.needSections))
    }
    
    func push_class(){
        for obj in DataManager.shared.getClasses().filter({ (id, name) -> Bool in return id < 5 }) {
            self.options.append(Option(title: obj.name, subtitle: R.strings.options_notification_class_desc))
        }
        self.xView.table.reloadData()
    }
    
    func history(){
        for obj in DataManager.shared.getClasses() {
            self.options.append( Option(title: obj.name, subtitle: "", enable: (DataManager.settingsHelper.storyMask & (1 << obj.id)) != 0 , isT: false ) )
        }
        self.xView.table.reloadData()
    }
    
    func language(){
        self.options.append( Option(title: R.strings.options_language_app, subtitle: DataManager.defaultHelper.language.localized()) )
        self.xView.table.reloadData()
    }
    
    func lan_app() {
        guard let nV = navigationController else { return }

        let alert = AlertUtil.myXAlert(R.strings.options_language_app,
                                        messages: [
                                            R.strings.options_language_ru,
                                            R.strings.options_language_de,
                                            R.strings.options_language_en,
                                            R.strings.options_language_es
            ],
                                        voids: [{self.setLan_app("ru")}, {self.setLan_app("de")}, {self.setLan_app("en")}, {self.setLan_app("es")} ])
        nV.present(alert, animated: false)
    }
    
    private func setLan_app(_ lan: String) {
        showLoader()
        Localize.setCurrentLanguage(lan)
        DataManager.defaultHelper.language = lan
        R.reinit()
        let _ = DataManager.shared.setLocale(lan)
            .subscribe(onNext: { r in
                self.hideLoader()
                DataManager.shared.clearAfterLang()
                self.navigationController?.present(SplashController(), animated: false)
            }, onError: { e in
                self.hideLoader()
                DataManager.shared.clearAfterLang()
                self.navigationController?.present(SplashController(), animated: false)
            })
    }
    
    func exit() {
        self.navigationController?.present(AlertUtil.exitAlert({
            self.showLoader()
            DataManager.disconnectDisposable = DataManager.shared.disconnect().subscribe(onNext: { a in
                DataManager.disconnectDisposable?.dispose()
                self.hideLoader()
                NavigationHelper.shared.show(LoginController())
            })
        }), animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return options.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OptionsCell = tableView.dequeueReusableCell(withIdentifier: OptionsCell.cellId, for: indexPath) as! OptionsCell
        cell.setContent(options[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        guard let nV = navigationController else { return }

        let roles = DataManager.shared.getUser().roles
        let _title = options[indexPath.row].title
        switch _title {
        case R.strings.options_current_operator:
            let operators = DataManager.shared.getOperator()
            let mes = "\(R.strings.operator_name): \(operators.name)\n\n" +
                "\(R.strings.operator_login): \(operators.login)\n\n" +
                R.strings.operator_cell_permission + (
                    operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin :
                        operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view ) + "\n\n" +
                R.strings.operator_sites_count + String(DataManager.shared.getSitesCount()) + "\n";
            if roles & Roles.ORG_ADMIN != 0 || roles & Roles.DOMEN_ADMIN != 0 {
                AlertUtil.infoAlertUser(mes, name: operators.name, listener: { (name) in
                    _ = DataManager.shared.changeOperator(id: operators.id, value: name ?? "", name: "name").subscribe()
                }) { (pas) in
                    _ = DataManager.shared.changeOperator(id: operators.id, value: pas?.md5 ?? "", name: "password").subscribe()
                    UserDefaults.standard.set(nil, forKey: "saveMeViewPass")
                    UserDefaults.standard.set(nil, forKey: "saveMeViewLogin")
                }
            }else{ AlertUtil.infoAlert(mes) }
        case R.strings.options_expert_mode_on:      nV.pushViewController(ExpertController(), animated: true)
        case R.strings.options_expert_mode_off:     nV.pushViewController(ExpertController(), animated: true)

        case R.strings.options_operators:           nV.pushViewController(OperatorsController(), animated: false)
        case R.strings.options_security:            nV.pushViewController(OptionsController(_title), animated: false)
        case R.strings.options_secutiry_set_pin:
            let setC = PinController(type: .newPin, code: nil, cancelTitle: R.strings.button_back,
                                     cancel: { nV.popViewController(animated: true) },
                                     complete: {
                                        guard let vc = nV.viewControllers.last(where: { $0 is OptionsController }) else { return }
                                        nV.popToViewController(vc, animated: true)
                                     })
            let checkC = PinController(type: .inputPin, code: nil, cancelTitle: R.strings.button_back,
                                       cancel: { nV.popViewController(animated: true) },
                                       complete: { nV.pushViewController(setC, animated: false) })
            nV.pushViewController(checkC, animated: false)
        case R.strings.options_events:                nV.pushViewController(OptionsController(_title), animated: false)
        case R.strings.options_notification_classes:          nV.pushViewController(OptionsController(_title), animated: false)
        case R.strings.options_history:             nV.pushViewController(OptionsController(_title), animated: false)

        case R.strings.options_interface:    nV.pushViewController(OptionsController(_title), animated: false)
        case R.strings.options_site_add:             nV.pushViewController(SiteAddController(site_id: nil), animated: false)
        case R.strings.options_site_delegate:      AlertUtil.delegateAlert()
        case R.strings.options_language:            nV.pushViewController(OptionsController(_title), animated: false)
        case R.strings.options_language_app: lan_app()
        case R.strings.button_exit: exit()
        case R.strings.options_chop:
//            let string = "https://cloud.security-hub.ru/wiki/doku.php?id=%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%BE%D1%80%D0%B3%D0%B0%D0%BD%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B9"
//            if let url = URL(string: string) {
//                nV.pushViewController(WikiController(url: url), animated: false)
//            }
            nV.pushViewController(ChopController(), animated: false)
        case R.strings.options_faq:
            nV.pushViewController(WikiController(), animated: false)
//            let string = "https://cloud.security-hub.ru/wiki/doku.php?id=%D1%87%D0%B0%D1%81%D1%82%D0%BE_%D0%B7%D0%B0%D0%B4%D0%B0%D0%B2%D0%B0%D0%B5%D0%BC%D1%8B%D0%B5_%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%8B_%D0%BF%D0%BE_%D0%BC%D0%BF"
//            if let url = URL(string: string) {
//                nV.pushViewController(WikiController(url: url), animated: false)
//            }
        default: break
        }
        if  titleOp == R.strings.options_notification_classes,
            let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == options[indexPath.row].title }) {
            nV.pushViewController(OptionsController(_class.name), animated: false)
        } else if options[indexPath.row].title == R.strings.options_notification_class_sound,
            let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == titleOp }) {
            let key =   _class.id == 0 ? "alarm" :
                        _class.id == 1 ? "sabotage" :
                        _class.id == 2 ? "malfunction" :
                        _class.id == 3 ? "attention" : "arm"
            let path = (DataManager.shared.sounds[key] as? String) ?? "default"
            nV.pushViewController(SelectSoundController(path: path, void: { name in
                _ = DataManager.shared.setSound(class_id: _class.id, name: name ?? "default").subscribe()
            }), animated: false)
        }
    }
    
    func _class(id: Int64, name: String) {
        options = []
        let enable = (DataManager.settingsHelper.pushMask & (1 << id)) != 0
        options.append(Option(title: R.strings.options_notification_class_need, subtitle:
            (enable ? R.strings.options_notification_class_need_on : R.strings.options_notification_class_need_off), enable: enable ) )
        options.append( Option(title: R.strings.options_notification_class_sound, subtitle: "") )
    }
}

