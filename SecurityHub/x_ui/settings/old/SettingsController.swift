////
////  SettingsController.swift
////  SecurityHub
////
////  Created by Dan on 01.02.2019.
////  Copyright © 2019 TEKO. All rights reserved.
////
//
//import UIKit
//import Eureka
//import RxSwift
//import Localize_Swift
//import RappleProgressHUD
//
//class SettingsController: FormViewController{
//       private var options: [Option] = []
//       private var titleOp: String
//       private var createButton: XButtonView?
//       private var createProfile: XProfileView?
//
//
//       init(_ title: String = R.strings.title_options){
//           self.titleOp = title
//           super.init(nibName: nil, bundle: nil)
//       }
//
//       required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
//
//    private var observer: Any?
//       override func viewWillAppear(_ animated: Bool) {
//           super.viewWillAppear(animated)
//           navigationController?.setNavigationBarHidden(false, animated: false)
//           observer = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: OptionsCell.cellId), object: nil, queue: OperationQueue.main){ notification in
//            let tit: String = notification.object as! String
//           }
//       }
//
//       override func viewWillDisappear(_ animated: Bool) {
//           super.viewDidDisappear(animated)
//           if let o = observer {
//               NotificationCenter.default.removeObserver(o)
//               observer = nil
//           }
//       }
//
//   override func viewDidLoad() {
//        super.viewDidLoad()
//        title = titleOp
//        switch titleOp {
//        case R.strings.title_options:          load()
//        case R.strings.options_security:       security()
//        case R.strings.options_events:         events()
//        case R.strings.options_notification_classes: push_class()
//        case R.strings.options_history:        history()
//        case R.strings.options_language:       language()
//        case R.strings.options_interface:      visual_interface()
//        case R.strings.options_header_user:    user_interface()
//        default: break;
//        }
//
//        if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == titleOp }) {
//               self._class(id: _class.id, name: _class.name)
//           }
//      }
//    private func load(){
//        initCreateProfile()
//        initTypeInfoBlock()
//    }
//
//    private func initCreateProfile() {
//        guard let nV = navigationController else { return }
//        let operators = DataManager.shared.getOperator()
//        let role =  R.strings.operator_cell_permission + (operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin : operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view );
//        createProfile = XProfileView(frame: XProfileView.defaultRect, title: operators.name, subtitle: role)
//        form +++ ViewRow<XProfileView>()
//            { (row) in
//                row.onCellSelection { (lab,st) in
//                  nV.pushViewController(SettingsController(R.strings.options_header_user), animated: false)
//                }
//            }
//            .cellSetup { (cell, row) in cell.view = self.createProfile; cell.update() }
//
//    }
//
//    private func initTypeInfoBlock(){
//         guard let nV = navigationController else { return }
//        form +++ LabelRow(){
//            $0.title = DataManager.settingsHelper.needExpert ? R.strings.options_expert_mode_on : R.strings.options_expert_mode_off
//            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                nV.pushViewController(ExpertController(), animated: true)
//            }
//            .cellUpdate({ (cell, row) in
//                if (DataManager.settingsHelper.needExpert)
//                {
//                    cell.tintColor =  UIColor(red:0.15, green:0.55, blue:0.16, alpha:1.0)
//                    cell.textLabel?.textColor = UIColor(red:0.15, green:0.55, blue:0.16, alpha:1.0)
//                }
//                else
//                {
//                    cell.tintColor =  UIColor.red
//                    cell.textLabel?.textColor = UIColor.red
//                }
//            })
//        }
//
//        form +++  LabelRow(){
//                         $0.title = R.strings.options_operators
//                         $0.hidden =  Roles.manageOperators ? false : true
//                           $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                               nV.pushViewController(OperatorsController(), animated: false)
//                       }
//                   }
//            <<< LabelRow(){
//                  $0.title = R.strings.options_events
//                  $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(SettingsController(R.strings.options_events), animated: false)}
//              }
//              <<< LabelRow(){
//                  $0.title = R.strings.options_interface
//                  $0.hidden =  DataManager.settingsHelper.needExpert ? false : true
//                  $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(SettingsController(R.strings.options_interface), animated: false)}
//              }
//
//
//
//        if Roles.manageSites {
//            form +++ Section(R.strings.options_header_site)
//            <<< LabelRow(){
//                $0.title = R.strings.options_site_add
//                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(SiteAddController(site_id: nil), animated: false)}
//            }
//            <<< LabelRow(){
//                $0.title = R.strings.options_site_delegate
//                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in AlertUtil.delegateAlert()}
//            }
//        }
//        form +++ Section(R.strings.options_information)
//        <<< LabelRow(){
//            $0.title = R.strings.options_language_app
//            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in self.lan_app()}
//        }
//        <<< LabelRow(){
//            $0.hidden =  (XTargetUtils.isHubTarget || DataManager.defaultHelper.language == "ru") ? false : true
//            $0.title = R.strings.options_faq
//            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(WikiController(url: nil), animated: false)}
//        }
//        <<< LabelRow(){
//            $0.hidden =  (XTargetUtils.isHubTarget && DataManager.defaultHelper.language == "ru") ? false : true
//            $0.title = R.strings.options_chop
//            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(ChopController(), animated: false)}
//        }
//
//        <<< LabelRow(){
//            $0.title = R.strings.options_about_app
//            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                let version : String! = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
//                let build : String! = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
//                let mes : String
//
//
//                if (DataManager.defaultHelper.language == "de")
//                {
//                    mes =
//                    "\n\(R.strings.options_app_version): \(version ?? "0").\(build ?? "0")\n\n" +
//                    "controlex GmbH \n" +
//                    "Philosophenweg 31 – 33\n" +
//                    "47051 Duisburg\n" +
//                    "Deutschland\n" +
//                    "Tel.: +49 (0) 203 393 91 188\n" +
//                    "Fax: +49 (0) 203 393 91 189\n" +
//                    "E-Mail: info@controlex.eu\n" +
//                    "Internet: www.controlex.eu\n\n" +
//
//                    "Steuer-Nr.:  109-5920-1026\n" +
//                    "Handelsregister: Düsseldorf\n" +
//                    "Handelsregister Nr.: HRB 58098\n"
//                }
//                else
//                {
//                    if (XTargetUtils.isHubTarget)
//                    {
//                        mes =
//                       "\n\(R.strings.options_app_version): \(version ?? "0").\(build ?? "0")\n\n" +
//                       "\(R.strings.options_app_developer)\n\n" +
//                       "\(R.strings.options_app_web): \nteko.biz \nsecurity-hub.ru \nhelp@security-hub.ru\n\n" +
//                       "\(R.strings.options_app_techsupport): \nsupport@security-hub.ru \n\n" +
//                       "\(R.strings.options_app_phone): 8 800 100 8945\n\n"
//                    }
//                    else
//                    {
//                        switch XTargetUtils.target{
//                        case .teleplus:
//                            mes =
//                            "\n\(R.strings.options_app_version): \(version ?? "0").\(build ?? "0")\n\n" +
//                            "\(R.strings.options_app_developer_teleplus)\n\n" +
//                            "\(R.strings.options_app_phone): 8 3462 555 333\n\n"
//                            break;
//                        default:
//                            mes =
//                            "\n\(R.strings.options_app_version): \(version ?? "0").\(build ?? "0")\n\n" +
//                            "\(R.strings.options_app_developer)\n\n"
//                        }
//                    }
//
//                }
//                AlertUtil.infoAlertApp(mes)
//            }
//        }
//    }
//
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        var mes = ""
//        for obj in DataManager.shared.getClasses() { if (DataManager.settingsHelper.storyMask & (1 << obj.id)) != 0 { mes += obj.name + ", " } }
//        self.options.first(where: { $0.title == R.strings.options_history })?.subtitle = "\(R.strings.options_history_desc)\(mes)"
//    }
//
//    func user_interface() {
//        guard let nV = navigationController else { return }
//        let roles = DataManager.shared.getUser().roles
//        form +++ LabelRow(){
//              $0.title = R.strings.options_current_operator
//                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                    let operators = DataManager.shared.getOperator()
//                    let mes = "\(R.strings.operator_name): \(operators.name)\n\n" +
//                              "\(R.strings.operator_login): \(operators.login)\n\n" +
//                                 R.strings.operator_cell_permission + (
//                                       operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin :
//                                           operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view ) + "\n\n" +
//                                   R.strings.operator_sites_count + String(DataManager.shared.getSitesCount()) + "\n";
//
//                    if roles & Roles.ORG_ADMIN != 0 || roles & Roles.DOMEN_ADMIN != 0 || roles & Roles.DOMEN_HOZ_ORG != 0 {
//                                   AlertUtil.infoAlertUser(mes, name: operators.name, listener: { (name) in
//                                       _ = DataManager.shared.changeOperator(id: operators.id, value: name ?? "", name: "name").subscribe()
//                                   }) { (pas) in
//                                       _ = DataManager.shared.changeOperator(id: operators.id, value: pas?.md5 ?? "", name: "password").subscribe()
//                                       UserDefaults.standard.set(nil, forKey: "saveMeViewPass")
//                                       UserDefaults.standard.set(nil, forKey: "saveMeViewLogin")
//                                   }
//                               }else{ AlertUtil.infoAlert(mes) }
//                            }
//                    }
//        <<< LabelRow(){
//              $0.title = R.strings.options_security
//                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                   nV.pushViewController(SettingsController(R.strings.options_security), animated: false)
//            }
//        }
//        createButton = XButtonView(XButtonView.defaultRect, title: R.strings.options_exit, style: .text , mainColor: UIColor.red)
//        createButton?.click = self.exit
//        form +++ ViewRow<XButtonView>().cellSetup {
//            (cell, row) in cell.view = self.createButton;
//            cell.update() }
//    }
//
//
//    func security() {
//        guard let nV = navigationController else { return }
//        form +++ SwitchRow("securitySwitch") {
//            $0.title = R.strings.options_secutiry_need_pin
//            $0.value = DataManager.settingsHelper.needPin ? true : false
//        }
//        .onChange { row in
//            row.title = R.strings.options_secutiry_need_pin
//            let checkC = PinController(type: .inputPin, code: nil, cancelTitle: R.strings.button_back,
//                                       cancel: { nV.popViewController(animated: true) },
//                                       complete: {
//                                            DataManager.settingsHelper.needPin = DataManager.settingsHelper.needPin ? false : true;
//                                            nV.popViewController(animated: true)
//                                       })
//            nV.pushViewController(checkC, animated: false)
//        }
//
//        <<< LabelRow(){
//            $0.title = R.strings.options_secutiry_set_pin
//            $0.hidden =  Condition.function(["securitySwitch"], { form in
//                return !((form.rowBy(tag: "securitySwitch") as? SwitchRow)?.value ?? false)
//            })
//            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//               let setC = PinController(type: .newPin, code: nil, cancelTitle: R.strings.button_back,
//                                       cancel: { nV.popViewController(animated: true) },
//                                       complete: {
//                                           DataManager.settingsHelper.needPin = DataManager.settingsHelper.needPin ? true : false;
//                                           guard let vc = nV.viewControllers.last(where: { $0 is SettingsController }) else { return }
//                                           nV.popToViewController(vc, animated: true)
//                                       })
//               nV.pushViewController(setC, animated: false)
//           }
//        }
//     if (SimpleTouch.isTouchIDEnabled == .success && UIScreen.main.nativeBounds.height != 2436) {
//            form +++ LabelRow(){
//                $0.title = R.strings.options_secutiry_need_touch_id
//                $0.hidden = (Condition.function(["securitySwitch"], { form in return !((form.rowBy(tag: "securitySwitch") as? SwitchRow)?.value ?? false)}))
//                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                    DataManager.settingsHelper.touchId = DataManager.settingsHelper.needPin ? true : false
//                }
//            }
//        }
//    }
//
//       func events() {
//        guard let nV = navigationController else { return }
//        form +++ Section(R.strings.options_notification_title)
//            <<< SwitchRow("eventsSwitch") {
//                $0.title = R.strings.options_notification_need
//                $0.value = DataManager.settingsHelper.needPush ? true : false
//            }
//            .onChange { row in
//                DataManager.settingsHelper.needPush = DataManager.settingsHelper.needPush ? false : true
//                DataManager.settingsHelper.needPush ? DataManager.shared.setFCM() : DataManager.shared.deleteFCM()
//            }
//            <<< LabelRow(){
//                $0.title = R.strings.options_notification_classes
//                $0.hidden =  Condition.function(["eventsSwitch"], { form in
//                    return !((form.rowBy(tag: "eventsSwitch") as? SwitchRow)?.value ?? false)
//                })
//                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                    nV.pushViewController(SettingsController(R.strings.options_notification_classes), animated: false)
//               }
//            }
//
//            if DataManager.settingsHelper.needExpert {
//                form +++ Section(R.strings.options_history_title)
//                <<< LabelRow(){
//                    $0.title = R.strings.options_history
//                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                       nV.pushViewController(SettingsController(R.strings.options_history), animated: false)
//                   }
//                }
//            }
//       }
//
//       func visual_interface() {
//            form +++ SwitchRow() {
//                   $0.title = R.strings.options_interface_devices_need
//                   $0.value = DataManager.settingsHelper.needDevices ? true : false
//               }
//            .onChange{row in
//                DataManager.settingsHelper.needDevices = DataManager.settingsHelper.needDevices ? false : true
//                NotificationCenter.default.post(name: .objectListStructUpdate, object: nil)}
//
//            <<< SwitchRow() {
//                $0.title = R.strings.options_interface_sections_need
//                $0.value = DataManager.settingsHelper.needSections ? true : false
//            }.onChange{row in
//                DataManager.settingsHelper.needSections = DataManager.settingsHelper.needSections ? false : true
//                NotificationCenter.default.post(name: .objectListStructUpdate, object: nil)}
//       }
//
//       func push_class(){
//            guard let nV = navigationController else { return }
//            let section = Section()
//            form +++ section
//
//
//           for obj in DataManager.shared.getClasses().filter({ (id, name) -> Bool in return id < 5 }) {
//                section <<< LabelRow() {
//                    $0.title = obj.name
//                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
//                                        if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == obj.name})
//                                        {
//                                              nV.pushViewController(SettingsController(_class.name), animated: false)
//                                        }
//                                      }
//                }
//           }
//       }
//
//       func history(){
//       let section = Section()
//         form +++ section
//           for obj in DataManager.shared.getClasses() {
//             section <<< SwitchRow() {
//                    $0.title = obj.name
//                    $0.value = (DataManager.settingsHelper.storyMask & (1 << obj.id)) != 0 ? true : false
//                }
//             .onChange{row in
//                if  let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == obj.name }){
//                      if (DataManager.settingsHelper.storyMask & (1 << _class.id)) == 0 {
//                          DataManager.settingsHelper.storyMask = DataManager.settingsHelper.storyMask | (1 << _class.id)
//                      }else{
//                          DataManager.settingsHelper.storyMask = DataManager.settingsHelper.storyMask & ~(1 << _class.id)
//                      }
//                      NotificationCenter.default.post(name: .eventHistoryUpdate, object: nil)
//                    }
//                }
//           }
//
//       }
//    func language(){
//        self.options.append( Option(title: R.strings.options_language_app, subtitle: DataManager.defaultHelper.language.localized()) )
//    }
//    func lan_app() {
//        guard let nV = navigationController else { return }
//
//        let alert = AlertUtil.myXAlert(R.strings.options_language_app,
//                                        messages: [
//                                            R.strings.options_language_ru,
//                                            R.strings.options_language_de,
//                                            R.strings.options_language_en,
//                                            R.strings.options_language_es
//            ],
//                                        voids: [{self.setLan_app("ru")}, {self.setLan_app("de")}, {self.setLan_app("en")}, {self.setLan_app("es")} ])
//        nV.present(alert, animated: false)
//    }
//
//    private func setLan_app(_ lan: String) {
//        self.showLoader()
//        Localize.setCurrentLanguage(lan)
//        DataManager.defaultHelper.language = lan
//        R.reinit()
//        let _ = DataManager.shared.setLocale(lan)
//            .subscribe(onNext: { r in
//                self.hideLoader()
//                DataManager.shared.clearAfterLang()
//                self.navigationController?.present(SplashController(), animated: false)
//            }, onError: { e in
//                self.hideLoader()
//                DataManager.shared.clearAfterLang()
//                self.navigationController?.present(SplashController(), animated: false)
//            })
//    }
//
//    private func exit() {
//        self.navigationController?.present(AlertUtil.exitAlert({
//            self.showLoader()
//            DataManager.disconnectDisposable = DataManager.shared.disconnect().subscribe(onNext: { a in
//                DataManager.disconnectDisposable?.dispose()
//                self.hideLoader()
//                NavigationHelper.shared.show(LoginController())
//            })
//        }), animated: true)
//    }
//
//    func showLoader(){ RappleActivityIndicatorView.startAnimating() }
//    func hideLoader(){ RappleActivityIndicatorView.stopAnimation() }
//
//    func _class(id: Int64, name: String) {
//           guard let nV = navigationController else { return }
//           let enable = (DataManager.settingsHelper.pushMask & (1 << id)) != 0
//                form +++ SwitchRow() {
//                    $0.title = R.strings.options_notification_class_need
//                    $0.value = enable ? true : false
//                }.onChange{row in
//                    if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == self.titleOp }) {
//                    if (DataManager.settingsHelper.pushMask & (1 << _class.id)) == 0 {
//                        DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask | (1 << _class.id)
//                    }else{
//                        DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask & ~(1 << _class.id)
//                    }
//                    }
//                }
//                    <<< LabelRow() {
//                         $0.title = R.strings.options_notification_class_sound
//                         $0.onCellSelection  { (lab : LabelCellOf<String>, labrw : LabelRow) in
//
//                            if  let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == self.titleOp }) {
//                                let key =   _class.id == 0 ? "alarm" :
//                                            _class.id == 1 ? "sabotage" :
//                                            _class.id == 2 ? "malfunction" :
//                                            _class.id == 3 ? "attention" : "arm"
//                                let path = (DataManager.shared.sounds[key] as? String) ?? "default"
//                                nV.pushViewController(SelectSoundController(path: path, void: { name in
//                                    DataManager.shared.setSound(class_id: _class.id, name: name ?? "default").subscribe()
//                                }), animated: false)
//                            }
//                        }
//                    }
//
//           options.append(Option(title: R.strings.options_notification_class_need, subtitle:
//               (enable ? R.strings.options_notification_class_need_on : R.strings.options_notification_class_need_off), enable: enable ) )
//           options.append( Option(title: R.strings.options_notification_class_sound, subtitle: "") )
//       }
//}
//
//

import UIKit
import Eureka
import RxSwift
import Localize_Swift
import RappleProgressHUD

class SettingsController: FormViewController {
    
    private var options: [Option] = []
    private var titleOp: String
    private var createButton: XButtonView?
    private var createProfile: XProfileView?
    private var observer: Any?
    
    init(_ title: String = R.strings.title_options){
        self.titleOp = title
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        observer = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: OptionsCell.cellId), object: nil, queue: OperationQueue.main){ notification in
            let _: String = notification.object as! String
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let o = observer {
            NotificationCenter.default.removeObserver(o)
            observer = nil
        }
    }
    
   override func viewDidLoad() {
        super.viewDidLoad()
    
        title = titleOp
    
        switch titleOp {
        case R.strings.title_options:          load()
        case R.strings.options_security:       security()
        case R.strings.options_events:         events()
        case R.strings.options_notification_classes: push_class()
        case R.strings.options_history:        history()
        case R.strings.options_language:       language()
        case R.strings.options_interface:      visual_interface()
        case R.strings.options_header_user:    user_interface()
        default: break;
        }
       
        if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == titleOp }) {
               self._class(id: _class.id, name: _class.name)
           }
      }
    private func load(){
        initCreateProfile()
        initTypeInfoBlock()
    }
    
    private func initCreateProfile() {
        guard let nV = navigationController else { return }
        let operators = DataManager.shared.getOperator()
        let role =  R.strings.operator_cell_permission + (operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin : operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view );
        createProfile = XProfileView(frame: XProfileView.defaultRect, title: operators.name, subtitle: role)
        form +++ ViewRow<XProfileView>()
            { (row) in
                row.onCellSelection { (lab,st) in
                  nV.pushViewController(SettingsController(R.strings.options_header_user), animated: false)
                }
            }
            .cellSetup { (cell, row) in cell.view = self.createProfile; cell.update() }
        
    }
    
    private func initTypeInfoBlock(){
         guard let nV = navigationController else { return }
        form +++ LabelRow(){
            $0.title = DataManager.settingsHelper.needExpert ? R.strings.options_expert_mode_on : R.strings.options_expert_mode_off
            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                nV.pushViewController(ExpertController(), animated: true)
            }
            .cellUpdate({ (cell, row) in
                if (DataManager.settingsHelper.needExpert)
                {
                    cell.tintColor =  UIColor(red:0.15, green:0.55, blue:0.16, alpha:1.0)
                    cell.textLabel?.textColor = UIColor(red:0.15, green:0.55, blue:0.16, alpha:1.0)
                }
                else
                {
                    cell.tintColor =  UIColor.red
                    cell.textLabel?.textColor = UIColor.red
                }
            })
        }
        
        form +++  LabelRow(){
                         $0.title = R.strings.options_operators
                         $0.hidden =  Roles.manageOperators ? false : true
                           $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                               nV.pushViewController(OperatorsController(), animated: false)
                       }
                   }
            <<< LabelRow(){
                  $0.title = R.strings.options_events
                  $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(SettingsController(R.strings.options_events), animated: false)}
              }
              <<< LabelRow(){
                  $0.title = R.strings.options_interface
                  $0.hidden =  DataManager.settingsHelper.needExpert ? false : true
                  $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(SettingsController(R.strings.options_interface), animated: false)}
              }
              
        
        
        if Roles.manageSites {
            form +++ Section(R.strings.options_header_site)
            <<< LabelRow(){
                $0.title = R.strings.options_site_add
                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(SiteAddController(site_id: nil), animated: false)}
            }
            <<< LabelRow(){
                $0.title = R.strings.options_site_delegate
                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in AlertUtil.delegateAlert()}
            }
        }
        
        if DataManager.defaultHelper.language == "ru" && XTargetUtils.target == .security_hub {
            form +++ Section(R.strings.options_information)
                <<< LabelRow(){
                    $0.title = R.strings.options_chop
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(ChopController(), animated: true)}
                }
                
                <<< LabelRow(){
                    $0.title = R.strings.options_language_app
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(LanguageController(), animated: true)}
                }
                
                <<< LabelRow(){
                    $0.title = R.strings.options_faq
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(WikiController(), animated: true)}
                }
                
                <<< LabelRow(){
                    $0.title = R.strings.options_about_app
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(AboutController(), animated: true)}
                }
        } else {
            form +++ Section(R.strings.options_information)
                <<< LabelRow(){
                    $0.title = R.strings.options_language_app
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(LanguageController(), animated: true)}
                }
                
                <<< LabelRow(){
                    $0.title = R.strings.options_faq
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(WikiController(), animated: true)}
                }
                
                <<< LabelRow(){
                    $0.title = R.strings.options_about_app
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in nV.pushViewController(AboutController(), animated: true)}
                }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        var mes = ""
        for obj in DataManager.shared.getClasses() { if (DataManager.settingsHelper.storyMask & (1 << obj.id)) != 0 { mes += obj.name + ", " } }
        self.options.first(where: { $0.title == R.strings.options_history })?.subtitle = "\(R.strings.options_history_desc)\(mes)"
    }
    
    func user_interface() {
        guard let nV = navigationController else { return }
        let roles = DataManager.shared.getUser().roles
        form +++ LabelRow(){
              $0.title = R.strings.options_current_operator
                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                    let operators = DataManager.shared.getOperator()
                    let mes = "\(R.strings.operator_name): \(operators.name)\n\n" +
                              "\(R.strings.operator_login): \(operators.login)\n\n" +
                                 R.strings.operator_cell_permission + (
                                       operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin :
                                           operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view ) + "\n\n" +
                                   R.strings.operator_sites_count + String(DataManager.shared.getSitesCount()) + "\n";
                    
                    if roles & Roles.ORG_ADMIN != 0 || roles & Roles.DOMEN_ADMIN != 0 || roles & Roles.DOMEN_HOZ_ORG != 0 {
                                   AlertUtil.infoAlertUser(mes, name: operators.name, listener: { (name) in
                                       _ = DataManager.shared.changeOperator(id: operators.id, value: name ?? "", name: "name").subscribe()
                                   }) { (pas) in
                                       _ = DataManager.shared.changeOperator(id: operators.id, value: pas?.md5 ?? "", name: "password").subscribe()
                                       UserDefaults.standard.set(nil, forKey: "saveMeViewPass")
                                       UserDefaults.standard.set(nil, forKey: "saveMeViewLogin")
                                   }
                               }else{ AlertUtil.infoAlert(mes) }
                            }
                    }
        <<< LabelRow(){
              $0.title = R.strings.options_security
                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                   nV.pushViewController(SettingsController(R.strings.options_security), animated: false)
            }
        }
        createButton = XButtonView(XButtonView.defaultRect, title: R.strings.options_exit, style: .text , mainColor: UIColor.red)
        createButton?.click = self.exit
        form +++ ViewRow<XButtonView>().cellSetup {
            (cell, row) in cell.view = self.createButton;
            cell.update() }
        
    }
    
    
    func security() {
        guard let nV = navigationController else { return }
        form +++ SwitchRow("securitySwitch") {
            $0.title = R.strings.options_secutiry_need_pin
            $0.value = DataManager.settingsHelper.needPin ? true : false
        }
        .onChange { row in
            row.title = R.strings.options_secutiry_need_pin
            let checkC = PinController(type: .inputPin, code: nil, cancelTitle: R.strings.button_back,
                                       cancel: { nV.popViewController(animated: true) },
                                       complete: {
                                            DataManager.settingsHelper.needPin = DataManager.settingsHelper.needPin ? false : true;
                                            nV.popViewController(animated: true)
                                       })
            nV.pushViewController(checkC, animated: false)
        }
            
        <<< LabelRow(){
            $0.title = R.strings.options_secutiry_set_pin
            $0.hidden =  Condition.function(["securitySwitch"], { form in
                return !((form.rowBy(tag: "securitySwitch") as? SwitchRow)?.value ?? false)
            })
            $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
               let setC = PinController(type: .newPin, code: nil, cancelTitle: R.strings.button_back,
                                       cancel: { nV.popViewController(animated: true) },
                                       complete: {
                                           DataManager.settingsHelper.needPin = DataManager.settingsHelper.needPin ? true : false;
                                           guard let vc = nV.viewControllers.last(where: { $0 is SettingsController }) else { return }
                                           nV.popToViewController(vc, animated: true)
                                       })
               nV.pushViewController(setC, animated: false)
           }
        }
     if (SimpleTouch.isTouchIDEnabled == .success && UIScreen.main.nativeBounds.height != 2436) {
            form +++ LabelRow(){
                $0.title = R.strings.options_secutiry_need_touch_id
                $0.hidden = (Condition.function(["securitySwitch"], { form in return !((form.rowBy(tag: "securitySwitch") as? SwitchRow)?.value ?? false)}))
                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                    DataManager.settingsHelper.touchId = DataManager.settingsHelper.needPin ? true : false
                }
            }
        }
    }
    
       func events() {
        guard let nV = navigationController else { return }
        form +++ Section(R.strings.options_notification_title)
            <<< SwitchRow("eventsSwitch") {
                $0.title = R.strings.options_notification_need
                $0.value = DataManager.settingsHelper.needPush ? true : false
            }
            .onChange { row in
                DataManager.settingsHelper.needPush = DataManager.settingsHelper.needPush ? false : true
                DataManager.settingsHelper.needPush ? DataManager.shared.setFCM() : DataManager.shared.deleteFCM()
            }
            <<< LabelRow(){
                $0.title = R.strings.options_notification_classes
                $0.hidden =  Condition.function(["eventsSwitch"], { form in
                    return !((form.rowBy(tag: "eventsSwitch") as? SwitchRow)?.value ?? false)
                })
                $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                    nV.pushViewController(SettingsController(R.strings.options_notification_classes), animated: false)
               }
            }
        
            if DataManager.settingsHelper.needExpert {
                form +++ Section(R.strings.options_history_title)
                <<< LabelRow(){
                    $0.title = R.strings.options_history
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                       nV.pushViewController(SettingsController(R.strings.options_history), animated: false)
                   }
                }
            }
       }
       
       func visual_interface() {
            form +++ SwitchRow() {
                   $0.title = R.strings.options_interface_devices_need
                   $0.value = DataManager.settingsHelper.needDevices ? true : false
               }
            .onChange{row in
                DataManager.settingsHelper.needDevices = DataManager.settingsHelper.needDevices ? false : true
                NotificationCenter.default.post(name: .objectListStructUpdate, object: nil)}
                
            <<< SwitchRow() {
                $0.title = R.strings.options_interface_sections_need
                $0.value = DataManager.settingsHelper.needSections ? true : false
            }.onChange{row in
                DataManager.settingsHelper.needSections = DataManager.settingsHelper.needSections ? false : true
                NotificationCenter.default.post(name: .objectListStructUpdate, object: nil)}
       }
       
       func push_class(){
            guard let nV = navigationController else { return }
            let section = Section()
            form +++ section
        
        
           for obj in DataManager.shared.getClasses().filter({ (id, name) -> Bool in return id < 5 }) {
                section <<< LabelRow() {
                    $0.title = obj.name
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                                        if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == obj.name})
                                        {
                                              nV.pushViewController(SettingsController(_class.name), animated: false)
                                        }
                                      }
                }
           }
       }
       
       func history(){
       let section = Section()
         form +++ section
           for obj in DataManager.shared.getClasses() {
             section <<< SwitchRow() {
                    $0.title = obj.name
                    $0.value = (DataManager.settingsHelper.storyMask & (1 << obj.id)) != 0 ? true : false
                }
             .onChange{row in
                if  let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == obj.name }){
                      if (DataManager.settingsHelper.storyMask & (1 << _class.id)) == 0 {
                          DataManager.settingsHelper.storyMask = DataManager.settingsHelper.storyMask | (1 << _class.id)
                      }else{
                          DataManager.settingsHelper.storyMask = DataManager.settingsHelper.storyMask & ~(1 << _class.id)
                      }
                      NotificationCenter.default.post(name: .eventHistoryUpdate, object: nil)
                    }
                }
           }
        
       }
    
    func language(){
        self.options.append( Option(title: R.strings.options_language_app, subtitle: DataManager.defaultHelper.language.localized()) )
    }
    
    private func exit() {
        self.navigationController?.present(AlertUtil.exitAlert({
            self.showLoader()
            DataManager.disconnectDisposable = DataManager.shared.disconnect().subscribe(onNext: { a in
                DataManager.disconnectDisposable?.dispose()
                self.hideLoader()
                NavigationHelper.shared.show(SplashController())
            })
        }), animated: true)
    }
    
    func showLoader(){ RappleActivityIndicatorView.startAnimating() }
    func hideLoader(){ RappleActivityIndicatorView.stopAnimation() }
    
    func _class(id: Int64, name: String) {
           guard let nV = navigationController else { return }
           let enable = (DataManager.settingsHelper.pushMask & (1 << id)) != 0
                form +++ SwitchRow() {
                    $0.title = R.strings.options_notification_class_need
                    $0.value = enable ? true : false
                }.onChange{row in
                    if let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == self.titleOp }) {
                    if (DataManager.settingsHelper.pushMask & (1 << _class.id)) == 0 {
                        DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask | (1 << _class.id)
                    }else{
                        DataManager.settingsHelper.pushMask = DataManager.settingsHelper.pushMask & ~(1 << _class.id)
                    }
                    }
                }
                    <<< LabelRow() {
                         $0.title = R.strings.options_notification_class_sound
                         $0.onCellSelection  { (lab : LabelCellOf<String>, labrw : LabelRow) in
                          
                            if  let _class = DataManager.shared.getClasses().first(where: { (id, name) -> Bool in return name == self.titleOp }) {
                                let key =   _class.id == 0 ? "alarm" :
                                            _class.id == 1 ? "sabotage" :
                                            _class.id == 2 ? "malfunction" :
                                            _class.id == 3 ? "attention" : "arm"
                                let path = (DataManager.shared.sounds[key] as? String) ?? "default"
                                nV.pushViewController(SelectSoundController(path: path, void: { name in
                                    _ = DataManager.shared.setSound(class_id: _class.id, name: name ?? "default").subscribe()
                                }), animated: false)
                            }
                        }
                    }
        
           options.append(Option(title: R.strings.options_notification_class_need, subtitle:
               (enable ? R.strings.options_notification_class_need_on : R.strings.options_notification_class_need_off), enable: enable ) )
           options.append( Option(title: R.strings.options_notification_class_sound, subtitle: "") )
       }
}


