//
//  ExpertController.swift
//  SecurityHub
//
//  Created by Timerlan on 07/10/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit

class ExpertController: XBaseController<ExpertView> {
    override func loadView() {
        super.loadView()
        title = R.strings.expert_mode
        if DataManager.settingsHelper.needExpert {
            xView.setAction(text: R.strings.expert_mode_btn_off, color: DEFAULT_COLOR_RED, void: off)
        } else {
            xView.setAction(text: R.strings.expert_mode_btn_on, color: DEFAULT_COLOR_GREEN, void: on)
        }
    }
    
    private func on() {
        let alert = AlertUtil.myXAlert(R.strings.expert_mode_action_on_desc,
                           messages: [R.strings.expert_mode_action_on], voids: [{
                                DataManager.settingsHelper.needExpert = true
                                NavigationHelper.shared.show(SplashController())
                                DataManager.shared.setFCM()
                            }])
        navigationController?.present(alert, animated: false, completion: {})
    }
    
    private func off() {
        let alert = AlertUtil.myXAlert(R.strings.expert_mode_action_off_desc,
                           messages: [R.strings.expert_mode_action_off], voids: [{
                            DataManager.settingsHelper.needExpert = false
                            NavigationHelper.shared.show(SplashController())
                            DataManager.shared.setFCM()
                            }])
        navigationController?.present(alert, animated: false, completion: {})
    }
}

class ExpertView: XBaseView {
    private var void: (() -> Void)?
    private let descView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.light)
        view.numberOfLines = 0
        view.text = R.strings.expert_mode_desc
        return view
    }()
    
    private let actionView: ZFRippleButton = {
        let layerTop = CALayer()
        layerTop.frame = CGRect(x: 0, y: -1, width: UIScreen.main.bounds.width, height: 0.5)
        layerTop.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4).cgColor
        let layerBottom = CALayer()
        layerBottom.frame = CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 0.5)
        layerBottom.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4).cgColor
        let view = ZFRippleButton()
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.gray.withAlphaComponent(0.1)
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.setTitle("Включить", for: .normal)
        view.setTitleColor(DEFAULT_COLOR_GREEN, for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        view.layer.addSublayer(layerTop)
        view.layer.addSublayer(layerBottom)
        return view
    }()
    
    override func setContent() {
        xView.backgroundColor = DEFAULT_BACKGROUND
        xView.addSubview(descView)
        xView.addSubview(actionView)
    }
    
    override func setConstraints() {
        descView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(32)
            make.centerX.equalToSuperview()
            make.width.equalToSuperview().offset(-32)
        }
        actionView.snp.remakeConstraints { (make) in
            make.top.equalTo(descView.snp.bottom).offset(32)
            make.centerX.equalToSuperview()
            make.height.equalTo(44)
            make.width.equalToSuperview()
        }
    }
    
    func setAction(text: String, color: UIColor, void: @escaping (()->Void)) {
        self.void = void
        actionView.setTitle(text, for: .normal)
        actionView.setTitleColor(color, for: .normal)
        actionView.addTarget(self, action: #selector(doAction), for: UIControl.Event.touchUpInside)
    }
    @objc private func doAction() { void?() }
}
