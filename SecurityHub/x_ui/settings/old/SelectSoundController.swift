//
//  SelectSoundController.swift
//  SecurityHub
//
//  Created by Timerlan on 18/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import UIKit
import AVFoundation

class SelectSoundController: XBaseController<XSelectSoundView> {
    private let path: String?
    private let void: (_ name: String?)->Void
    
    init(path: String?, void: @escaping ((_ name: String?)->Void)) {
        self.path = path
        self.void = void
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func loadView() {
        super.loadView()
        //TODO
        title = R.strings.title_sounds
        let infos = getSystemSounds()
        xView.selected = infos.first(where: { (info) -> Bool in return info.path == path })
        xView.setMusic(infos)
        rightButton(UIImage())
        rightButton.setTitle(R.strings.button_save, for: .normal)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            if let player = xView.player { player.stop() }
            void(xView.selected?.path)
        }
    }
    
    private func getSystemSounds() -> [SoundInfo] {
        return [ SoundInfo(path: "default",     name: R.strings.sounds_default),
                 SoundInfo(path: "coin.wav",    name: R.strings.sounds_coin),
                 SoundInfo(path: "sirena.wav",  name: R.strings.sounds_sirena),
                 SoundInfo(path: "smeh.wav",    name: R.strings.sounds_haha),
                 SoundInfo(path: "sms.wav",     name: R.strings.sounds_sms)]
    }
    
    override func rightClick() {
        navigationController?.popViewController(animated: true)
    }
}

struct SoundInfo {
    let path: String
    let name: String
}

class XSelectSoundView: XBaseView, UITableViewDelegate, UITableViewDataSource {
    private var musics: [SoundInfo] = []
    var player: AVAudioPlayer?
    var selected: SoundInfo?
    private lazy var tableView : UITableView = {
        let view = UITableView()
        view.backgroundColor = DEFAULT_COLOR_WINDOW_BACKGROUND
        view.separatorStyle = .none
        view.estimatedRowHeight = 130
        view.register(XSelectSoundCell.self, forCellReuseIdentifier: XSelectSoundCellId)
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    override func setContent() {
        xView.addSubview(tableView)
    }
    
    override func setConstraints() {
        tableView.snp.remakeConstraints{ make in
            make.edges.equalToSuperview()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return musics.count }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: XSelectSoundCellId, for: indexPath) as? XSelectSoundCell else {
            return tableView.dequeueReusableCell(withIdentifier: XSelectSoundCellId, for: indexPath)
        }
        cell.setContent(musics[indexPath.row].name, check: musics[indexPath.row].name == selected?.name)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        let info = musics[indexPath.row]
        if indexPath.row == 0 { return }
        guard let audioFilePath = Bundle.main.path(forResource: String(info.path.split(separator: ".")[0]),
                                                   ofType: String(info.path.split(separator: ".")[1])) else { return }
        if let player = player { player.stop() }
        let audioFileUrl = NSURL.fileURL(withPath: audioFilePath)
        if let audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl, fileTypeHint: nil) {
            player = audioPlayer
            selected = info
            audioPlayer.play()
        }
        tableView.reloadData()
    }
    
    func setMusic(_ musics: [SoundInfo]) {
        self.musics = musics
        tableView.reloadData()
    }
}

let XSelectSoundCellId = "XSelectSoundCellId"
class XSelectSoundCell: UITableViewCell {
    private var titleView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        return view
    }()
    private var selectView: UIImageView = {
        let view = UIImageView()
        view.contentMode = UIView.ContentMode.scaleAspectFit
        view.image = XImages.view_check
        return view
    }()
    private var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = DEFAULT_COLOR_GRAY
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(titleView)
        addSubview(selectView)
        addSubview(lineView)
        updateConstraints()
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        titleView.snp.remakeConstraints { (make) in
            make.top.left.equalTo(16)
            make.bottom.equalTo(-16)
        }
        selectView.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(titleView.snp.right)
            make.right.equalTo(-16)
            make.height.width.equalTo(30)
        }
        lineView.snp.remakeConstraints { (make) in
            make.left.right.bottom.equalTo(0)
            make.height.equalTo(1)
        }
    }
    
    func setContent(_ name: String, check: Bool) {
        titleView.text = name
        selectView.isHidden = !check
    }
}
