//
//  AddOperatorController.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class AddOperatorController: BaseController<AddOperatorView> {
    
    override func loadView() {
        super.loadView()
        title = R.strings.title_add_operator
        mainView.listener = add
    }
    
    @objc func add(){
        if (mainView.loginField.text ?? "").contains(where: {$0 == "@"}) {
            AlertUtil.errorAlert(R.strings.error_login_hororgan_no_dog)
        }else{
            _ = DataManager.shared.addOperator(
                name: mainView.nameField.text!,
                login: mainView.loginField.text!,
                password: mainView.pasField.text!,
                role: mainView.segmentCont.selectedSegmentIndex == 0 ? 8192 : 40960
            )
            .subscribe(onNext: { res in
                guard let nV = self.navigationController else { return }
                nV.popViewController(animated: false)
            })
        }
    }
}
