//
//  OperatorCell.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit
import SCLAlertView

class OperatorCell : BaseCell<OperatorView>{
    static let cellId = "OperatorCell"
    private var operators: Operators!
    
    override func setContent(_ obj: SQLCommand, isEnd: Bool = false) {
        operators = obj as? Operators
        mainView.titleView.text = operators.name
        mainView.loginView.text = "\(R.strings.operator_cell_login): \(operators.login)"
        mainView.pravView.text = R.strings.operator_cell_permission + (
            operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin :
                operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view );
        mainView.statusView.text = R.strings.operator_cell_status + ": " + (operators.active == 1 ? R.strings.operator_cell_status_active : R.strings.operator_cell_status_block )
        mainView.menuBtn.addTarget(self, action: #selector(menu), for: .touchUpInside)
    }
    
    @objc func menu(){
        let a = AlertUtil.myXAlert(operators.name ,messages: [
            R.strings.menu_operator_about,
            R.strings.menu_operator_rename,
            R.strings.menu_operator_change_login,
            R.strings.menu_operator_change_password,
            (operators.active == 1 ? R.strings.menu_operator_change_status_block : R.strings.menu_operator_change_status_active ),
            R.strings.menu_operator_change_permission,
            R.strings.menu_operator_delete
            ], voids: [ user,name,login,pas,active,prav,del ])
        nV?.present(a, animated: false)
    }
    
    private func user(){
        let mes = "\(R.strings.operator_name): \(operators.name)\n\n" +
            "\(R.strings.operator_login): \(operators.login)\n\n" +
            R.strings.operator_cell_permission + (
                operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin :
                    operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view ) + "\n\n" +
            R.strings.operator_sites_count + String(DataManager.shared.getSitesCount()) + "\n";
        AlertUtil.infoAlert(title: "", mes)
    }
    private func name(){
       AlertUtil.renameXAlert(text: operators.name) { (name) in
            _ = DataManager.shared.changeOperator(id: self.operators.id, value: name, name: "name").subscribe()
        }
    }
    private func pas(){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField(R.strings.operator_input_password)
        alert.addButton(R.strings.title_operator_change_password) {
            _ = DataManager.shared.changeOperator(id: self.operators.id, value: txt.text?.md5 ?? "", name: "password").subscribe()
        }
        alert.showEdit(R.strings.button_change_password, subTitle: "", closeButtonTitle: R.strings.button_cancel, colorStyle: UIColor.hubMainColorUINT)
    }
    private func login(){
        let alert = SCLAlertView(appearance: AlertUtil.appearance)
        let txt = alert.addTextField(R.strings.operator_input_login)
        txt.keyboardType = .asciiCapable
        alert.addButton(R.strings.title_operator_change_login) {
            if (txt.text ?? "").contains(where: {$0 == "@"}) { AlertUtil.errorAlert(R.strings.error_login_hororgan_no_dog) }else{
                _ = DataManager.shared.changeOperator(id: self.operators.id, value: txt.text ?? "", name: "login").subscribe()
            }
        }
        alert.showEdit(R.strings.button_change_login, subTitle: "", closeButtonTitle: R.strings.button_cancel, colorStyle: UIColor.hubMainColorUINT)
    }
    private func active(){
        _ = DataManager.shared.changeOperator(id: self.operators.id, value: (operators.active == 0), name: "active").subscribe()
    }
    private func prav(){
        let a = AlertUtil.myXAlert(messages: [R.strings.operator_cell_permission_manage_and_view, R.strings.operator_cell_permission_view], voids: [{
            _ = DataManager.shared.changeOperator(id: self.operators.id, value: 40960, name: "roles").subscribe()
            },{
                _ = DataManager.shared.changeOperator(id: self.operators.id, value: 8192, name: "roles").subscribe()
            }])
        nV?.present(a, animated: false)
    }
    private func del(){
        AlertUtil.deleteAlert({
            _ = DataManager.shared.delOperator(id: self.operators.id).subscribe()
        }, text: "\(R.strings.operator_delete) \(self.operators.name)?")
    }
}
