//
//  OperatorsController.swift
//  SecurityHub
//
//  Created by Timerlan on 22.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class OperatorsController: BaseController<BaseListView<OperatorView, OperatorCell>>{
   
    override func loadView() {
        super.loadView()
        title = R.strings.title_operators
        mainView.setNV(navigationController)
        mainView.initTable(cellId: OperatorCell.cellId, cellHeight: OperatorView.cellHeight, refresh: update)
        load()
        rightButton()
    }
    
    private func load(){
        _ = DataManager.shared.getOperators()
        .subscribe(onNext: { (operators,type) in
            self.mainView.updateList(operators, type: type)
        })
    }
    
    private func update(){ DataManager.shared.updateOperators() }
    
    override func rightClick() {
        guard let nV = navigationController else { return }
        
        nV.pushViewController(AddOperatorController(), animated: false)
    }
}
