import UIKit

class XSettingsView: XBaseView {
    
    var events: [Events] = []
    var alertType = -1
    var textInAlert = ""
    
    lazy var shadowLayer: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertTitle: UILabel = {
        let view = UILabel()

        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.techSupportCall
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubGrey
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertPinText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.enterPin
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertPinInput: UITextField = {
        let view = UITextField()
        
        view.isHidden = true
        view.keyboardType = .numberPad
        
        return view
    }()
    
    lazy var alertPinDots: UIStackView = {
        let view = UIStackView()
        
        view.axis = .horizontal
        view.distribution = .equalSpacing
        
        return view
    }()
    
    lazy var alertPinError: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.invalidPin
        view.font = UIFont(name: "OpenSans-Light", size: 12)
        view.font = view.font.withSize(12)
        view.textColor = UIColor.hubRedColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isHidden = true
        
        return view
    }()
    
    lazy var alertButtonContinue: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(alertContinue)
        )
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertButtonCancel: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(hideAlert)
        )
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var bottomNavFrame: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubTransparent
        
        return view
    }()
    
    lazy var bottomNavBackground: UIView = {
        return UIView()
    }()
    
    lazy var bottomNavLine: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubSeparator
        
        return view
    }()
    
    lazy var bottomNavIcons: UIStackView = {
        let view = UIStackView()
        
        view.axis = .horizontal
        view.distribution = .equalSpacing
        
        return view
    }()
    
    lazy var bottomNavState: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.numberOfLines = 1
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(stateClick)
        )
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var bottomNavMain: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_main!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavScripts: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_script!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavEvents: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_history!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavCameras: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_cameras!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavSettings: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_menu!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubMainColor
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var profileFrame: UIView = {
        let view = UIView()
        
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(openProfileSettings)
            )
        )
        
        return view
    }()
    
    lazy var profileImage: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "profileImage")
        
        return view
    }()
    
    lazy var profileUsername: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.numberOfLines = 2
        view.lineBreakMode = .byTruncatingTail
        view.textColor = UIColor.hubTextBlack
        
        return view
    }()
    
    lazy var techFrame: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1)
        view.alpha = 0.02
        view.isUserInteractionEnabled = true
        
        let gesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(sectionClicked)
        )
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var techImage: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "techSupport")
        
        return view
    }()
    
    lazy var techText: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.numberOfLines = 0
        view.text = R.strings.techSupportButton
        view.textColor = UIColor.hubTextBlack
        
        return view
    }()
    
    lazy var topLine: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubSeparator
        
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        
        return view
    }()
    
    override func setContent() {
        xView.addSubview(bottomNavFrame)
        xView.addSubview(bottomNavLine)
        bottomNavFrame.addSubview(bottomNavBackground)
        bottomNavFrame.addSubview(bottomNavIcons)
        bottomNavFrame.addSubview(bottomNavState)
        bottomNavIcons.addArrangedSubview(bottomNavMain)
        bottomNavIcons.addArrangedSubview(bottomNavScripts)
        bottomNavIcons.addArrangedSubview(bottomNavEvents)
        bottomNavIcons.addArrangedSubview(bottomNavCameras)
        bottomNavIcons.addArrangedSubview(bottomNavSettings)
        
        xView.addSubview(profileFrame)
        xView.addSubview(profileImage)
        xView.addSubview(profileUsername)
        xView.addSubview(techFrame)
        xView.addSubview(techImage)
        xView.addSubview(techText)
        xView.addSubview(topLine)
        
        xView.addSubview(tableView)
    }
    
    override func setConstraints() {
        bottomNavFrame.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(100)
            make.bottom.equalToSuperview().offset(-50)
        }
        
        bottomNavBackground.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        bottomNavLine.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(1)
            make.bottom.equalTo(bottomNavFrame.snp.top)
        }
        
        bottomNavIcons.snp.remakeConstraints { make in
            make.height.equalTo(36)
            make.bottom.equalTo(xView.snp.bottom).offset(-100)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavState.snp.remakeConstraints { make in
            make.bottom.equalTo(bottomNavIcons.snp.top).offset(-20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavMain.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
            
        bottomNavScripts.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavEvents.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavCameras.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavSettings.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        profileFrame.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(90)
        }
        
        profileImage.snp.remakeConstraints { make in
            make.width.equalTo(46)
            make.height.equalTo(51)
            make.leading.equalToSuperview().offset(20)
            make.top.equalToSuperview().offset(40)
        }
        
        profileUsername.snp.remakeConstraints { make in
            make.top.equalTo(profileImage.snp.top)
            make.leading.equalTo(profileImage.snp.trailing).offset(11)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        techFrame.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(50)
            make.top.equalTo(profileImage.snp.bottom).offset(5)
        }
        
        techImage.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
            make.centerY.equalTo(techFrame.snp.centerY)
            make.centerX.equalTo(profileImage.snp.centerX)
        }
        
        techText.snp.remakeConstraints { make in
            make.centerY.equalTo(techFrame.snp.centerY)
            make.leading.equalTo(profileUsername.snp.leading)
            make.trailing.equalTo(profileUsername.snp.trailing)
        }
        
        topLine.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(techFrame.snp.bottom)
        }
        
        changeBottomNavStatus(type: DataManager.shared.bottomNavState)
        
        tableView.snp.remakeConstraints{make in
            make.top.equalTo(topLine.snp.bottom)
            make.width.equalToSuperview()
            make.bottom.equalTo(bottomNavLine.snp.top)
        }
    }
    
    func changeBottomNavStatus(type: Int) {
        DataManager.shared.bottomNavState = type
        
        if type == 0 {
            bottomNavBackground.backgroundColor = UIColor.hubRedColor
            
            bottomNavState.text = R.strings.alarm
            bottomNavState.textColor = UIColor.white
            
            bottomNavSettings.tintColor = UIColor.hubMainColor
        } else if type == 1 {
            bottomNavBackground.backgroundColor = UIColor.hubGold
            
            bottomNavState.text = R.strings.criticalEvents
            bottomNavState.textColor = UIColor.hubTextBlack
            
            bottomNavSettings.tintColor = UIColor.hubTextBlack
        } else {
            bottomNavBackground.backgroundColor = UIColor.white
            bottomNavBackground.layer.cornerRadius = 0
            
            bottomNavMain.tintColor = UIColor.hubSeparator
            bottomNavScripts.tintColor = UIColor.hubSeparator
            bottomNavEvents.tintColor = UIColor.hubSeparator
            bottomNavCameras.tintColor = UIColor.hubSeparator
            bottomNavSettings.tintColor = UIColor.hubMainColor
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(100)
                make.bottom.equalToSuperview().offset(-50)
            }
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(1)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
        }
        
        if type != 2 {
            bottomNavBackground.layer.cornerRadius = 32
            bottomNavBackground.layer.masksToBounds = true
            bottomNavBackground.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            bottomNavMain.tintColor = UIColor.white
            bottomNavScripts.tintColor = UIColor.white
            bottomNavEvents.tintColor = UIColor.white
            bottomNavCameras.tintColor = UIColor.white
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(0)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(150)
                make.bottom.equalToSuperview().offset(-50)
            }
        }
    }
    
    func showAlert() {
        alertContainer.subviews.forEach { $0.removeFromSuperview() }
        
        view.addSubview(shadowLayer)
        shadowLayer.addSubview(alertContainer)
        alertContainer.addSubview(alertTitle)
        
        shadowLayer.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        if alertType == 3 {
            alertTitle.attributedText =
                textInAlert.withBoldText(
                    text: "Security-hub.ru",
                    font: alertTitle.font
                )
        } else {
            alertTitle.text = textInAlert
        }
        
        switch alertType {
        case 1:
            alertContainer.addSubview(alertText)
            alertContainer.addSubview(alertButtonContinue)
            alertContainer.addSubview(alertButtonCancel)
            
            alertButtonContinue.text = R.strings.button_call
            alertButtonCancel.text = R.strings.button_cancel
            
            alertContainer.snp.remakeConstraints { make in
                make.height.equalTo(DataManager.settingsHelper.needPin ? 260 : 170)
                make.centerY.equalToSuperview().offset(-100)
                make.leading.equalToSuperview().offset(40)
                make.trailing.equalToSuperview().offset(-40)
            }
            
            alertTitle.snp.remakeConstraints { make in
                make.top.equalToSuperview().offset(24)
                make.leading.equalToSuperview().offset(20)
                make.trailing.equalToSuperview().offset(-20)
            }
            
            alertText.snp.remakeConstraints { make in
                make.top.equalTo(alertTitle.snp.bottom).offset(10)
                make.leading.equalToSuperview().offset(20)
                make.trailing.equalToSuperview().offset(-20)
            }
            
            alertButtonCancel.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-25)
                make.leading.equalToSuperview().offset(35)
            }
            
            alertButtonContinue.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-25)
                make.trailing.equalToSuperview().offset(-35)
            }
            
            if DataManager.settingsHelper.needPin {
                alertContainer.addSubview(alertPinText)
                alertContainer.addSubview(alertPinInput)
                alertContainer.addSubview(alertPinDots)
                alertContainer.addSubview(alertPinError)
                
                alertPinError.isHidden = true
                
                alertPinText.snp.remakeConstraints { make in
                    make.top.equalTo(alertText.snp.bottom).offset(26)
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                alertPinDots.snp.remakeConstraints { make in
                    make.top.equalTo(alertPinText.snp.bottom).offset(20)
                    make.height.equalTo(8)
                    make.leading.equalToSuperview().offset(85)
                    make.trailing.equalToSuperview().offset(-85)
                }
                
                alertPinError.snp.remakeConstraints { make in
                    make.top.equalTo(alertPinDots.snp.bottom).offset(10)
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                alertPinInput.addTarget(
                    self,
                    action: #selector(pinInputChanged),
                    for: .editingChanged
                )
                alertPinInput.text!.removeAll()
                alertPinInput.becomeFirstResponder()
                
                showDots()
            }
            
        case 2:
            alertContainer.snp.remakeConstraints { make in
                make.height.equalTo(100)
                make.centerY.equalToSuperview()
                make.leading.equalToSuperview().offset(40)
                make.trailing.equalToSuperview().offset(-40)
            }
            
            alertTitle.snp.remakeConstraints { make in
                make.centerY.equalToSuperview()
                make.leading.equalToSuperview().offset(43)
                make.trailing.equalToSuperview().offset(-43)
            }
        
        case 3:
            alertContainer.addSubview(alertButtonContinue)
            alertContainer.addSubview(alertButtonCancel)
            
            alertButtonContinue.text = R.strings.yes
            alertButtonCancel.text = R.strings.no
            
            alertContainer.snp.remakeConstraints { make in
                make.height.equalTo(300)
                make.centerY.equalToSuperview()
                make.leading.equalToSuperview().offset(40)
                make.trailing.equalToSuperview().offset(-40)
            }
            
            alertTitle.snp.remakeConstraints { make in
                make.top.equalToSuperview().offset(34)
                make.leading.equalToSuperview().offset(20)
                make.trailing.equalToSuperview().offset(-20)
            }
            
            alertButtonCancel.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-35)
                make.leading.equalToSuperview().offset(55)
            }
            
            alertButtonContinue.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-35)
                make.trailing.equalToSuperview().offset(-55)
            }
            
        default:
            alertContainer.addSubview(alertButtonContinue)
            alertContainer.addSubview(alertButtonCancel)
            
            alertButtonContinue.text = R.strings.yes
            alertButtonCancel.text = R.strings.no
            
            alertContainer.snp.remakeConstraints { make in
                make.height.equalTo(200)
                make.centerY.equalToSuperview()
                make.leading.equalToSuperview().offset(40)
                make.trailing.equalToSuperview().offset(-40)
            }
            
            alertTitle.snp.remakeConstraints { make in
                make.top.equalToSuperview().offset(34)
                make.leading.equalToSuperview().offset(20)
                make.trailing.equalToSuperview().offset(-20)
            }
            
            alertButtonCancel.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-35)
                make.leading.equalToSuperview().offset(55)
            }
            
            alertButtonContinue.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-35)
                make.trailing.equalToSuperview().offset(-55)
            }
        }
        
        shadowLayer.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.shadowLayer.alpha = 1
        }
    }
    
    func callTechSupport() {
        guard let number = URL(string: "tel://88001008945") else { return }
        UIApplication.shared.open(number)
        print("Called")
    }
    
    @objc func showDots() {
        alertPinDots.subviews.forEach { $0.removeFromSuperview() }
        
        for i in 1...4 {
            let dot = UIView()
            
            dot.layer.cornerRadius = 4
            dot.backgroundColor =
                i <= alertPinInput.text!.count
                ? UIColor.hubMainColor
                : UIColor.hubGrey
            
            dot.snp.remakeConstraints { make in
                make.width.height.equalTo(8)
            }
            
            alertPinDots.addArrangedSubview(dot)
        }
    }
    
    @objc func pinInputChanged() {
        alertPinError.isHidden = true
        
        if alertPinInput.text!.count > 4 {
            alertPinInput.deleteBackward()
        } else {
            showDots()
        }
    }
    
    @objc func bottomNavClicked(gesture: UITapGestureRecognizer) {
        var viewControllers = nV!.viewControllers
        _ = viewControllers.popLast()
        
        switch gesture.view {
        case bottomNavMain:
            viewControllers.append(XMainController(type: .site, name: nil))
        case bottomNavScripts:
            viewControllers.append(XScriptsController())
        case bottomNavEvents:
            viewControllers.append(AllEventsController())
        case bottomNavCameras:
            viewControllers.append(CameraListController())
        default:
            viewControllers.append(XSettingsController())
        }
        
        nV!.setViewControllers(viewControllers, animated: false)
    }
    
    @objc func stateClick(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.alertType = 2
                self.textInAlert = self.bottomNavState.text!
                
                self.showAlert()
            }
        }
    }
    
    @objc func sectionClicked(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                gesture.view!.alpha = 1
            }
            
            techText.textColor = UIColor.orange
        } else if gesture.state == .changed {
            if gesture.location(in: gesture.view).y > gesture.view!.bounds.maxY || gesture.location(in: gesture.view).y < gesture.view!.bounds.minY {
                UIView.animate(withDuration: 0.3) {
                    gesture.view!.alpha = 0.02
                }
                
                self.techText.textColor = UIColor.hubTextBlack
            }
        } else if gesture.state == .ended {
            if gesture.view!.alpha == 1 {
                UIView.animate(withDuration: 0.3) {
                    gesture.view!.alpha = 0.02
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.alertType = 1
                    self.textInAlert = R.strings.techSupport
                    
                    self.techText.textColor = UIColor.hubTextBlack
                    
                    self.showAlert()
                }
            }
        }
    }
    
    @objc func alertContinue(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if self.alertType == 1 {
                    if !DataManager.settingsHelper.needPin || (DataManager.settingsHelper.needPin && self.alertPinInput.text == DataManager.shared.getUser().pin) {
                        self.callTechSupport()
                        
                        self.hideAlert(gesture: nil)
                    } else {
                        self.alertPinError.isHidden = false
                    }
                } else if self.alertType == 3 {
                    if let url = URL(string: "\(XTargetUtils.site!)") {
                        UIApplication.shared.open(url)
                    }
                    
                    self.hideAlert(gesture: nil)
                } else if self.alertType == 4 {
                    _ = DataManager.shared.exitIvi().subscribe(onNext: { _ in
                        DataManager.settingsHelper.ivideonToken = ""
                        
                        self.textInAlert = R.strings.ivideon_disconnected
                        self.alertType = 2
                        
                        self.showAlert()
                    })
                    
                    self.hideAlert(gesture: nil)
                }
            }
        }
    }
    
    @objc func openProfileSettings() {
        nV?.pushViewController(
            ProfileSettingsController(),
            animated: true
        )
    }
    
    @objc func hideAlert(gesture: UILongPressGestureRecognizer?) {
        if gesture != nil && gesture!.view != shadowLayer {
            if gesture!.state == .began {
                UIView.animate(withDuration: 0.1) {
                    gesture!.view!.transform =
                        CGAffineTransform(scaleX: 0.9, y: 0.9)
                }
            } else if gesture!.state == .ended {
                alertPinInput.resignFirstResponder()
                
                UIView.animate(withDuration: 0.1) {
                    gesture!.view!.transform = CGAffineTransform.identity
                }
                
                UIView.animate(withDuration: 0.3) {
                    self.alertContainer.frame =
                        CGRect(
                            x: self.alertContainer.frame.minX,
                            y: self.alertContainer.superview!.frame.midY - self.alertContainer.frame.height / 2,
                            width: self.alertContainer.frame.width,
                            height: self.alertContainer.frame.height)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    UIView.animate(withDuration: 0.3) {
                        self.shadowLayer.alpha = 0
                    }
                }
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.shadowLayer.alpha = 0
            }
            
            alertPinInput.resignFirstResponder()
        }
    }
}

extension String {
    
    func withBoldText(text: String, font: UIFont? = nil) -> NSAttributedString {
        let _font = font ?? UIFont.systemFont(ofSize: 14, weight: .regular)
        let fullString = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: _font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: _font.pointSize)]
        let range = (self as NSString).range(of: text)
        
        fullString.addAttributes(boldFontAttribute, range: range)
        
        return fullString
    }
}
