import UIKit
import RAMAnimatedTabBarController

class XMenuController: RAMAnimatedTabBarController, UITabBarControllerDelegate {
    
    private var oldIndex = 0
    private var isMenuHidden = false
    private var menuList: [(controller: UIViewController, image: UIImage?)] = []
    var i: RAMAnimatedTabBarItem?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        DataManager.shared.setFCM()
        
        menuList.append((XMainController(type: .site, name: nil), XImages.menu_main))
        menuList.append((XScriptsController(), XImages.menu_script))
        menuList.append((AllEventsController(), XImages.menu_history))
        menuList.append((CameraListController(), XImages.menu_cameras))
        menuList.append((XSettingsController(), XImages.menu_menu))
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        tabBar.isTranslucent = false
        
        var k = 0
        var navigations: [UIViewController] = []
        
        menuList.forEach { item in
            let navigation = UINavigationController()
            navigation.isNavigationBarHidden = false
            let tabItem = RAMAnimatedTabBarItem(title: nil, image: item.image, selectedImage: nil)
            tabItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 100)
            tabItem.animation = XRAMBounceAnimation()
            tabItem.yOffSet = -5
            if (k == 1) {i = tabItem}
            k+=1
            navigation.viewControllers = [item.controller]
            navigation.tabBarItem = tabItem
            navigation.isNavigationBarHidden = true
            navigations.append(navigation)
        }
        
        viewControllers = navigations
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if (selectedIndex == 0 && viewController.children.count > 0 && oldIndex == 0) {
            (menuList[selectedIndex].controller as! XMainController).tabItemClick()
        }
        
        oldIndex = selectedIndex
    }
    
    func setTabBarHidden(_ isHidden: Bool){
        if isHidden == isMenuHidden { return }
        isMenuHidden = isHidden
        
        guard let items = tabBar.items as? [RAMAnimatedTabBarItem] else { return }
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (isHidden ? height : -height)
        
        for item in items{
            if let v = item.iconView?.icon.superview {
                v.frame = v.frame.offsetBy(dx: 0, dy: offsetY)
            }
        }
            
        self.view.frame =
            CGRect(x: 0, y: 30,
                    width: UIScreen.main.bounds.width,
                    height: UIScreen.main.bounds.height + (isHidden ? height : 0)
            )
    }
}

class XRAMBounceAnimation : RAMItemAnimation {
    
    override func playAnimation(_ icon: UIImageView, textLabel: UILabel) {
        XAnimationUtils.playBounceAnimation(icon)
        icon.image = icon.image?.withColor(color: DEFAULT_SELECTED)
    }
    
    override func deselectAnimation(_ icon: UIImageView, textLabel: UILabel, defaultTextColor: UIColor, defaultIconColor: UIColor) {
        icon.image = icon.image?.withColor(color: DEFAULT_UNSELECTED)
    }
    
    override func selectedState(_ icon: UIImageView, textLabel: UILabel) {
        icon.image = icon.image?.withColor(color: DEFAULT_SELECTED)
    }
}
