//
//  IvideonApi.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import Foundation
import RxSwift
import SwiftHTTP
import Starscream
import RxCocoa
import UIKit

class IvideonApi {
    public static let urlIvideonApi = "https://openapi-alpha-eu01.ivideon.com"
//    static var authUrl : String{
//        switch XTargetUtils.target {
//        case "com.ttk.dpk":         return "https://89.232.115.81/iv.php?session="
//        case "com.teko.dpk":        return "https://test.opasnost.net/iv.php?session="
//        case "com.teko.dev":        return "https://dev.security-hub.ru/iv.php?session="
//        case "com.teko.omicron":    return "https://80.76.185.205/iv.php?session="
//        case "com.teko.uvo.krasnodar":    return "https://83.239.21.74/iv.php?session="
//        default:                return "https://cloud.security-hub.ru/iv.php?session="
//        }
//    }
    
    func getCameraList(accessToken: String) -> Observable<[Camera]?> {
        return Observable<[Camera]?>.create{ observer -> Disposable in
            HTTP.POST("\(IvideonApi.urlIvideonApi)/cameras?op=FIND&access_token=\(accessToken)",parameters: nil,headers: nil,requestSerializer: JSONParameterSerializer(), completionHandler:{ response in
                do{
                    let json = try JSONSerialization.jsonObject(with: response.data)
                    let result = IvideonResponse(json: json as! [String : Any])
                    if (result?.success)!{
                        DataManager.shared.getIviRelations()
                        observer.onNext( CameraList(json: (result?.result)!)!.items);observer.onCompleted()
                    }else if response.statusCode == 401 {
                        observer.onNext( nil);observer.onCompleted()
                    }else{
                        observer.onError(NSError(domain: (result?.message)!, code: response.statusCode!, userInfo: nil));observer.onCompleted()
                    }
                }catch{ observer.onError(error);observer.onCompleted() }
            })
            return Disposables.create{}
            }
    }

    static func getCameraPreviewImg(accessToken: String, id: String) -> UIImage? {
        do {
            let data = try Data(contentsOf: URL(string: "\(urlIvideonApi)/cameras/\(id)/live_preview?access_token=\(accessToken)")!)
            return UIImage(data: data)
        } catch {
            return #imageLiteral(resourceName: "camera_btn")
        }
    }

    static func getLiveUrl(accessToken: String, id: String) -> String! {
        return "\(urlIvideonApi)/cameras/\(id)/live_stream?access_token=\(accessToken)&format=hls&q=" + "0"
    }

    static func updateCameraName(_ camera: Camera, accessToken: String, value: String) -> Observable<Bool> {
        return Observable.create{ observer -> Disposable in
            let url = IvideonApi.urlIvideonApi + "/cameras/"+camera.id+"/name?op=SET&access_token=" + accessToken
            _ = HTTP.POST(url, parameters: ["value" : value], requestSerializer: JSONParameterSerializer()) { response in
                if response.statusCode == 200 { observer.onNext(true) } else{ observer.onNext(false) }
            }
            return Disposables.create()
            }
        .observe(on: MainScheduler.init())
        .subscribe(on: CurrentThreadScheduler.instance)
    }
}

