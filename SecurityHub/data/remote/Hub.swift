//
//  HubCommand.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import UIKit
//import Localize_Swift

enum HubCommand: String {
    case EVENT             = "EVENT"
    case EVENTS            = "EVENTS"
    case AFFECT_RMV        = "AFFECT_RMV"
    case SITES             = "SITES"
    case SITE_UPD          = "SITE_UPD"
    case SITE_RMV          = "SITE_RMV"
    case SECTION_ZONE_UPD  = "SECTION_ZONE_UPD"
    case SECTION_ZONE_RMV  = "SECTION_ZONE_RMV"
    case OPERATORS         = "OPERATORS"
    case OPERATOR_UPD      = "OPERATOR_UPD"
    case OPERATOR_RMV      = "OPERATOR_RMV"
    case ROLES_GET         = "ROLES_GET"
    case ROLES_UPD         = "ROLES_UPD"
    case CONNECTION_DROP   = "CONNECTION_DROP"
    case IV_GET_RELATIONS  = "IV_GET_RELATIONS"
    case COMMAND_RESULT_UPD = "COMMAND_RESULT_UPD"
    case CLUSTERS          = "CLUSTERS"
    case DEVICE_SITES      = "DEVICE_SITES"
    case DEVICE_SITE_UPD   = "DEVICE_SITE_UPD"
    case DEVICE_USER_RMV   = "DEVICE_USER_RMV"
    case DEVICE_USER_UPD   = "DEVICE_USER_UPD"
    case DEVICE_SITE_RMV   = "DEVICE_SITE_RMV"
    case COMMAND_SET       = "COMMAND_SET"
    case FIREBASE_ID_SET = "FIREBASE_ID_SET"
    case FIREBASE_ID_DEL = "FIREBASE_ID_DEL"
    case FIREBASE_GET_SOUNDS = "FIREBASE_GET_SOUNDS"
    case REFERENCES        = "REFERENCES"
    case SCRIPTS           = "SCRIPTS"
    case DEVICE_SCRIPT_UPD = "DEVICE_SCRIPT_UPD"
    case DEVICE_SCRIPT_RMV = "DEVICE_SCRIPT_RMV"
}

enum HubCommandWithId:String {
    case DEVICE_ASSIGN_DOMAIN = "DEVICE_ASSIGN_DOMAIN"
    case DEVICE_ASSIGN_SITE   = "DEVICE_ASSIGN_SITE"
    case DEVICE_REVOKE_DOMAIN = "DEVICE_REVOKE_DOMAIN"
    case DELEGATE_REQ      = "DELEGATE_REQ"
    case DELEGATE_REVOKE   = "DELEGATE_REVOKE"
    case ZONE_SET          = "ZONE_SET"
    case ZONE_NAME         = "ZONE_NAME"
    case ZONE_DEL          = "ZONE_DEL"
    case SITE_SET          = "SITE_SET"
    case SITE_DEL          = "SITE_DEL"
    case OPERATOR_SET      = "OPERATOR_SET"
    case OPERATOR_DEL      = "OPERATOR_DEL"
    case COMMAND_SET       = "COMMAND_SET"
    case IV_BIND_CAM       = "IV_BIND_CAM"
    case IV_DEL_RELATION   = "IV_DEL_RELATION"
    case IV_DEL_USER       = "IV_DEL_USER"
    case SET_LOCALE        = "SET_LOCALE"
    case FIREBASE_SET_SOUND = "FIREBASE_SET_SOUND"
    case SCRIPT_SET        = "SCRIPT_SET"
    case SCRIPT_DEL        = "SCRIPT_DEL"
}

enum HubCommandWithResult:String {
    case DELEGATE_NEW           = "DELEGATE_NEW"
    case IV_GET_TOKEN           = "IV_GET_TOKEN"
    case OPERATOR_SAFE_SESSION  = "OPERATOR_SAFE_SESSION"
    case SITE_DELEGATES         = "SITE_DELEGATES"
    case LOCALES                = "LOCALES"
    case DEVICES                = "DEVICES"
    case FIREBASE_GET_SOUNDS    = "FIREBASE_GET_SOUNDS"
    case LIBRARY_SCRIPTS        = "LIBRARY_SCRIPTS"
}

class HubResponseCode{
    static let NO_SIGNAL_CODE: Int64            = -777;
    static let NO_RESPONSE_CODE: Int64          = -778;
    static let RECONNECT_CODE: Int64            = -888;
    static let DEVICE_ARMED_CODE: Int64         = -889;
    static let NO_SIGNAL_DEVICE_CODE: Int64     = -890;
    
    static func getError (error : Int64) -> String {
        switch error {
        case -1:    return R.strings.error_minus_1
        case -2:    return R.strings.error_minus_2
        case -3:    return R.strings.error_minus_3
        case -4:    return R.strings.error_minus_4
        case -5:    return R.strings.error_minus_5
        case -6:    return R.strings.error_minus_6
        case -7:    return R.strings.error_minus_7
        case -8:    return R.strings.error_minus_8
        case -9:    return R.strings.error_minus_9
        case -10:   return R.strings.error_minus_10
        case -11:   return R.strings.error_minus_11
        case -21:   return R.strings.error_minus_21
        case -22:   return R.strings.error_minus_22
        case -23:   return R.strings.error_minus_23
        case -24:   return R.strings.error_minus_24
        case -25:   return R.strings.error_minus_25
        case -27:   return R.strings.error_minus_27
        case -50:   return R.strings.error_minus_50
        case -61:   return R.strings.error_minus_61
        case -62:   return R.strings.error_minus_62
        case -100:  return R.strings.error_minus_100
        case -101:  return R.strings.error_minus_101
        case NO_SIGNAL_CODE:            return R.strings.error_no_signal
        case NO_RESPONSE_CODE:          return R.strings.error_no_response
        case RECONNECT_CODE:            return R.strings.error_reconnect
        case DEVICE_ARMED_CODE:         return R.strings.error_device_armed
        case NO_SIGNAL_DEVICE_CODE:     return R.strings.error_no_signal_device
        default:                        return R.strings.error
        }
    }
}

class HubConst{
    static let SECTION_TYPE_SECURITY: Int64 = 1
    static let SECTION_TYPE_ALARM: Int64 = 2
    static let SECTION_TYPE_FIRE: Int64 = 3
    static let SECTION_TYPE_FIRE_DOUBLE: Int64 = 4
    static let SECTION_TYPE_TECHNOLOGIES: Int64 = 5
    static let SECTION_TYPE_TECHNOLOGIES_DISARMED: Int64 = 6
    
    //TODO: нет картинки в других таргетах section_no_type
    static func getSectionsTypes() -> [Int64:(name: String, color: UIColor, icon: String)] {
        return [
            0                                           : (name: R.strings.section_no_type,
                                                           color: UIColor.hubGreenColor,
                                                           icon: XImages.section_no_type_name),
            HubConst.SECTION_TYPE_SECURITY              : (name: R.strings.section_secutiry,
                                                           color: UIColor.hubGreenColor,
                                                           icon: XImages.section_alarm_default_name),
            HubConst.SECTION_TYPE_ALARM                 : (name: R.strings.section_alarm,
                                                           color: UIColor.hubYellowColor,
                                                           icon: XImages.section_alarm_default_name),
            HubConst.SECTION_TYPE_FIRE                  : (name: R.strings.section_fire,
                                                           color: UIColor.hubRedColor,
                                                           icon: XImages.section_fire_default_name),
            HubConst.SECTION_TYPE_FIRE_DOUBLE           : (name: R.strings.section_fire_double,
                                                           color: UIColor.hubRedColor,
                                                           icon: XImages.section_fire_default_name),
            HubConst.SECTION_TYPE_TECHNOLOGIES          : (name: R.strings.section_technologies,
                                                           color: UIColor.gray,
                                                           icon: XImages.section_leak_default_name),
            HubConst.SECTION_TYPE_TECHNOLOGIES_DISARMED : (name: R.strings.section_technologies_disrmed,
                                                           color: UIColor.darkGray,
                                                           icon: XImages.section_leak_default_name)
        ]
    }
    
    static func getSectionsHUBTypes() -> [DSectionTypeEntity] {
        return [
            DSectionTypeEntity(id: HubConst.SECTION_TYPE_SECURITY,      name: R.strings.section_secutiry, color: UIColor.hubGreenColor, icon: #imageLiteral(resourceName: "sec_type_security")),
            DSectionTypeEntity(id: HubConst.SECTION_TYPE_ALARM,         name: R.strings.section_alarm, color: UIColor.hubYellowColor, icon: #imageLiteral(resourceName: "sec_type_alarm")),
            DSectionTypeEntity(id: HubConst.SECTION_TYPE_FIRE,          name: R.strings.section_fire, color: UIColor.hubRedColor, icon: #imageLiteral(resourceName: "sec_type_fire")),
            DSectionTypeEntity(id: HubConst.SECTION_TYPE_TECHNOLOGIES,  name: R.strings.section_technologies, color: UIColor.gray, icon: #imageLiteral(resourceName: "sec_type_tech")),
        ]
    }
    
    static func getZonesOutTypes() -> [(index: Int64, name: String)]{
        return [
            (index: 0x01, name: R.strings.zone_out_type_1),
            (index: 0x02, name: R.strings.zone_out_type_2),
            (index: 0x03, name: R.strings.zone_out_type_3),
            (index: 0x04, name: R.strings.zone_out_type_4),
            (index: 0x05, name: R.strings.zone_out_type_5)
        ]
    }
    
    static func  getUIDTypes() -> [Int64:String]{
        return [
            1 : R.strings.uid_type_1,
            2 : R.strings.uid_type_2,
            3 : R.strings.uid_type_3
        ]
    }
    
    static func  getRIMNames() -> [String:(String,UIImage?)]{
        return [
            "3": (R.strings.astra_8, XImages.astra_8),
            "4": (R.strings.astra_361, XImages.astra_361),
            "5": (R.strings.astra_3731, XImages.astra_3731),
            "6": (R.strings.astra_ri_rpdr, XImages.astra_ri_rpdr),
            "7": (R.strings.astra_3221, XImages.astra_3221),
            "8": (R.strings.astra_rpdk, XImages.astra_rpdk),
            "9": (R.strings.astra_3531, XImages.astra_3531),
            "A": (R.strings.astra_6131, XImages.astra_6131),
            "B": (R.strings.astra_3321, XImages.astra_3321),
            "C": (R.strings.astra_5131, XImages.astra_5131),
            "D": (R.strings.astra_421, XImages.astra_421),
            "E": (R.strings.astra_rim_E, nil)
        ]
    }
    
    static func  getRKNames() -> [String:(String,UIImage?)]{
        return [
            "1B22": (R.strings.astra_8731, XImages.astra_8731),
            "2720": (R.strings.astra_8231, XImages.astra_8231),
            "1B09": (R.strings.astra_2331, XImages.astra_2331)
        ]
    }
    
    static func getСhargingText() -> [Int64:String]{
        return [
            -1 : R.strings.charging_code_minus_1,
             0 : R.strings.charging_code_0,
             1 : R.strings.charging_code_1
        ]
    }
    
    static func getRebootText() -> [Int64:String]{
        return [
            0 : R.strings.reboot_code_0,
            1 : R.strings.reboot_code_1,
            2 : R.strings.reboot_code_2,
            3 : R.strings.reboot_code_3,
            4 : R.strings.reboot_code_4
        ]
    }
    
    static func getPhysicTypes(type: Int64) -> String{
        switch type {
        case 1:     return R.strings.physic_1
        case 2:     return R.strings.physic_2
        case 3:     return R.strings.physic_3
        case 4:     return R.strings.physic_4
        case 5:     return R.strings.physic_5
        case 6:     return R.strings.physic_6
        case 7:     return R.strings.physic_7
        case 8:     return R.strings.physic_8
        case 9:     return R.strings.physic_9
        case 10:    return R.strings.physic_10
        case 11:    return R.strings.physic_11
        case 12:    return R.strings.physic_12
        case 13:    return R.strings.physic_13
        case 14:    return R.strings.physic_14
        case 16:    return R.strings.physic_16
        case 17:    return R.strings.physic_17
        case 29:    return R.strings.physic_29
        case 30:    return R.strings.physic_30
        case 31:    return R.strings.physic_31
        case 32:    return R.strings.physic_32
        case 33:    return R.strings.physic_33
        case 34:    return R.strings.physic_34
        case 35:    return R.strings.physic_35
        case 36:    return R.strings.physic_36
        case 37:    return R.strings.physic_37
        case 38:    return R.strings.physic_38
        case 39:    return R.strings.physic_39
        case 40:    return R.strings.physic_40
        case 41:    return R.strings.physic_41
        case 42:    return R.strings.physic_42
        case 43:    return R.strings.physic_43
        case 44:    return R.strings.physic_44
        case 45:    return R.strings.physic_45
        case 46:    return R.strings.physic_46
        case 47:    return R.strings.physic_47
        case 48:    return R.strings.physic_48
        case 49:    return R.strings.physic_49
        case 50:    return R.strings.physic_50
        case 51:    return R.strings.physic_51
        case 52:    return R.strings.physic_52
        case 256:   return R.strings.physic_256
        default:    return R.strings.physic_0
        }
    }
    
    static func getDeviceTypeInfo(_ cV: Int64, cluster: Int64) -> DDeviceTypeInfo {
        let configVersion = UInt32(cV)
        switch cluster {
        case 1:
            switch (configVersion >> 8) & 255 {
            case 1:
                switch configVersion & 255 {
                case 1:     return DDeviceTypeInfo(type: .SecurityHub, name: R.strings.device_sh_v1,   icon: XImages.device_astra_sh_name)
                case 2:     return DDeviceTypeInfo(type: .SecurityHub, name: R.strings.device_sh_v1_1, icon: XImages.device_astra_sh_name)
                case 3:     return DDeviceTypeInfo(type: .SecurityHub, name: R.strings.device_sh_v2,   icon: XImages.device_astra_sh_name)
                case 4:     return DDeviceTypeInfo(type: .SecurityHub, name: R.strings.device_sh_4g,   icon: XImages.device_astra_sh_name)
                default:    return DDeviceTypeInfo(type: .SecurityHub, name:R.strings.device_sh,      icon: XImages.device_astra_sh_name)
                }
            case 2:     return DDeviceTypeInfo(type: .Astra, name: R.strings.device_astra_dozor,   icon: XImages.device_astra_dozor_name)
            case 10:    return DDeviceTypeInfo(type: .Astra, name: R.strings.device_astra_8945,    icon: XImages.device_astra_8945_name)
            case 11:    return DDeviceTypeInfo(type: .Astra, name: R.strings.device_astra_812,     icon: XImages.device_astra_812_name)
            default:    return DDeviceTypeInfo(type: .Astra, name: R.strings.device_astra,         icon: XImages.device_astra_name)
            }
        case 2:     return DDeviceTypeInfo(type: .Dozor,          name: R.strings.device_astra_dozor,   icon: XImages.device_astra_dozor_name)
        case 3:     return DDeviceTypeInfo(type: .SecurityButton, name: R.strings.device_mobile_button, icon: XImages.device_mobile_button_name)
        default:    return DDeviceTypeInfo(type: .Other,          name: R.strings.device_no_name,       icon: XImages.device_astra_name)
        }
    }
    
    static func isHub(_ cV: Int64, cluster: Int64) -> Bool {
        let configVersion = UInt32(cV)
        let _c = (configVersion >> 8) & 255
        return cluster == 1 && _c == 1
    }
    
    static func getHubVersion(_ cV: Int64) -> String? {
        let configVersion = UInt32(cV)
        let first = (configVersion >> 24) & 255, second  = (configVersion >> 16) & 255
        return "\(first).\(second)"
    }

    static func isRelay(_ detector: Int64) -> Bool { return detector == 23 || detector == 13 }
    static func isExit(_ detector: Int64) -> Bool  { return detector == 14 || detector == 15 }

    
    static func getCommandResults(code: Int64) -> String {
        switch code {
        case 1:     return R.strings.command_result_1
        case 2:     return R.strings.command_result_2
        case 3:     return R.strings.command_result_3
        case 4:     return R.strings.command_result_4
        case 5:     return R.strings.command_result_5
        case 6:     return R.strings.command_result_6
        case 7:     return R.strings.command_result_7
        case 8:     return R.strings.command_result_8
        case 9:     return R.strings.command_result_9
        case 10:    return R.strings.command_result_10
        case 11:    return R.strings.command_result_11

        case 64: return R.strings.command_result_64
        case 65: return R.strings.command_result_65
        case 66: return R.strings.command_result_66
        case 67: return R.strings.command_result_67
        case 68: return R.strings.command_result_68
        case 69: return R.strings.command_result_69
        case 70: return R.strings.command_result_70
        case 71: return R.strings.command_result_71
        case 72: return R.strings.command_result_72
        case 73: return R.strings.command_result_73
        case 74: return R.strings.command_result_74
        case 75: return R.strings.command_result_75
        case 76: return R.strings.command_result_76
        case 77: return R.strings.command_result_77
        case 78: return R.strings.command_result_78
        case 79: return R.strings.command_result_79
        case 80: return R.strings.command_result_80
        default: return R.strings.command_result_no
        }
    }
    
    static func getCommandResultsNegative(code: Int64) -> String {
        switch code {
        case  -5:     return R.strings.command_negative_result_5
        case -10:     return R.strings.command_negative_result_10
        case -20:     return R.strings.command_negative_result_20
        default:      return R.strings.command_negative_result_no
        }
    }
    
    static func getDetectors(_ d: Int64) -> String {
        switch d {
        case 1:     return R.strings.detectors_1
        case 2:     return R.strings.detectors_2
        case 3:     return R.strings.detectors_3
        case 4:     return R.strings.detectors_4
        case 5:     return R.strings.detectors_5
        case 6:     return R.strings.detectors_6
        case 7:     return R.strings.detectors_7
        case 8:     return R.strings.detectors_8
        case 9:     return R.strings.detectors_9
        case 10:    return R.strings.detectors_10
        case 11:    return R.strings.detectors_11
        case 12:    return R.strings.detectors_12
        case 13:    return R.strings.detectors_13
        case 14:    return R.strings.detectors_14
        case 15:    return R.strings.detectors_15
        case 16:    return R.strings.detectors_16
        case 17:    return R.strings.detectors_17
        case 18:    return R.strings.detectors_18
        case 19:    return R.strings.detectors_19
        case 20:    return R.strings.detectors_20
        case 21:    return R.strings.detectors_21
        case 22:    return R.strings.detectors_22
        case 23:    return R.strings.detectors_23
        case 24:    return R.strings.detectors_24
        case 25:    return R.strings.detectors_25
        case 26:    return R.strings.detectors_26
        case 40:    return R.strings.detectors_25
        case 41:    return R.strings.detectors_25
        case 42:    return R.strings.detectors_25
        default:    return R.strings.detectors_0
        }
    }
    
    static func getDefaultSectionType(_ sectionMask: Int) -> Int64? {
        for i in 0...5 {
            let value = Int(pow(Double(2), Double(i)))
            if (sectionMask & value) == value { return Int64(i + 1) }
        }
        
        
        if (sectionMask & 1) == 1 { return HubConst.SECTION_TYPE_SECURITY }
        if (sectionMask & 2) == 2 { return HubConst.SECTION_TYPE_ALARM }
        if (sectionMask & 4) == 4 { return HubConst.SECTION_TYPE_FIRE }
        if (sectionMask & 8) == 8 { return SECTION_TYPE_FIRE_DOUBLE }
        if (sectionMask & 16) == 16 { return HubConst.SECTION_TYPE_TECHNOLOGIES }
        if (sectionMask & 32) == 16 { return HubConst.SECTION_TYPE_TECHNOLOGIES }
        return nil
    }
}

