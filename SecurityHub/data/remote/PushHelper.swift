//
//  PushHelper.swift
//  SecurityHub
//
//  Created by Timerlan on 26.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import UIKit
import RxSwift

class PushHelper{
    
    init(app : UIApplication, appDelegate : AppDelegate) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = appDelegate
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = appDelegate
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            app.registerUserNotificationSettings(settings)
        }
        app.registerForRemoteNotifications()
        FirebaseApp.configure()
    }
    
    func getDeviceToken() -> Data? {
        return Messaging.messaging().apnsToken
    }
    
    func getFcmToken() -> String? {
        return Messaging.messaging().fcmToken
    }
}


