//
//  DM+XHubOutProtocol.swift
//  SecurityHub
//
//  Created by Timerlan on 23/07/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Foundation


extension DataManager : XHubOutProtocol {
    func addReference(command_id: Int64, name: String) { dbHelper.addReference(command_id: command_id, name: name) }
}
