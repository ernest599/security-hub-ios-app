//
//  DBHelper+IVIDEON.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//IVIDEON
extension DBHelper{
    func add(ivVideo: IvVideo){
        let _ = add(ivVideo, table: self.main.ivVideo)
        NotificationCenter.default.post(name: HubNotification.ivVideo(ivVideo.eventId), object: ivVideo)
        NotificationCenter.default.post(name: HubNotification.ivVideo(camId: ivVideo.camId), object: ivVideo)
    }
    
    func get(ivVideo_id: Int64) -> IvVideo? {
        if let row = try? main.db.pluck(main.ivVideo.filter(IvVideo.eventId == ivVideo_id)){
            return IvVideo.fromRow(row) }
        return nil
    }
    
    func getEvents(cam_id: String) -> [(event: Events, iv: IvVideo)] {
        var result: [(event: Events, iv: IvVideo)] = []
        guard let rows = try? main.db.prepare(main.ivVideo.filter(IvVideo.camId == cam_id)) else { return result }
        for row in rows {
            let video: IvVideo = IvVideo.fromRow(row)
            if let r = try? main.db.pluck(main.events.filter(Events.id == video.eventId)) {
                let event: Events = Events.fromRow(r)
                result.append((event: event, iv: video))
            }
        }
        return result
    }
    
    func getCamSet(cam_id: String) -> CamSet{
        if let row = try? self.main.db.pluck(self.main.camSet.filter(CamSet.camid == cam_id)) {
            return CamSet.fromRow(row)
        }else{
            let newSet = CamSet(camid: cam_id)
            newSet.insert(main.db, table: main.camSet)
            return newSet
        }
    }
    
    func updCamSet(camSet: CamSet){ camSet.update(main.db, table: main.camSet) }
}
