//
//  DBHelper+EVENTS.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//EVENTS
extension DBHelper{
    func isEventInDb(_ id: Int64) -> Bool {
        if let _ = try? main.db.pluck(main.events.filter(Events.id == id)) { return true }
        return false
    }
    
    func getReferenceName(_ command_id: Int64) -> String? {
        if let name = try? main.db.scalar("SELECT name FROM [References] WHERE command_id = \(command_id)") as? String { return name }
        return nil
    }
    
    func addReference(command_id: Int64, name: String) {
        References(command_id: command_id, name: name).insert(main)
    }
    
    func clearAffects() -> [DAffectRelation]{
        let query = "SELECT device, section, zone From Events Where affect = 1 GROUP BY device, section, zone"
        
        var result: [DAffectRelation] = []
        guard let rows = try? main.db.prepare(query) else { return result}
        for row in rows { if let aInfo = DataEntityMapper.affectRelation(row) { result.append(aInfo) } }
        let _ = try? main.db.run(main.events.update(Events.affect <- 0))
        return result
    }
    
    func setAffect(_ event_id: Int64) {
        let _ = try? main.db.run(main.events.filter(Events.id == event_id).update(Events.affect <- 1))
    }
    
    
    func clearAffect(_ event_id: Int64) -> DAffectRelation? {
        let query = "SELECT device, section, zone FROM Events WHERE id = \(event_id)"
        
        var result: DAffectRelation?
        guard let rows = try? main.db.prepare(query) else { return result}
        if  let row = rows.first(where: { (_) -> Bool in return true }),
            let aInfo = DataEntityMapper.affectRelation(row) {
            result = aInfo
        }
        let _ = try? main.db.run(main.events.filter(Events.id == event_id).update(Events.affect <- 0))
        return result
    }
    
    ////////////////////////////////////////////////////////////
    
    
    func add(event: Events){
        if event._class == 15 && event.detector == 0 && (event.reason == 2 || event.reason == 1) {
            return
        }
        NotificationCenter.default.post(name: HubNotification.eventUpdate, object: (event: event, type: add(event, table: main.events)))
    }
  
    func deleteEvents(device_id: Int64? = nil, section_id: Int64? =  nil, zone_id: Int64? = nil) {
        do{
            try main.db.run( main.events.filter(device_id == nil || Events.device == device_id ?? 0).filter(section_id == nil || Events.section == section_id ?? 0).filter(zone_id == nil || Events.zone == zone_id ?? 0).delete())
        }catch {}
    }
    
    func get(_class: Int64, reason: Int64, active: Int64, device_id: Int64) -> Events? {
        let query = "SELECT * FROM Events WHERE device = \(device_id) AND _class = \(_class) AND reason = \(reason) AND active = \(active) AND id = (SELECT MAX(id) FROM Events WHERE device = \(device_id) AND _class = \(_class) AND reason = \(reason) AND active = \(active))"
        guard let rows = try? main.db.prepare(query), let row = rows.first(where: { _ in return true }) else { return nil }
        return CastHelper.events(obj: row)
    }
    
    func get(event_id: Int64) -> Events? {
        if let row = try? main.db.pluck(main.events.filter(Events.id == event_id)) { return Events.fromRow(row) }
        return nil
    }
    
    func rmvAffects(event_id: Int64) -> Events?{
        if let q = ((try? main.db.pluck(main.events.filter(Events.id == event_id))) as Row??),
            let row = q {
            var event: Events = Events.fromRow(row)
            event.affect = 0; event.update(main.db, table: main.events)
            return Events.fromRow(row)
        };return nil
    }
    
    func rmvAffects(){
        do {
            try main.db.run(main.events.update(Events.affect <- 0))
        }catch {}
    }
    
    func getLastEventTime() -> Int64 {
        if let time = try? main.db.scalar(main.events.select(Events.time.max)) { return time }
        return 0
    }
    
    func getMaxTime(device_id: Int64? = nil, section_id: Int64? = nil, zone_id: Int64? = nil) -> Int64{
        return (try? main.db.scalar(
            main.events
                .filter(device_id == nil || Events.device == device_id ?? 0)
                .filter(section_id == nil || Events.section == section_id ?? 0)
                .filter(zone_id == nil || Events.zone == zone_id ?? 0)
                .select(Events.time.max)
            )) ?? 0
    }
    
    func getEvents(device_id: Int64? = nil, section_id: Int64? = nil, zone_id: Int64? = nil) -> [Events]{
        var result: [Events] = []
        guard let rows = try? main.db.prepare(
            main.events
                .filter(device_id == nil || Events.device == device_id ?? 0)
                .filter(section_id == nil || Events.section == section_id ?? 0)
                .filter(zone_id == nil || Events.zone == zone_id ?? 0)
                .order(Events.time.desc)
            ) else { return result }
        for row in rows { result.append(Events.fromRow(row)) }
        return result
    }
    
    func getAlarmEvents() -> [Events] {
        var result: [Events] = []
        
        for event in getEvents() {
            if event._class >= 0 && event._class <= 3 && event.affect == 1 {
                result.append(event)
            }
        }
        
        return result
    }
}
