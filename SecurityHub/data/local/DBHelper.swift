//
//  RealmHelper.swift
//  SecurityHub test
//
//  Created by Timerlan on 13.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import SQLite
import RxSwift

class DBHelper {
    var main: DB!
    let classes: DBClasses = DBClasses()
    func reinit(){ ThreadUtil.shared.backOpetarion.sync { main = DB(login: DataManager.shared.getUser().login) } }
    
    init() {
        DispatchQueue.main.async {
            while self.main == nil {
                self.reinit()
                
                sleep(1)
            }
        }
    }
    
    func add<T:SQLCommand>(_ obj: T, table: Table) -> NUpdateType{
        let inDB = obj.isWasInDb(self.main.db, table: table)
        if !inDB { obj.insert(self.main.db, table: table) }else{ obj.update(self.main.db , table: table) }
        return inDB ? NUpdateType.update : NUpdateType.insert
    }
    
    func getName(device_id: Int64, section_id: Int64 = 0, zone_id: Int64 = 0) -> String{
        if  let q = ((try? self.main.db.scalar("SELECT name FROM Devices Where id = \(device_id)")) as Binding??),
            let name = q as? String  { return name
        } else if section_id != 0 {
            guard let q = ((try? self.main.db.scalar("SELECT name FROM Sections Where device = \(device_id) AND section = \(section_id)")) as Binding??),
                let name = q as? String else { return "no_name" }
            return "\(name)(\(section_id)), \(getName(device_id: device_id))"
        } else if zone_id != 0 {
            guard let q = ((try? self.main.db.scalar("SELECT name FROM Zones Where device = \(device_id) AND section = \(section_id) AND zone = \(zone_id)")) as Binding??),
                let name = q as? String
                else { return "no_name" }
            return "\(name)(\(zone_id)), \(getName(device_id: device_id, section_id: section_id))"
        } else{ return "no_name" }
    }
    
    func getAffects(site_id: Int64? = nil, device_id: Int64? = nil, section_id: Int64? = nil, zone_id: Int64? = nil) -> [Events] {
        if site_id != nil && device_id == nil && section_id == nil && zone_id == nil { return getAffects(site_id: site_id!) }
        if site_id == nil && device_id != nil && section_id == nil && zone_id == nil { return getAffects(device_id: device_id!) }
        if site_id == nil && device_id != nil && section_id != nil && zone_id == nil { return getAffects(device_id: device_id!, section_id: section_id!) }
        if site_id == nil && device_id != nil && section_id != nil && zone_id != nil { return getAffects(device_id: device_id!, section_id: section_id!, zone_id: zone_id!) }
        return []
    }
}

enum ARMSTATUS { case arm, disarm, notfull, notsecure, device, on, off }
