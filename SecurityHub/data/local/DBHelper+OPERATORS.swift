//
//  DBHelper+OPERATORS.swift
//  SecurityHub
//
//  Created by Timerlan on 15.08.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

//OPERATORS
extension DBHelper{
    func getOperatorName(_ id: Int64) -> String? {
        if let name = try? main.db.scalar("SELECT name From Operators Where id = \(id)") as? String { return name }
        return nil
    }
    
    /////
    
    func add(operators: Operators){
        NotificationCenter.default.post(name: HubNotification.operatorsUpdate, object: (operators: [operators], type: add(operators, table: main.operators) ))
    }
    
    func deleteOperators(operator_id: Int64? = nil, time: Int64? = nil) {
        var query = main.operators
        if let operator_id = operator_id { query = query.filter(Operators.id == operator_id) }
        if let time = time { query = query.filter(Operators.time != time) }
        guard let rows = try? main.db.prepare(query) else { return }
        for row in rows {
            let operators: Operators = Operators.fromRow(row)
            NotificationCenter.default.post(name: HubNotification.operatorsUpdate, object: (operators: [operators], type: NUpdateType.delete ))
            operators.delete(main.db, table: main.operators)
        }
    }
    
    func get(operator_id: Int64) -> Operators? {
        if let row = try? main.db.pluck(main.operators.filter(Operators.id == operator_id)) { return Operators.fromRow(row) }
        return nil
    }
    
    func getOperators() -> [Operators] {
        var result: [Operators] = []
        guard let rows = try? main.db.prepare(main.operators.order(Operators.id.asc)) else { return result }
        for obj in rows { result.append(Operators.fromRow(obj)) }
        return result
    }
}
