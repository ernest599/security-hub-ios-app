//
//  Settings.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

class SettingsHelper {
    private let defaults = UserDefaults.standard
    private static let defaultsHelper = DefaultsHelper()
    private let NEED_PUSH = "NEED_PUSHH" + SettingsHelper.defaultsHelper.login
    private let NEED_PIN = "NEED_PINN" + SettingsHelper.defaultsHelper.login
    private let MASK_PUSH = "MASK_PUSH" + SettingsHelper.defaultsHelper.login
    private let MASK_STORY = "MASK_STORY" + SettingsHelper.defaultsHelper.login
    private let STRING_IVI_TOKEN = "STRING_IVI_TOKEN" + SettingsHelper.defaultsHelper.login
    private let NEED_TOUCH_ID = "NEED_TOUCH_ID" + SettingsHelper.defaultsHelper.login
    
    private let NEED_ANIMATION = "NEED_ANIMATION" + SettingsHelper.defaultsHelper.login
    private let NEED_DEVICES = "NEED_DEVICES" + SettingsHelper.defaultsHelper.login
    private let NEED_SECTIONS = "NEED_SECTIONSS" + SettingsHelper.defaultsHelper.login
    private let NEED_EXPERT = "NEED_EXPERT" + SettingsHelper.defaultsHelper.login


    var needPush : Bool {
        get { return !defaults.bool(forKey: NEED_PUSH) }
        set { set(value: !newValue, forKey: NEED_PUSH)}
    }
    
    var pushMask : Int {
        get {
            let o = defaults.integer(forKey: MASK_PUSH)
            if o == 0 { return 31 }
            if o == -1 { return 0 }
            return o
        }
        set {
            if newValue == 0 {
                set(value: -1, forKey: MASK_PUSH)
            }else{
                set(value: newValue, forKey: MASK_PUSH)
            }
        }
    }
    
    var storyMask : Int {
        get {
            let o = defaults.integer(forKey: MASK_STORY)
            if !needExpert { return 31 }
            if o == 0 { return 31 }
            if o == -1 { return 0 }
            return o
        }
        set {
            if newValue == 0 {
                set(value: -1, forKey: MASK_STORY)
            }else{
                set(value: newValue, forKey: MASK_STORY)
            }
        }
    }
    
    var ivideonToken : String {
        get { return defaults.string(forKey: STRING_IVI_TOKEN) ?? "" }
        set { set(value: newValue, forKey: STRING_IVI_TOKEN)}
    }
    
    var needPin : Bool {
        get { return !defaults.bool(forKey: NEED_PIN) }
        set { set(value: !newValue, forKey: NEED_PIN)}
    }
    
    var touchId : Bool {
        get { return defaults.bool(forKey: NEED_TOUCH_ID) }
        set { set(value: newValue, forKey: NEED_TOUCH_ID) }
    }
    
    var needAnimation : Bool {
        get { return defaults.bool(forKey: NEED_ANIMATION) }
        set { set(value: newValue, forKey: NEED_ANIMATION) }
    }
    
    var needDevices : Bool {
        get {
            if !needExpert { return false }
            return defaults.bool(forKey: NEED_DEVICES)
        }
        set { set(value: newValue, forKey: NEED_DEVICES) }
    }
    
    var needSections : Bool {
        get {
            if !needExpert { return false }
            return defaults.bool(forKey: NEED_SECTIONS)
        }
        set { set(value: newValue, forKey: NEED_SECTIONS) }
    }
    
    var needExpert : Bool {
        get { return defaults.bool(forKey: NEED_EXPERT) }
        set { set(value: newValue, forKey: NEED_EXPERT) }
    }
    
    func setDefaults(){
//        defaults.register(defaults: [
//            NEED_PUSH: true,
//            NEED_PIN: true,
//            MASK_PUSH: 31,
//            MASK_STORY: 31,
//            NEED_TOUCH_ID: false,
//            NEED_ANIMATION: true,
//            NEED_DEVICES: false,
//            NEED_SECTIONS: false
//        ])
    }

    private func set(value: Any, forKey: String) {
        defaults.set(value, forKey: forKey)
        defaults.synchronize()
    }
}
