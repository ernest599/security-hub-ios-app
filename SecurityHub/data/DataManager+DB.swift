//
//  DataManager+DB.swift
//  SecurityHub
//
//  Created by Timerlan on 24/06/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import RxSwift

extension DataManager {
    func getSiteInfo(site_id: Int64) -> Single<DSiteInfo>{
        let single = Single<DSiteInfo>.create { event -> Disposable in
            let dSI = self.dbHelper.getSiteInfo(site_id: site_id)
            event(SingleEvent.success(dSI))
            return Disposables.create { }
        }
        return single.subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    func isAvailableDevice(device_id: Int64) -> Single<DCommandResult> {
        let single = Single<DCommandResult>.create { event -> Disposable in
            if !self.dbHelper.isDeviceConnected(device_id: device_id) { event(SingleEvent.success(DCommandResult.NO_SIGNAL_DEVICE)) }
            else if self.dbHelper.getArmStatus(device_id: device_id) != .disarmed { event(SingleEvent.success(DCommandResult.DEVICE_ARMED)) }
            else { event(SingleEvent.success(DCommandResult.SUCCESS)) }
            return Disposables.create { }
        }
        return single.subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .utility))
    }
    
    func getHubDeviceIds(site_id: Int64? = nil) -> [Int64] {
        return dbHelper.getDeviceIds(site_id: site_id, onlyHubId: true)
    }
    
    func isHub(device_id: Int64) -> Bool {
        return dbHelper.isHub(device_id: device_id)
    }
    
    func getDeviceIds(site_id: Int64) -> [Int64] {
        return dbHelper.getDeviceIds(site_id: site_id)
    }
    
    func getDeviceInfo(device_id: Int64) -> DDeviceWithSitesInfo? {
        return dbHelper.getDevice(device_id: device_id)
    }
    
    func getDeviceSections(device_id: Int64) -> [Int64: DSectionNameAndTypeEntity] {
        return dbHelper.getSectionsNames(device_id: device_id)
    }
    
    func getScriptCommands(device_id: Int64, zone_id: Int64) -> [String : Int64] {
        return dbHelper.getScripts(device_ids: [device_id]).first { $0.bind == zone_id }?.getCommands() ?? [:]
    }
    
    ///ADD SCRIPT UI
    func getOnlyZonesScriptUi(device_id: Int64, notSelectValue: String? = nil) -> [(key: Int64, value: String)] {
        var result: [Int64 : String] = [ : ]
        if let notSelectValue = notSelectValue { result.updateValue(notSelectValue, forKey: 0) }
        dbHelper.getZones()
            .filter { $0.zone.device == device_id && $0.zone.zone != 100 && !HubConst.isRelay($0.zone.detector) && !HubConst.isExit($0.zone.detector) }
            .forEach { result.updateValue($0.zone.name, forKey: $0.zone.zone) }
        return Array(result).sorted { $0.key < $1.key }
    }
    func getSectionsScriptUi(device_id: Int64, notSelectValue: String? = nil) -> [(key: Int64, value: String)] {
        var result: [Int64 : String] = [ : ]
        if let notSelectValue = notSelectValue { result.updateValue(notSelectValue, forKey: 0) }
        dbHelper.getSections()
            .filter{ $0.section.device == device_id && $0.section.section != 0 }
            .forEach { result.updateValue($0.section.name, forKey: $0.section.section) }
        return Array(result).sorted { $0.key < $1.key }
    }
    func getAutoRelayScriptUi(device_id: Int64, notSelectValue: String) -> [(key: Int64, value: String)] {
        var result: [Int64 : String] = [ 0 : notSelectValue ]
        dbHelper.getZones()
            .filter { $0.zone.device == device_id && $0.zone.detector == 13 }
            .forEach { result.updateValue($0.zone.name, forKey: $0.zone.zone) }
        return Array(result).sorted { $0.key < $1.key }
    }
}
