struct UserData {
    static let shared = UserData()
    
    var events: [Events]!

    private init() { }
}
