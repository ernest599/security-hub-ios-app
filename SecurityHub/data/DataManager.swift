//
//  DataManager.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import UIKit
import RappleProgressHUD
import RxSwift
import Localize_Swift
import RxCocoa

class DataManager {
    var disposables: CompositeDisposable = CompositeDisposable()
    let queue = DispatchQueue.global(qos: .utility)
    let nCenter = NotificationCenter.default

    func dispose(_ key: CompositeDisposable.DisposeKey?) {
        guard let key = key else { return }
        disposables.remove(for: key)
    }
        
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static var shared: DataManager!
    static let defaultHelper = DefaultsHelper()
    static var settingsHelper = SettingsHelper()
    
    var events = BehaviorRelay<[Events]>(value: [])
    var bottomNavState = 2
    
    let hubHelper: HubHelper = HubHelper()
    private let ivideonApi: IvideonApi = IvideonApi()
    var dbHelper: DBHelper
    var pushHelper: PushHelper
    private var connectionStateChange: Any?
    
    
    var scriptsLibrary: [Int64 : HubLibraryScripts] = [:]

    
    init(app : UIApplication, appDelegate : AppDelegate) {
        pushHelper = PushHelper(app : app, appDelegate : appDelegate)
        dbHelper = DBHelper()
        ThreadUtil.shared.backOpetarion.async { if self.dbHelper.needJson(){ self.getJson() } }
        Localize.setCurrentLanguage(DataManager.defaultHelper.language)
    }
    
    private func getJson() {
        var bundle = Bundle.main;
        if let path = Bundle.main.path(forResource: Localize.currentLanguage(), ofType: "lproj"), let bun = Bundle(path: path) { bundle = bun }
        if let path = bundle.path(forResource: "events", ofType: "json"), let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
            let classes = JSONClasses(json: try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! [String : Any])
            var result: [jClasses] = []
            for cl in classes!.classes {
                for cRea in cl.reasons{
                    for cDet in cRea.detectors{
                        for cStat in cDet.statements{
                            result.append(jClasses(_class: cl.id, reasons: cRea.id, detectors: cDet.id, statements: cStat.id, name: cStat.name, icon: cDet.iconSmall, iconBig: cStat.icon, affect_desc: cStat.desc))
                        }
                    }
                    for cReaStat in cRea.statements{
                         result.append(jClasses(_class: cl.id, reasons: cRea.id, detectors: nil, statements: cReaStat.id, name: cReaStat.name, icon: cRea.iconSmall, iconBig: cReaStat.icon, affect_desc: cReaStat.desc))
                    }
                }
                for clDet in cl.detectors{
                    for clDetStat in clDet.statements{
                        result.append(jClasses(_class: cl.id, reasons: nil, detectors: clDet.id, statements: clDetStat.id, name: clDetStat.name, icon: clDet.iconSmall, iconBig: clDetStat.icon, affect_desc: clDetStat.desc))
                    }
                }
                for clStat in cl.statements{
                     result.append(jClasses(_class: cl.id, reasons: nil, detectors: nil, statements: clStat.id, name: clStat.name, icon: cl.iconSmall, iconBig: clStat.icon, affect_desc: clStat.desc))
                }
                result.append(jClasses(_class: cl.id, reasons: -1, detectors: -1, statements: -1, name: cl.name, icon: cl.iconSmall, iconBig: cl.iconSmall, affect_desc: cl.affect))
            }
            dbHelper.addJson(result)
        }
    }
    
    func getZoneTypes() -> DTypesEntity {
        var bundle = Bundle.main;
        if  let path = Bundle.main.path(forResource: Localize.currentLanguage(), ofType: "lproj"),
            let bun = Bundle(path: path) { bundle = bun }
        if  let path = bundle.path(forResource: "types", ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as? [String : Any] {
            return DTypesEntity(json: json)
        }
        return DTypesEntity(json: [:])
    }
    
    public func getAllClasses() -> Observable<[(id: Int64, name: String)]>{
        return Observable.just(dbHelper.getAllClasses()).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getClasses() -> [(id: Int64, name: String)]{
        return dbHelper.getAllClasses()
    }
    
    public static var connectDisposable: Disposable?
    public func connect() -> Observable<Bool>{
        return Observable<Bool>.create{ observer in
            if self.hubHelper.isAlive() { observer.onNext(true);observer.onCompleted(); return Disposables.create{} }
            if self.getUser().login.count > 0 && self.getUser().pin.count > 0 {
                self.hubHelper.connect(login: self.getUser().login, pass: self.getUser().password, self, self)
                self.connectionStateChange = NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: OperationQueue.main){ notification in
                    NotificationCenter.default.removeObserver(self.connectionStateChange!)
                    let code = notification.object as? Int ?? 0
                    observer.onNext(code > 0 || code == HubResponseCode.RECONNECT_CODE); observer.onCompleted()
                    if code > 0 || code == HubResponseCode.RECONNECT_CODE {
                        self.dbHelper.reinit()
//                        DataManager.settingsHelper.setDefaults()
                    }
                    //UI message in BASEVIEW
                }
            }else{ observer.onNext(false);observer.onCompleted() }
            return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func connect(login: String, password: String) -> Observable<(success: Bool, errorMes: String?)>{
        return Observable.create{ observer in
            self.hubHelper.connect(login: login, pass: password, self, self)
            self.connectionStateChange = NotificationCenter.default.addObserver(forName: HubNotification.connectionStateChange, object: nil, queue: OperationQueue.main ){ notification in
                NotificationCenter.default.removeObserver(self.connectionStateChange!)
                let code = notification.object as? Int64 ?? 0
                if code > 0 {
                    DataManager.defaultHelper.login = login;
                    DataManager.defaultHelper.password = password;
                    self.dbHelper.reinit();                    
                }
                observer.onNext((success: code > 0, errorMes: HubResponseCode.getError(error: code)));observer.onCompleted()
            };
            return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public static var disconnectDisposable: Disposable?
    public func disconnect() -> Observable<Any>{
        return Observable<Any>.create{ observer in
            DataManager.shared.bottomNavState = 2
            
            DataManager.shared.events = BehaviorRelay<[Events]>(value: [])
            DataManager.shared.events.accept([])
            
            self.deleteFCM()
            self.hubHelper.disconnect()
            DataManager.defaultHelper.clear()
            DataManager.settingsHelper.setDefaults()
            self.dbHelper.reinit()
            observer.onNext(1); observer.onCompleted();
            return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getUser() -> User{
        let dH = DataManager.defaultHelper
        return User(login: dH.login, password: dH.password, pin: dH.pin, id: dH.id, domainId: dH.domainId, orgId: dH.orgId, roles: dH.roles)
    }
    
    public func clearAfterLang(){
        dbHelper.clearJClass()
        getJson()
        dbHelper.deleteEvents()
        hubHelper.disconnect()
    }
        
    public func setPin(_ pin: String){ DataManager.defaultHelper.pin = pin }
    
    public func getSites(completable: Bool = false)->Observable<(site: Sites, type: NUpdateType)>{
        return Observable.create{ obs in
            self.dbHelper.getSites().forEach({ (s) in
                obs.onNext((site: s, type: .insert))
            })
            if completable { obs.onCompleted() }
            let o = NotificationCenter.default.addObserver(forName: HubNotification.siteUpdate, object: nil, queue: OperationQueue.main) { not in
                obs.onNext(not.object as! (site: Sites, type: NUpdateType))
            }
            return Disposables.create{ NotificationCenter.default.removeObserver(o) }
        }.subscribe(on: ThreadUtil.shared.backScheduler)
    }
    
    public func getSections(site: Int64? = nil, device: Int64)->Observable<[Sections]>{
        return Observable.just(dbHelper.getSections(site_id: site, device_id: device)).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getSections(site_id: Int64? = nil, device_id: Int64)-> [Sections] {
        return dbHelper.getSections(site_id: site_id, device_id: device_id)
    }
    
    public func getEmptySectionIndex(device_id: Int64)-> Int64? {
        var indexes: [Int64] = Array(1...8)
        dbHelper.getSections(site_id: nil, device_id: device_id).forEach { (s) in
            indexes = indexes.filter({ (i) -> Bool in i != s.section })
        }
        return indexes.first
    }
    
    public func getSection(device: Int64, section: Int64) -> Observable<Sections?> {
        return Observable.just(dbHelper.get(device_id: device, section_id: section)).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getSection(device: Int64, section: Int64) -> Sections? { return dbHelper.get(device_id: device, section_id: section) }
    
    public func getZone(device: Int64, section: Int64, zone: Int64) -> Zones? { return dbHelper.get(device_id: device, section_id: section, zone_id: zone) }
    
    public func getName(device: Int64, section: Int64 = 0, zone: Int64 = 0) -> Observable<String>{
        return Observable.just(dbHelper.getName(device_id: device, section_id: section, zone_id: zone)).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getDeviceName(device: Int64) -> String{
        return dbHelper.getName(device_id: device)
    }
   
    public func getEvent(_class: Int64, reason: Int64, active: Int64, device: Int64) -> Observable<Events?>{
        return Observable.just(dbHelper.get(_class: _class, reason: reason, active: active, device_id: device)).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getEvents() -> Observable<(event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)>{
        return Observable<(event: Events, siteIds: [Int64], type: NUpdateType, isFromBD: Bool)>.create{ obs in
            self.dbHelper.getEvents().forEach({ (e) in
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.device)
                    .filter{ (rel) -> Bool in return rel.device == e.device && rel.section == e.section }
                    .map{ (rel) -> Int64 in return rel.site }
                obs.onNext((event: e, siteIds: siteIds, type: .insert, isFromBD: true))
            })
            let o = NotificationCenter.default.addObserver(forName: HubNotification.eventUpdate, object: nil, queue: OperationQueue.main) { notification in
                guard let e = notification.object as? (event: Events, type: NUpdateType) else { return }
                let siteIds = self.dbHelper.getSiteDeviceSectionIds(device: e.event.device)
                    .filter{ (rel) -> Bool in return rel.device == e.event.device && rel.section == e.event.section }
                    .map{ (rel) -> Int64 in return rel.site }
                obs.onNext((event: e.event, siteIds: siteIds, type: e.type, isFromBD: false))
            }
            return Disposables.create{ NotificationCenter.default.removeObserver(o) }
        }.subscribe(on: ThreadUtil.shared.backScheduler)
    }
    
    public func getNameSites(_ device_id: Int64) -> String {
        return dbHelper.getDeviceSitesInfo(device_id: device_id).siteNames
    }
    
    public func findVideo(_ id: Int64) -> Observable<IvVideo?>{
        return Observable.just(dbHelper.get(ivVideo_id: id)).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getZoneRelation(cam: String) -> Observable<[SelectElement]> {
        return Observable.create{ observer in
            var result: [SelectElement] = []
            for site in self.dbHelper.getSites() {
                var seS: [SelectElement] = []
                for sec in self.dbHelper.getSections(site_id: site.id){
                    var zS: [SelectElement] = []
                    for zone in self.dbHelper.getZones(device_id: sec.device, section_id: sec.section){
                        if let rel = self.relation.first(where: {$0.device == zone.device && $0.section == zone.section && $0.zone == zone.zone}){
                            if rel.camId == cam{ zS.append(SelectElement(name: zone.name, selected: true, obj: zone)) }
                        }else{
                            zS.append(SelectElement(name: zone.name, selected: false, obj: zone))
                            
                        }
                    }
                    seS.append(SelectElement(name: sec.name, list: zS))
                }
                result.append(SelectElement(name: site.name, list: seS))
            }
            observer.onNext(result);
            observer.onCompleted()
        return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func getCamSet(_ cam: String) -> Observable<CamSet> {
        return Observable.just(dbHelper.getCamSet(cam_id: cam)).subscribe(on: ConcurrentMainScheduler.instance).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func updCamSet(_ set: CamSet) { ThreadUtil.shared.backOpetarion.sync { self.dbHelper.updCamSet(camSet: set) }  }
    
    func getEvents(camId: String) -> Observable<[(event: Events, iv: IvVideo)]> {
        return Observable.create{ observer in
            observer.onNext(self.dbHelper.getEvents(cam_id: camId))
            NotificationCenter.default.addObserver(forName: HubNotification.ivVideo(camId: camId), object: nil, queue: OperationQueue.main){ notification in
                //let video = notification.object as! IvVideo
                observer.onNext(self.dbHelper.getEvents(cam_id: camId))
            }
            return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func getEvent(id: Int64) -> Events? { return self.dbHelper.get(event_id: id) }
    
    func getOperator() -> Operators { return dbHelper.get(operator_id: getUser().id)! }
    func getOperator(_ id: Int64) -> Operators? { return dbHelper.get(operator_id:id) }
    func getSitesCount() -> Int { return dbHelper.getSites().count }
    
    private var operatorNotification: Any?
    public func getOperators()->Observable<(operators: [Operators], type: NUpdateType)>{
        return Observable.create{ observer in
            observer.onNext((operators: self.dbHelper.getOperators().filter({ (op) -> Bool in op.id != self.getUser().id }),
                             type: .insert))
            if self.operatorNotification != nil{ NotificationCenter.default.removeObserver(self.operatorNotification!) }
            self.operatorNotification = NotificationCenter.default.addObserver(forName: HubNotification.operatorsUpdate, object: nil, queue: OperationQueue.main){ notification in
                let op = notification.object as! (operators: [Operators], type: NUpdateType)
                observer.onNext((operators: op.operators.filter({ (op) -> Bool in op.id != self.getUser().id }),
                                 type: op.type))
            }
            return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public var clusters: HubClusters?
    public var relation: [HubRelation] = []
    public var sounds: [String:Any] = [:]
    
    public func getDevice(device: Int64)->Observable<Devices?>{
        return Observable.just(dbHelper.get(device_id: device)).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getDeviceUsers(device_ids: [Int64]) -> Observable<(deviceUsers: [DeviceUsers], type: NUpdateType)>{
        return Observable.create{ observer in
            observer.onNext((deviceUsers: self.dbHelper.getDeviceUsers(device_ids: device_ids), type: .insert))
            device_ids.forEach({ (device) in
                NotificationCenter.default.addObserver(forName: HubNotification.deviceUsersUpdate(device), object: nil, queue: OperationQueue.main){ notification in
                    observer.onNext(notification.object as! (deviceUsers: [DeviceUsers], type: NUpdateType)) }
            })
            return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    public func getUserSection(user: DeviceUsers) -> Observable<[Int64]>{
        return Observable.just(dbHelper.getUserSectionIds(deviceUser: user)).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    public func addIvVideo(_ ivVideo: IvVideo){ ThreadUtil.shared.backOpetarion.sync { self.dbHelper.add(ivVideo: ivVideo) } }
    
//    func add(_ obj: SQLCommand) -> Observable<Bool>{
//        return Observable.create{ obs in self.dbHelper.add(obj);obs.onNext(true); obs.onCompleted(); return Disposables.create{} }
//        .subscribeOn(ThreadUtil.shared.backScheduler).observeOn(ThreadUtil.shared.backScheduler)
//    }

    private func rmvAffects(id: Int64? = nil) -> Observable<Events?>{
        return Observable.create{ obs in
            if id != nil{ obs.onNext(self.dbHelper.rmvAffects(event_id: id!))
            }else{ self.dbHelper.rmvAffects();obs.onNext(nil)
            }; obs.onCompleted(); return Disposables.create{}
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
}

//HUBHELPER Command
extension DataManager{
    func updateSites() { hubHelper.setCommand(HubCommand.SITES) }
    func updateOperators() { hubHelper.setCommand(HubCommand.OPERATORS) }
    func updateAffects() {
        self.hubHelper.setCommand(HubCommand.EVENTS, D: ["affect" : 1], id: HubHelper.AFFECT_COMMAND_ID)
    }
//    func updateEvents() {
//         _ = getMaxTime().subscribe(onNext: { time in
//            let ntime = Int64(NSDate().timeIntervalSince1970 - 86400 * 4)
////            self.hubHelper.setCommand(HubCommand.EVENTS, D: ["tmin": time > ntime ? time : ntime])
//            self.hubHelper.setCommand(HubCommand.EVENTS, D: ["tmin": time > ntime, "affect" : 0])
//         })
//    }
    func updateClusters() { hubHelper.setCommand(HubCommand.CLUSTERS) }
    
    
    func delete(site_id: Int64) -> Observable<Bool> { return hubHelper.setCommandWhithId(.SITE_DEL, D: ["site": site_id]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler) }
    func addSite(name: String, sn: String, cluster: Int) -> Observable<Bool> {
        let pincode = Int(sn[sn.count-4..<sn.count]) ?? 0
        let account = Int(sn[0..<sn.count-4]) ?? 0
        return hubHelper.setCommandWhithId(.DEVICE_ASSIGN_DOMAIN, D: ["account": account, "pin_code": pincode, "new_site_name": name, "cluster_id": cluster])
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    func addDevice(site: Int64, sn: String, cluster: Int) -> Observable<Bool> {
        let pincode = Int(sn[sn.count-4..<sn.count]) ?? 0
        let account = Int(sn[0..<sn.count-4]) ?? 0
        return hubHelper.setCommandWhithResult(.DEVICES, D: [:])
        .map({ (r) -> [HubDevice] in return HubDevices.init(json: r.d)?.items ?? [] })
        .concatMap({ (devices) -> Observable<Bool> in
            if let d = devices.first(where: {$0.account == account }){ return self.hubHelper.setCommandWhithId(.DEVICE_ASSIGN_SITE, D: ["site_id": site, "device_id": d.id]) }
            else{ return self.hubHelper.setCommandWhithId(.DEVICE_ASSIGN_DOMAIN, D: ["account": account, "pin_code": pincode, "cluster": cluster], whithError: false)
                .catch({ (e) -> Observable<Bool> in return Observable.just(true) })
                .concatMap({ (b) -> Observable<Bool> in return self.addDevice(site: site, sn: sn, cluster: cluster) }) }
        }).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    func removeDevice(device_id: Int64, site: Int64 = 0) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.DEVICE_REVOKE_DOMAIN, D: ["device_id" : device_id, "site": site]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func ussd(device: Int64, text: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["command": ["USSD":["text":text]], "device":device]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler) }
    func addSection(device: Int64, name: String?,  index: Int64? = nil, type: Int64? = nil, needError: Bool = true) -> Observable<Bool> {
        var params: [String:Any] = [:]
        if name != nil { params.updateValue(name!, forKey: "name") }
        if index != nil { params.updateValue(index!, forKey: "index") }
        if type != nil { params.updateValue(type!, forKey: "type") }
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["SECTION_SET" : params ]], whithError: needError).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)}
    func deleteSectionZone(device: Int64, section: Int64, zone: Int64 = 0) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.ZONE_DEL, D: ["device_id" : device, "device_section": section, "zone": zone]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler) }
    func deleteSection(device: Int64, index: Int64) -> Observable<Bool> {
        return Observable<Any>.create({ obs in
            if self.dbHelper.getZones(device_id: device, section_id: index).count == 0 { obs.onNext(true) }
            else{ obs.onError(MyError.haveZones) }
            obs.onCompleted();return Disposables.create { }
        }).concatMap({ (b) -> Observable<Bool> in
            return self.hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["SECTION_DEL" : ["index" : index]] ])
        }).subscribe(on: ThreadUtil.shared.backScheduler)
    }
    
    func updateZoneName(device: Int64, section: Int64, zone: Int64, name: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.ZONE_NAME, D: ["device_id": device, "section" : section, "zone" : zone, "name" : name ]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler) }

    func startInteractiveMode(device_id: Int64) {
        ThreadUtil.shared.backOpetarion.sync { hubHelper.setCommand(.COMMAND_SET, D: ["device" : device_id, "command" : ["INTERACTIVE" : 1]]) }
    }
    
    func stopInteractiveMode(device_id: Int64) {
        ThreadUtil.shared.backOpetarion.sync { hubHelper.setCommand(.COMMAND_SET, D: ["device" : device_id, "command" : ["INTERACTIVE" : 0]]) }
    }
    
    func reset(device: Int64, section: Int64, zone: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["RESET" : ["section" : section, "zone" : zone]]])
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }

    func addZone(device: Int64, uid: String, section_id: Int64, name: String, delay: Int? = nil , detector: Int? = nil) -> Observable<Bool> {
        let delay = delay == nil ? "" : ", \"delay\": \(delay!)"
        let detector = detector == nil ? "" : ", \"detector\":\(detector!)"
        let text = "{ \"ZONE_SET\": { \"uid\": \"\(uid)\", \"section\": \(section_id), \"name\": \"\(name)\" \(delay) \(detector) }}"
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command": text])
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func deregisterZone(device: Int64, section: Int64, zone: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["ZONE_DEL" : ["section" : section, "index" : zone]] ]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler) }
    
    func delegate_Req(_ user_code: String?) -> Observable<Bool> { return hubHelper.setCommandWhithId(.DELEGATE_REQ, D: ["user_code": user_code ?? "as"]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler) }
    func delegate_delete(site: Int64, domain_id: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.DELEGATE_REVOKE, D: ["site_id": site, "domain_id" : domain_id ]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler) }
    func delegate_new(site_id: Int64) -> Observable<(result: Int64, user_code: String )> {
        return hubHelper.setCommandWhithResult(.DELEGATE_NEW, D: ["site_id": site_id ])
            .map({ (resp) -> (result: Int64, user_code: String ) in
                if let c = resp.d["user_code"] { return (result: resp.r, user_code: c as! String ) }else{ return (result: resp.r, user_code: "" ) }
            }).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    func delegate_sites(site_id: Int64) -> Observable<[HubSiteDelegate]> {
        return hubHelper.setCommandWhithResult(.SITE_DELEGATES, D: ["site_id": site_id ])
            .map({ (resp) -> [HubSiteDelegate] in return HubSiteDelegates(json: resp.d)!.items })
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func reboot(device: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["REBOOT" : [] ]]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func getIviRelations() { hubHelper.setCommand(.IV_GET_RELATIONS, D: ["device": 0,"section":0,"zone": 0]) }
    
    func getIviSaveSession() -> Observable<String> {
        return hubHelper.setCommandWhithResult(.OPERATOR_SAFE_SESSION, D: [:])
            .map{ (resp) -> String in return resp.d["session"] as? String ?? "k" }
            .subscribe(on: ThreadUtil.shared.backScheduler)
    }
    
    func getCameraList() -> Observable<[Camera]?> {
       return hubHelper.setCommandWhithResult(.IV_GET_TOKEN, D: ["domain": getUser().domainId ])
        .map{ (resp) -> String? in return resp.d["access_token"] as? String }
        .concatMap{ s -> Observable<[Camera]?> in
            if s == nil { return Observable.just(nil)
            }else{
                DataManager.settingsHelper.ivideonToken = s!
                return self.ivideonApi.getCameraList(accessToken: s!) }
        }.subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func bindCam(zone: Zones, cam: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.IV_BIND_CAM, D: ["device": zone.device, "section": zone.section, "zone": zone.zone, "id": cam]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func delRelation(zone: Zones) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.IV_DEL_RELATION, D: ["device": zone.device, "section": zone.section, "zone": zone.zone]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func changeOperator(id: Int64, value: Any, name: String) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.OPERATOR_SET, D: ["id": id, name: value]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func delOperator(id: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.OPERATOR_DEL, D: ["id": id]).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func addOperator(name: String, login: String, password: String, role: Int64 ) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.OPERATOR_SET, D: ["name" : name, "login" : login, "password" : password.md5, "roles" : role, "active" : true, "contacts" : ""])
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func exitIvi() -> Observable<Bool>{
        return hubHelper.setCommandWhithId(.IV_DEL_USER, D: ["domain" : getUser().domainId ] ).subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func setLocale(_ lan: String) -> Observable<Bool>{
        let items: [(String, Int64)] = [ ("ru", 136), ("es", 148), ("de", 33), ("en", 37)]
        if let loc = items.first(where: { (i) -> Bool in return i.0 == lan }) {
            return self.hubHelper.setCommandWhithId(.SET_LOCALE, D: ["locale_id" : loc.1 ])
                .subscribe(on: ThreadUtil.shared.backScheduler)
                .observe(on: ThreadUtil.shared.mainScheduler)
        }
        return Observable.just(false).observe(on: ThreadUtil.shared.mainScheduler)
    }
    
    func setHozOrgan(device: Int64, name: String,  index: Int64? = nil, sections: [Int64]? ) -> Observable<Bool> {
        var params: [String:Any] = ["name" : name]
        if index != nil { params.updateValue(index!, forKey: "index") }
        if sections != nil {  params.updateValue(sections!, forKey: "sections") }
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: [ "device" : device, "command" : ["USER_SET" : params ]])
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
    func delHozOrgan(device: Int64, index: Int64) -> Observable<Bool> {
        return hubHelper.setCommandWhithId(.COMMAND_SET, D: ["device" : device, "command" : ["USER_DEL" : ["index" : index] ] ] )
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
    }
}

//HUBHELPER RESULT
extension DataManager{
    public func ROLES_GET(obj: HubUserRole){
        if obj.roles & Roles.ORG_INGEN != 0 || obj.roles & Roles.ORG_ADMIN != 0 || obj.roles & Roles.ORG_BUH != 0 || obj.roles & Roles.ORG_LAYER != 0 || obj.roles & Roles.ORG_BKS_OPER != 0{
            DispatchQueue.main.async {
                //TODO localization
                AlertUtil.warAlert("Вы зашли под меж-доменным инженером. Мы не гарантируем работоспособность приложания. Некоторый функционал для вас будет ограничен. В приложении не будут работать push-уведомления, из-за большого их количества!".localized())
            }
        }
        DataManager.defaultHelper.setUserInfo(id: obj.id, domainId: obj.domainId, orgId: obj.orgId, roles: obj.roles)
    }
    
    public func OPERATORS(obj: HubOperators, time : Int64){
        _ = Observable.from(obj.items)
            .map({ (obj) -> Operators in return CastHelper.operators(obj: obj, time: time )})
            .do(onNext: { (oper) in self.dbHelper.add(operators: oper) })
            .toArray()
            .asObservable()
            .do(onNext:{ r in return self.dbHelper.deleteOperators(time: time) })
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.backScheduler)
            .subscribe()
    }
    
    public func OPERATOR_UPD(obj: HubOperator, time : Int64){
        _ = Observable.just(obj)
            .map({ (obj) -> Operators in return CastHelper.operators(obj: obj, time: time )})
            .do(onNext: { (oper) in self.dbHelper.add(operators: oper) })
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.backScheduler)
            .subscribe()
    }
    
    public func OPERATOR_RMV(operatorId: Int64){
        ThreadUtil.shared.backOpetarion.sync { self.dbHelper.deleteOperators(operator_id: operatorId) }
    }
    
    public func CONNECTION_DROP(){
        RappleActivityIndicatorView.startAnimating()
        DataManager.disconnectDisposable = disconnect().subscribe(onNext:{ a in
            DataManager.disconnectDisposable?.dispose()
            RappleActivityIndicatorView.stopAnimation()
            NavigationHelper.shared.show(SplashController())
        })
        _ = Observable<Int64>.timer(.seconds(1), scheduler: ThreadUtil.shared.backScheduler)
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.mainScheduler)
            //TODO:
        .subscribe(onNext:{ t in AlertUtil.errorAlert("CONNECTION_DROP".localized()) })
    }
    
    public func CLUSTERS(_ obj: HubClusters){ clusters = obj }
    
    public func IV_GET_RELATIONS(obj: HubRelations){
        relation = obj.items }
    
    public func ROLES_UPD(role: Int64){ }
    
    public func DEVICE_SITES(_ obj: HubDeviceSites){ }
    
    public func DEVICE_SITE_UPD(_ obj: HubDeviceSite){ }
    
    public func DEVICE_USER_UPD(_ obj: HubUser, time: Int64){
        _ = CastHelper.castUser(obj: obj, time: time)
            .do(onNext: { (sql) in
                if let deviceUsers = sql as? DeviceUsers{ self.dbHelper.add(deviceUsers: deviceUsers) }
                else if let userSection = sql as? UserSection{ self.dbHelper.add(userSection: userSection) }
            })
            .subscribe(on: ThreadUtil.shared.backScheduler).observe(on: ThreadUtil.shared.backScheduler)
            .subscribe()
    }
    
    public func DEVICE_USER_RMV(_ obj: HubUser){
        ThreadUtil.shared.backOpetarion.sync { self.dbHelper.deleteDeviceUsers(device_id: obj.device_id, userIndex: obj.user_index) }
    }
    
    public func DEVICE_SITE_RMV(){ }
    
    func FIREBASE_GET_SOUNDS(_ obj: [String:Any]) {
        
    }
}
