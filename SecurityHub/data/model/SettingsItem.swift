struct SettingsItem {
    let title: String
    let image: String
    let sectionName: String?
    let onClick: (() -> Void)
}
