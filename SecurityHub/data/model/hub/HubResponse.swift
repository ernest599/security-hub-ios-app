//
//  HubResponse.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import Gloss

public class HubResponseLang: Glossy {
       public var d : JSON = [:]
       public var id : Int64 = 0
       public var q : String = ""

       
       //MARK: Default Initializer
       init(Q: String! = "" ,D: JSON! = [:], id: Int64! = 0 )
       {
           d = D
           self.id = id
           q = Q
       }
       
       static func getFrom(text: String) -> HubResponse?{
           if let jsonData = ((try? JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? JSON) as JSON??) {
               return HubResponse(json: jsonData!)
           }
           return nil
       }
       
       //MARK: Decodable
       public required init?(json: JSON){
           if let d : JSON = "D" <~~ json {
               self.d = d
           }
           if let id : Int64 = "ID" <~~ json {
               self.id = id
           }
           if let q : String = "Q" <~~ json {
               self.q = q
           }
       }
       
       
       //MARK: Encodable
       public func toJSON() -> JSON? {
           return jsonify([
               "Q" ~~> q,
               "D" ~~> d,
               "ID" ~~> id,
               ])
       }
       
       public func toData() -> Data {
           return try! JSONSerialization.data(withJSONObject: toJSON() as Any, options: [.prettyPrinted])
       }

}

//MARK: - HubResponse
public class HubResponse: Glossy {
    public var d : JSON = [:]
    public var id : Int64 = 0
    public var q : String = ""
    public var t : Int64 = 0
    public var r : Int64 = 0

    
    //MARK: Default Initializer
    init(Q: String! = "" ,D: JSON! = [:], id: Int64! = 0, r: Int64! = 0 )
    {
        d = D
        self.id = id
        q = Q
        t = 0
        self.r = r
    }
    
    static func getFrom(text: String) -> HubResponse?{
        if let jsonData = ((try? JSONSerialization.jsonObject(with: text.data(using: .utf8)!, options: .allowFragments) as? JSON) as JSON??) {
            return HubResponse(json: jsonData!)
        }
        return nil
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let d : JSON = "D" <~~ json {
            self.d = d
        }
        if let id : Int64 = "ID" <~~ json {
            self.id = id
        }
        if let q : String = "Q" <~~ json {
            self.q = q
        }
        if let t : Int64 = "T" <~~ json {
            self.t = t
        }
        if let r : Int64 = "R" <~~ json {
            self.r = r
        }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "Q" ~~> q,
            "R" ~~> r,
            "T" ~~> t,
            "D" ~~> d,
            "ID" ~~> id,
            ])
    }
    
    public func toData() -> Data {
        return try! JSONSerialization.data(withJSONObject: toJSON() as Any, options: [.prettyPrinted])
    }
}
