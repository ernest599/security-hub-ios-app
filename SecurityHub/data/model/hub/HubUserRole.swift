//
//  HubUserRole.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - HubUserRole
public class HubUserRole: Glossy {
    public var domainId : Int64 = 0
    public var id : Int64 = 0
    public var orgId : Int64 = 0
    public var roles : Int64 = 0
    public var locale: HubLocale = HubLocale()
    
    //MARK: Default Initializer
    init()
    {
        domainId = 0
        id = 0
        orgId = 0
        roles = 0
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let domainId : Int64 = "domain_id" <~~ json {
            self.domainId = domainId
        }
        if let id : Int64 = "id" <~~ json {
            self.id = id
        }
        if let orgId : Int64 = "org_id" <~~ json {
            self.orgId = orgId
        }
        if let roles : Int64 = "roles" <~~ json {
            self.roles = roles
        }
        if let locale : HubLocale = "locale" <~~ json {
            self.locale = locale
        }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "domain_id" ~~> domainId,
            "id" ~~> id,
            "org_id" ~~> orgId,
            "roles" ~~> roles,
            "locale" ~~> locale,
            ])
    }
}

//MARK: - HubLocales
public class HubLocales: Glossy {
    public var count : Int64 = 0
    public var items : [HubLocale] = []
    
    //MARK: Default Initializer
    init()
    {
        count = 0
        items = []
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json {
            self.count = count
        }
        if let items : [HubLocale] = "items" <~~ json {
            self.items = items
        }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
    
}

//MARK: - HubLocale
public class HubLocale: Glossy {
    public var locale_id: Int64 = 33
    public var iso: String = "en"
    public var description: String = "English"
    
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let locale_id : Int64 = "locale_id" <~~ json { self.locale_id = locale_id }
        if let iso : String = "iso" <~~ json { self.iso = iso }
        if let description : String = "description" <~~ json { self.description = description }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "locale_id" ~~> locale_id,
            "iso" ~~> iso,
            "description" ~~> description,
            ])
    }
}
