//
//  HubRelations.swift
//  SecurityHub
//
//  Created by Timerlan on 26.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import Gloss

//MARK: - HubRelations
public class HubRelations: Glossy {
    public var count : Int64 = 0
    public var items : [HubRelation] = []
    
    //MARK: Default Initializer
    init()
    {
        count = 0
        items = []
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json {
            self.count = count
        }
        if let items : [HubRelation] = "items" <~~ json {
            self.items = items
        }        
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
    
}

//MARK: - HubRelation
public class HubRelation: Glossy {
    public var camId : String!
    public var name : String!
    public var ownerId : Int64!
    public var section : Int64!
    public var device : Int64!
    public var zone : Int64!
    
    //MARK: Default Initializer
    init()
    {
        camId = ""
        name = ""
        ownerId = 0
        section = 0
        device = 0
        zone = 0
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let camId : String = "cam_id" <~~ json {
            self.camId = camId
        }else{
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }else{
        }
        if let ownerId : Int64 = "owner_id" <~~ json {
            self.ownerId = ownerId
        }else{
            self.ownerId = 0
        }
        if let section : Int64 = "section" <~~ json {
            self.section = section
        }else{
            self.section = 0
        }
        if let device : Int64 = "device" <~~ json {
            self.device = device
        }else{
            self.device = 0
        }
        if let zone : Int64 = "zone" <~~ json {
            self.zone = zone
        }else{
            self.zone = 0
        }
        
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "cam_id" ~~> camId,
            "name" ~~> name,
            "owner_id" ~~> ownerId,
            "section" ~~> section,
            "device" ~~> device,
            "zone" ~~> zone,
            ])
    }
    
}
