//
//  HubSiteDelegates.swift
//  SecurityHub test
//
//  Created by Timerlan on 10.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import Foundation
import Gloss

//MARK: - HubSiteDelegates
public class HubSiteDelegates: Glossy {
    public var count : Int64 = 0
    public var items : [HubSiteDelegate] = []
    
    //MARK: Default Initializer
    init()
    {
        count = 0
        items = []
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json {
            self.count = count
        }
        if let items : [HubSiteDelegate] = "items" <~~ json {
            self.items = items
        }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
}

//MARK: - HubSiteDelegate
public class HubSiteDelegate: Glossy {
    public var id : Int64 = 0
    public var domain_id : Int64 = 0
    public var delegated_domain : Int64 = 0
    public var permissions : Int64 = 0
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let delegated_domain : Int64 = "delegated_domain" <~~ json { self.delegated_domain = delegated_domain }
        if let domain_id : Int64 = "domain_id" <~~ json { self.domain_id = domain_id }
        if let permissions : Int64 = "permissions" <~~ json { self.permissions = permissions }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "delegated_domain" ~~> delegated_domain,
            "domain_id" ~~> domain_id,
            "permissions" ~~> permissions,
            ])
    }
}

