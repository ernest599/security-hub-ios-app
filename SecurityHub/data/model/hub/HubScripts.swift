//
//  HubScripts.swift
//  SecurityHub
//
//  Created by Timerlan on 15/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//

import Gloss
import Localize_Swift

//MARK: - HubScripts
public class HubScripts: Glossy {
    public var count : Int64 = 0
    public var items : [HubScript] = []
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json { self.count = count }
        if let items : [HubScript] = "items" <~~ json { self.items = items }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
}


//MARK: - HubScripts
public class HubScript: Glossy {
    public var id : Int64 = 0
    public var name : String = ""
    public var program : String = ""
    public var compile_program : String = ""
    public var bind : Int64 = 0
    public var enabled : Bool = false
    public var uid : String = ""
    public var params: String = ""
    public var extra : String = ""
    public var device_id: Int64?

    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let name : String = "name" <~~ json { self.name = name }
        if let program : String = "program" <~~ json { self.program = program }
        if let compile_program : String = "compile_program" <~~ json { self.compile_program = compile_program }
        if let bind : Int64 = "bind" <~~ json { self.bind = bind }
        if let enabled : Bool = "enabled" <~~ json { self.enabled = enabled }
        if let uid : String = "uid" <~~ json { self.uid = uid }
        if let params: String = "params" <~~ json { self.params = params }
        if let extra : String = "extra" <~~ json { self.extra = extra }
        if let device_id : Int64 = "device_id" <~~ json { self.device_id = device_id }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "name" ~~> name,
            "program" ~~> program,
            "compile_program" ~~> compile_program,
            "bind" ~~> bind,
            "enabled" ~~> enabled,
            "uid" ~~> uid,
            "params" ~~> params,
            "extra" ~~> extra,
            "device_id" ~~> device_id
            ])
    }

    class Extra : Glossy {
        var _switch : Switch?

        init(){}
        
        public required init?(json: JSON){
            if let _switch : Switch = "switch" <~~ json { self._switch = _switch }
        }
        public func toJSON() -> JSON? {
            return jsonify([
                "switch" ~~> _switch
            ])
        }
        
        public func toData() -> Data {
            return try! JSONSerialization.data(withJSONObject: toJSON() as Any, options: [.prettyPrinted])
        }
        
        class Switch: Glossy {
            var rows : [Rows] = []

            init(){}

            public required init?(json: JSON){
            if let rows : [Rows] = "rows" <~~ json { self.rows = rows }
            }
            public func toJSON() -> JSON? {
                return jsonify([
                    "rows" ~~> rows
                ])
            }
        }
        
        class Rows : Glossy {
            var items : [Items] = []
            var size : Int64 = 0
            
            init(){}
            
            public required init?(json: JSON){
                if let items : [Items] = "items" <~~ json { self.items = items }
                if let size : Int64 = "size" <~~ json { self.size = size }
            }
            public func toJSON() -> JSON? {
                return jsonify([
                    "items" ~~> items,
                    "size" ~~> size,
                ])
            }
        }
        class Items : Glossy {
            
            var content: [HubLibraryScript.Data.LocalazedString] = []
            var value: Int64 = 0
            
            init(){}
            
            public required init?(json: JSON){
                if let content : [HubLibraryScript.Data.LocalazedString] = "content" <~~ json { self.content = content }
                if let value : Int64 = "value" <~~ json { self.value = value }
            }
            public func toJSON() -> JSON? {
                return jsonify([
                    "content" ~~> content,
                    "value" ~~> value,
                ])
            }
        }
    }
}

//MARK: - HubLibraryScripts
public class HubLibraryScripts: Glossy {
    public var count : Int64 = 0
    public var items : [HubLibraryScript] = []
    
    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json { self.count = count }
        if let items : [HubLibraryScript] = "items" <~~ json { self.items = items }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
}


//MARK: - HubLibraryScript
public class HubLibraryScript: Glossy {
    public var id : Int64 = 0
    public var uid : String = ""
    private var _data: String = ""

    //MARK: Default Initializer
    init(){}
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let uid : String = "uid" <~~ json { self.uid = uid }
        if let data: String = "data" <~~ json { self._data = data }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "uid" ~~> uid,
            "data" ~~> _data
            ])
    }
    
    var data: Data? {
        get { return Data(json: _data.toJson()) }
    }

    class Data : Glossy {
        var script_uid : String = ""
        var category : [LocalazedString] = []
        var name : [LocalazedString] = []
        var description : [LocalazedString] = []
        var source : String = ""
        var params : [Params] = []
        var extra : HubScript.Extra?
        var firmwares : [String] = []

        //MARK: Default Initializer
        init(){}
        
        //MARK: Decodable
        public required init?(json: JSON){
            if let script_uid : String = "script_uid" <~~ json { self.script_uid = script_uid }
            if let category : [LocalazedString] = "category" <~~ json { self.category = category }
            if let name : [LocalazedString] = "name" <~~ json { self.name = name }
            if let description : [LocalazedString] = "description" <~~ json { self.description = description }
            if let source : String = "source" <~~ json { self.source = source }
            if let params : [Params] = "params" <~~ json { self.params = params }
            if let extra : HubScript.Extra = "extra" <~~ json { self.extra = extra }
            if let firmwares : [String] = "firmwares" <~~ json { self.firmwares = firmwares }
        }
        public func toJSON() -> JSON? { return nil }
        
        class LocalazedString : Glossy {
            var iso : String = ""
            var value : String = ""
            
            init(){}
            
            public required init?(json: JSON){
                if let iso : String = "iso" <~~ json { self.iso = iso }
                if let value : String = "value" <~~ json { self.value = value }
            }
            public func toJSON() -> JSON? {
                return jsonify([
                    "iso" ~~> iso,
                    "value" ~~> value,
                ])
            }
            
            static func getName(_ names: [LocalazedString]) -> String? {
               return names.first{ $0.iso == Localize.currentLanguage() }?.value ?? names.first?.value
            }
        }
        class Params : Glossy {
            var name : String?
            var format : String?
            var type : String = ""
            var _default : String?
            var extra : [Extra] = []
            var description : [LocalazedString] = []
            var group: [Params]?
            
            init(){}
                         
            public required init?(json: JSON){
                if let name : String = "name" <~~ json { self.name = name }
                if let format : String = "format" <~~ json { self.format = format }
                if let type : String = "type" <~~ json { self.type = type }
                if let _default : String = "default" <~~ json { self._default = _default }
                if let extra : [Extra] = "extra" <~~ json { self.extra = extra }
                if let description : [LocalazedString] = "description" <~~ json { self.description = description }
                if let group : [Params] = "group" <~~ json { self.group = group }
            }
            public func toJSON() -> JSON? { return nil }
        }
        class Extra : Glossy {
           var name : String = ""
           var description : [LocalazedString] = []
           var value : String = ""
           
           init(){}
                  
           public required init?(json: JSON){
               if let name : String = "name" <~~ json { self.name = name }
               if let description : [LocalazedString] = "description" <~~ json { self.description = description }
               if let value : String = "value" <~~ json { self.value = value }
           }
           public func toJSON() -> JSON? { return nil }
        }
    }
}


