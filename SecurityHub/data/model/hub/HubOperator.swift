//
//  HubOperator.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - Item
public class HubOperator: Glossy {
    public var active : Int64 = 0
    public var contacts : String = ""
    public var domain : Int64 = 0
    public var id : Int64 = 0
    public var login : String = ""
    public var name : String = ""
    public var org : Int64 = 0
    public var roles : Int64 = 0
    
    //MARK: Default Initializer
    init()
    {
        active = 0
        contacts = ""
        domain = 0
        id = 0
        login = ""
        name = ""
        org = 0
        roles = 0
    }
    
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let active : Int64 = "active" <~~ json {
            self.active = active
        }
        if let contacts : String = "contacts" <~~ json {
            self.contacts = contacts
        }
        if let domain : Int64 = "domain" <~~ json {
            self.domain = domain
        }
        if let id : Int64 = "id" <~~ json {
            self.id = id
        }
        if let login : String = "login" <~~ json {
            self.login = login
        }
        if let name : String = "name" <~~ json {
            self.name = name
        }
        if let org : Int64 = "org" <~~ json {
            self.org = org
        }
        if let roles : Int64 = "roles" <~~ json {
            self.roles = roles
        }
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "active" ~~> active,
            "contacts" ~~> contacts,
            "domain" ~~> domain,
            "id" ~~> id,
            "login" ~~> login,
            "name" ~~> name,
            "org" ~~> org,
            "roles" ~~> roles,
            ])
    }
    
}

