//
//  HubEvents.swift
//  SecurityHub
//
//  Created by Timerlan on 23.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//
import Foundation
import Gloss

//MARK: - HubDeviceSites
public class HubDeviceSites: Glossy {
    public var count : Int64 = 0
    public var items : [HubDeviceSite] = []
    
    //MARK: Default Initializer
    init()
    {
        count = 0
        items = []
    }
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let count : Int64 = "count" <~~ json {
            self.count = count
        }
        if let items : [HubDeviceSite] = "items" <~~ json {
            self.items = items
        }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "count" ~~> count,
            "items" ~~> items,
            ])
    }
}

//MARK: - HubDeviceSite
public class HubDeviceSite: Glossy {
    public var id : Int64 = 0
    public var device : Int64 = 0
    public var site : Int64 = 0
    public var device_section : Int64 = 0
    public var site_section : Int64 = 0
    public var domain : Int64 = 0
    public var delegated : Int64 = 0
    
    //MARK: Decodable
    public required init?(json: JSON){
        if let id : Int64 = "id" <~~ json { self.id = id }
        if let device : Int64 = "device" <~~ json { self.device = device }
        if let site : Int64 = "site" <~~ json { self.site = site }
        if let device_section : Int64 = "device_section" <~~ json { self.device_section = device_section }
        if let site_section : Int64 = "site_section" <~~ json { self.site_section = site_section }
        if let domain : Int64 = "domain" <~~ json { self.domain = domain }
        if let delegated : Int64 = "delegated" <~~ json { self.delegated = delegated }
    }
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "id" ~~> id,
            "device" ~~> device,
            "site" ~~> site,
            "device_section" ~~> device_section,
            "site_section" ~~> site_section,
            "domain" ~~> domain,
            "delegated" ~~> delegated,
            ])
    }
}
