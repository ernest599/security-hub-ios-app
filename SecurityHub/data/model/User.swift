//
//  User.swift
//  SecurityHub
//
//  Created by Timerlan on 22.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation


class User{
    var login: String
    var password: String
    var pin : String
    var id : Int64
    var domainId : Int64
    var orgId : Int64
    var roles : Int64
    
    init(login: String, password: String, pin : String, id : Int64, domainId : Int64, orgId : Int64, roles : Int64) {
        self.login = login
        self.password = password
        self.pin = pin
        self.id = id
        self.domainId = domainId
        self.orgId = orgId
        self.roles = roles
    }
}
