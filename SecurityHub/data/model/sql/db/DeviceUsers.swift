//
//  DeviceUsers.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct DeviceUsers: SQLCommand {
    var id: Int64
    var device: Int64
    var name : String = ""
    var comment : String = ""
    var phone : String = ""
    var userIndex : Int64 = 0
    var time: Int64 = 0
    
    init(id: Int64 = 0, device: Int64 = 0, name: String = "", comment: String = "", phone: String = "", userIndex: Int64 = 0, time: Int64) {
        self.id = id
        self.device = device
        self.name = name
        self.comment = comment
        self.phone = phone
        self.userIndex = userIndex
        self.time = time
    }
    
    static let id = Expression<Int64>("id")
    static let device = Expression<Int64>("device")
    static let name = Expression<String>("name")
    static let comment = Expression<String>("comment")
    static let phone = Expression<String>("phone")
    static let userIndex = Expression<Int64>("userIndex")
    static let time = Expression<Int64>("time")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            DeviceUsers.id <- id,
            DeviceUsers.device <- device,
            DeviceUsers.name <- name,
            DeviceUsers.comment <- comment,
            DeviceUsers.phone <- phone,
            DeviceUsers.userIndex <- userIndex,
            DeviceUsers.time <- time
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(DeviceUsers.id == id).update(
            DeviceUsers.device <- device,
            DeviceUsers.name <- name,
            DeviceUsers.comment <- comment,
            DeviceUsers.phone <- phone,
            DeviceUsers.userIndex <- userIndex,
            DeviceUsers.time <- time
        ))
    }
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(device)
            t.column(name)
            t.column(comment)
            t.column(phone)
            t.column(userIndex)
            t.column(time)
        })
        return table
    }
    
    static func tableName() -> String { return "DeviceUsers" }
    func delete(_ db: Connection, table: Table) { try! db.run(table.filter(DeviceUsers.id == id).delete()) }
    func isWasInDb(_ db: Connection, table: Table) -> Bool { return try! db.pluck(table.filter(DeviceUsers.id == id)) != nil }
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand { return DeviceUsers(id: row[id], device: row[device], name: row[name], comment: row[comment], phone: row[phone], userIndex: row[userIndex], time: row[time] ) as! T }
    
    static func getId(_ device_id: Int64, _ user_index: Int64) -> Int64 {
        return device_id * 100 + user_index
    }
}
