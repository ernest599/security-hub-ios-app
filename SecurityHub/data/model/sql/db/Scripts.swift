//
//  Scripts.swift
//  SecurityHub
//
//  Created by Timerlan on 19/11/2019.
//  Copyright © 2019 TEKO. All rights reserved.
//


import SQLite

struct Script: SQLCommand {
    static let id = Expression<Int64>("id")
    static let device = Expression<Int64>("device")
    static let name = Expression<String>("name")
    static let program = Expression<String>("program")
    static let compile_program = Expression<String>("compile_program")
    static let bind = Expression<Int64>("bind")
    static let enabled = Expression<Bool>("enabled")
    static let uid = Expression<String>("uid")
    static let params = Expression<String>("params")
    static let extra = Expression<String>("extra")
    static let time = Expression<Int64>("time")

    var id : Int64
    var device: Int64
    var name : String
    var program : String
    var compile_program : String
    var bind : Int64
    var enabled : Bool
    var uid : String
    var params : String
    var extra : String
    var time : Int64
    
    init(id : Int64, device: Int64, name : String, program : String, compile_program : String, bind : Int64, enabled : Bool, uid : String, params : String, extra : String,
        time : Int64) {
        self.id = id
        self.device = device
        self.name = name
        self.program = program
        self.compile_program = compile_program
        self.bind = bind
        self.enabled = enabled
        self.uid = uid
        self.params = params
        self.extra = extra
        self.time = time
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(Script.id <- id, Script.device <- device, Script.name <- name, Script.program <- program, Script.compile_program <- compile_program, Script.bind <- bind, Script.enabled <- enabled, Script.uid <- uid, Script.params <- params, Script.extra <- extra, Script.time <- time))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Script.id == id).update(Script.device <- device, Script.name <- name, Script.program <- program, Script.compile_program <- compile_program, Script.bind <- bind, Script.enabled <- enabled, Script.uid <- uid, Script.params <- params, Script.extra <- extra, Script.time <- time))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Script.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        if let _ = try? db.pluck(table.filter(Script.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(Script.id == id)) else { return .needInsert }
        if row[Script.name] == name && row[Script.device] == device && row[Script.program] == program && row[Script.compile_program] == compile_program && row[Script.bind] == bind && row[Script.enabled] == enabled && row[Script.uid] == uid && row[Script.params] == params && row[Script.extra] == extra { return .noNeed }
        return .needUpdate
    }
    
    func updateTime(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Script.id == id).update(Script.time <- time))
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Script(id: row[id], device: row[device], name: row[name], program: row[program], compile_program: row[compile_program], bind: row[bind], enabled: row[enabled], uid: row[uid], params: row[params], extra: row[extra], time: row[time]) as! T
    }
    
    static func tableName() -> String{ return "Scripts"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(device)
            t.column(name)
            t.column(program)
            t.column(compile_program)
            t.column(bind)
            t.column(enabled)
            t.column(uid)
            t.column(params)
            t.column(extra)
            t.column(time)
        })
        return table
    }
}

