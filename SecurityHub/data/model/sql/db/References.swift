//
//  BDID.swift
//  SecurityHub
//
//  Created by Timerlan on 28.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

class References {
    static let command_id = Expression<Int64>("command_id")
    static let name = Expression<String>("name")
    
    var command_id: Int64
    var name: String

    init(command_id: Int64, name: String) {
        self.command_id = command_id
        self.name = name
    }
    
    func insert(_ main: DB) {
        let _ = try? main.db.run(main.references.insert(References.command_id <- command_id, References.name <- name))
    }

    static func createTable(_ db: Connection) -> Table {
        let table = Table("References")
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(command_id, primaryKey: true)
            t.column(name)
        })
        return table
    }
}
