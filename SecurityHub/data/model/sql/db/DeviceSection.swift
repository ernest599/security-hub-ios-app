//
//  DeviceSite.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct DeviceSection: SQLCommand {
    static let id = Expression<Int64>("id")
    static let device = Expression<Int64>("device")
    static let section = Expression<Int64>("section")
    static let site = Expression<Int64>("site")
    static let time = Expression<Int64>("time")
    static let parted = Expression<Int64>("parted")
    
    var id: Int64 = 0
    var device: Int64
    var section: Int64
    var site: Int64
    var time: Int64
    var parted: Int64
    
    init(site: Int64, device: Int64, section: Int64, parted: Int64, time: Int64 = 0) {
        self.site = site
        self.device = device
        self.section = section
        self.parted = parted
        self.time = time
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(DeviceSection.device <- device, DeviceSection.section <- section, DeviceSection.site <- site, DeviceSection.time <- time, DeviceSection.parted <- parted))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(DeviceSection.site == site && DeviceSection.device == device && DeviceSection.section == section).update(DeviceSection.time <- time, DeviceSection.parted <- parted))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(DeviceSection.site == site && DeviceSection.device == device && DeviceSection.section == section).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool {
        if let _ = try? db.pluck(table.filter(DeviceSection.site == site && DeviceSection.device == device && DeviceSection.section == section)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(DeviceSection.site == site && DeviceSection.device == device && DeviceSection.section == section)) else { return .needInsert }
        if row[DeviceSection.parted] == parted { return .noNeed }
        return .needUpdate
    }
    
    func updateTime(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(DeviceSection.site == site && DeviceSection.device == device && DeviceSection.section == section).update(DeviceSection.time <- time))
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return DeviceSection(site: row[site], device: row[device], section: row[section], parted: row[parted], time: row[time]) as! T
    }
    
    static func tableName() -> String { return "DeviceSection" }
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: .autoincrement)
            t.column(site)
            t.column(device)
            t.column(section)
            t.column(parted)
            t.column(time)
        })
        return table
    }
}
