//
//  UserSection.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct UserSection: SQLCommand {
    var id: Int64
    var userId: Int64
    var sectionId: Int64
    var time: Int64
    
    init(id: Int64 = 0, userId: Int64 = 0, sectionId: Int64 = 0, time: Int64) {
        self.id = id
        self.userId = userId
        self.sectionId = sectionId
        self.time = time
    }
    
    static let id = Expression<Int64>("id")
    static let userId = Expression<Int64>("userId")
    static let sectionId = Expression<Int64>("sectionId")
    static let time = Expression<Int64>("time")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            UserSection.id <- id,
            UserSection.userId <- userId,
            UserSection.sectionId <- sectionId,
            UserSection.time <- time
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(UserSection.id == id)
            .update( UserSection.userId <- userId,
                     UserSection.sectionId <- sectionId,
                     UserSection.time <- time
        ))
    }
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(userId)
            t.column(sectionId)
            t.column(time)
        })
        return table
    }
    
    static func tableName() -> String { return "UserSection" }
    func delete(_ db: Connection, table: Table) { try! db.run(table.filter(UserSection.userId == userId && UserSection.sectionId == sectionId).delete()) }
    func isWasInDb(_ db: Connection, table: Table) -> Bool { return try! db.pluck(table.filter(UserSection.userId == userId && UserSection.sectionId == sectionId)) != nil }
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand { return UserSection(id: row[id], userId: row[userId], sectionId: row[sectionId], time: row[time]) as! T }
    
    static func getId(_ device_id: Int64, _ user_index: Int64, _ userSection: Int64) -> Int64 {
        return ((device_id * 100) + user_index) * 100 + userSection
    }
}
