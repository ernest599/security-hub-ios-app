//
//  CamSet.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct CamSet: SQLCommand {
    var id: Int64 = 0
    var camid: String
    var q: Int64
    var audio: Bool
    
    init(camid: String, q: Int64 = 0, audio: Bool = true) {
        self.camid = camid
        self.q = q
        self.audio = audio
    }
    
    static let camid = Expression<String>("camid")
    static let q = Expression<Int64>("q")
    static let audio = Expression<Bool>("audio")
    
    func insert(_ db: Connection, table: Table) {
        try! db.run(table.insert(
            CamSet.camid <- camid,
            CamSet.q <- q,
            CamSet.audio <- audio
        ))
    }
    
    func update(_ db: Connection, table: Table) {
        try! db.run(table.filter(CamSet.camid == camid).update(
            CamSet.q <- q,
            CamSet.audio <- audio
        ))
    }
    
    func delete(_ db: Connection, table: Table) {
        try! db.run(table.filter(CamSet.camid == camid).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        return try! db.pluck(table.filter(CamSet.camid == camid)) != nil
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return CamSet(camid: row[camid], q: row[q], audio: row[audio]) as! T
    }
    
    static func tableName() -> String{ return "CamSet"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(camid, primaryKey: true)
            t.column(q)
            t.column(audio)
        })
        return table
    }
}


