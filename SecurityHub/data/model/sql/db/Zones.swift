//
//  Zones.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import SQLite

struct Zones: SQLCommand {
    static let id = Expression<Int64>("id")
    static let section = Expression<Int64>("section")
    static let device = Expression<Int64>("device")
    static let zone = Expression<Int64>("zone")
    static let name = Expression<String>("name")
    static let delay = Expression<Int64>("delay")
    static let detector = Expression<Int64>("detector")
    static let physic = Expression<Int64>("physic")
    static let time = Expression<Int64>("time")
    
    var id: Int64
    var device: Int64
    var section: Int64
    var zone: Int64
    var name: String
    var delay: Int64
    var detector: Int64
    var physic: Int64
    var time: Int64
    
    init(device: Int64, section: Int64, zone: Int64, name: String, delay: Int64, detector: Int64, physic: Int64, time: Int64) {
        self.id = Zones.getId(device, section, zone)
        self.device = device
        self.section = section
        self.zone = zone
        self.name = name
        self.delay = delay
        self.detector = detector
        self.physic = physic
        self.time = time
    }
    
    func insert(_ db: Connection, table: Table) {
        let _ = try? db.run(table.insert(Zones.id <- id, Zones.device <- device, Zones.section <- section, Zones.zone <- zone, Zones.name <- name, Zones.delay <- delay, Zones.detector <- detector, Zones.physic <- physic, Zones.time <- time))
    }
    
    func update(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Zones.id == id).update(Zones.device <- device, Zones.section <- section, Zones.zone <- zone, Zones.name <- name, Zones.delay <- delay, Zones.detector <- detector, Zones.physic <- physic, Zones.time <- time))
    }
    
    func delete(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Zones.id == id).delete())
    }
    
    func isWasInDb(_ db: Connection, table: Table) -> Bool{
        if let _ = try? db.pluck(table.filter(Zones.id == id)) { return true }
        return false
    }
    
    func getDbStatus(_ db: Connection, table: Table) -> DbStatus {
        guard let row = try? db.pluck(table.filter(Zones.id == id)) else { return .needInsert }
        if row[Zones.name] == name && row[Zones.delay] == delay && row[Zones.detector] == detector && row[Zones.physic] == physic { return .noNeed }
        return .needUpdate
    }
    
    func updateTime(_ db: Connection, table: Table) {
        let _ = try? db.run(table.filter(Zones.id == id).update(Zones.time <- time))
    }
    
    static func fromRow<T>(_ row: Row) -> T where T : SQLCommand {
        return Zones(device: row[device], section: row[section], zone: row[zone], name: row[name], delay: row[delay], detector: row[detector], physic: row[physic], time: row[time]) as! T
    }
    
    static func tableName() -> String{ return "Zones"}
    
    static func createTable(_ db: Connection) -> Table {
        let table = Table(tableName())
        try! db.run(table.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(device)
            t.column(section)
            t.column(zone)
            t.column(name)
            t.column(delay)
            t.column(detector)
            t.column(physic)
            t.column(time)
        })
        return table
    }
    
    static func getId(_ device: Int64, _ section: Int64, _ zone: Int64) -> Int64 {  return (device * 100 + section) * 100 + zone }
}
