//
//  SQLCommand.swift
//  SecurityHub
//
//  Created by Timerlan on 21.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import SQLite

protocol SQLCommand {
    var id: Int64 { get }
    func insert(_ db: Connection, table: Table)
    func update(_ db: Connection, table: Table)
    func delete(_ db: Connection, table: Table)
    func isWasInDb(_ db: Connection, table: Table) -> Bool
    static func fromRow<T:SQLCommand>(_ row: Row) -> T
    static func createTable(_ db: Connection) -> Table
    static func tableName() -> String
}
extension Connection {
    public var userVersion: Int64 {
        get { return (try? scalar("PRAGMA user_version") as? Int64) ?? 0 }
        set { do { try run("PRAGMA user_version = \(newValue)") } catch { } }
    }
}
enum DbStatus{
    case needInsert, needUpdate, noNeed
}
