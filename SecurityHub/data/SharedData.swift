struct SharedData {
    static let shared = SharedData()
    
    var events: Events!

    private init() { }
}
