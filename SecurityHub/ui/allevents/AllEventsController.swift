import UIKit
import RxSwift

class AllEventsController: XBaseController<AllEventsView>, UITableViewDataSource, UITableViewDelegate {
    
    private var elist: [Events] = []
    private var eTUDisp: Disposable?
    private var disp: Disposable?
    private let dm = DataManager.shared!
    
    var eventDisp: Disposable!

    override func loadView() {
        super.loadView()
        
        xView.setNV(navigationController)
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        title = R.strings.title_events
        
        xView.table.register(EventBigCell.self, forCellReuseIdentifier: EventBigCell.cellId)
        xView.table.dataSource = self
        xView.table.delegate = self
        xView.listener = { (obj, _class, from, to) in
            self.load(obj == -1 ? nil : obj, _class == -1 ? nil : _class, from, to)
        }
        
        let next_day = Date(timeInterval: 86400, since: Date())
        let year = Calendar(identifier: .gregorian).component(Calendar.Component.year, from: next_day)
        let month = Calendar(identifier: .gregorian).component(Calendar.Component.month, from: next_day)
        let day = Calendar(identifier: .gregorian).component(Calendar.Component.day, from: next_day)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        
        let _date = formatter.date(from: "\(year)/\(month)/\(day)")!
        let time = _date.timeIntervalSince1970
        load(nil, nil, Int64(time  - 86400 * 3), Int64(time))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        eventDisp = dm.events
            .asObservable()
            .subscribe(onNext: { events in
                DispatchQueue.main.async { [self] in
                    if events.count > 0 {
                        for event in events {
                            if event._class == 0 {
                                xView.changeBottomNavStatus(type: 0)

                                break
                            } else if event._class >= 1 && event._class <= 3 {
                                xView.changeBottomNavStatus(type: 1)
                            }
                        }
                    } else {
                        xView.changeBottomNavStatus(type: 2)
                    }
                }
            })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        eventDisp.dispose()
    }
    
    private func load( _ obj: Int64?, _ _class: Int64?, _ from: Int64, _ to: Int64){
        disp?.dispose()
        elist = []
        
        xView.table.reloadData()
        
        disp = DataManager.shared.getEvents()
            .filter({ (result) -> Bool in
                if let obj = obj { return result.siteIds.contains(obj) }
                return true
            })
            .filter({ (result) -> Bool in
                if let _class = _class { return result.event._class == _class }
                return true
            })
            .filter({ (result) -> Bool in
                return result.event.time >= from && result.event.time <= to
            })
            .subscribe(onNext: { (result) in
                self.addEvent(result.event, type: result.type, isFromBD: result.isFromBD)
            })
    }
    
    func addEvent(_ event: Events, type: NUpdateType, isFromBD: Bool) {
        switch type {
        case .insert:
            if let max = elist.map({ (e) -> Int64 in e.time }).max(), max <= event.time, !isFromBD, eTUDisp == nil {
                elist.insert(event, at: 0)
                xView.table.insertRows(at: [IndexPath(row: 0, section: 0)], with: .none)
            } else {
                elist.append(event)
                eTableUpdateEndDetecting()
            }
        case .update:
            guard let pos = elist.firstIndex(where: { $0.id == event.id }) else { break }
            elist[pos] = event
            eTableUpdateEndDetecting()
        case .delete: break;
        }
    }
    
    private func eTableUpdateEndDetecting(){
        eTUDisp?.dispose()
        eTUDisp = Observable<Int64>.timer(.milliseconds(100), scheduler: SerialDispatchQueueScheduler.init(qos: .utility))
            .subscribe(on: ThreadUtil.shared.backScheduler)
            .observe(on: ThreadUtil.shared.backScheduler)
            .do(onNext: { (t) in self.elist = self.elist.sorted(by: { (e1, e2) -> Bool in return e1.time > e2.time }) })
            .observe(on: ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { t in
                self.xView.table.reloadData()
                self.eTUDisp = nil
            }, onError: { e in
                self.eTUDisp = nil
            })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventBigCell.cellId, for: indexPath) as! EventBigCell
        
        cell.setContent(elist[indexPath.row])
        cell.setNV(navigationController)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return EventBigView.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.isSelected = false
        
        let event = elist[indexPath.row]
        _ = DataManager.shared.getName(device: event.device, section: event.section, zone: event.zone)
            .subscribe(onNext:{ (name) in
                let site = DataManager.shared.getNameSites(event.device)
                let text = site + "\n" + name + "\n\n" + self.elist[indexPath.row].affect_desc + "\n\n" + (self.elist[indexPath.row].time == 0 ? "--:--:--" : self.elist[indexPath.row].time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short)) + "\n"
                AlertUtil.infoAlert(title: R.strings.title_information, text)
            } )
    }
}
