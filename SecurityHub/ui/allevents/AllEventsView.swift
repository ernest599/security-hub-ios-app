import UIKit
import RxSwift

class AllEventsView: XBaseView {
    
    var listener: (( _ obj: Int64, _ _class: Int64, _ from: Int64, _ to: Int64)->Void)?
    private var objDisp: Disposable?
    var events: [Events] = []
    
    lazy var shadowLayer: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.regText
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var bottomNavFrame: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubTransparent
        
        return view
    }()
    
    lazy var bottomNavBackground: UIView = {
        return UIView()
    }()
    
    lazy var bottomNavLine: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.hubSeparator
        
        return view
    }()
    
    lazy var bottomNavIcons: UIStackView = {
        let view = UIStackView()
        
        view.axis = .horizontal
        view.distribution = .equalSpacing
        
        return view
    }()
    
    lazy var bottomNavState: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.numberOfLines = 1
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(stateClick)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var bottomNavMain: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_main!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavScripts: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_script!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavEvents: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_history!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubMainColor
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavCameras: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_cameras!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var bottomNavSettings: UIImageView = {
        let view = UIImageView()
        
        view.image = XImages.menu_menu!.withRenderingMode(.alwaysTemplate)
        view.tintColor = UIColor.hubSeparator
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(bottomNavClicked)
            )
        )
        
        return view
    }()
    
    lazy var form: UIView = {
        var view = UIView()
        
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        
        return view
    }()
    
    lazy var objLable: UILabel = {
        var view = UILabel()
        
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = R.strings.events_site
        view.textColor = UIColor.black
        
        return view
    }()
    
    lazy var objField: ZFRippleButton = {
        var view = ZFRippleButton()
        
        view.setTitleColor(UIColor.black, for: .normal)
        view.setTitle(R.strings.events_site_all, for: .normal)
        view.tag = -1
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.contentHorizontalAlignment = .left
        
        return view
    }()
    
    lazy var objIc: ZFRippleButton = {
        var view = ZFRippleButton()
        
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_arrow_down"), for: UIControl.State.normal)
        
        return view
    }()
    
    lazy var formType: UIView = {
        var view = UIView()
        
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        
        return view
    }()
    
    lazy var typeLable: UILabel = {
        var view = UILabel()
        
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = R.strings.events_class_type
        view.textColor = UIColor.black
        
        return view
    }()
    
    lazy var typeField: ZFRippleButton = {
        var view = ZFRippleButton()
        
        view.setTitleColor(UIColor.black, for: .normal)
        view.setTitle(R.strings.events_class_type_all, for: .normal)
        view.tag = -1
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.contentHorizontalAlignment = .left
        
        return view
    }()
    
    lazy var typeIc: ZFRippleButton = {
        var view = ZFRippleButton()
        
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.setImage( #imageLiteral(resourceName: "ic_arrow_down"), for: UIControl.State.normal)
        
        return view
    }()
    
    lazy var formDate: UIView = {
        var view = UIView()
        
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.lightGray.cgColor
        
        return view
    }()
    
    lazy var fromLable: UILabel = {
        var view = UILabel()
        
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = R.strings.events_time_from
        view.textColor = UIColor.black
        
        return view
    }()
    
    lazy var fromField: ZFRippleButton = {
        var view = ZFRippleButton()
        
        view.setTitleColor(UIColor.black, for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.contentHorizontalAlignment = .left
        
        return view
    }()
    
    lazy var toLable: UILabel = {
        var view = UILabel()
        
        view.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        view.text = R.strings.events_time_to
        view.textColor = UIColor.black
        
        return view
    }()
    
    lazy var toField: ZFRippleButton = {
        var view = ZFRippleButton()
        
        view.setTitleColor(UIColor.black, for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        view.contentHorizontalAlignment = .left
        
        return view
    }()
    
    lazy var table: UITableView = {
        let view = UITableView()
        
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        
        return view
    }()
    
    override func setContent() {
        xView.addSubview(table)
        xView.addSubview(form)
        
        xView.addSubview(bottomNavFrame)
        xView.addSubview(bottomNavLine)
        bottomNavFrame.addSubview(bottomNavBackground)
        bottomNavFrame.addSubview(bottomNavIcons)
        bottomNavFrame.addSubview(bottomNavState)
        bottomNavIcons.addArrangedSubview(bottomNavMain)
        bottomNavIcons.addArrangedSubview(bottomNavScripts)
        bottomNavIcons.addArrangedSubview(bottomNavEvents)
        bottomNavIcons.addArrangedSubview(bottomNavCameras)
        bottomNavIcons.addArrangedSubview(bottomNavSettings)
            
        form.addSubview(objLable)
        form.addSubview(objField)
        form.addSubview(objIc)
        
        xView.addSubview(formType)
        formType.addSubview(typeLable)
        formType.addSubview(typeField)
        formType.addSubview(typeIc)
        
        xView.addSubview(formDate)
        formDate.addSubview(fromLable)
        formDate.addSubview(fromField)
        formDate.addSubview(toLable)
        formDate.addSubview(toField)
        
        var time = Int64(NSDate().timeIntervalSince1970)
        time = time / 86400
        time = time * 86400 + 86399 - Int64(TimeZone.current.secondsFromGMT())
        toField.setTitle(time.getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
        toField.tag = Int(time)
        fromField.setTitle((time  - 86400 * 3).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
        fromField.tag = Int(time  - 86400 * 3)
        
        objField.addTarget(self, action: #selector(obj), for: .touchUpInside)
        objIc.addTarget(self, action: #selector(obj), for: .touchUpInside)
        typeField.addTarget(self, action: #selector(type), for: .touchUpInside)
        typeIc.addTarget(self, action: #selector(type), for: .touchUpInside)
        fromField.addTarget(self, action: #selector(from), for: .touchUpInside)
        toField.addTarget(self, action: #selector(to), for: .touchUpInside)
    }
    
    @objc func obj() {
        let alert = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet
        )
        
        alert.addAction(UIAlertAction(title: R.strings.events_site_all, style: .default, handler: { a in
            self.objField.setTitle(R.strings.events_site_all, for: .normal)
            self.objField.tag = -1
            self.listener?(-1, Int64(self.typeField.tag), Int64(self.fromField.tag), Int64(self.toField.tag))
        }))
        
        objDisp = DataManager.shared.getSites(completable: true)
            .toArray()
            .observe(on: MainScheduler.init())
            .subscribe(onSuccess: { sites in
                for site in sites{
                    alert.addAction(UIAlertAction(title: site.site.name, style: .default, handler: { a in
                        self.objField.setTitle(site.site.name, for: .normal)
                        self.objField.tag = Int(site.site.id)
                        self.listener?(site.site.id, Int64(self.typeField.tag), Int64(self.fromField.tag), Int64(self.toField.tag))
                    }))
                }
                
                alert.addAction(UIAlertAction(title:  R.strings.button_cancel, style: .cancel, handler: nil))
                
                self.nV?.present(alert, animated: false)
            })
    }
    
    @objc func type() {
        let alert = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet
        )
        
        alert.addAction(UIAlertAction(title: R.strings.events_class_type_all, style: .default, handler: { a in
            self.typeField.setTitle(R.strings.events_class_type_all, for: .normal)
            self.typeField.tag = -1
            self.listener?(Int64(self.objField.tag), -1, Int64(self.fromField.tag), Int64(self.toField.tag))
        }))
        
        _ = DataManager.shared.getAllClasses().subscribe(onNext: { classes in
            for cl in classes{
                alert.addAction(UIAlertAction(title: cl.name, style: .default, handler: { a in
                    self.typeField.setTitle(cl.name, for: .normal)
                    self.typeField.tag = Int(cl.id)
                    self.listener?(Int64(self.objField.tag), cl.id,Int64(self.fromField.tag), Int64(self.toField.tag))
                }))
            }
            
            alert.addAction(UIAlertAction(title: R.strings.button_cancel, style: .cancel, handler: nil))
            
            self.nV?.present(alert, animated: false)
        })
    }
    
    @objc func from() {
        let start_time = Int64(NSDate().timeIntervalSince1970) - (86400 * 180)
        
        let a = AlertUtil.alertDatePicker({ (date) in
            let year = Calendar(identifier: .gregorian).component(Calendar.Component.year, from: date)
            let month = Calendar(identifier: .gregorian).component(Calendar.Component.month, from: date)
            let day = Calendar(identifier: .gregorian).component(Calendar.Component.day, from: date)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            
            let _date = formatter.date(from: "\(year)/\(month)/\(day)")!
            let time = _date.timeIntervalSince1970
            
            self.fromField.setTitle(Int64(time).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
            self.fromField.tag = Int(time)
            self.listener?(Int64(self.objField.tag), Int64(self.typeField.tag), Int64(time), Int64(self.toField.tag))
        }, max: Int64(toField.tag) + 1, min: start_time)
        
        nV?.present(a, animated: false)
    }
    
    @objc func to() {
        let end_time = Int64(NSDate().timeIntervalSince1970)
        
        let a = AlertUtil.alertDatePicker({ (date) in
            let next_day = Date(timeInterval: 86400, since: date)
            
            let year = Calendar(identifier: .gregorian).component(Calendar.Component.year, from: next_day)
            let month = Calendar(identifier: .gregorian).component(Calendar.Component.month, from: next_day)
            let day = Calendar(identifier: .gregorian).component(Calendar.Component.day, from: next_day)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            
            let _date = formatter.date(from: "\(year)/\(month)/\(day)")!
            let time = _date.timeIntervalSince1970
            
            self.toField.setTitle(Int64(date.timeIntervalSince1970).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .none), for: .normal)
            self.toField.tag = Int(time)
            self.listener?(Int64(self.objField.tag), Int64(self.typeField.tag), Int64(self.fromField.tag), Int64(time))
        }, max: end_time, min: Int64(fromField.tag))
        
        nV?.present(a, animated: false)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bottomNavFrame.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(100)
            make.bottom.equalToSuperview().offset(-50)
        }
        
        bottomNavBackground.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        bottomNavLine.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(1)
            make.bottom.equalTo(bottomNavFrame.snp.top)
        }
        
        bottomNavIcons.snp.remakeConstraints { make in
            make.height.equalTo(36)
            make.bottom.equalTo(xView.snp.bottom).offset(-100)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavState.snp.remakeConstraints { make in
            make.bottom.equalTo(bottomNavIcons.snp.top).offset(-20)
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        
        bottomNavMain.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
            
        bottomNavScripts.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavEvents.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavCameras.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        bottomNavSettings.snp.remakeConstraints { make in
            make.width.height.equalTo(36)
        }
        
        form.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        formType.snp.remakeConstraints{ make in
            make.top.equalTo(form.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        formDate.snp.remakeConstraints{ make in
            make.top.equalTo(formType.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        table.snp.remakeConstraints{make in
            make.top.equalTo(formDate.snp.bottom)
            make.width.equalToSuperview()
            make.bottom.equalTo(bottomNavLine.snp.top)
        }
        
        objLable.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(100)
            make.centerY.equalTo(objField.snp.centerY)
        }
        
        objField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(objLable.snp.trailing)
        }
        
        objIc.snp.remakeConstraints{ make in
            make.leading.equalTo(objField.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-5)
            make.width.height.equalTo(35)
            make.centerY.equalTo(objField.snp.centerY)
        }
        
        typeLable.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(100)
            make.centerY.equalTo(typeField.snp.centerY)
        }
        
        typeField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(typeLable.snp.trailing)
        }
        
        typeIc.snp.remakeConstraints{ make in
            make.leading.equalTo(typeField.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-5)
            make.width.height.equalTo(35)
            make.centerY.equalTo(typeField.snp.centerY)
        }
        
        fromLable.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(15)
            make.width.equalTo(50)
            make.centerY.equalTo(fromField.snp.centerY)
        }
        
        fromField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(fromLable.snp.trailing)
            make.width.equalTo(toField.snp.width)
        }
        
        toLable.snp.remakeConstraints{ make in
            make.leading.equalTo(fromField.snp.trailing)
            make.width.equalTo(40)
            make.centerY.equalTo(toField.snp.centerY)
        }
        
        toField.snp.remakeConstraints{ make in
            make.top.bottom.equalToSuperview()
            make.height.equalTo(45)
            make.leading.equalTo(toLable.snp.trailing)
            make.trailing.equalToSuperview().offset(5)
        }
        
        changeBottomNavStatus(type: DataManager.shared.bottomNavState)
    }
    
    func changeBottomNavStatus(type: Int) {
        DataManager.shared.bottomNavState = type
        
        if type == 0 {
            bottomNavBackground.backgroundColor = UIColor.hubRedColor
            
            bottomNavState.text = R.strings.alarm
            bottomNavState.textColor = UIColor.white
            
            bottomNavEvents.tintColor = UIColor.hubMainColor
        } else if type == 1 {
            bottomNavBackground.backgroundColor = UIColor.hubGold
            
            bottomNavState.text = R.strings.criticalEvents
            bottomNavState.textColor = UIColor.hubTextBlack
            
            bottomNavEvents.tintColor = UIColor.hubTextBlack
        } else {
            bottomNavBackground.layer.cornerRadius = 0
            bottomNavBackground.backgroundColor = UIColor.white
            
            bottomNavMain.tintColor = UIColor.hubSeparator
            bottomNavScripts.tintColor = UIColor.hubSeparator
            bottomNavEvents.tintColor = UIColor.hubMainColor
            bottomNavCameras.tintColor = UIColor.hubSeparator
            bottomNavSettings.tintColor = UIColor.hubSeparator
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(100)
                make.bottom.equalToSuperview().offset(-50)
            }
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(1)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
        }
        
        if type != 2 {
            bottomNavBackground.layer.cornerRadius = 32
            bottomNavBackground.layer.masksToBounds = true
            bottomNavBackground.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            
            bottomNavMain.tintColor = UIColor.white
            bottomNavScripts.tintColor = UIColor.white
            bottomNavCameras.tintColor = UIColor.white
            bottomNavSettings.tintColor = UIColor.white
            
            bottomNavLine.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(0)
                make.bottom.equalTo(bottomNavFrame.snp.top)
            }
            
            bottomNavFrame.snp.remakeConstraints { make in
                make.width.equalToSuperview()
                make.height.equalTo(150)
                make.bottom.equalToSuperview().offset(-50)
            }
        }
    }
    
    @objc func bottomNavClicked(gesture: UITapGestureRecognizer) {
        var viewControllers = nV!.viewControllers
        _ = viewControllers.popLast()
        
        switch gesture.view {
        case bottomNavMain:
            viewControllers.append(XMainController(type: .site, name: nil))
        case bottomNavScripts:
            viewControllers.append(XScriptsController())
        case bottomNavEvents:
            viewControllers.append(AllEventsController())
        case bottomNavCameras:
            viewControllers.append(CameraListController())
        default:
            viewControllers.append(XSettingsController())
        }
        
        nV!.setViewControllers(viewControllers, animated: false)
    }
    
    @objc func stateClick(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                gesture.view!.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.view.addSubview(self.shadowLayer)
                self.shadowLayer.addSubview(self.alertContainer)
                self.alertContainer.addSubview(self.alertText)
                
                self.alertText.text = self.bottomNavState.text
                
                self.shadowLayer.addGestureRecognizer(
                    UITapGestureRecognizer(
                        target: self,
                        action: #selector(self.hideAlert)
                    )
                )

                self.shadowLayer.snp.remakeConstraints { make in
                    make.width.height.equalToSuperview()
                }
                
                self.alertContainer.snp.remakeConstraints { make in
                    make.height.equalTo(100)
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                self.alertText.snp.remakeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(43)
                    make.trailing.equalToSuperview().offset(-43)
                }
                
                self.shadowLayer.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.shadowLayer.alpha = 1
                }
            }
        }
    }
    
    @objc func hideAlert(gesture: UILongPressGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.shadowLayer.alpha = 0
        }
    }
}
