import UIKit
import Localize_Swift

class StartView: XBaseView {
    
    lazy var mainView: UIView = {
        return UIView()
    }()
    
    lazy var shadowLayer: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertContainerScroll: UIScrollView = {
        let view = UIScrollView()
        
        view.alwaysBounceVertical = true
        
        return view
    }()
    
    lazy var alertLicTitle: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.title_faq
        view.font = UIFont(name: "OpenSans-Light", size: 22)
        view.font = view.font.withSize(22)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertLicText: UILabel = {
        let view = UILabel()
        
        view.text =
            "Об условиях использования программного обеспечения " +
            "Секьюрити Хаб TM, " +
            "Security Hub TM, " +
            "Астра TM Astra TM\n" +
            "Настоящее Лицензионное соглашение является официальным документом и заключается между Вами, физическим или юридическим лицом, и Обществом с ограниченной ответственностью 'Теко- Торговый дом' (далее по тексту – «Правообладатель»), являющимся обладателем исключительных имущественных авторских прав на использование программного обеспечения\n" +
            "Секьюрити Хаб TM " +
            "Security Hub TM\n" +
            "(далее по тексту – «Программное обеспечение» или «ПО»), предоставляемого на условиях условно-бесплатного использования, о нижеследующем:\n" +
            "1. Все пункты и условия настоящего Лицензионного соглашения относятся к использованию Программного обеспечения, которое является объектом исключительных прав Правообладателя. Использование ПО с нарушением условий настоящего Лицензионного соглашения и/или Инструкции ПО является нарушением законодательства и влечет за собой гражданскую, а также административную или уголовную ответственность.\n" +
            "2. Выбор Вами пункта «Я принимаю условия Лицензионного соглашения» и нажатие на кнопку «Регистрация» в процессе регистрации аккаунта на публичном сервере ТЕКО (public.opasnost.net), после скачивания ПО и установки на мобильное устройство, означают Ваше полное и безоговорочное согласие со всеми пунктами и условиями настоящего Лицензионного соглашения.\n" +
            "3. При условии Вашего полного и безоговорочного согласия со всеми пунктами и условиями настоящего Лицензионного соглашения, Вам предоставляется на территории всех стран мира неисключительное и непередаваемое право на использование ПО путем воспроизведения, ограниченного запуском экземпляра ПО и записью его в память мобильного устройства.\n" +
            "4. Все положения настоящего Соглашения распространяются как на ПО в целом, так и на его отдельные компоненты.\n" +
            "5. Настоящее Соглашение заключается до или непосредственно в момент начала использования ПО и действует на протяжении всего срока его правомерного использования Вами в пределах срока действия авторского права на него, при условии надлежащего соблюдения Вами условий настоящего Соглашения.\n" +
            "6. Вы имеете право использовать ПО в полном соответствии с условиями настоящего Лицензионного соглашения:\n" +
            "6.1. Настоящее Соглашение предоставляет право установки (инсталляции), запуска и использования ПО в рамках его функциональных возможностей. Вам предоставляется право удаленной настройки, мониторинга и управления оборудованием, входящим в состав Security Hub (абонентский контроллер и радиодатчики). ПО - это условно бесплатная дополнительная опция к правомерно приобретенному и установленному в помещении, являющегося Вашей собственностью, оборудованию Security Hub, которое не влияет на работу самого оборудования. ПО является упрощенным инструментом настройки, управления и наблюдения за работой оборудования.\n" +
            "6.1.1. Функциональные возможности ПО:\n" +
            "- настраивать оборудование Security Hub;\n" +
            "- контролировать состояние оборудования Security Hub , правомерно приобретенного и установленного в помещении, являющегося Вашей собственностью;\n" +
            "- ставить/снимать оборудование Security Hub с охраны целиком или по отдельным разделам;\n" +
            "- вести журнал событий.\n" +
            "6.1.2. Условия использования ПО:\n" +
            "Обмен информацией между ПО и оборудованием Security Hub осуществляется через ресурсы публичного Сервера. Связь с сервером обеспечивается по каналам Интернет. В связи с этим для корректной работоспособности всего функционала ПО должны быть выполнены следующие условия ('ПРИ НАЛИЧИИ'):\n" +
            "6.1.2.1. правильно выполненные работы по подготовке и настройке оборудования Security Hub, согласно встроенной в ПО инструкции;\n" +
            "6.1.2.2. постоянная, стабильная Интернет связь (2G/3G/LTE/Wi-Fi) на мобильном устройстве, на котором ПО установлено;\n" +
            "6.1.2.3. постоянная, стабильная Интернет связь в сети, в которую подключен абонентский контроллер Security Hub. Скорость в Интернет канале должна быть не менее 2 Мбит/сек.;\n" +
            "6.1.2.4. полная работоспособность мобильного устройства, на которое установлено ПО и соответствие его техническим требованием к мобильному устройству:\n" +
            "Требования к ОС Android – версия 4.2 и выше;\n" +
            "6.2. Принцип работы ПО предполагает постоянный обмен данными с абонентским контроллером Security Hub. В связи с этим Вы должны самостоятельно следить за положительным балансом Вашего счета для оплаты трафика сети Интернет, как на мобильном устройстве, на котором установлено ПО, так и на Объекте, где установлено оборудование системы Security Hub.\n" +
            "6.3. Вы вправе изготовить копию ПО при условии, что эта копия предназначена только для архивных целей или для замены правомерно приобретенного экземпляра ПО в случаях, когда такой экземпляр утерян, уничтожен или стал непригоден для использования. При этом все копии экземпляра ПО не могут быть использованы в целях, отличных от перечисленных в настоящем пункте, и должны быть уничтожены, если владение экземпляром ПО перестало быть правомерным.\n" +
            "6.4. В течение срока использования ПО Вы имеете право обращаться в Службу технической поддержки Правообладателя либо распространителя ПО, имеющего соответствующий договор с Правообладателем. Правообладатель гарантирует ответ на обращение пользователя, указавшего свои регистрационные данные, поступившее через Службу технической поддержки на интернет-сайте Правообладателя (http://www.teko.biz). Вы соглашаетесь с тем, что полученные от Вас в процессе регистрации данные, а также информация, сообщенная при обращении в Службу технической поддержки, могут быть использованы Правообладателем по его усмотрению исключительно для внутренних нужд. При направлении Правообладателю обращения Пользователя со своей персональной информацией Пользователь в соответствии с Федеральным законом от 27.07.2006 No 152-ФЗ «О персональных данных» свободно, своей волей и в своем интересе дает согласие Обществу с ограниченной ответственностью 'Теко-Торговый дом' (его работникам) на обработку своих персональных данных (фамилия, имя, отчество, номер мобильного телефона, адрес электронной почты, IP-адрес устройства, с которого была направлена заявка) на срок 5 (пять) лет в целях: исполнения настоящего Соглашения, включая реализацию из него прав и обязанностей Правообладателя. Обработка персональных данных Пользователя включает следующие действия: сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, обезличивание, блокирование, удаление, уничтожение Персональных данных (далее – обработка). Обработка персональных данных Правообладателем может осуществляться на бумажных и электронных носителях с использованием и без использования средств автоматизации.\n" +
            "6.5. Вам не разрешается изменять, декомпилировать, деассемблировать, дешифровать и производить иные действия с объектным кодом ПО, имеющие целью получение информации о реализации алгоритмов, используемых в ПО, без письменного согласия на то Правообладателя. Вы не имеете права каким-либо образом модифицировать механизм внутренней защиты ПО. Копирование ПО с заведомо устраненным или испорченным механизмом внутренней защиты, равно как неправомерное использование такого ПО, является незаконным. Вам также запрещено осуществлять распространение ПО в любой материальной форме и любым способом, в том числе, сетевыми или иными способами, а также путем продажи, сдачи в наем, предоставления взаймы, включая импорт, для любой из этих целей.\n" +
            "6.6. Особые условия:\n" +
            "6.6.1. ПО предоставляется Вам «КАК ЕСТЬ» («AS IS») и «ПРИ НАЛИЧИИ», без каких-либо гарантий, в соответствии с общепринятым в международной практике принципом. Это означает, что за проблемы, возникающие в процессе эксплуатации Вами экземпляра ПО (в том числе: проблемы совместимости с другими программными продуктами (пакетами, драйверами и др.), проблемы, возникающие из- за неоднозначного толкования Вами справочной информации в отношении ПО, несоответствия результатов использования ПО Вашим ожиданиям и т.п.), Правообладатель ответственности не несет.\n" +
            "6.6.2. Вы самостоятельно несете ответственность за возможные негативные последствия, вызванные несовместимостью или конфликтами ПО с другими программными продуктами, установленными на том же мобильном устройстве. ПО не предназначено и не может быть использовано в информационных системах, работающих в опасных средах, либо обслуживающих системы жизнеобеспечения, в которых сбой в работе ПО может создать угрозу жизни и здоровью людей или иные тяжкие последствия для живых существ и имущества. ВЫ ОСОЗНАЕТЕ И СОГЛАШАЕТЕСЬ С ТЕМ, ЧТО Общество с ограниченной ответственностью 'Теко-Торговый дом', ЕЕ ДОЧЕРНИЕ И АФФИЛИРОВАННЫЕ КОМПАНИИ И ЕЕ ЛИЦЕНЗИАРЫ НЕ НЕСУТ НИКАКОЙ ОТВЕТСТВЕННОСТИ ПЕРЕД ВАМИ ЗА ПОНЕСЕННЫЙ ВАМИ ПРЯМОЙ, КОСВЕННЫЙ, СЛУЧАЙНЫЙ, СПЕЦИАЛЬНЫЙ, ОПОСРЕДОВАННЫЙ ИЛИ ШТРАФНОЙ УЩЕРБ, ВКЛЮЧАЯ ПОТЕРЮ ДАННЫХ, ВНЕ ЗАВИСИМОСТИ ОТ ТОГО, ИМЕЛИСЬ ЛИ У Общества с ограниченной ответственностью 'Теко-Торговый дом' ИЛИ ЕЕ ПРЕДСТАВИТЕЛЕЙ СВЕДЕНИЯ ИЛИ ПРЕДПОЛОЖЕНИЯ О ВОЗМОЖНОСТИ ТАКОГО УЩЕРБА.\n" +
            "6.6.3. Общество с ограниченной ответственностью 'Теко-Торговый дом' не инициирует и не контролирует размещение Вами любой информации в процессе использования ПО или полученной с использованием ПО, не влияет на ее содержание и целостность, а также, в момент ее размещения, не знает и не может знать, нарушает ли она охраняемые законом права и интересы третьих лиц, международные договоры и действующее Применимое законодательство.\n" +
            "6.6.4. Общество с ограниченной ответственностью 'Теко-Торговый дом' не несет никакой ответственности за выбранный Вами тариф, оператора связи, не компенсирует никакой прямой, косвенный или иной ущерб, или Ваши затраты на обмен данными по сети Интернет, как в роуминге, так и в домашнем регионе.\n" +
            "7. По всем вопросам, не урегулированным настоящим Соглашением, Стороны руководствуются нормами действующего Применимого законодательства.\n" +
            "8. В случае нарушения Вами условий настоящего Соглашения по использованию ПО, в том числе, но не ограничиваясь положениями раздела 6 настоящего Соглашения, с такого момента Вы не вправе использовать ПО.\n" +
            "9. ПО является результатом интеллектуальной деятельности и объектом авторских прав (программа для ЭВМ), которые регулируются и защищены Применимым законодательством об интеллектуальной собственности и нормами международного права."
        
        view.font = UIFont(name: "OpenSans-Light", size: 12)
        view.font = view.font.withSize(12)
        view.textColor = UIColor.hubHint
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var accept: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "checkBoxEmpty")
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(acceptChange)
            )
        )
        
        return view
    }()
    
    lazy var acceptLabel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.acceptLic
        view.font = UIFont(name: "OpenSans-Light", size: 12)
        view.font = view.font!.withSize(12)
        view.textColor = UIColor.hubTextBlack
        view.isUserInteractionEnabled = true
        view.numberOfLines = 0
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(acceptChange)
            )
        )
        
        return view
    }()
    
    lazy var alertLicButtonAccept: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.accept
        view.font = UIFont(name: "OpenSans-Light", size: 14)
        view.font = view.font.withSize(14)
        view.textColor = UIColor.hubGrey
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(licAccept)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertLicButtonCancel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.decline
        view.font = UIFont(name: "OpenSans-Light", size: 14)
        view.font = view.font.withSize(14)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(showCancelAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertCancelText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.declineLic
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertCancelButtonContinue: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.decline
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(exitApp)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertCancelButtonCancel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_cancel
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(showLicAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertRegText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.regText
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertRegButtonContinue: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_continue
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(openSite)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertRegButtonCancel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_cancel
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    private lazy var logo: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "logo")
        view.contentMode = .scaleAspectFill
        
        return view
    }()
    
    lazy var signIn: UIButton = {
        let view = UIButton()
        
        view.backgroundColor = UIColor.hubMainColor
        view.layer.cornerRadius = 12
        view.setTitle(R.strings.button_sign_in, for: .normal)
        view.setTitleColor(UIColor.white, for: .normal)
        
        return view
    }()
    
    lazy var signUp: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_sign_up
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(showRegAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    override func setContent() {
        xView.addSubview(mainView)
        mainView.addSubview(logo)
        mainView.addSubview(signIn)
        mainView.addSubview(signUp)
        
        if UserDefaults.standard.bool(forKey: "licAccepted") != true {
            showLicAlert(gesture: nil)
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        mainView.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        
        logo.snp.remakeConstraints { make in
            make.centerY.equalToSuperview().offset(-UIApplication.shared.statusBarFrame.height)
            make.centerX.equalToSuperview()
            make.width.equalTo(184)
            make.height.equalTo(139)
        }
        
        signUp.snp.remakeConstraints { make in
            make.bottom.equalToSuperview().offset(-40)
            make.centerX.equalToSuperview()
        }
        
        signIn.snp.remakeConstraints { make in
            make.height.equalTo(40)
            make.leading.equalToSuperview().offset(18)
            make.trailing.equalToSuperview().offset(-18)
            make.bottom.equalTo(signUp.snp.top).offset(-11)
        }
        
        mainView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.mainView.alpha = 1
        }
    }
    
    @objc func showLicAlert(gesture: UILongPressGestureRecognizer?) {
        if gesture != nil {
            if gesture!.state == .began {
                UIView.animate(withDuration: 0.1) {
                    self.alertCancelButtonCancel.transform =
                        CGAffineTransform(scaleX: 0.9, y: 0.9)
                }
            } else if gesture!.state == .ended {
                UIView.animate(withDuration: 0.1) {
                    self.alertCancelButtonCancel.transform = CGAffineTransform.identity
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.alertContainer.subviews.forEach { $0.removeFromSuperview() }
                    
                    self.mainView.addSubview(self.shadowLayer)
                    self.shadowLayer.addSubview(self.alertContainer)
                    self.alertContainer.addSubview(self.alertContainerScroll)
                    self.alertContainer.addSubview(self.alertLicTitle)
                    self.alertContainerScroll.addSubview(self.alertLicText)
                    self.alertContainer.addSubview(self.accept)
                    self.alertContainer.addSubview(self.acceptLabel)
                    self.alertContainer.addSubview(self.alertLicButtonAccept)
                    self.alertContainer.addSubview(self.alertLicButtonCancel)
                    
                    self.shadowLayer.gestureRecognizers?.removeAll()
                    
                    self.shadowLayer.snp.remakeConstraints { make in
                        make.width.height.equalToSuperview()
                    }
                    
                    self.alertContainer.snp.remakeConstraints { make in
                        make.leading.equalToSuperview().offset(20)
                        make.trailing.equalToSuperview().offset(-20)
                        make.top.equalToSuperview().offset(60)
                        make.bottom.equalToSuperview().offset(-60)
                    }
                    
                    self.alertLicTitle.snp.remakeConstraints { make in
                        make.leading.equalToSuperview().offset(35)
                        make.trailing.equalToSuperview().offset(-35)
                        make.top.equalToSuperview().offset(20)
                    }
                    
                    self.alertLicButtonCancel.snp.remakeConstraints { make in
                        make.bottom.equalToSuperview().offset(-35)
                        make.leading.equalToSuperview().offset(60)
                    }
                    
                    self.alertLicButtonAccept.snp.remakeConstraints { make in
                        make.bottom.equalToSuperview().offset(-35)
                        make.trailing.equalToSuperview().offset(-60)
                    }
                    
                    self.accept.snp.remakeConstraints { make in
                        make.width.height.equalTo(20)
                        make.leading.equalToSuperview().offset(20)
                        make.bottom.equalTo(self.alertLicButtonCancel.snp.top).offset(-30)
                    }
                    
                    self.acceptLabel.snp.remakeConstraints { make in
                        make.leading.equalTo(self.accept.snp.trailing).offset(8)
                        make.trailing.equalToSuperview().offset(-20)
                        make.centerY.equalTo(self.accept.snp.centerY)
                    }
                    
                    self.alertContainerScroll.snp.remakeConstraints { make in
                        make.bottom.equalTo(self.accept.snp.top).offset(-20)
                        make.width.equalToSuperview()
                        make.top.equalTo(self.alertLicTitle.snp.bottom).offset(20)
                    }
                    
                    self.alertLicText.snp.remakeConstraints { make in
                        make.width.equalToSuperview().offset(-40)
                        make.top.equalToSuperview()
                        make.bottom.equalToSuperview()
                        make.leading.equalToSuperview().offset(20)
                    }
                    
                    self.shadowLayer.alpha = 0
                    UIView.animate(withDuration: 0.3) {
                        self.shadowLayer.alpha = 1
                    }
                }
            }
        } else {
            alertContainer.subviews.forEach { $0.removeFromSuperview() }
            
            mainView.addSubview(shadowLayer)
            shadowLayer.addSubview(alertContainer)
            alertContainer.addSubview(alertContainerScroll)
            alertContainer.addSubview(alertLicTitle)
            alertContainerScroll.addSubview(alertLicText)
            alertContainer.addSubview(accept)
            alertContainer.addSubview(acceptLabel)
            alertContainer.addSubview(alertLicButtonAccept)
            alertContainer.addSubview(alertLicButtonCancel)
            
            shadowLayer.gestureRecognizers?.removeAll()
            
            shadowLayer.snp.remakeConstraints { make in
                make.width.height.equalToSuperview()
            }
            
            alertContainer.snp.remakeConstraints { make in
                make.leading.equalToSuperview().offset(20)
                make.trailing.equalToSuperview().offset(-20)
                make.top.equalToSuperview().offset(60)
                make.bottom.equalToSuperview().offset(-60)
            }
            
            alertLicTitle.snp.remakeConstraints { make in
                make.leading.equalToSuperview().offset(35)
                make.trailing.equalToSuperview().offset(-35)
                make.top.equalToSuperview().offset(20)
            }
            
            alertLicButtonCancel.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-35)
                make.leading.equalToSuperview().offset(60)
            }
            
            alertLicButtonAccept.snp.remakeConstraints { make in
                make.bottom.equalToSuperview().offset(-35)
                make.trailing.equalToSuperview().offset(-60)
            }
            
            accept.snp.remakeConstraints { make in
                make.width.height.equalTo(20)
                make.leading.equalToSuperview().offset(20)
                make.bottom.equalTo(alertLicButtonCancel.snp.top).offset(-30)
            }
            
            acceptLabel.snp.remakeConstraints { make in
                make.leading.equalTo(accept.snp.trailing).offset(8)
                make.trailing.equalToSuperview().offset(-20)
                make.centerY.equalTo(accept.snp.centerY)
            }
            
            alertContainerScroll.snp.remakeConstraints { make in
                make.bottom.equalTo(accept.snp.top).offset(-20)
                make.width.equalToSuperview()
                make.top.equalTo(alertLicTitle.snp.bottom).offset(20)
            }
            
            alertLicText.snp.remakeConstraints { make in
                make.width.equalToSuperview().offset(-40)
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview().offset(20)
            }
            
            shadowLayer.alpha = 0
            UIView.animate(withDuration: 0.3) {
                self.shadowLayer.alpha = 1
            }
        }
    }
    
    @objc func showCancelAlert(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.alertLicButtonCancel.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.alertLicButtonCancel.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.alertContainer.subviews.forEach { $0.removeFromSuperview() }
                
                self.mainView.addSubview(self.shadowLayer)
                self.shadowLayer.addSubview(self.alertContainer)
                self.alertContainer.addSubview(self.alertCancelText)
                self.alertContainer.addSubview(self.alertCancelButtonContinue)
                self.alertContainer.addSubview(self.alertCancelButtonCancel)
                
                self.shadowLayer.gestureRecognizers?.removeAll()
                
                self.shadowLayer.snp.remakeConstraints { make in
                    make.width.height.equalToSuperview()
                }
                
                self.alertContainer.snp.remakeConstraints { make in
                    make.height.equalTo(400)
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                self.alertCancelText.snp.remakeConstraints { make in
                    make.top.equalToSuperview().offset(25)
                    make.leading.equalToSuperview().offset(43)
                    make.trailing.equalToSuperview().offset(-43)
                }
                
                self.alertCancelButtonCancel.snp.remakeConstraints { make in
                    make.bottom.equalToSuperview().offset(-25)
                    make.leading.equalToSuperview().offset(25)
                    make.trailing.equalToSuperview().offset(-25)
                }
                
                self.alertCancelButtonContinue.snp.remakeConstraints { make in
                    make.bottom.equalTo(self.alertCancelButtonCancel.snp.top).offset(-25)
                    make.leading.equalToSuperview().offset(25)
                    make.trailing.equalToSuperview().offset(-25)
                }

                self.shadowLayer.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.shadowLayer.alpha = 1
                }
            }
        }
    }
    
    @objc func showRegAlert(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.signUp.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.signUp.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.alertContainer.subviews.forEach { $0.removeFromSuperview() }
                
                self.mainView.addSubview(self.shadowLayer)
                self.shadowLayer.addSubview(self.alertContainer)
                self.alertContainer.addSubview(self.alertRegText)
                self.alertContainer.addSubview(self.alertRegButtonContinue)
                self.alertContainer.addSubview(self.alertRegButtonCancel)
                
                self.shadowLayer.addGestureRecognizer(
                    UITapGestureRecognizer(
                        target: self,
                        action: #selector(self.hideAlert)
                    )
                )

                self.shadowLayer.snp.remakeConstraints { make in
                    make.width.height.equalToSuperview()
                }
                
                self.alertContainer.snp.remakeConstraints { make in
                    make.height.equalTo(300)
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                self.alertRegText.snp.remakeConstraints { make in
                    make.top.equalToSuperview().offset(25)
                    make.leading.equalToSuperview().offset(43)
                    make.trailing.equalToSuperview().offset(-43)
                }
                
                self.alertRegButtonCancel.snp.remakeConstraints { make in
                    make.bottom.equalToSuperview().offset(-25)
                    make.leading.equalToSuperview().offset(25)
                    make.trailing.equalToSuperview().offset(-25)
                }
                
                self.alertRegButtonContinue.snp.remakeConstraints { make in
                    make.bottom.equalTo(self.alertRegButtonCancel.snp.top).offset(-25)
                    make.leading.equalToSuperview().offset(25)
                    make.trailing.equalToSuperview().offset(-25)
                }

                self.shadowLayer.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.shadowLayer.alpha = 1
                }
            }
        }
    }
    
    @objc func hideAlert(gesture: UILongPressGestureRecognizer?) {
        if gesture != nil {
            if gesture!.view == alertRegButtonCancel {
                if gesture!.state == .began {
                    UIView.animate(withDuration: 0.1) {
                        self.alertRegButtonCancel.transform =
                            CGAffineTransform(scaleX: 0.9, y: 0.9)
                    }
                } else if gesture!.state == .ended {
                    UIView.animate(withDuration: 0.1) {
                        self.alertRegButtonCancel.transform = CGAffineTransform.identity
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        UIView.animate(withDuration: 0.3) {
                            self.shadowLayer.alpha = 0
                        }
                    }
                }
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.shadowLayer.alpha = 0
            }
        }
    }
    
    @objc func licAccept(gesture: UILongPressGestureRecognizer) {
        if alertLicButtonAccept.textColor == UIColor.hubMainColor {
            if gesture.state == .began {
                UIView.animate(withDuration: 0.1) {
                    self.alertLicButtonAccept.transform =
                        CGAffineTransform(scaleX: 0.9, y: 0.9)
                }
            } else if gesture.state == .ended {
                UIView.animate(withDuration: 0.1) {
                    self.alertLicButtonAccept.transform = CGAffineTransform.identity
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    UserDefaults.standard.setValue(true, forKey: "licAccepted")
                        
                    self.hideAlert(gesture: nil)
                }
            }
        }
    }
    
    @objc func acceptChange() {
        alertLicButtonAccept.textColor =
            alertLicButtonAccept.textColor == UIColor.hubGrey
                ? UIColor.hubMainColor
                : UIColor.hubGrey
        
        accept.image =
            alertLicButtonAccept.textColor == UIColor.hubGrey
                ? UIImage(named: "checkBoxEmpty")
                : UIImage(named: "checkBox")
    }
    
    @objc func exitApp(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.alertCancelButtonContinue.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.alertCancelButtonContinue.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                exit(0)
            }
        }
    }
    
    @objc func openSite(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.alertRegButtonContinue.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.alertRegButtonContinue.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if let url = URL(string: "\(XTargetUtils.regLink!)?p=100&l=\(Localize.currentLanguage())") {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
}
