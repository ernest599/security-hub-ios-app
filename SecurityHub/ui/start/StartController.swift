import Foundation
import WebKit

class StartController: XBaseController<StartView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        xView.nV = self.navigationController
        
        configViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !(DataManager.defaultHelper.language == "ru" && XTargetUtils.target == .security_hub) {
            xView.signUp.isHidden = true
        }
    }
    
    func configViews() {
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(goToSignIn)
            )
        
        gesture.minimumPressDuration = 0
        xView.signIn.addGestureRecognizer(gesture)
    }
    
    @objc func goToSignIn(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.xView.signIn.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.xView.signIn.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.xView.nV!.pushViewController(LoginController(), animated: true)
            }
        }
    }
}
