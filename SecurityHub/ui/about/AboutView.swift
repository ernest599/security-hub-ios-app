import UIKit
import Cosmos
import MessageUI
import StoreKit

class AboutView: BaseView, MFMailComposeViewControllerDelegate {
    
    lazy var mainView: UIView = {
        return UIView()
    }()
    
    lazy var shadowLayer: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertText: UILabel = {
        let view = UILabel()
        
        view.text = "Спасибо за отзыв!"
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertButton: UILabel = {
        let view = UILabel()
        
        view.text = "Ок"
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var starsView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 26
        view.layer.backgroundColor = UIColor.hubMainColor.cgColor
        
        return view
    }()
    
    lazy var titleWhite: UILabel = {
        let view = UILabel()
        
        view.text = "Оцените"
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.textColor = UIColor.white
        view.numberOfLines = 0
        
        return view
    }()
    
    lazy var ratingBar: CosmosView = {
        let view = CosmosView()
        
        view.settings.fillMode = .full
        view.settings.filledColor = UIColor.hubGold
        view.settings.emptyColor = UIColor.white
        view.settings.emptyBorderWidth = 0
        view.settings.filledBorderWidth = 0
        view.settings.starSize = 32
        view.settings.starMargin = 22
        view.rating = 0
        view.didFinishTouchingCosmos = { rating in
            self.starsView.removeFromSuperview()
            self.mainView.removeFromSuperview()

            self.updateConstraints()
            self.showWithoutStars()
            
            if rating < 4 {
                self.showAlert()
            } else {
                SKStoreReviewController.requestReview()
            }
            
            UserDefaults.standard.setValue(true, forKey: "isUserEstimated")
        }
        
        return view
    }()

    lazy var backArrow: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "backArrow")
        
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.title_about
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        
        return view
    }()
    
    private lazy var logo: UIImageView = {
        let view = UIImageView()
        
        view.image = #imageLiteral(resourceName: "logo_blue_name")
        view.contentMode = .scaleAspectFill
        
        return view
    }()
    
    lazy var version: UILabel = {
        let view = UILabel()
        
        view.text = "\(R.strings.options_app_version): \(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")!)"
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var developer: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_app_developer
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var webSite: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_app_web
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var webSite1: UILabel = {
        let view = UILabel()
        
        view.text = "teko.biz"
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var webSite2: UILabel = {
        let view = UILabel()
        
        view.text = "security-hub.ru"
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var webSite3: UILabel = {
        let view = UILabel()
        
        view.text = "help@security-hub.ru"
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var support: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_app_techsupport
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var supportEmail: UILabel = {
        let view = UILabel()
        
        view.text = "support@security-hub.ru"
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font.withSize(15.5)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var copyright: UILabel = {
        let view = UILabel()
        
        view.text = "© 2017-2021 Security Hub"
        view.font = UIFont(name: "OpenSans-Light", size: 11.3)
        view.font = view.font.withSize(11.3)
        view.textColor = UIColor.hubHint
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    override func setContent() {
        contentView.isScrollEnabled = false
        
        if UserDefaults.standard.bool(forKey: "isUserEstimated") {
            showWithoutStars()
        } else {
            showWithStars()
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        contentView.addSubview(mainView)
        mainView.addSubview(version)
        mainView.addSubview(developer)
        mainView.addSubview(webSite)
        mainView.addSubview(webSite1)
        mainView.addSubview(webSite2)
        mainView.addSubview(webSite3)
        mainView.addSubview(support)
        mainView.addSubview(supportEmail)
        mainView.addSubview(copyright)
        
        mainView.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        
        version.snp.remakeConstraints { make in
            make.top.equalTo(logo.snp.bottom).offset(32)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        developer.snp.remakeConstraints { make in
            make.top.equalTo(version.snp.bottom).offset(9)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        webSite.snp.remakeConstraints { make in
            make.top.equalTo(developer.snp.bottom)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        webSite1.snp.remakeConstraints { make in
            make.top.equalTo(webSite.snp.bottom)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        webSite2.snp.remakeConstraints { make in
            make.top.equalTo(webSite1.snp.bottom)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        webSite3.snp.remakeConstraints { make in
            make.top.equalTo(webSite2.snp.bottom)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        support.snp.remakeConstraints { make in
            make.top.equalTo(webSite3.snp.bottom).offset(9)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        supportEmail.snp.remakeConstraints { make in
            make.top.equalTo(support.snp.bottom)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        copyright.snp.remakeConstraints { make in
            make.bottom.equalToSuperview().offset(-91)
            make.leading.equalToSuperview().offset(52)
            make.trailing.equalToSuperview().offset(-52)
        }
        
        webSite1.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(openSite(gesture:))
            )
        )
        
        webSite2.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(openSite(gesture:))
            )
        )
        
        webSite3.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(sendEmail(gesture:))
            )
        )
        
        supportEmail.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(sendEmail(gesture:))
            )
        )
    }
    
    func showWithStars() {
        mainView.addSubview(starsView)
        starsView.addSubview(backArrowView)
        starsView.addSubview(backArrow)
        starsView.addSubview(titleWhite)
        starsView.addSubview(ratingBar)
        mainView.addSubview(logo)
        
        backArrowView.gestureRecognizers?.removeAll()
        let gesture = UILongPressGestureRecognizer(
            target: self,
            action: #selector(backClickedWhite(gesture:))
        )
        gesture.minimumPressDuration = 0
        backArrowView.addGestureRecognizer(gesture)
        
        backArrow.image = UIImage(named: "backArrowWhite")
        
        starsView.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(220)
            make.top.equalToSuperview().offset(-50)
        }
        
        backArrowView.snp.remakeConstraints { make in
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.top.equalTo(mainView.snp.top).offset(32)
            make.leading.equalToSuperview().offset(12)
        }
        
        backArrow.snp.remakeConstraints { make in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.top.equalTo(mainView.snp.top).offset(42)
            make.leading.equalToSuperview().offset(22)
        }
        
        titleWhite.snp.remakeConstraints { make in
            make.top.equalTo(mainView.snp.top).offset(33)
            make.centerX.equalToSuperview()
        }
        
        ratingBar.snp.remakeConstraints { make in
            make.height.equalTo(32)
            make.top.equalTo(titleWhite.snp.bottom).offset(42)
            make.centerX.equalToSuperview()
        }
        
        logo.snp.remakeConstraints { make in
            make.width.equalTo(145)
            make.height.equalTo(110)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-100)
        }
    }
    
    func showWithoutStars() {
        mainView.addSubview(backArrowView)
        mainView.addSubview(backArrow)
        mainView.addSubview(title)
        mainView.addSubview(logo)
        
        backArrow.image = UIImage(named: "backArrow")
        backArrowView.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1)
        
        backArrowView.snp.remakeConstraints { make in
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.top.equalTo(mainView.snp.top).offset(32)
            make.leading.equalToSuperview().offset(12)
        }
        
        backArrow.snp.remakeConstraints { make in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.top.equalToSuperview().offset(42)
            make.leading.equalToSuperview().offset(22)
        }
        
        title.snp.remakeConstraints { make in
            make.centerY.equalTo(backArrow.snp.centerY)
            make.leading.equalTo(backArrow.snp.trailing).offset(21)
            make.width.equalToSuperview().offset(-42)
        }
        
        logo.snp.remakeConstraints { make in
            make.width.equalTo(145)
            make.height.equalTo(110)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(-100)
        }
    }
    
    func showAlert() {
        view.addSubview(shadowLayer)
        shadowLayer.addSubview(alertContainer)
        alertContainer.addSubview(alertText)
        alertContainer.addSubview(alertButton)

        shadowLayer.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        alertContainer.snp.remakeConstraints { make in
            make.height.equalTo(192)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
        }
        
        alertText.snp.remakeConstraints { make in
            make.centerY.equalToSuperview().offset(-25)
            make.leading.equalToSuperview().offset(43)
            make.trailing.equalToSuperview().offset(-43)
        }
        
        alertButton.snp.remakeConstraints { make in
            make.bottom.equalToSuperview().offset(-25)
            make.leading.equalToSuperview().offset(125)
            make.trailing.equalToSuperview().offset(-125)
        }

        UIView.animate(withDuration: 0.3) {
            self.shadowLayer.alpha = 1
        }
    }
    
    @objc func sendEmail(gesture: UITapGestureRecognizer) {
        print("Mailed")
        let targetMail = (gesture.view as! UILabel).text!
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            
            mail.mailComposeDelegate = self
            mail.setToRecipients([targetMail])
            
            vc!.present(mail, animated: true)
        } else if let emailUrl = createEmailUrl(to: targetMail) {
            UIApplication.shared.open(emailUrl)
        }
    }
    
    private func createEmailUrl(to: String) -> URL? {
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)")
        let defaultUrl = URL(string: "mailto:\(to)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
    
    @objc func openSite(gesture: UITapGestureRecognizer) {
        print("Sited")
        guard let url = URL(string: "http://\((gesture.view as! UILabel).text!)") else { return }
        UIApplication.shared.open(url)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @objc func hideAlert(gesture: UILongPressGestureRecognizer) {
        if gesture.view == self.alertButton {
            if gesture.state == .began {
                UIView.animate(withDuration: 0.1) {
                    self.alertButton.transform =
                        CGAffineTransform(scaleX: 0.8, y: 0.8)
                }
            } else if gesture.state == .ended {
                UIView.animate(withDuration: 0.1) {
                    self.alertButton.transform = CGAffineTransform.identity
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    UIView.animate(withDuration: 0.3) {
                        self.shadowLayer.alpha = 0
                    }
                }
            }
        }
    }
    
    @objc func backClickedWhite(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                self.backArrowView.alpha = 1
            }
        } else if gesture.state == .changed {
            if distanceBetween2Points(
                self.backArrowView.center,
                gesture.location(in: self.backArrowView.superview)
            ) > self.backArrowView.frame.height / 2 {
                UIView.animate(withDuration: 0.3) {
                    self.backArrowView.alpha = 0.02
                }
            }
        } else if gesture.state == .ended {
            if self.backArrowView.alpha == 1 {
                UIView.animate(withDuration: 0.3) {
                    self.backArrowView.alpha = 0.02
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.nV?.popViewController(animated: true)
                }
            }
        }
    }
}
