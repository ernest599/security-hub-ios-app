import Foundation
import UIKit

class AboutController: BaseController<AboutView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = R.strings.title_about
        mainView.vc = self
        
        navigationController!.interactivePopGestureRecognizer!.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension AboutController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
