import UIKit
import RxSwift
import Localize_Swift

class LoginController: BaseController<LoginView> {
    
    override func loadView() {
        super.loadView()
        
        self.mainView.loginInput.delegate = self
        self.mainView.passwordInput.delegate = self
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            AppUtility.lockOrientation(.portrait)
            
            self.mainView.addGestureRecognizer(
                UITapGestureRecognizer.init(
                    target: self,
                    action: #selector(self.viewTapped)
                )
            )
            
            let gesture =
                UILongPressGestureRecognizer(
                    target: self,
                    action: #selector(self.signIn)
                )
            
            gesture.minimumPressDuration = 0
            self.mainView.signIn.addGestureRecognizer(gesture)
            
            self.navigationController!.interactivePopGestureRecognizer!.delegate = self

            if let login = UserDefaults.standard.string(forKey: "saveMeViewLogin"),
               let pass = UserDefaults.standard.string(forKey: "saveMeViewPassword") {
                self.mainView.loginInput.text = login
                self.mainView.passwordInput.text = pass
                
                self.mainView.saveMeEnabled = true
                self.mainView.saveMe.image = UIImage(named: "checkBox")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !(DataManager.defaultHelper.language == "ru" && XTargetUtils.target == .security_hub) {
            self.mainView.signUp.isHidden = true
        }
    }
    
    @objc func signIn(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.mainView.signIn.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.mainView.signIn.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.login()
            }
        }
    }

    @objc func login() {
        mainView.removeConnactionStateChange()
        
        mainView.loginInput.text =
            mainView.loginInput.text!.replacingOccurrences(of: " ", with: "")
        
        guard let login = mainView.loginInput.text,
              let pass = mainView.passwordInput.text,
              login.count > 0 && pass.count > 0
        else {
            mainView.errorMessage = "Введите логин и пароль"
            return mainView.showErrorAlert()
        }
        
        DataManager.connectDisposable =
            DataManager.shared.connect(login: login, password: pass.md5).subscribe(onNext: { result in
                DataManager.connectDisposable?.dispose()
                
                if result.success {
                    self.navigationController?.pushViewController(
                        PinController(type: .newPin, code: nil, cancelTitle: nil, cancel: nil, complete: {
                            NavigationHelper.shared.showMain()
                        }
                    ), animated: true)
                    
                    UserDefaults.standard.set(nil, forKey: "saveMeViewLogin")
                    UserDefaults.standard.set(nil, forKey: "saveMeViewPassword")
                    
                    if self.mainView.saveMeEnabled {
                        UserDefaults.standard.set(login, forKey: "saveMeViewLogin")
                        UserDefaults.standard.set(pass, forKey: "saveMeViewPassword")
                    }
                } else {
                    self.mainView.errorMessage = result.errorMes!
                    self.mainView.showErrorAlert()
                }
            }
        )
    }

    @objc func viewTapped() {
        if mainView.loginInput.isFirstResponder == true {
            mainView.loginInput.resignFirstResponder()
        } else if mainView.passwordInput.isFirstResponder == true {
            mainView.passwordInput.resignFirstResponder()
        }
    }
}

extension LoginController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension LoginController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.hubMainColor.cgColor
        
        textField.attributedPlaceholder =
            NSAttributedString(
                string: "",
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.hubGrey]
            )
        
        if textField == mainView.loginInput {
            mainView.loginLabel.textColor = UIColor.hubMainColor
            
            UIView.animate(withDuration: 0.3) {
                self.mainView.loginLabel.alpha = 1
            }
        } else {
            mainView.passwordLabel.textColor = UIColor.hubMainColor
            
            UIView.animate(withDuration: 0.3) {
                self.mainView.passwordLabel.alpha = 1
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = UIColor.hubGrey.cgColor
        
        if textField == mainView.loginInput {
            mainView.loginLabel.textColor = UIColor.hubGrey
            
            textField.attributedPlaceholder =
                NSAttributedString(
                    string: R.strings.emailLogin,
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.hubGrey]
                )
            
            if textField.text!.count == 0 {
                UIView.animate(withDuration: 0.3) {
                    self.mainView.loginLabel.alpha = 0
                }
            }
        } else {
            mainView.passwordLabel.textColor = UIColor.hubGrey
            
            textField.attributedPlaceholder =
                NSAttributedString(
                    string: R.strings.login_password,
                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.hubGrey]
                )
            
            if textField.text!.count == 0 {
                UIView.animate(withDuration: 0.3) {
                    self.mainView.passwordLabel.alpha = 0
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == mainView.loginInput {
            mainView.passwordInput.becomeFirstResponder()
        } else {
            mainView.passwordInput.resignFirstResponder()
                
            login()
        }
        
        return true
    }
}
