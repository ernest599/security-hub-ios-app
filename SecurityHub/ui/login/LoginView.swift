import UIKit
import Localize_Swift

class LoginView: BaseView {
    
    var saveMeEnabled: Bool = false
    var errorMessage: String!
    
    lazy var shadowLayer: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertRegText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.regText
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertRegButtonContinue: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_continue
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(openReg)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertRegButtonCancel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_cancel
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true

        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var backArrow: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "backArrow")
        
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_sign_in
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        
        return view
    }()
    
    lazy var loginInput: UITextField = {
        let view = UITextField()
        
        view.attributedPlaceholder =
            NSAttributedString(
                string: R.strings.emailLogin,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.hubGrey]
            )

        view.layer.cornerRadius = 12
        view.layer.borderWidth = 1.3
        view.layer.borderColor = UIColor.hubGrey.cgColor
        view.textContentType = .emailAddress
        view.returnKeyType = .next
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font!.withSize(15.5)
        view.tintColor = UIColor.hubMainColor
        view.autocorrectionType = .no
        view.autocapitalizationType = .none
        view.spellCheckingType = .no

        view.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 52))
        view.leftViewMode = .always

        view.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 52))
        view.rightViewMode = .always
        
        return view
    }()
    
    lazy var loginLabel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.emailLogin
        view.backgroundColor = UIColor.white
        view.font = UIFont(name: "OpenSans-Light", size: 11)
        view.font = view.font!.withSize(11)
        view.textColor = UIColor.hubMainColor
        
        return view
    }()
    
    lazy var passwordInput: UITextField = {
        let view = UITextField()
        
        view.attributedPlaceholder =
            NSAttributedString(
                string: R.strings.login_password,
                attributes: [NSAttributedString.Key.foregroundColor: UIColor.hubGrey]
            )

        view.layer.cornerRadius = 12
        view.layer.borderWidth = 1.3
        view.layer.borderColor = UIColor.hubGrey.cgColor
        view.textContentType = .password
        view.returnKeyType = .done
        view.isSecureTextEntry = true
        view.font = UIFont(name: "OpenSans-Light", size: 15.5)
        view.font = view.font!.withSize(15.5)
        view.tintColor = UIColor.hubMainColor

        view.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 52))
        view.leftViewMode = .always

        view.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        view.rightViewMode = .always
        
        return view
    }()
    
    lazy var passwordLabel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.login_password
        view.backgroundColor = UIColor.white
        view.font = UIFont(name: "OpenSans-Light", size: 11)
        view.font = view.font!.withSize(11)
        view.textColor = UIColor.hubMainColor
        view.alpha = 0
        
        return view
    }()
    
    lazy var passwordShow: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "showPassword")
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(changePasswordVisibility)
            )
        )
        
        return view
    }()
    
    lazy var saveMe: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "checkBoxEmpty")
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(saveMeChange)
            )
        )
        
        return view
    }()
    
    lazy var saveMeLabel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.login_lable_save_me
        view.font = UIFont(name: "OpenSans-Light", size: 12)
        view.font = view.font!.withSize(12)
        view.textColor = UIColor.hubHint
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(saveMeChange)
            )
        )
        
        return view
    }()
    
    lazy var recoverPassword: UILabel = {
        let view = UILabel()
        
        view.attributedText =
            NSAttributedString(
                string: R.strings.button_lose_password,
                attributes:
                    [.underlineStyle: NSUnderlineStyle.single.rawValue]
            )
        
        view.font = UIFont(name: "OpenSans-Light", size: 12)
        view.font = view.font!.withSize(12)
        view.textColor = UIColor.hubHint
        view.isUserInteractionEnabled = true
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(showRecoverAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertRecoverText: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.recoverPassword
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertRecoverContinue: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_continue
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(openRecover)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertRecoverCancel: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_cancel
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var signIn: UIButton = {
        let view = UIButton()
        
        view.backgroundColor = UIColor.hubMainColor
        view.layer.cornerRadius = 12
        view.setTitle(R.strings.ivideon_sign_in, for: .normal)
        view.setTitleColor(UIColor.white, for: .normal)
        
        return view
    }()
    
    lazy var signUp: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_sign_up
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(showRegAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var alertErrorText: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertErrorContinue: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.button_continue
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.isUserInteractionEnabled = true
        
        let gesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        
        gesture.minimumPressDuration = 0
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    override func setContent() {
        contentView.isScrollEnabled = false
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        contentView.addSubview(backArrowView)
        contentView.addSubview(backArrow)
        contentView.addSubview(title)
        contentView.addSubview(loginInput)
        contentView.addSubview(loginLabel)
        contentView.addSubview(passwordInput)
        contentView.addSubview(passwordLabel)
        contentView.addSubview(passwordShow)
        contentView.addSubview(saveMe)
        contentView.addSubview(saveMeLabel)
        contentView.addSubview(recoverPassword)
        contentView.addSubview(signIn)
        contentView.addSubview(signUp)
        
        loginInput.becomeFirstResponder()
        backArrowView.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1)
        
        backArrowView.snp.remakeConstraints { make in
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.top.equalToSuperview().offset(32)
            make.leading.equalToSuperview().offset(12)
        }
        
        backArrow.snp.remakeConstraints { make in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.top.equalToSuperview().offset(42)
            make.leading.equalToSuperview().offset(22)
        }
        
        title.snp.remakeConstraints { make in
            make.centerY.equalTo(backArrow.snp.centerY)
            make.leading.equalTo(backArrow.snp.trailing).offset(21)
            make.width.equalToSuperview().offset(-42)
        }
        
        loginInput.snp.remakeConstraints { make in
            make.height.equalTo(52)
            make.centerX.equalToSuperview()
            make.top.equalTo(title.snp.bottom).offset(41)
            make.leading.equalToSuperview().offset(19)
            make.trailing.equalToSuperview().offset(-38)
        }
        
        loginLabel.snp.remakeConstraints { make in
            make.top.equalTo(loginInput.snp.top).offset(-5)
            make.leading.equalTo(loginInput.snp.leading).offset(14)
        }
        
        passwordInput.snp.remakeConstraints { make in
            make.height.equalTo(52)
            make.centerX.equalToSuperview()
            make.top.equalTo(loginInput.snp.bottom).offset(15)
            make.leading.equalToSuperview().offset(19)
            make.trailing.equalToSuperview().offset(-38)
        }
        
        passwordLabel.snp.remakeConstraints { make in
            make.top.equalTo(passwordInput.snp.top).offset(-5)
            make.leading.equalTo(loginInput.snp.leading).offset(14)
        }
        
        passwordShow.snp.remakeConstraints { make in
            make.width.height.equalTo(24)
            make.trailing.equalTo(passwordInput.snp.trailing).offset(-14)
            make.top.equalTo(passwordInput.snp.top).offset(14)
        }
        
        saveMe.snp.remakeConstraints { make in
            make.width.height.equalTo(20)
            make.leading.equalTo(passwordInput.snp.leading)
            make.top.equalTo(passwordInput.snp.bottom).offset(16)
        }
        
        saveMeLabel.snp.remakeConstraints { make in
            make.leading.equalTo(saveMe.snp.trailing).offset(6)
            make.centerY.equalTo(saveMe.snp.centerY)
        }
        
        recoverPassword.snp.remakeConstraints { make in
            make.trailing.equalTo(passwordInput.snp.trailing)
            make.top.equalTo(passwordInput.snp.bottom).offset(19)
        }
        
        signIn.snp.remakeConstraints { make in
            make.height.equalTo(40)
            make.top.equalTo(saveMe.snp.bottom).offset(33)
            make.leading.equalToSuperview().offset(18)
            make.trailing.equalToSuperview().offset(-36)
        }
        
        signUp.snp.remakeConstraints { make in
            make.top.equalTo(signIn.snp.bottom).offset(11)
            make.centerX.equalToSuperview()
        }
    }
    
    func showErrorAlert() {
        alertContainer.subviews.forEach { $0.removeFromSuperview() }
        
        view.endEditing(true)
        
        view.addSubview(shadowLayer)
        shadowLayer.addSubview(alertContainer)
        alertContainer.addSubview(alertErrorText)
        alertContainer.addSubview(alertErrorContinue)
        
        alertErrorText.text = errorMessage
        
        shadowLayer.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )

        shadowLayer.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        alertContainer.snp.remakeConstraints { make in
            make.height.equalTo(150)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
        }
        
        alertErrorText.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(25)
            make.leading.equalToSuperview().offset(43)
            make.trailing.equalToSuperview().offset(-43)
        }
        
        alertErrorContinue.snp.remakeConstraints { make in
            make.bottom.equalToSuperview().offset(-25)
            make.leading.equalToSuperview().offset(25)
            make.trailing.equalToSuperview().offset(-25)
        }
        
        shadowLayer.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.shadowLayer.alpha = 1
        }
    }
    
    @objc func showRecoverAlert() {
        alertContainer.subviews.forEach { $0.removeFromSuperview() }
        
        view.endEditing(true)
        
        view.addSubview(shadowLayer)
        shadowLayer.addSubview(alertContainer)
        alertContainer.addSubview(alertRecoverText)
        alertContainer.addSubview(alertRecoverContinue)
        alertContainer.addSubview(alertRecoverCancel)
        
        shadowLayer.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )

        shadowLayer.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        alertContainer.snp.remakeConstraints { make in
            make.height.equalTo(300)
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
        }
        
        alertRecoverText.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(25)
            make.leading.equalToSuperview().offset(43)
            make.trailing.equalToSuperview().offset(-43)
        }
        
        alertRecoverCancel.snp.remakeConstraints { make in
            make.bottom.equalToSuperview().offset(-25)
            make.leading.equalToSuperview().offset(25)
            make.trailing.equalToSuperview().offset(-25)
        }
        
        alertRecoverContinue.snp.remakeConstraints { make in
            make.bottom.equalTo(alertRecoverCancel.snp.top).offset(-25)
            make.leading.equalToSuperview().offset(25)
            make.trailing.equalToSuperview().offset(-25)
        }

        shadowLayer.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.shadowLayer.alpha = 1
        }
    }
    
    @objc func showRegAlert(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.signUp.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.signUp.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.alertContainer.subviews.forEach { $0.removeFromSuperview() }
                
                self.view.endEditing(true)
                
                self.view.addSubview(self.shadowLayer)
                self.shadowLayer.addSubview(self.alertContainer)
                self.alertContainer.addSubview(self.alertRegText)
                self.alertContainer.addSubview(self.alertRegButtonContinue)
                self.alertContainer.addSubview(self.alertRegButtonCancel)
                
                self.shadowLayer.addGestureRecognizer(
                    UITapGestureRecognizer(
                        target: self,
                        action: #selector(self.hideAlert)
                    )
                )

                self.shadowLayer.snp.remakeConstraints { make in
                    make.width.height.equalToSuperview()
                }
                
                self.alertContainer.snp.remakeConstraints { make in
                    make.height.equalTo(300)
                    make.centerY.equalToSuperview()
                    make.leading.equalToSuperview().offset(40)
                    make.trailing.equalToSuperview().offset(-40)
                }
                
                self.alertRegText.snp.remakeConstraints { make in
                    make.top.equalToSuperview().offset(25)
                    make.leading.equalToSuperview().offset(43)
                    make.trailing.equalToSuperview().offset(-43)
                }
                
                self.alertRegButtonCancel.snp.remakeConstraints { make in
                    make.bottom.equalToSuperview().offset(-25)
                    make.leading.equalToSuperview().offset(25)
                    make.trailing.equalToSuperview().offset(-25)
                }
                
                self.alertRegButtonContinue.snp.remakeConstraints { make in
                    make.bottom.equalTo(self.alertRegButtonCancel.snp.top).offset(-25)
                    make.leading.equalToSuperview().offset(25)
                    make.trailing.equalToSuperview().offset(-25)
                }

                self.shadowLayer.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.shadowLayer.alpha = 1
                }
            }
        }
    }
    
    @objc func changePasswordVisibility() {
        passwordInput.isSecureTextEntry = !passwordInput.isSecureTextEntry
        
        passwordShow.image =
            passwordInput.isSecureTextEntry
                ? UIImage(named: "showPassword")
                : UIImage(named: "showPasswordBlue")
    }
    
    @objc func saveMeChange() {
        saveMeEnabled = !saveMeEnabled
        
        saveMe.image =
            saveMeEnabled
                ? UIImage(named: "checkBox")
                : UIImage(named: "checkBoxEmpty")
    }
    
    @objc func hideAlert(gesture: UILongPressGestureRecognizer) {
        if gesture.view != shadowLayer {
            if gesture.state == .began {
                UIView.animate(withDuration: 0.1) {
                    gesture.view!.transform =
                        CGAffineTransform(scaleX: 0.9, y: 0.9)
                }
            } else if gesture.state == .ended {
                UIView.animate(withDuration: 0.1) {
                    gesture.view!.transform = CGAffineTransform.identity
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    UIView.animate(withDuration: 0.3) {
                        self.shadowLayer.alpha = 0
                    }
                }
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.shadowLayer.alpha = 0
            }
        }
    }
    
    @objc func openReg(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.alertRegButtonContinue.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.alertRegButtonContinue.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if let url = URL(string: "\(XTargetUtils.regLink!)?p=100&l=\(Localize.currentLanguage())") {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
    
    @objc func openRecover(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.1) {
                self.alertRecoverContinue.transform =
                    CGAffineTransform(scaleX: 0.9, y: 0.9)
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.1) {
                self.alertRecoverContinue.transform = CGAffineTransform.identity
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if let url = URL(string: "\(XTargetUtils.regLink!)?p=101&l=\(Localize.currentLanguage())") {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
}
