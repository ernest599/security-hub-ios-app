import UIKit
import RxSwift
import WebKit

class IvideonAuthController: BaseController<IvideonAuthView>, WKNavigationDelegate {

    override func loadView() {
        super.loadView()
        
        clearCookie()
        
        mainView.webView.navigationDelegate = self
        
        navigationController!.interactivePopGestureRecognizer!.delegate = self
        
        _ = DataManager.shared.getIviSaveSession()
            .observe(on: ThreadUtil.shared.mainScheduler)
            .subscribe(onNext: { ses in
                guard let url = XTargetUtils.ivideon else { return }
                self.mainView.webView.load(URLRequest(url: URL(string: url + "&session=" + ses)!))
            })
    }
    
    private func clearCookie(){
        let dataStore = WKWebsiteDataStore.default()
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { (records) in
            for record in records {
                if record.displayName.contains("ivideon") {
                    dataStore.removeData(
                        ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(),
                        for: [record],
                        completionHandler: {
                            print("Deleted: " + record.displayName);
                        }
                    )
                }
            }
        }
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        
        if parent == nil {
            NotificationCenter.default.post(name: HubNotification.iviAuth, object: nil)
        }
    }
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let serverTrust = challenge.protectionSpace.serverTrust {
            completionHandler(.useCredential, URLCredential(trust: serverTrust))
        } else {
            completionHandler(.useCredential, nil)
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (_: WKNavigationActionPolicy) -> Void) {
        guard let url = navigationAction.request.url else { decisionHandler(.allow); return }
        if (url.absoluteString.contains("sign-up") || url.absoluteString.contains("password-recover")) {
            UIApplication.shared.open(
                url,
                options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                completionHandler: nil
            )
            
            decisionHandler(.cancel)
            
            return
        }
        
        if (url.absoluteString.contains("go.ivideon")) {
            decisionHandler(.cancel)
            return
        }
        
        decisionHandler(.allow)
        
        if url.absoluteString.contains("iv.php") && url.absoluteString.contains("code=") {
            finish()
        }
    }
    
    func finish(){
        showLoader()
        
        let obs: Observable<String?> = Observable<Int64>.timer(.seconds(5), scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
            .asObservable()
            .concatMap({ (_) -> Observable<String?> in
                return DataManager.shared.getIvideonToken()
            })
            .observe(on: MainScheduler())
        
        _ = obs.subscribe(onNext: { (token) in
                self.hideLoader()
                    
                self.navigationController?.popViewController(animated: true)
            }
        )
    }
}

extension IvideonAuthController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
