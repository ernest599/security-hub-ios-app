import UIKit
import WebKit

class IvideonAuthView: BaseView {
    lazy var webView: WKWebView = {
        let view = WKWebView()
        
        view.isOpaque = false
        
        return view
    }()
    
    override func setContent() {
        super.setContent()
        
        contentView.alwaysBounceVertical = false
        contentView.alwaysBounceHorizontal = false
        
        contentView.addSubview(webView)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        webView.snp.remakeConstraints { make in
            make.top.left.equalTo(0)
            make.width.equalToSuperview()
            make.height.equalToSuperview().offset(-50)
        }
    }
}
