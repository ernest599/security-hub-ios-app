import UIKit
import RxSwift

class CameraListController : XBaseController<CameraListView>, UITableViewDataSource, UITableViewDelegate {
    
    private var cameras: [Camera] = []
    private let dm = DataManager.shared!
    
    var eventDisp: Disposable!
    
    override func loadView() {
        super.loadView()
        
        xView.setNV(navigationController)
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        title = R.strings.title_cameras
        
        xView.table.register(CameraCell.self, forCellReuseIdentifier: CameraCell.cellId)
        xView.table.dataSource = self
        xView.table.delegate = self
        xView.table.reloadData()
        xView.refreshControl.addTarget(self, action: #selector(update), for: UIControl.Event.valueChanged)
        
        if (DataManager.shared.getUser().login != "ios" || DataManager.shared.getUser().login != "test_ios") {
            rightButton(#imageLiteral(resourceName: "menu_exit"))
        }
        
        initNeedAuto()
        load()
        
        NotificationCenter.default.addObserver(forName: HubNotification.iviAuth, object: nil, queue: OperationQueue.main) { notification in
            self.load()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        eventDisp = dm.events
            .asObservable()
            .subscribe(onNext: { events in
                DispatchQueue.main.async { [self] in
                    if events.count > 0 {
                        for event in events {
                            if event._class == 0 {
                                xView.changeBottomNavStatus(type: 0)

                                break
                            } else if event._class >= 1 && event._class <= 3 {
                                xView.changeBottomNavStatus(type: 1)
                            }
                        }
                    } else {
                        xView.changeBottomNavStatus(type: 2)
                    }
                }
            })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        eventDisp.dispose()
    }
    
    private let label = UILabel()
    private let button = ZFRippleButton()
    
    private func initNeedAuto() {
        rightButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

        label.font = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.medium)
        label.text = R.strings.ivideon_need_auto
        label.isHidden = true
        
        xView.addSubview(label)
        
        label.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview().offset(-30)
            make.centerX.equalToSuperview()
        }
        
        button.setTitle(R.strings.ivideon_sign_in.uppercased(), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)
        button.backgroundColor = DEFAULT_COLOR_MAIN
        button.rippleBackgroundColor = DEFAULT_COLOR_MAIN
        button.rippleColor = DEFAULT_SELECTED.withAlphaComponent(0.4)
        button.shadowRippleEnable = false
        button.ripplePercent = 1
        button.layer.shadowOffset = CGSize(width: 2, height: 2)
        button.layer.shadowOpacity = 0.7
        button.layer.shadowRadius = 3
        button.layer.shadowColor = UIColor.lightGray.cgColor
        button.layer.cornerRadius = 15
        button.isHidden = true
        button.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        xView.addSubview(button)
        
        button.snp.remakeConstraints { (make) in
            make.centerY.equalToSuperview().offset(30)
            make.height.equalTo(50)
            make.width.equalTo(label.snp.width).offset(-40)
            make.centerX.equalToSuperview()
        }
    }
    
    @objc private func click(){
        navigationController?.pushViewController(IvideonAuthController(), animated: false)
        needAuto(false)
    }

    private func load(){
        _ = DataManager.shared.getCameraList().subscribe(onNext: { (cameras) in
            self.xView.refreshControl.endRefreshing()
            
            if cameras == nil {
                self.needAuto(true)
                self.cameras = []
                self.xView.table.reloadData()
            } else {
                self.needAuto(false)
                self.cameras = cameras!
                self.xView.table.reloadData()
            }
        }, onError: { error in
            self.needAuto(false)
            self.cameras = []
            self.xView.table.reloadData()
            
            AlertUtil.errorAlert(error.localizedDescription)
        })
    }
    
    private func needAuto(_ isHidden: Bool){
        label.isHidden = !isHidden
        button.isHidden = !isHidden
        xView.table.isHidden = isHidden
        rightButton.isHidden = isHidden
    }
    
    @objc func update() {
        load()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cameras.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CameraCell = tableView.dequeueReusableCell(withIdentifier: CameraCell.cellId, for: indexPath) as! CameraCell
        
        cell.setContent(cameras[indexPath.row])
        cell.setNV(navigationController)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(CameraPreviewController(cameras[indexPath.row]), animated: false)
    }
    
    override func rightClick() {
        let alert = AlertUtil.exitAlert( {
            _ = DataManager.shared.exitIvi().subscribe(onNext: {res in
                self.needAuto(true)
                self.cameras = []
                self.xView.table.reloadData()
            })
        }, mes: R.strings.camera_exit_message)
        
        navigationController?.present(alert, animated: false)
    }
}
