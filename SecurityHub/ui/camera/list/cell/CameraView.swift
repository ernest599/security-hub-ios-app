//
//  CameraView.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class CameraView: UIView {
    
    var margin = 15
    
    lazy var img: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "camera_btn")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy var name: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14.0)
        view.textColor = UIColor.black
        view.numberOfLines = 1
        view.lineBreakMode = .byTruncatingTail
        return view
    }()
    
    lazy var status: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        return view
    }()
    
    lazy var time: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        return view
    }()
    
    lazy var menu : ZFRippleButton = {
        var view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "points") ,for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.lightGray
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 0.5
        layer.borderColor = UIColor.lightGray.cgColor
        addSubview(img)
        addSubview(name)
        addSubview(status)
        addSubview(time)
        addSubview(menu)
    }
    
    override func updateConstraints() {
        img.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview()//.offset(margin)
            make.trailing.equalToSuperview()//.offset(-margin)å
            make.height.equalTo(self.snp.width).dividedBy(1.78)
        }
        name.snp.remakeConstraints{ make in
            make.top.equalTo(img.snp.bottom).offset(margin)
            make.leading.equalToSuperview().offset(margin)
        }
        menu.snp.remakeConstraints{make in
            make.top.equalTo(img.snp.bottom).offset(margin)
            make.leading.equalTo(name.snp.trailing).offset(margin)
            make.trailing.equalToSuperview().offset(-margin)
            make.height.width.equalTo(35)
        }
        status.snp.remakeConstraints{ make in
            make.top.equalTo(name.snp.bottom).offset(margin/2)
            make.leading.equalToSuperview().offset(margin)
            make.bottom.equalToSuperview().offset(-margin)
        }
        time.snp.remakeConstraints{ make in
            make.top.equalTo(name.snp.bottom).offset(margin/2)
            make.leading.equalTo(status.snp.trailing).offset(margin)
            make.trailing.equalTo(menu).offset(-margin)
            make.bottom.equalToSuperview().offset(-margin)
        }
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
}
