//
//  CameraCell.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//
import UIKit
import RxSwift

class CameraCell: BaseCell<CameraView>{
    static let cellId = "CameraCell"
    private var camera: Camera!
    private var set: CamSet!
    
    func setContent(_ obj: Camera) {
        selectionStyle = .none
        self.camera = obj
        _ = camera.getPreview()
            .observe(on: MainScheduler.init())
            .subscribe(onNext: { img in self.mainView.img.image = img })
        mainView.name.text = camera.name
        mainView.status.text = camera.online ? R.strings.camera_cell_online : R.strings.camera_cell_offline
        mainView.status.textColor = camera.online ? UIColor.hubMainColor : UIColor.red
        //mainView.time.text = Int(camera.lastOnline).getDateStringFromUnixTime(dateStyle: .short, timeStyle: .short)
        mainView.menu.addTarget(self, action: #selector(menu_pressed), for: .touchUpInside)
    }
    
    @objc func menu_pressed(){
        let _ = DataManager.shared.getCamSet(camera.id).subscribe(onNext: { set in
            self.set = set
            let al = AlertUtil.myXAlert(R.strings.camera_settings + self.camera.name, messages: [
                R.strings.menu_camera_rename,
                R.strings.menu_camera_zones,
                set.audio ? R.strings.menu_camera_audio_on : R.strings.menu_camera_audio_off,
                R.strings.menu_camera_quality + (set.q == 0 ? R.strings.menu_camera_quality_low :
                            set.q == 1 ? R.strings.menu_camera_quality_medium : R.strings.menu_camera_quality_high )
                ], voids: [self.rename,self.relation, self.audio, self.q])
            self.nV?.present(al, animated: false)
        })
    }
    
    private func rename(){
        AlertUtil.renameXAlert(text: camera.name) { (text) in
            _ = IvideonApi.updateCameraName(self.camera, accessToken: DataManager.settingsHelper.ivideonToken, value: text)
                .subscribe(onNext: { res in
                    if res { self.mainView.name.text = text }
                    else{ AlertUtil.errorAlert(R.strings.error_rename) }
                })
        }
    }
    
    private func relation(){
        _ = DataManager.shared.getZoneRelation(cam: camera.id)
            .subscribe(onNext: { res in
                self.nV?.pushViewController(SelectZoneController(list: res, title: R.strings.title_select_objects, obj: self.camera), animated: false)
            })
    }
    
    private func audio(){
        set.audio = !set.audio
        DataManager.shared.updCamSet(set)
    }
    
    private func q(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
        alert.addAction(UIAlertAction(title: R.strings.menu_camera_quality_low, style: .default, handler: { a in
            self.set.q = 0;DataManager.shared.updCamSet(self.set)
        }))
        alert.addAction(UIAlertAction(title: R.strings.menu_camera_quality_medium, style: .default, handler: { a in
            self.set.q = 1;DataManager.shared.updCamSet(self.set)
        }))
        alert.addAction(UIAlertAction(title: R.strings.menu_camera_quality_high, style: .default, handler: { a in
            self.set.q = 2;DataManager.shared.updCamSet(self.set)
        }))
        alert.addAction(UIAlertAction(title: R.strings.button_cancel, style: .cancel, handler: nil))
        self.nV?.present(alert, animated: false)
    }
}
