//
//  CameraPreviewView.swift
//  SecurityHub test
//
//  Created by Timerlan on 18.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class CameraPreviewView: BaseView {
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        return view
    }()
    
    lazy var player: PlayerView = {
        let view = PlayerView()
        return view
    }()

    override func setContent() {
        super.setContent()
        contentView.alwaysBounceVertical = false
        player.frame = CGRect(x: 0, y: 0, width:  UIScreen.main.bounds.width , height: UIScreen.main.bounds.width / 1.78)
        contentView.addSubview(tableView)
        contentView.addSubview(player)
    }

    override func updateConstraints() {
        player.snp.remakeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(self.snp.width).dividedBy(1.78)
        }
        tableView.snp.remakeConstraints{ make in
            make.top.equalTo(player.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview().offset(-player.frame.size.height)
        }
        super.updateConstraints()
    }
}

