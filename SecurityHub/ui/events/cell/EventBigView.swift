//
//  EventBigView.swift
//  SecurityHub test
//
//  Created by Timerlan on 03.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class EventBigView: UIView {
    static let height: CGFloat = 100
    
    lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_restriction_shield_grey_500_big")
        view.contentMode = .scaleAspectFill
        return view
    }()
    lazy var siteView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        view.text = nil
        return view
    }()
    lazy var deviceView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        view.text = nil
        return view
    }()
    lazy var sectionZoneView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 2
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        view.text = nil
        return view
    }()
    
    lazy var descView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.semibold)
        view.numberOfLines = 2
        view.lineBreakMode = .byWordWrapping
        return view
    }()
    
    lazy var timeView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.medium)
        view.numberOfLines = 1
        return view
    }()
    
    lazy var camBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "camera_btn") ,for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.hubMainColor
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderWidth = 0.7
        layer.borderColor = UIColor.lightGray.cgColor
        addSubview(camBtn)
        addSubview(iconView)
        addSubview(siteView)
        addSubview(deviceView)
        addSubview(sectionZoneView)
        addSubview(descView)
        addSubview(timeView)
        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        iconView.snp.remakeConstraints{ make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(5)
            make.height.width.equalTo(70)
        }
        siteView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(4)
            make.left.equalTo(iconView.snp.right).offset(4)
        }
        camBtn.snp.remakeConstraints{ make in
            make.top.equalToSuperview()
            make.leading.equalTo(siteView.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-10)
            make.height.width.equalTo(35)
        }
        deviceView.snp.remakeConstraints{ make in
            make.top.equalTo(siteView.snp.bottom)
            make.left.equalTo(iconView.snp.right).offset(4)
            make.right.equalToSuperview().offset(-4)
        }
        sectionZoneView.snp.remakeConstraints{ make in
            make.top.equalTo(deviceView.snp.bottom)
            make.left.equalTo(iconView.snp.right).offset(4)
            make.right.equalToSuperview().offset(-4)
        }
        descView.snp.remakeConstraints{ make in
            make.left.equalTo(iconView.snp.right).offset(4)
            make.right.equalToSuperview().offset(-4)
            make.bottom.equalTo(timeView.snp.top).offset(-4)
        }
        timeView.snp.remakeConstraints{ make in
            make.top.equalTo(descView.snp.bottom)
            make.left.equalTo(iconView.snp.right).offset(4)
            make.right.equalToSuperview().offset(-4)
            make.bottom.equalToSuperview().offset(-4)
        }
    }
}
