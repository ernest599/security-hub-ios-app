//
//  HozOrganView.swift
//  SecurityHub
//
//  Created by Timerlan on 25.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class HozOrganView: UIView {
    static let cellHeight: CGFloat = 105;
    
    private lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "hoz_key")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy var nameView: UILabel = {
        let view = UILabel()
        return view
    }()
    
    lazy var menuBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "points") ,for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.lightGray
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var deviceView: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.lightGray
        view.text = " "
        view.font = UIFont.systemFont(ofSize: 12)
        return view
    }()
    
    lazy var indexView: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.lightGray
        view.font = UIFont.systemFont(ofSize: 12)
        return view
    }()
    
    lazy var sectionView: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.lightGray
        view.font = UIFont.systemFont(ofSize: 12)
        return view
    }()
    
    lazy var line: UIView = { let view = UILabel();view.backgroundColor = UIColor.lightGray;return view }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconView)
        addSubview(nameView)
        addSubview(deviceView)
        addSubview(indexView)
        addSubview(sectionView)
        addSubview(menuBtn)
        addSubview(line)
    }
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
//        snp.remakeConstraints{make in make.height.equalTo(HozOrganView.cellHeight) }
        iconView.snp.remakeConstraints{make in
            make.leading.equalToSuperview().offset(15)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(60)
        }
        nameView.snp.remakeConstraints{make in
            make.top.equalToSuperview().offset(15)
            make.leading.equalTo(iconView.snp.trailing).offset(15)
        }
        menuBtn.snp.remakeConstraints{make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalTo(nameView.snp.trailing).offset(5)
            make.trailing.equalToSuperview().offset(-5)
            make.height.width.equalTo(35)
        }
        deviceView.snp.remakeConstraints{make in
            make.top.equalTo(nameView.snp.bottom).offset(10)
            make.leading.equalTo(iconView.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        indexView.snp.remakeConstraints{make in
            make.top.equalTo(deviceView.snp.bottom).offset(0)
            make.leading.equalTo(iconView.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        sectionView.snp.remakeConstraints{make in
            make.top.equalTo(indexView.snp.bottom).offset(0)
            make.leading.equalTo(iconView.snp.trailing).offset(15)
            make.trailing.equalToSuperview().offset(-15)
        }
        line.snp.remakeConstraints{make in
            make.top.equalTo(sectionView.snp.bottom).offset(15)
            make.leading.trailing.width.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}
