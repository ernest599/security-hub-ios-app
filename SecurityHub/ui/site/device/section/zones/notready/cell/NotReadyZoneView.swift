//
//  NotReadyZoneView.swift
//  SecurityHub
//
//  Created by Timerlan on 08.06.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class NotReadyZoneView: UIView {
    
    lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_sensor_brand_color")
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    lazy var sectionView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)
        return view
    }()
    
    lazy var zoneView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 14)
        return view
    }()
    
    lazy var line: UIView = { let view = UILabel();view.backgroundColor = UIColor.lightGray;return view }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconView)
        addSubview(sectionView)
        addSubview(zoneView)
        addSubview(line)
        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented") }
    
    override func updateConstraints() {
        super.updateConstraints()
        iconView.snp.remakeConstraints{ make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(10)
            make.height.width.equalTo(50)
        }
        sectionView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalTo(iconView.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(-10)
        }
        zoneView.snp.remakeConstraints{ make in
            make.top.equalTo(sectionView.snp.bottom).offset(5)
            make.leading.equalTo(iconView.snp.trailing).offset(10)
            make.trailing.equalToSuperview().offset(0)
            make.bottom.equalTo(line.snp.top).offset(-10)
        }
        line.snp.remakeConstraints{make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}


