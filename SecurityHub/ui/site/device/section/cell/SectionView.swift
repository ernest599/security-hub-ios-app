//
//  SectionView.swift
//  SecurityHub test
//
//  Created by Timerlan on 26.04.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class SectionView: UIView {
    
    lazy var iconView: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ic_restriction_shield_grey_500_big")
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy var titleView: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        return view
    }()
    
    lazy var mainEventView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.text = "norm".localized()
        return view
    }()
    
    lazy var statusView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.text = "disarmed".localized()
        return view
    }()
    
    lazy var timeView: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 12)
        view.textColor = UIColor.gray
        view.numberOfLines = 1
        view.text = "--:--:--"
        return view
    }()
    
    lazy var cont: UIView = { return UIView() }()
    
    lazy var menuBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "points") ,for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.lightGray
        view.ripplePercent = 1
        view.isHidden = true
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var armBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubDarkMainColor
        view.shadowRippleEnable = false
        view.setTitle("arm".localized(), for: .normal)
        view.ripplePercent = 1
        return view
    }()
    
    lazy var disarmBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubDarkMainColor
        view.shadowRippleEnable = false
        view.setTitle("disarm".localized(), for: .normal)
        view.ripplePercent = 1
        return view
    }()
    
    lazy var datBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.backgroundColor = UIColor.hubMainColor
        view.rippleBackgroundColor = UIColor.hubMainColor
        view.rippleColor = UIColor.hubDarkMainColor
        view.shadowRippleEnable = false
        view.setTitle("zones".localized(), for: .normal)
        view.ripplePercent = 1
        return view
    }()
    
    lazy var tableView : UITableView = {
        let view = UITableView()
        view.rowHeight = UITableViewAutomaticDimension
        view.isScrollEnabled = false
        view.estimatedRowHeight = 20
        view.separatorStyle = .none
        return view
    }()
    
    lazy var tableMiniView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 20, height: 20)
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        flowLayout.minimumInteritemSpacing = -2
        flowLayout.minimumLineSpacing = 0
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = UIColor.white
        return collectionView
    }()
    
    
    lazy var line: UIView = {
        let view = UILabel()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(iconView)
        addSubview(tableMiniView)
        addSubview(titleView)
        addSubview(menuBtn)
        addSubview(line)
        addSubview(mainEventView)
        addSubview(statusView)
        addSubview(timeView)
        addSubview(cont)
        cont.addSubview(armBtn)
        cont.addSubview(disarmBtn)
        cont.addSubview(datBtn)
        cont.addSubview(tableView)
    }
    
    func open(_ h: Int, butH: Int) {
        self.h = h
        self.butH = butH
        if butH == 0 { butM = 0 } else { butM = 16 }
//        if MainController.hHoz_Org == 0 {
//            armBtn.isHidden = true
//            disarmBtn.isHidden = true
//        }
        cont.isHidden = false
        tableMiniView.isHidden = true
        updateConstraints()
    }
    
    func close(_ count: Int = 0) {
        h = 0
        cont.isHidden = true
        tableMiniView.isHidden = false
        updateConstraints()
    }
    
    private let margin = 16
    private var h = 0
    private var butH = 35
    private var butM = 16
    override func updateConstraints() {
        super.updateConstraints()
        iconView.snp.remakeConstraints{ make in
            make.top.leading.equalToSuperview().offset(6)
            make.width.equalTo(100)
            make.height.equalTo(80)
        }
        tableMiniView.snp.remakeConstraints{ make in
            make.top.equalTo(iconView.snp.bottom)//.offset(margin-6)
            make.leading.equalToSuperview().offset(margin-6)
            make.trailing.equalTo(titleView.snp.leading).offset(-margin)
            //make.bottom.equalTo(cont.snp.top)//.offset(-(margin-6))
            make.height.equalTo(40)
        }
        titleView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(margin)
            make.leading.equalTo(iconView.snp.trailing)
        }
        menuBtn.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(margin)
            make.leading.equalTo(titleView.snp.trailing).offset(margin)
            make.trailing.equalToSuperview().offset(-margin)
            make.width.height.equalTo(30)
        }
        mainEventView.snp.remakeConstraints{ make in
            make.top.equalTo(titleView.snp.bottom).offset(margin+10)
            make.leading.equalTo(iconView.snp.trailing)
            make.trailing.equalToSuperview().offset(-margin)
        }
        statusView.snp.remakeConstraints{ make in
            make.top.equalTo(mainEventView.snp.bottom).offset(4)
            make.leading.equalTo(iconView.snp.trailing)
            make.trailing.equalToSuperview().offset(-margin)
        }
        timeView.snp.remakeConstraints{ make in
            make.top.equalTo(statusView.snp.bottom).offset(4)
            make.leading.equalTo(iconView.snp.trailing)
            make.trailing.equalToSuperview().offset(-margin)
        }
        cont.snp.remakeConstraints{ make in
            make.top.equalTo(timeView.snp.bottom)
            make.leading.trailing.width.equalToSuperview()
            make.height.equalTo(self.h)
        }
        line.snp.remakeConstraints{ make in
            make.top.equalTo(cont.snp.bottom).offset(margin)
            make.leading.trailing.width.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        armBtn.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(butM)
            make.leading.equalToSuperview().offset(self.margin)
            make.height.equalTo(butH)
            make.width.equalToSuperview().offset(-margin).dividedBy(3)
            make.bottom.equalTo(tableView.snp.top).offset(-margin/2)
        }
        disarmBtn.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(butM)
            make.leading.equalTo(armBtn.snp.trailing).offset(margin/2)
            make.height.equalTo(butH)
            make.width.equalToSuperview().offset(-margin).dividedBy(3)
            make.bottom.equalTo(tableView.snp.top).offset(-margin/2)
        }
//        datBtn.snp.remakeConstraints{ make in
//            make.top.equalToSuperview().offset(butM)
//            make.leading.equalTo(MainController.hHoz_Org == 0 ? cont.snp.leading : armBtn.snp.trailing).offset(MainController.hHoz_Org == 0 ? margin : margin/2)
//            make.trailing.equalToSuperview().offset(-margin)
//            make.height.equalTo(butH)
//            make.width.equalToSuperview().offset(MainController.hHoz_Org == 0 ? -2 * margin : -margin).dividedBy(MainController.hHoz_Org == 0 ? 1 : 3)
//            make.bottom.equalTo(tableView.snp.top).offset(-margin/2)
//        }
        tableView.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(margin)
            make.trailing.equalToSuperview().offset(-margin)
            make.bottom.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
}
