//
//  DeleagationViewCell.swift
//  SecurityHub test
//
//  Created by Timerlan on 10.05.2018.
//  Copyright © 2018 TEKO. All rights reserved.
//

import UIKit

class DelegationViewCell: UIView {
    
    lazy var name: UILabel = {
        let view = UILabel()
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 1
        return view
    }()
    
    lazy var domen: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        return view
    }()
    
    lazy var menuBtn : ZFRippleButton = {
        var view = ZFRippleButton()
        view.setImage(#imageLiteral(resourceName: "points") ,for: .normal)
        view.backgroundColor = UIColor.white
        view.rippleBackgroundColor = UIColor.white
        view.rippleColor = UIColor.lightGray
        view.ripplePercent = 1
        view.shadowRippleEnable = false
        return view
    }()
    
    lazy var line: UIView = {
        let view = UILabel()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(name)
        addSubview(menuBtn)
        addSubview(line)
        addSubview(domen)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        name.snp.remakeConstraints{ make in make.top.leading.equalToSuperview().offset(16) }
        domen.snp.remakeConstraints{ make in
            make.top.equalTo(name.snp.bottom).offset(5)
            make.leading.equalToSuperview().offset(16)
        }
        menuBtn.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.width.equalTo(30)
        }
        line.snp.remakeConstraints{ make in
            make.top.equalTo(domen.snp.bottom).offset(16)
            make.leading.trailing.width.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {fatalError("init(coder:) has not been implemented")}
}

