import Foundation
import Eureka

class ProfileSettingsController: FormViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController!.interactivePopGestureRecognizer!.delegate = self
        
        if form.isEmpty {
            guard let nV = navigationController else { return }
            let roles = DataManager.shared.getUser().roles
            
            form +++ LabelRow(){
                  $0.title = R.strings.options_current_operator
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                        let operators = DataManager.shared.getOperator()
                        
                        let mes = "\(R.strings.operator_name): \(operators.name)\n\n" +
                                  "\(R.strings.operator_login): \(operators.login)\n\n" +
                                     R.strings.operator_cell_permission + (
                                           operators.roles & Roles.ORG_ADMIN != 0 || operators.roles & Roles.DOMEN_ADMIN != 0 ? R.strings.operator_cell_permission_admin :
                                               operators.roles == 40960 ? R.strings.operator_cell_permission_manage_and_view : R.strings.operator_cell_permission_view ) + "\n\n" +
                                       R.strings.operator_sites_count + String(DataManager.shared.getSitesCount()) + "\n";
                        
                        if roles & Roles.ORG_ADMIN != 0 || roles & Roles.DOMEN_ADMIN != 0 || roles & Roles.DOMEN_HOZ_ORG != 0 {
                                       AlertUtil.infoAlertUser(mes, name: operators.name, listener: { (name) in
                                           _ = DataManager.shared.changeOperator(id: operators.id, value: name ?? "", name: "name").subscribe()
                                       }) { (pas) in
                                           _ = DataManager.shared.changeOperator(id: operators.id, value: pas?.md5 ?? "", name: "password").subscribe()
                                           UserDefaults.standard.set(nil, forKey: "saveMeViewPass")
                                           UserDefaults.standard.set(nil, forKey: "saveMeViewLogin")
                                       }
                                   } else { AlertUtil.infoAlert(mes) }
                                }
                        }
            
            <<< LabelRow(){
                  $0.title = R.strings.options_security
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                       nV.pushViewController(SettingsController(R.strings.options_security), animated: false)
                }
            }
            
            let createButton = XButtonView(
                XButtonView.defaultRect,
                title: R.strings.options_exit,
                style: .text,
                mainColor: UIColor.red
            )
            createButton.click = self.exit
            form +++ ViewRow<XButtonView>().cellSetup { (cell, row) in
                cell.view = createButton;
                cell.update()
            }
        }
    }
    
    private func exit() {
        self.navigationController?.present(AlertUtil.exitAlert({
            DataManager.disconnectDisposable = DataManager.shared.disconnect().subscribe(onNext: { a in
                DataManager.disconnectDisposable?.dispose()
                NavigationHelper.shared.show(SplashController())
            })
        }), animated: true)
    }
}

extension ProfileSettingsController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
