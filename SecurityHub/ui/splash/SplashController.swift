import UIKit

class SplashController: BaseController<SplashView> {
    
    let dm = DataManager.shared!
    
    override func loadView() {
        super.loadView()
        
        AppUtility.lockOrientation(.portrait)
        navigationController?.setNavigationBarHidden(true, animated: false)
        mainView.removeConnactionStateChange()
        
        dm.dbHelper.reinit()
    }
    
    override func viewDidLoad() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            self.checkUser()
        }
    }

    private func checkUser() {
        DataManager.connectDisposable = DataManager.shared.connect().subscribe(onNext: { success in
            DataManager.connectDisposable?.dispose()
            if !success {
                UIView.animate(withDuration: 0.3) {
                    self.mainView.logo.alpha = 0
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    NavigationHelper.shared.show(StartController())
                }
            } else {
                self.needPin()
            }
        })
    }
    
    private func needPin() {
        if DataManager.settingsHelper.needPin {
            NavigationHelper.shared.show(PinController(complete: {
                NavigationHelper.shared.showMain()
            }))
        } else {
            NavigationHelper.shared.showMain()
        }
    }
}
