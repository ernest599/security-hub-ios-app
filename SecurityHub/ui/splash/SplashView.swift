import UIKit

class SplashView: BaseView {
    
    lazy var logo: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "logo")
        view.contentMode = .scaleAspectFill
        
        return view
    }()
    
    override func setContent() {
        contentView.alwaysBounceVertical = false
        
        contentView.addSubview(logo)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        logo.snp.remakeConstraints{ make in
            make.centerY.equalToSuperview().offset(-UIApplication.shared.statusBarFrame.height)
            make.centerX.equalToSuperview()
            make.width.equalTo(184)
            make.height.equalTo(139)
        }
    }
}
