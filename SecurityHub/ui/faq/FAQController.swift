import Foundation
import WebKit
import Localize_Swift

class WikiController: BaseController<FAQView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = R.strings.title_faq
        
        navigationController!.interactivePopGestureRecognizer!.delegate = self
    }
}

extension WikiController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
