import UIKit
import MessageUI

class ChopView: BaseView, MFMailComposeViewControllerDelegate {
    
    private var companies: [Company]!
    var sheetTableData: [Depart] = []
    var sheetNumbers: [String] = []
    var sheetTableType = 0
    var selectedRegion = 0
    var selectedRC = 0
    var targetNumber: String?
    var targetMail: String?
    var targetWebSite: String?
    
    let sheetTitles = [
        "Выберите регион",
        "Выберите РЦ",
        "Выберите номер"
    ]
    
    lazy var layerView: UIView = {
        let view = UIView()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alpha = 0.02
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(hideAlert)
            )
        )
        
        return view
    }()
    
    lazy var alertContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        )
        
        return view
    }()
    
    lazy var alertInfo: UIScrollView = {
        return UIScrollView()
    }()
    
    lazy var alertInfoContent: UIView = {
        return UIView()
    }()
    
    lazy var alertTitle: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 24)
        view.font = view.font.withSize(24)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertAdresses: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor(red: 0.75, green: 0.75, blue: 0.75, alpha: 1)
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertPhones: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor(red: 0.75, green: 0.75, blue: 0.75, alpha: 1)
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertEmails: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 20)
        view.font = view.font.withSize(20)
        view.textColor = UIColor(red: 0.75, green: 0.75, blue: 0.75, alpha: 1)
        view.numberOfLines = 0
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var alertCall: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 24)
        view.font = view.font.withSize(24)
        view.textColor = UIColor.hubMainColor
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = "Позвонить"
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var alertMessage: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 24)
        view.font = view.font.withSize(24)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = "Написать"
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var alertWebSite: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 24)
        view.font = view.font.withSize(24)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = "Перейти на сайт"
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var sheetContainer: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 32
        view.backgroundColor = UIColor.white
        
        let gesture =
            UITapGestureRecognizer(
                target: self,
                action: #selector(emptyGesture)
            )
        gesture.cancelsTouchesInView = false
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var sheetTitle: UILabel = {
        let view = UILabel()
        
        view.font = UIFont(name: "OpenSans-Light", size: 16)
        view.font = view.font.withSize(16)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        view.textAlignment = .natural
        
        return view
    }()
    
    lazy var sheetTable: UITableView = {
        let view = UITableView()
        
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.rowHeight = 50
        
        return view
    }()

    lazy var backArrow: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "backArrow")
        
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.title_chop
        view.font = UIFont(name: "OpenSans-Light", size: 25)
        view.font = view.font.withSize(25)
        view.textColor = UIColor.hubTextBlack
        view.numberOfLines = 0
        
        return view
    }()
    
    lazy var table: UITableView = {
        let view = UITableView()
        
        view.rowHeight = 106
        view.separatorStyle = .none
        view.estimatedRowHeight = 106
        
        return view
    }()
    
    override func setContent() {
        companies =
            try! JSONDecoder().decode(
                SecurityCompanyModel.self,
                from:
                    NSData(
                        contentsOfFile:
                            Bundle.main.path(
                                forResource: "security_companies",
                                ofType: "json"
                            )!
                    )! as Data
            ).xml.companies.company
        
        contentView.isScrollEnabled = false
        contentView.addSubview(backArrowView)
        contentView.addSubview(backArrow)
        contentView.addSubview(title)
        contentView.addSubview(table)
        
        table.backgroundView = refreshControl
    }
    
    var refreshControl: UIRefreshControl = { return UIRefreshControl() } ()
    
    func showBottomSheet() {
        alertContainer.removeFromSuperview()
        
        view.addSubview(layerView)
        layerView.addSubview(sheetContainer)
        sheetContainer.addSubview(sheetTitle)
        sheetContainer.addSubview(sheetTable)
        
        layerView.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        sheetContainer.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalToSuperview().dividedBy(2)
            make.bottom.equalToSuperview().offset(200)
        }
        
        sheetTitle.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(30)
            make.top.equalToSuperview().offset(30)
        }
        
        sheetTable.snp.remakeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-70)
            make.top.equalTo(sheetTitle.snp.bottom).offset(20)
        }
        
        sheetTitle.text = sheetTitles[sheetTableType]
        sheetTable.delegate = self
        sheetTable.dataSource = self
        sheetTable.register(SheetCell.self, forCellReuseIdentifier: SheetCell.cellId)
        
        UIView.animate(withDuration: 0.3) {
            self.layerView.alpha = 0
        }
        
        UIView.animate(withDuration: 0.3) {
            self.layerView.alpha = 1
            self.layerView.isUserInteractionEnabled = true
        }
    }
    
    func showAlert() {
        sheetContainer.removeFromSuperview()
        
        view.addSubview(layerView)
        layerView.addSubview(alertContainer)
        alertContainer.addSubview(alertInfo)
        alertInfo.addSubview(alertInfoContent)
        alertInfoContent.addSubview(alertTitle)
        alertInfoContent.addSubview(alertAdresses)
        alertInfoContent.addSubview(alertPhones)
        alertInfoContent.addSubview(alertEmails)
        alertContainer.addSubview(alertCall)
        alertContainer.addSubview(alertMessage)
        alertContainer.addSubview(alertWebSite)
        
        layerView.snp.remakeConstraints { make in
            make.width.height.equalToSuperview()
        }
        
        alertContainer.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(40)
            make.top.equalToSuperview().offset(90)
            make.trailing.equalToSuperview().offset(-40)
            make.bottom.equalToSuperview().offset(-70)
        }
        
        alertInfo.snp.remakeConstraints { make in
            make.width.equalTo(alertContainer.snp.width)
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalTo(alertCall.snp.top).offset(-20)
        }
        
        alertInfoContent.snp.remakeConstraints { make in
            make.width.height.top.bottom.equalToSuperview()
        }
        
        alertTitle.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(40)
        }
        
        alertAdresses.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(alertTitle.snp.bottom).offset(40)
        }
        
        alertPhones.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(alertAdresses.snp.bottom).offset(20)
        }
        
        alertEmails.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(alertPhones.snp.bottom).offset(20)
        }
        
        UIView.animate(withDuration: 0.3) {
            self.layerView.alpha = 0
        }
        
        UIView.animate(withDuration: 0.3) {
            self.layerView.alpha = 1
            self.layerView.isUserInteractionEnabled = true
        }
    }
    
    func alertConstraintsNoEmail() {
        alertMessage.removeFromSuperview()
        
        alertWebSite.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-40)
        }
        
        alertCall.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(alertWebSite.snp.top).offset(-40)
        }
    }
    
    func alertConstraintsNoWebSite() {
        alertWebSite.removeFromSuperview()
        
        alertMessage.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-40)
        }
        
        alertCall.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(alertMessage.snp.top).offset(-40)
        }
    }
    
    func alertConstraintsAll() {
        alertWebSite.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-40)
        }
        
        alertMessage.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(alertWebSite.snp.top).offset(-40)
        }
        
        alertCall.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(alertMessage.snp.top).offset(-40)
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        backArrowView.snp.remakeConstraints { make in
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.top.equalToSuperview().offset(28)
            make.leading.equalToSuperview().offset(12)
        }
        
        backArrow.snp.remakeConstraints { make in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.top.equalToSuperview().offset(38)
            make.leading.equalToSuperview().offset(22)
        }
        
        title.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(32)
            make.leading.equalTo(backArrow.snp.trailing).offset(20)
            make.width.equalToSuperview().offset(-42)
        }
        
        table.snp.remakeConstraints { make in
            make.leading.trailing.width.equalToSuperview()
            make.top.equalTo(title.snp.bottom).offset(27)
            make.height.equalToSuperview().offset(-94)
        }
    }
    
    @objc func hideAlert() {
        UIView.animate(withDuration: 0.3) {
            self.layerView.alpha = 0
            self.layerView.isUserInteractionEnabled = false
        }
        
        UIView.animate(withDuration: 0.3) {
            self.sheetContainer.transform =
                CGAffineTransform(translationX: 0, y: 150)
        }
    }
}

extension ChopView: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sheetTableType == 0 {
            return sheetTableData.count
        } else if sheetTableType == 1 {
            return sheetTableData[selectedRegion].rcs.rc.count
        } else {
            return sheetNumbers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SheetCell = (tableView.dequeueReusableCell(withIdentifier: SheetCell.cellId, for: indexPath) as? SheetCell)!
        
        if sheetTableType == 0 {
            cell.titleLabel.text = sheetTableData[indexPath.row].subject
        } else if sheetTableType == 1 {
            cell.titleLabel.text = sheetTableData[selectedRegion].rcs.rc[indexPath.row].caption
        } else {
            cell.titleLabel.text = sheetNumbers[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if self.sheetTableType == 0 {
                self.selectedRegion = indexPath.row
            } else if self.sheetTableType == 1 {
                self.selectedRC = indexPath.row
                
                let company = self.sheetTableData[self.selectedRegion].rcs.rc[self.selectedRC]
                var phonesText = ""

                self.alertTitle.text = self.companies[0].caption

                for phone in 0...company.phones.string.count - 1 {
                    phonesText.append("\(company.phones.string[phone])\n")
                }

                self.alertAdresses.text = self.companies[0].message
                self.alertPhones.text = company.caption
                self.alertEmails.text = phonesText

                self.targetNumber = company.phones.string[0]
                self.targetWebSite = company.sites.string
                
                self.showAlert()

                self.alertCall.addGestureRecognizer(
                    UITapGestureRecognizer(
                        target: self,
                        action: #selector(self.callNumber)
                    )
                )
                
                self.alertWebSite.addGestureRecognizer(
                    UITapGestureRecognizer(
                        target: self,
                        action: #selector(self.openSite)
                    )
                )

                self.alertConstraintsNoEmail()
            } else if self.sheetTableType == 2 {
                self.targetNumber = self.sheetNumbers[indexPath.row]
                self.callNumber()
            }
            
            self.sheetTableType += 1
            if self.sheetTableType < 3 {
                self.sheetTitle.text = self.sheetTitles[self.sheetTableType]
                self.sheetTable.reloadData()
            }
        }
    }
    
    @objc func callNumber() {
        print("Called")
        guard let number = URL(string: "tel://" + targetNumber!.replacingOccurrences(of: " ", with: "")) else { return }
        UIApplication.shared.open(number)
    }
    
    @objc func sendEmail() {
        print("Mailed")
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            
            mail.mailComposeDelegate = self
            mail.setToRecipients([targetMail!])
            
            nV!.present(mail, animated: true)
        } else if let emailUrl = createEmailUrl(to: targetMail!) {
            UIApplication.shared.open(emailUrl)
        }
    }
    
    private func createEmailUrl(to: String) -> URL? {
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)")
        let defaultUrl = URL(string: "mailto:\(to)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
    
    @objc func openSite() {
        print("Sited")
        guard let url = URL(string: targetWebSite!) else { return }
        UIApplication.shared.open(url)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

class SheetCell: UITableViewCell {
    
    static let cellId = "SheetCellId"
    
    lazy var backView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1)
        view.alpha = 0.02
        view.isUserInteractionEnabled = true

        let pressGesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(sectionPressed(gesture:))
            )

        pressGesture.minimumPressDuration = 0.1
        pressGesture.cancelsTouchesInView = false
        pressGesture.numberOfTouchesRequired = 1

        view.addGestureRecognizer(pressGesture)
        
        let clickGesture =
            UITapGestureRecognizer(
                target: self,
                action: #selector(sectionClicked)
            )
        clickGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(clickGesture)
        
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        
        label.font = UIFont(name: "OpenSans-Light", size: 20)
        label.font = label.font.withSize(20)
        label.numberOfLines = 0
        label.textAlignment = .natural
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.hubTextBlack
        
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        contentView.addSubview(backView)
        contentView.addSubview(titleLabel)
        
        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    override func updateConstraints() {
        
        backView.snp.remakeConstraints{ make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalToSuperview()
        }
        
        titleLabel.snp.remakeConstraints{ make in
            make.leading.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(30)
            make.height.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
    @objc func sectionClicked() {
        print(123)
        UIView.animate(withDuration: 0.3) {
            self.backView.alpha = 1
        }
        
        UIView.animate(withDuration: 0.3) {
            self.backView.alpha = 0.02
        }
    }
    
    @objc func sectionPressed(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                self.backView.alpha = 1
            }
        } else if gesture.state == .changed {
            if gesture.location(in: contentView).y < self.backView.bounds.minY || gesture.location(in: contentView).y > self.backView.bounds.maxY {
                UIView.animate(withDuration: 0.3) {
                    self.backView.alpha = 0.02
                }
            }
        } else if gesture.state == .ended {
            if self.backView.alpha == 1 {
                UIView.animate(withDuration: 0.3) {
                    self.backView.alpha = 0.02
                }
            }
        }
    }
}
