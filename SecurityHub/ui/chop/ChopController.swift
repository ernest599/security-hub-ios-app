//
//  File.swift
//  SecurityHub
//
//  Created by Timerlan on 21.03.2018.
//  Copyright © 2018 Tattelecom. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class ChopController: BaseController<ChopView>, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    
    private var companies: [Company]!
    private var company: Company!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = R.strings.title_chop
        mainView.vc = self
        
        navigationController!.interactivePopGestureRecognizer!.delegate = self
        
        companies =
            try! JSONDecoder().decode(
                SecurityCompanyModel.self,
                from:
                    NSData(
                        contentsOfFile:
                            Bundle.main.path(
                                forResource: "security_companies",
                                ofType: "json"
                            )!
                    )! as Data
            ).xml.companies.company
        
        mainView.table.dataSource = self
        mainView.table.delegate = self
        mainView.table.backgroundView = nil
        mainView.table.register(ChopCell.self, forCellReuseIdentifier: ChopCell.cellId)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return companies.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ChopCell = (tableView.dequeueReusableCell(withIdentifier: ChopCell.cellId, for: indexPath) as? ChopCell)!
        
        cell.setContent(
            img: UIImage(
                    named:
                        companies[indexPath.row].logo.replacingOccurrences(
                            of: "@drawable/",
                            with: ""
                        )
                    ),
            name: companies[indexPath.row].caption
        )
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        company = companies[indexPath.row]
        
        if company.departs != nil {
            mainView.sheetTableType = 0
            mainView.sheetTableData = company.departs!.depart
            mainView.sheetTable.reloadData()
            mainView.showBottomSheet()
            
            UIView.animate(withDuration: 0.3) {
                self.mainView.sheetContainer.transform =
                    CGAffineTransform(translationX: 0, y: -150)
            }
        } else {
            var addressesText = ""
            var phonesText = ""
            var emailsText = ""

            mainView.alertTitle.text = company.caption

            if company.addresses != nil {
                for address in 0...company.addresses!.string.count - 1 {
                    addressesText.append("\(company.addresses!.string[address])\n")
                }
            }

            if company.cphones != nil {
                for phone in 0...company.cphones!.cphone.count - 1 {
                    phonesText.append("\(company.cphones!.cphone[phone].phone)\n(\(company.cphones!.cphone[phone].caption))\n")
                }
            } else {
                for phone in 0...company.phones!.string.count - 1 {
                    phonesText.append("\(company.phones!.string[phone])\n")
                }
            }

            if company.emails != nil {
                emailsText.append("\(company.emails!.string)\n")
                mainView.targetMail = company.emails!.string
            }

            mainView.alertAdresses.text = addressesText
            mainView.alertPhones.text = phonesText
            mainView.alertEmails.text = emailsText

            mainView.targetNumber =
                company.cphones != nil
                ? company.cphones!.cphone[0].phone
                : company.phones!.string[0]

            mainView.targetWebSite = company.sites?.string

            mainView.showAlert()

            mainView.alertCall.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(callNumber)
                )
            )

            mainView.alertMessage.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(sendEmail)
                )
            )
            
            mainView.alertWebSite.addGestureRecognizer(
                UITapGestureRecognizer(
                    target: self,
                    action: #selector(openSite)
                )
            )

            if company.emails == nil {
                mainView.alertConstraintsNoEmail()
            } else if company.sites == nil {
                mainView.alertConstraintsNoWebSite()
            } else {
                mainView.alertConstraintsAll()
            }
        }
    }
    
    @objc func callNumber() {
        if company.phones != nil && company.phones!.string.count > 1 {
            mainView.sheetTableType = 2
            mainView.sheetNumbers = company.phones!.string
            mainView.sheetTable.reloadData()
            mainView.showBottomSheet()
            
            UIView.animate(withDuration: 0.3) {
                self.mainView.sheetContainer.transform =
                    CGAffineTransform(translationX: 0, y: -150)
            }
        } else {
            print("Called")
            guard let number = URL(string: "tel://" + mainView.targetNumber!.replacingOccurrences(of: " ", with: "")) else { return }
            UIApplication.shared.open(number)
        }
    }
    
    @objc func sendEmail() {
        print("Mailed")
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            
            mail.mailComposeDelegate = self
            mail.setToRecipients([mainView.targetMail!])
            
            present(mail, animated: true)
        } else if let emailUrl = createEmailUrl(to: mainView.targetMail!) {
            UIApplication.shared.open(emailUrl)
        }
    }
    
    private func createEmailUrl(to: String) -> URL? {
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)")
        let defaultUrl = URL(string: "mailto:\(to)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
    
    @objc func openSite() {
        print("Sited")
        guard let url = URL(string: mainView.targetWebSite!) else { return }
        UIApplication.shared.open(url)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}


class ChopCell: UITableViewCell {
    
    static let cellId = "ChopCellId"
    
    lazy var backView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 25
        view.backgroundColor = UIColor(red: 0.93, green: 0.93, blue: 0.93, alpha: 1)
        view.alpha = 0.02
        view.isUserInteractionEnabled = true

        let pressGesture =
            UILongPressGestureRecognizer(
                target: self,
                action: #selector(sectionPressed(gesture:))
            )

        pressGesture.minimumPressDuration = 0.1
        pressGesture.cancelsTouchesInView = false
        pressGesture.numberOfTouchesRequired = 1

        view.addGestureRecognizer(pressGesture)
        
        let clickGesture =
            UITapGestureRecognizer(
                target: self,
                action: #selector(sectionClicked)
            )
        clickGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(clickGesture)
        
        return view
    }()
    
    lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        
        label.font = UIFont(name: "OpenSans-Light", size: 15.5)
        label.font = label.font.withSize(15.5)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.hubTextBlack
        
        return label
    }()
    
    lazy var dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.hubTransparent
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        contentView.addSubview(backView)
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        
        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    override func updateConstraints() {
        backView.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(104)
        }
        
        iconView.snp.remakeConstraints{ make in
            make.height.width.equalTo(76)
            make.leading.equalToSuperview().offset(23)
            make.top.equalToSuperview().offset(14)
        }

        titleLabel.snp.remakeConstraints{ make in
            make.leading.equalTo(iconView.snp.trailing).offset(17)
            make.trailing.equalToSuperview().offset(-23)
            make.top.equalTo(iconView.snp.top).offset(7)
        }
        
        super.updateConstraints()
    }
    
    func setContent(img: UIImage?, name: String){
        iconView.image = img
        titleLabel.text = name
    }
    
    @objc func sectionClicked() {
        UIView.animate(withDuration: 0.3) {
            self.backView.alpha = 1
        }
        
        UIView.animate(withDuration: 0.3) {
            self.backView.alpha = 0.02
        }
    }
    
    @objc func sectionPressed(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                self.backView.alpha = 1
            }
        } else if gesture.state == .changed {
            if gesture.location(in: contentView).y > self.backView.bounds.maxY || gesture.location(in: contentView).y < self.backView.bounds.minY || gesture.location(in: contentView).x > self.backView.bounds.maxX || gesture.location(in: contentView).x < self.backView.bounds.minX {
                
                UIView.animate(withDuration: 0.3) {
                    self.backView.alpha = 0.02
                }
            }
        } else if gesture.state == .ended {
            if self.backView.alpha == 1 {
                UIView.animate(withDuration: 0.3) {
                    self.backView.alpha = 0.02
                }
            }
        }
    }
}

extension ChopController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
