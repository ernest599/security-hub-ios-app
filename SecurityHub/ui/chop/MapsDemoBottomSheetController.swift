import UIKit
import UBottomSheet

class MapsDemoBottomSheetController: BaseController<BaseView>, Draggable, UITableViewDelegate, UITableViewDataSource {
    
    lazy var table : UITableView = {
        let view = UITableView()
        
        view.rowHeight = UITableView.automaticDimension
        view.separatorStyle = .none
        view.estimatedRowHeight = 50
        
        return view
    }()
    
    let titles = [
        "First",
        "Second",
        "Third"
    ]
      
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.addSubview(table)

        table.snp.remakeConstraints{make in
            make.leading.trailing.width.height.top.bottom.equalToSuperview()
        }
        
        if #available(iOS 11.0, *) {
            table.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        table.delegate = self
        table.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sheetCoordinator?.startTracking(item: self)
    }

    var sheetCoordinator: UBottomSheetCoordinator?

    func draggableView() -> UIScrollView? {
        return table
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DemoCell = table.dequeueReusableCell(withIdentifier: DemoCell.cellId) as! DemoCell
        
        cell.titleLabel.text = titles[indexPath.row]
        
        return cell
    }
}

class DemoCell: UITableViewCell {
    
    static let cellId = "DemoCell"
    
    lazy var backView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 25
        view.backgroundColor = UIColor(red: 0.69, green: 0.69, blue: 0.69, alpha: 0.4)
        view.alpha = 0.05
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(sectionPressed(gesture:)))
        gesture.minimumPressDuration = 0.2
        
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gesture)
        view.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(sectionClicked)
            )
        )
        
        return view
    }()
    
    lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.light)
        label.numberOfLines = 0
        label.textAlignment = .natural
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.black
        
        return label
    }()
    
    lazy var dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.hubTransparent
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        contentView.addSubview(backView)
        contentView.addSubview(iconView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(dividerView)
        
        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError("init(coder:) has not been implemented")}
    
    override func updateConstraints() {
        backView.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(128)
        }
        
        iconView.snp.remakeConstraints{ make in
            make.top.equalToSuperview().offset(16)
            make.bottom.equalTo(dividerView.snp.top).offset(-16)
            make.height.width.equalTo(96)
            make.leading.equalToSuperview().offset(24)
        }
        
        titleLabel.snp.remakeConstraints{ make in
            make.leading.equalTo(iconView.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-24)
            make.height.equalTo(96)
        }
        
        dividerView.snp.remakeConstraints{ make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        super.updateConstraints()
    }
    
    func setContent(img: UIImage?, name: String){
        iconView.image = img
        titleLabel.text = name
    }
    
    @objc func sectionClicked() {
        UIView.animate(withDuration: 0.3) {
            self.backView.alpha = 1.0
        }
        
        UIView.animate(withDuration: 0.3) {
            self.backView.alpha = 0.05
        }
    }
    
    @objc func sectionPressed(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            UIView.animate(withDuration: 0.3) {
                self.backView.alpha = 1.0
            }
        } else if gesture.state == .ended {
            UIView.animate(withDuration: 0.3) {
                self.backView.alpha = 0.05
            }
        }
    }
}
