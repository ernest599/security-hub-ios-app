import UIKit

class LanguageView: BaseView {
    
    lazy var backArrow: UIImageView = {
        let view = UIImageView()
        
        view.image = UIImage(named: "backArrow")

        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(backClicked(gesture:)))
        gesture.minimumPressDuration = 0
        
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gesture)
        
        return view
    }()
    
    lazy var title: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_language_app
        view.font = UIFont.systemFont(ofSize: 32, weight: UIFont.Weight.regular)
        view.textColor = UIColor.black
        view.numberOfLines = 0
        view.textAlignment = .natural
        
        return view
    }()
    
    lazy var languageRuView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 16
        
        return view
    }()
    
    lazy var languageRu: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_language_ru
        view.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.regular)
        view.textColor = UIColor.black
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var languageDeView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 16
        
        return view
    }()
    
    lazy var languageDe: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_language_de
        view.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.regular)
        view.textColor = UIColor.black
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var languageEnView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 16
        
        return view
    }()
    
    lazy var languageEn: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_language_en
        view.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.regular)
        view.textColor = UIColor.black
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    lazy var languageEsView: UIView = {
        let view = UIView()
        
        view.layer.cornerRadius = 16
        
        return view
    }()
    
    lazy var languageEs: UILabel = {
        let view = UILabel()
        
        view.text = R.strings.options_language_es
        view.font = UIFont.systemFont(ofSize: 24, weight: UIFont.Weight.regular)
        view.textColor = UIColor.black
        view.isUserInteractionEnabled = true
        
        return view
    }()
    
    
    override func setContent() {
        contentView.addSubview(backArrowView)
        contentView.addSubview(backArrow)
        contentView.addSubview(title)
        
        contentView.addSubview(languageRuView)
        contentView.addSubview(languageDeView)
        contentView.addSubview(languageEnView)
        contentView.addSubview(languageEsView)
        
        languageRuView.addSubview(languageRu)
        languageDeView.addSubview(languageDe)
        languageEnView.addSubview(languageEn)
        languageEsView.addSubview(languageEs)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        backArrowView.snp.remakeConstraints { make in
            make.width.equalTo(50)
            make.height.equalTo(50)
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(10)
        }
        
        backArrow.snp.remakeConstraints { make in
            make.width.equalTo(30)
            make.height.equalTo(30)
            make.top.equalToSuperview().offset(20)
            make.leading.equalToSuperview().offset(20)
        }
        
        title.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(20)
            make.leading.equalTo(backArrow.snp.trailing).offset(20)
            make.width.equalToSuperview().offset(-20)
        }
        
        languageRuView.snp.remakeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(40)
            make.width.equalToSuperview()
            make.height.equalTo(70)
        }
        
        languageRu.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
        }
        
        languageDeView.snp.remakeConstraints { make in
            make.top.equalTo(languageRuView.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.height.equalTo(70)
        }
        
        languageDe.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
        }
        
        languageEnView.snp.remakeConstraints { make in
            make.top.equalTo(languageDeView.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.height.equalTo(70)
        }
        
        languageEn.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
        }
        
        languageEsView.snp.remakeConstraints { make in
            make.top.equalTo(languageEnView.snp.bottom).offset(10)
            make.width.equalToSuperview()
            make.height.equalTo(70)
        }
        
        languageEs.snp.remakeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalToSuperview()
            make.leading.equalToSuperview().offset(20)
        }
    }
    
    @objc func backClicked() {
        nV?.popViewController(animated: true)
    }
}
