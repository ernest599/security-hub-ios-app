import Foundation
import WebKit
import Localize_Swift

class LanguageController: BaseController<LanguageView> {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = R.strings.options_language_app
        
        navigationController!.interactivePopGestureRecognizer!.delegate = self
        
        switch DataManager.defaultHelper.language {
            case "ru":
                mainView.languageRu.textColor = UIColor.white
                mainView.languageRuView.backgroundColor = #colorLiteral(red: 0.3921568627, green: 0.7411764706, blue: 0.9764705882, alpha: 1)
            
            case "de":
                mainView.languageDe.textColor = UIColor.white
                mainView.languageDeView.backgroundColor = #colorLiteral(red: 0.3921568627, green: 0.7411764706, blue: 0.9764705882, alpha: 1)
                
            case "en":
                mainView.languageEn.textColor = UIColor.white
                mainView.languageEnView.backgroundColor = #colorLiteral(red: 0.3921568627, green: 0.7411764706, blue: 0.9764705882, alpha: 1)
                
            default:
                mainView.languageEs.textColor = UIColor.white
                mainView.languageEsView.backgroundColor = #colorLiteral(red: 0.3921568627, green: 0.7411764706, blue: 0.9764705882, alpha: 1)
        }
        
        mainView.languageRu.addGestureRecognizer(createGestureRecognizer(lan: "ru"))
        mainView.languageDe.addGestureRecognizer(createGestureRecognizer(lan: "de"))
        mainView.languageEn.addGestureRecognizer(createGestureRecognizer(lan: "en"))
        mainView.languageEs.addGestureRecognizer(createGestureRecognizer(lan: "es"))
    }
    
    @objc func setLan(sender: GestureRecognizer) {
        let lan = sender.lan!

        self.showLoader()
        
        Localize.setCurrentLanguage(lan)
        DataManager.defaultHelper.language = lan
        R.reinit()
        
        let _ = DataManager.shared.setLocale(lan)
            .subscribe(onNext: { r in
                self.hideLoader()
                
                DataManager.shared.clearAfterLang()
                
                self.presentSplash()
            }, onError: { e in
                self.hideLoader()
                
                DataManager.shared.clearAfterLang()
                
                self.presentSplash()
            })
    }
    
    func createGestureRecognizer(lan: String) -> GestureRecognizer {
        let gestureRecognizer =
            GestureRecognizer(
                target: self,
                action: #selector(setLan(sender:))
            )
        
        gestureRecognizer.lan = lan
        gestureRecognizer.numberOfTapsRequired = 1
        
        return gestureRecognizer
    }
    
    func presentSplash() {
        let navigationController = UINavigationController(rootViewController: SplashController())
        
        navigationController.modalPresentationStyle = .fullScreen
        
        present(navigationController, animated: false)
    }
}

class GestureRecognizer: UITapGestureRecognizer {
    var lan: String?
}

extension LanguageController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
