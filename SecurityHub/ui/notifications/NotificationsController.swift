import Foundation
import Eureka

class NotificationsController: FormViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationController!.interactivePopGestureRecognizer!.delegate = self
        
        if form.isEmpty {
            guard let nV = navigationController else { return }
            form +++ Section(R.strings.options_notification_title)
                <<< SwitchRow("eventsSwitch") {
                    $0.title = R.strings.options_notification_need
                    $0.value = DataManager.settingsHelper.needPush ? true : false
                }
                .onChange { row in
                    DataManager.settingsHelper.needPush = DataManager.settingsHelper.needPush ? false : true
                    DataManager.settingsHelper.needPush ? DataManager.shared.setFCM() : DataManager.shared.deleteFCM()
                }
                <<< LabelRow(){
                    $0.title = R.strings.options_notification_classes
                    $0.hidden =  Condition.function(["eventsSwitch"], { form in
                        return !((form.rowBy(tag: "eventsSwitch") as? SwitchRow)?.value ?? false)
                    })
                    $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                        nV.pushViewController(SettingsController(R.strings.options_notification_classes), animated: false)
                   }
                }
            
                if DataManager.settingsHelper.needExpert {
                    form +++ Section(R.strings.options_history_title)
                    <<< LabelRow(){
                        $0.title = R.strings.options_history
                        $0.onCellSelection { (lab : LabelCellOf<String>, labrw : LabelRow) in
                           nV.pushViewController(SettingsController(R.strings.options_history), animated: false)
                       }
                    }
                }
        }
    }
    
    private func exit() {
        self.navigationController?.present(AlertUtil.exitAlert({
            DataManager.disconnectDisposable = DataManager.shared.disconnect().subscribe(onNext: { a in
                DataManager.disconnectDisposable?.dispose()
                NavigationHelper.shared.show(SplashController())
            })
        }), animated: true)
    }
}

extension NotificationsController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
